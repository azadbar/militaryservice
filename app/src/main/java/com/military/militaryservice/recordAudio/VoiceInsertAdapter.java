package com.military.militaryservice.recordAudio;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Environment;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.military.militaryservice.R;
import com.military.militaryservice.audio.AudioRecorderActivity;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.image.ImageInsertAdapter;
import com.military.militaryservice.resultInfo.FileAttachmentsItem;
import com.military.militaryservice.utils.AudioWife;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class VoiceInsertAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final int cellWidth;
    private final int maxImageCount;

    private Context context;
    private ArrayList<FileAttachmentsItem> list;
    private final OnItemClickListener listener;
    private final DeleteOnItemClickListener deleteOnItemClickListener;
    private FileAttachmentsItem voiceSelected;


    public VoiceInsertAdapter(Context context, ArrayList<FileAttachmentsItem> list, int cellWidth, int maxImageCount, OnItemClickListener listener, DeleteOnItemClickListener deleteOnItemClickListener) {
        this.list = list;
        this.listener = listener;
        this.cellWidth = cellWidth;
        this.maxImageCount = maxImageCount;
        this.context = context;
        this.deleteOnItemClickListener = deleteOnItemClickListener;
    }


    @Override
    public int getItemViewType(int position) {
        if (list.size() == maxImageCount) {
            return 2;
        }
        if (position == 0) {
            return 1;
        }
        return 2;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1:
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_add_voice, parent, false);
                itemView.getLayoutParams().height = cellWidth;
                itemView.getLayoutParams().width = cellWidth;
                return new addVoiceViewHolder(itemView);
            default:
                View itemView1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_voice_cell, parent, false);
                itemView1.getLayoutParams().height = cellWidth;
                itemView1.getLayoutParams().width = cellWidth;
                return new ViewHolderVoiceList(itemView1);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        switch (holder.getItemViewType()) {
            case 1:
                addVoiceViewHolder viewHolder0 = (addVoiceViewHolder) holder;
                ((addVoiceViewHolder) holder).bind(position, listener);
                break;

            default:
                ViewHolderVoiceList viewHolder2 = (ViewHolderVoiceList) holder;

                FileAttachmentsItem fileAttachmentsItem;

                if (list.size() == maxImageCount) {
                    fileAttachmentsItem = list.get(position);
                } else {
                    fileAttachmentsItem = list.get(position - 1);
                }


                ((ViewHolderVoiceList) holder).bind(position, fileAttachmentsItem, deleteOnItemClickListener);

                ((ViewHolderVoiceList) holder).itemClick(position, fileAttachmentsItem, deleteOnItemClickListener);

//                ((ViewHolderVoiceList) holder).doubleClick(position, fileAttachmentsItem, deleteOnItemClickListener);


                viewHolder2.setFileAttachment(fileAttachmentsItem);

                File file = new File(Environment.getExternalStorageDirectory(), fileAttachmentsItem.getFileAddress());
                viewHolder2.audioWife.getInstance()
                        .init(holder.itemView.getContext(), file.getAbsolutePath())
                        .setPlayView(holder.itemView.findViewById(R.id.pause))
                        .setPauseView(holder.itemView.findViewById(R.id.pause))
                        .setRuntimeView(holder.itemView.findViewById(R.id.run_time))
                        .setTotalTimeView(holder.itemView.findViewById(R.id.total_time));

                viewHolder2.audioWife.getInstance().addOnCompletionListener(mp -> {
                });

                viewHolder2.audioWife.getInstance().addOnPlayClickListener(v -> {
                });

                viewHolder2.audioWife.getInstance().addOnPauseClickListener(v -> {
                });


                if (fileAttachmentsItem.isDisable()) {
                    ((ViewHolderVoiceList) holder).disable.setBackgroundColor(((ViewHolderVoiceList) holder).rlAddImage.getContext().getResources().getColor(R.color.transparent));
                } else {
                    ((ViewHolderVoiceList) holder).disable.setBackgroundColor(((ViewHolderVoiceList) holder).rlAddImage.getContext().getResources().getColor(R.color.disable));

                }
                break;
        }

    }


    @Override
    public int getItemCount() {
        if (list.size() < maxImageCount) {
            return list.size() + 1;
        }
        return list.size();
    }

    public void setSelectedVideo(int position, FileAttachmentsItem file) {
        this.voiceSelected = file;
        list.get(position).setDisable(true);
        for (int i = 0; i < list.size(); i++) {
            if (position != i)
                list.get(i).setDisable(false);
        }
        notifyDataSetChanged();
    }


    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public interface DeleteOnItemClickListener {
        void onItemClickDelete(int position, FileAttachmentsItem voice);

        void onIemClick(int position, FileAttachmentsItem voice);
    }

    static class addVoiceViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgAdd)
        BaseImageView imgAdd;
        @BindView(R.id.text)
        BaseTextView text;
        @BindView(R.id.rlAddImage)
        BaseRelativeLayout rlAddImage;

        addVoiceViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(position));
        }

    }

    static class ViewHolderVoiceList extends RecyclerView.ViewHolder {
        private final GestureDetector dg;
        @BindView(R.id.play)
        ImageView play;
        @BindView(R.id.imgDelete)
        BaseImageView imgDelete;
        @BindView(R.id.run_time)
        TextView runTime;
        @BindView(R.id.total_time)
        TextView totalTime;
        @BindView(R.id.disable)
        BaseRelativeLayout disable;
        @BindView(R.id.rlAddImage)
        CardView rlAddImage;
        AudioWife audioWife;
        private FileAttachmentsItem fileAttachment;


        ViewHolderVoiceList(View view) {
            super(view);
            ButterKnife.bind(this, view);

            audioWife = new AudioWife();


            dg = new GestureDetector(rlAddImage.getContext(), new GestureDetector.OnGestureListener() {
                @Override
                public boolean onDown(MotionEvent e) {
                    return false;
                }

                @Override
                public void onShowPress(MotionEvent e) {

                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return false;
                }

                @Override
                public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                    return false;
                }

                @Override
                public void onLongPress(MotionEvent e) {

                }

                @Override
                public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                    return false;
                }
            });
            dg.setOnDoubleTapListener(new GestureDetector.OnDoubleTapListener() {
                @Override
                public boolean onDoubleTap(MotionEvent e) {
                    Intent intent = new Intent(itemView.getContext(), AudioRecorderActivity.class);
                    intent.putExtra("isPlay", true);
                    File file = new File(Environment.getExternalStorageDirectory(), fileAttachment.getFileAddress());
                    intent.putExtra("filePath", file.getAbsolutePath());
                    itemView.getContext().startActivity(intent);

                    return false;
                }

                @Override
                public boolean onDoubleTapEvent(MotionEvent e) {
                    // if the second tap hadn't been released and it's being moved

                    return false;
                }

                @Override
                public boolean onSingleTapConfirmed(MotionEvent e) {
                    // TODO Auto-generated method stub
                    return false;
                }

            });
        }

        public void bind(int position, FileAttachmentsItem fileAttachmentsItem, final DeleteOnItemClickListener listener) {
            imgDelete.setOnClickListener(v -> listener.onItemClickDelete(position, fileAttachmentsItem));
        }

        public void itemClick(int position, FileAttachmentsItem fileAttachmentsItem, final DeleteOnItemClickListener listener) {
            rlAddImage.setOnClickListener(v -> listener.onIemClick(position, fileAttachmentsItem));
        }

        public void setFileAttachment(FileAttachmentsItem fileAttachmentsItem) {
            this.fileAttachment = fileAttachmentsItem;
        }

        public void doubleClick(int position, FileAttachmentsItem fileAttachmentsItem, DeleteOnItemClickListener deleteOnItemClickListener) {
            rlAddImage.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    dg.onTouchEvent(event);
                    return false;
                }
            });
        }
//        public void doubleClick(int position, FileAttachmentsItem fileAttachmentsItem, DeleteOnItemClickListener deleteOnItemClickListener) {
//            rlAddImage.setOnTouchListener(new View.OnTouchListener() {
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//                    dg.onTouchEvent(event);
//                    return false;
//                }
//            });
//        }
    }

    public void setSelectedImage(int selectedPosition, FileAttachmentsItem fileAttachmentsItem) {
        this.voiceSelected = fileAttachmentsItem;
        list.get(selectedPosition).setDisable(true);
        for (int i = 0; i < list.size(); i++) {
            if (selectedPosition != i)
                list.get(i).setDisable(false);
        }
        notifyDataSetChanged();
    }

}
