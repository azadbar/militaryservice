package com.military.militaryservice.recordAudio;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.database.Voice;
import com.military.militaryservice.resultInfo.FileAttachmentsItem;
import com.military.militaryservice.utils.AudioWife;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class VoicesAdapter extends RecyclerView.Adapter<VoicesAdapter.ViewHolder> {


    private ArrayList<FileAttachmentsItem> list;
    private FileAttachmentsItem voiceSelected;
    private final OnItemClickListener listener;


    public VoicesAdapter(ArrayList<FileAttachmentsItem> list, FileAttachmentsItem voiceSelected, OnItemClickListener listener) {
        this.list = list;
        this.voiceSelected = voiceSelected;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_voice, parent, false);

        return new ViewHolder(itemView);

    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        FileAttachmentsItem voice = list.get(position);
//        holder.tvTitle.setText(image.getNamePerson());
//        holder.tvAddress.setText(image.getAddress());
//
//        Glide.with(holder.imageMag.getContext()).load(new File(image.getPath())).into(holder.imageMag);

        SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(voice.getDate().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            PersianDate datea = new PersianDate(date);
            PersianDateFormat pd = new PersianDateFormat("j F y");
            holder.tvData.setText(pd.format(datea) + "");
        } else {
            holder.tvData.setText(null);
        }

        File file = new File(Environment.getExternalStorageDirectory(), voice.getFileAddress());
        holder.audioWife.getInstance()
                .init(holder.itemView.getContext(), file.getAbsolutePath())
                .setPlayView(holder.itemView.findViewById(R.id.play))
                .setPauseView(holder.itemView.findViewById(R.id.pause))
                .setSeekBar(holder.itemView.findViewById(R.id.media_seekbar))
                .setRuntimeView(holder.itemView.findViewById(R.id.run_time))
                .setTotalTimeView(holder.itemView.findViewById(R.id.total_time));

        holder.audioWife.getInstance().addOnCompletionListener(mp -> {
        });

        holder.audioWife.getInstance().addOnPlayClickListener(v -> {
        });

        holder.audioWife.getInstance().addOnPauseClickListener(v -> {
        });

//        if (list != null && voiceSelected.getId() == voice.getId()) {
//            setSelected(holder, res);
//        } else {
//            setNormal(holder, res);
//        }

        if (voice.isDisable()) {
//            holder.disable.setBackgroundColor(holder.disable.getContext().getResources().getColor(R.color.transparent));
            holder.root.setBackgroundColor(res.getColor(R.color.white));
            holder.card.setBackgroundColor(res.getColor(R.color.white));
        } else {
//            holder.disable.setBackgroundColor(holder.disable.getContext().getResources().getColor(R.color.disable));
            holder.root.setBackgroundColor(res.getColor(R.color.voicedisable));
            holder.card.setBackgroundColor(res.getColor(R.color.voicedisable));

        }

        holder.bind(voice, position, listener);
        holder.btnDelete.setOnClickListener(v -> listener.onDeleteItem(position, voice));

        holder.root.setOnClickListener(v -> listener.onClick(position, list));
    }

    private void setSelected(ViewHolder holder, Resources res) {
        holder.root.setBackgroundColor(res.getColor(R.color.white));
        holder.card.setBackgroundColor(res.getColor(R.color.white));
    }

    private void setNormal(ViewHolder holder, Resources res) {
        holder.root.setBackgroundColor(res.getColor(R.color.redColor));
        holder.card.setBackgroundColor(res.getColor(R.color.redColor));
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setList(ArrayList<FileAttachmentsItem> voices) {
        this.list = voices;
        notifyDataSetChanged();
    }

    public void setSelectedVoice(int selectedPosition, FileAttachmentsItem voice) {
        this.voiceSelected = voice;
        list.get(selectedPosition).setDisable(true);
        for (int i = 0; i < list.size(); i++) {
            if (selectedPosition != i)
                list.get(i).setDisable(false);
        }
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.play)
        ImageView play;
        @BindView(R.id.pause)
        ImageView pause;
        @BindView(R.id.play_pause_layout)
        FrameLayout playPauseLayout;
        @BindView(R.id.media_seekbar)
        SeekBar mediaSeekbar;
        @BindView(R.id.run_time)
        TextView runTime;
        @BindView(R.id.total_time)
        TextView totalTime;
        @BindView(R.id.seekbar_layout)
        FrameLayout seekbarLayout;
        @BindView(R.id.root)
        LinearLayout root;
        @BindView(R.id.btnDelete)
        BaseImageView btnDelete;
        AudioWife audioWife;
        @BindView(R.id.tvData)
        BaseTextView tvData;
        @BindView(R.id.card)
        LinearLayout card;
        @BindView(R.id.disable)
        BaseRelativeLayout disable;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            audioWife = new AudioWife();
        }


        public void bind(final FileAttachmentsItem image, int position, final OnItemClickListener listener) {
            playPauseLayout.setOnClickListener(v -> listener.onItemClick(position, image));
        }

    }


    public interface OnItemClickListener {
        void onItemClick(int position, FileAttachmentsItem image);

        void onDeleteItem(int position, FileAttachmentsItem voice);

        void onAttachment(int position, FileAttachmentsItem voice);

        void onClick(int position, ArrayList<FileAttachmentsItem> voice);
    }

}
