package com.military.militaryservice.resultInfo;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.military.militaryservice.orderInfo.ProjectResponse;

import java.util.ArrayList;
import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface ResultsResponseDao {

    @Query("select * from  resultresponse")
    List<ResultResponse> getPersonList();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllList(ArrayList<ResultResponse> persons);

    @Insert(onConflict = REPLACE)
    void insertAddress(ResultResponse resultResponse);


    @Query("select * from resultresponse where orderIdentifier =:orderIdentifier")
    ResultResponse getProject(int orderIdentifier);

    @Query("DELETE FROM resultresponse")
    public void nukeTable();


    @Delete
    void delete(ProjectResponse persons);

    @Query("delete from resultresponse where orderIdentifier =:id")
    void deleteItem(int id);

    @Update
    int updatePerson(ResultResponse resultResponse);




}
