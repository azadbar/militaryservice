package com.military.militaryservice.resultInfo;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

public class FileAttachmentsDataConverter {

    private static Gson gson = new Gson();

    @TypeConverter
    public static List<FileAttachmentsItem> stringToList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<FileAttachmentsItem>>() {
        }.getType();
        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String ListToString(List<FileAttachmentsItem> questionInfosItems) {
        return gson.toJson(questionInfosItems);
    }
}
