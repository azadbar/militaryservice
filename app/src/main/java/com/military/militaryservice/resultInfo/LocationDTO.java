package com.military.militaryservice.resultInfo;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.SerializedName;
import com.military.militaryservice.customView.KeyValueObject;
import com.military.militaryservice.database.DateTypeConverter;
import com.military.militaryservice.database.User;

import java.io.Serializable;
import java.util.Date;

@Entity(tableName = "LocationDTO", indices = @Index(value = {"address"}, unique = true))
public class LocationDTO implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;
    @SerializedName("latitude")
    private double latitude;
    @SerializedName("longitude")
    private double longitude;
    @SerializedName("description")
    private String description;

    @SerializedName("address")
    private String address;

    @TypeConverters(DateTypeConverter.class)
    @SerializedName("date")
    private Date date;

    @SerializedName("userId")
    private int userId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public KeyValueObject getKeyValue() {
        return new KeyValueObject(this.id, this.address);
    }
}
