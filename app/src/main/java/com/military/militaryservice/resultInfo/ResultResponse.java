package com.military.militaryservice.resultInfo;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity(tableName = "ResultResponse")
public class ResultResponse {

    @PrimaryKey
    @NonNull
    @SerializedName("orderIdentifier")
    private String orderIdentifier;

    @TypeConverters(QuestionWithResiltsDataConverter.class)
    @SerializedName("questionWithResults")
    private List<QuestionWithResultsItem> questionWithResults;

    @SerializedName("complementaryReport")
    private String complementaryReport;

    @NonNull
    public String getOrderIdentifier() {
        return orderIdentifier;
    }

    public void setOrderIdentifier(@NonNull String orderIdentifier) {
        this.orderIdentifier = orderIdentifier;
    }

    public List<QuestionWithResultsItem> getQuestionWithResults() {
        return questionWithResults;
    }

    public void setQuestionWithResults(List<QuestionWithResultsItem> questionWithResults) {
        this.questionWithResults = questionWithResults;
    }

    public String getComplementaryReport() {
        return complementaryReport;
    }

    public void setComplementaryReport(String complementaryReport) {
        this.complementaryReport = complementaryReport;
    }
}