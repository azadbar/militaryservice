package com.military.militaryservice.resultInfo;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.TypeConverters;
import androidx.room.Update;

import com.military.militaryservice.database.DateTypeConverter;
import com.military.militaryservice.enums.AttachmentType;

import java.util.Date;
import java.util.List;

@Dao
public interface FileAttachmentDao {

    @Query("select * from  fileattachmentsitem order by date Desc")
    List<FileAttachmentsItem> getFileAttachments();


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllList(List<FileAttachmentsItem> fileAttachmentsItems);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(FileAttachmentsItem fileAttachmentsItems);


    @Query("DELETE FROM fileattachmentsitem")
    public void nukeTable();


    @Delete
    void delete(FileAttachmentsItem fileAttachmentsItem);

    @Query("select * from fileattachmentsitem where questionIdentifier =:questionIdentifier and format=:format order by date DESC")
    List<FileAttachmentsItem> select(int questionIdentifier, String format);

    @Query("select * from fileattachmentsitem where questionIdentifier =:questionIdentifier order by date DESC")
    List<FileAttachmentsItem> select(int questionIdentifier);

    @Update
    void updateTour(FileAttachmentsItem fileAttachmentsItem);


    @Query("UPDATE fileattachmentsitem SET description=:description WHERE identifier =:identifier")
    void update(String description, long identifier);

    @Query("UPDATE fileattachmentsitem SET description=:description WHERE identifier =:identifier")
    void setDescription(String description, long identifier);

    @Query("UPDATE fileattachmentsitem SET LocationDTO_latitude =:lat,LocationDTO_longitude =:longi,LocationDTO_address =:des WHERE identifier =:identifier")
    void setLocation(double lat, double longi, String des, long identifier);


    @TypeConverters({DateTypeConverter.class, AttachmentType.class})
    @Query("UPDATE fileattachmentsitem set date =:date,filename =:name,fileAddress =:fileAddress," +
            " fileFolder=:fileFolder, attachmentType=:attachmentType ,userId =:userId where identifier=:identifier")
    int updateObject(Date date, String name, String fileAddress, String fileFolder, AttachmentType attachmentType, int userId, long identifier);

    @Query("select * from fileattachmentsitem WHERE identifier =:identifier")
    FileAttachmentsItem getDescription(long identifier);
}
