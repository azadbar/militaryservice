package com.military.militaryservice.resultInfo;

import androidx.room.TypeConverters;

import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class QuestionWithResultsItem {

    @SerializedName("id")
    private int id;

    @SerializedName("result")
    private String result;

    @SerializedName("question")
    private String question;

    @SerializedName("questionIdentifier")
    private String questionIdentifier;

    @TypeConverters(FileAttachmentsDataConverter.class)
    @SerializedName("fileAttachments")
    private List<FileAttachmentsItem> fileAttachments;

    @TypeConverters(FileAttachmentsDataConverter.class)
    @SerializedName("geoLocations")
    private List<GeoLocationsItem> geoLocations;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQuestionIdentifier() {
        return questionIdentifier;
    }

    public void setQuestionIdentifier(String questionIdentifier) {
        this.questionIdentifier = questionIdentifier;
    }

    public List<FileAttachmentsItem> getFileAttachments() {
        return fileAttachments;
    }

    public void setFileAttachments(ArrayList<FileAttachmentsItem> fileAttachments) {
        if (fileAttachments.size() > 0) {
            this.fileAttachments = fileAttachments;
        } else
            this.fileAttachments = new ArrayList<>();
    }

    public List<GeoLocationsItem> getGeoLocations() {
        return geoLocations;
    }

    public void setGeoLocations(ArrayList<GeoLocationsItem> geoLocations) {
        this.geoLocations = geoLocations;
    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("id", id);
            obj.put("result", result);
            obj.put("question", question);
            if (fileAttachments.size() > 0) {
                for (int i = 0; i < fileAttachments.size(); i++) {
                }
            }
        } catch (JSONException e) {

        }
        return obj;
    }


}