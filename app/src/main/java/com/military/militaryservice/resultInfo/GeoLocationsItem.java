package com.military.militaryservice.resultInfo;

import com.google.gson.annotations.SerializedName;

public class GeoLocationsItem{


	@SerializedName("description")
	private String description;

	@SerializedName("latitude")
	private double latitude;

	@SerializedName("longitude")
	private double longitude;

	public void setLatitude(double latitude){
		this.latitude = latitude;
	}

	public double getLatitude(){
		return latitude;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setLongitude(double longitude){
		this.longitude = longitude;
	}

	public double getLongitude(){
		return longitude;
	}

	@Override
 	public String toString(){
		return 
			"GeoLocationsItem{" + 
			"latitude = '" + latitude + '\'' + 
			",description = '" + description + '\'' + 
			",longitude = '" + longitude + '\'' + 
			"}";
		}
}