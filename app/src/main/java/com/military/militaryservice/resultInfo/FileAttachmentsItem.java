package com.military.militaryservice.resultInfo;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.SerializedName;
import com.military.militaryservice.database.DateTypeConverter;
import com.military.militaryservice.enums.AttachmentType;

import java.io.Serializable;
import java.util.Date;

@Entity(tableName = "FileAttachmentsItem")
public class FileAttachmentsItem implements Serializable {


    @PrimaryKey(autoGenerate = true)
    @SerializedName("identifier")
    private long identifier;

    @SerializedName("filename")
    private String filename;

    @Embedded(prefix = "LocationDTO_")
    @SerializedName("geoLocationDTO")
    private LocationDTO geoLocationDTO;

    @SerializedName("description")
    private String description;

    @SerializedName("format")
    private String format;

    private int questionIdentifier;
    private String fileAddress;
    private String fileFolder;
    @TypeConverters(DateTypeConverter.class)
    private Date date;

    @TypeConverters(AttachmentType.class)
    private AttachmentType attachmentType;

    @SerializedName("userId")
    private int userId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getFileFolder() {
        return fileFolder;
    }

    public void setFileFolder(String fileFolder) {
        this.fileFolder = fileFolder;
    }

    private boolean isDisable;

    public boolean isDisable() {
        return isDisable;
    }

    public void setDisable(boolean disable) {
        isDisable = disable;
    }

    public int getQuestionIdentifier() {
        return questionIdentifier;
    }

    public void setQuestionIdentifier(int questionIdentifier) {
        this.questionIdentifier = questionIdentifier;
    }

    public String getFileAddress() {
        return fileAddress;
    }

    public void setFileAddress(String fileAddress) {
        this.fileAddress = fileAddress;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public AttachmentType getAttachmentType() {
        return attachmentType;
    }

    public void setAttachmentType(AttachmentType attachmentType) {
        this.attachmentType = attachmentType;
    }

    public long getIdentifier() {
        return identifier;
    }

    public void setIdentifier(long identifier) {
        this.identifier = identifier;
    }


    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public LocationDTO getGeoLocationDTO() {
        return geoLocationDTO;
    }

    public void setGeoLocationDTO(LocationDTO geoLocationDTO) {
        this.geoLocationDTO = geoLocationDTO;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }


}