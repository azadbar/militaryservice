package com.military.militaryservice.utils;


import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

/**
 * Created by R.taghizadeh on 10/17/2017.
 */

public class PermissionHandler {
    private static BroadcastReceiver permissionReceiver;

    public static boolean hasAllPermissions(Context context) {
        int writePermission = context.checkCallingOrSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readPermission = context.checkCallingOrSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
        int readContact = context.checkCallingOrSelfPermission(Manifest.permission.READ_CONTACTS);
        int voiceRecord = context.checkCallingOrSelfPermission(Manifest.permission.RECORD_AUDIO);
        int camere = context.checkCallingOrSelfPermission(Manifest.permission.CAMERA);
        int location = context.checkCallingOrSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
        int call = context.checkCallingOrSelfPermission(Manifest.permission.CALL_PHONE);
        int sms = context.checkCallingOrSelfPermission(Manifest.permission.SEND_SMS);
        int readCallLog = context.checkCallingOrSelfPermission(Manifest.permission.READ_CALL_LOG);
        int writeCallLog = context.checkCallingOrSelfPermission(Manifest.permission.WRITE_CALL_LOG);
        int writeSetting = context.checkCallingOrSelfPermission(Manifest.permission.WRITE_SETTINGS);
        int secure = context.checkCallingOrSelfPermission(Manifest.permission.WRITE_SECURE_SETTINGS);

        return (writePermission == PackageManager.PERMISSION_GRANTED) && (readPermission == PackageManager.PERMISSION_GRANTED) &&
                (readContact == PackageManager.PERMISSION_GRANTED) && (voiceRecord == PackageManager.PERMISSION_GRANTED)
                && (camere == PackageManager.PERMISSION_GRANTED) && (location == PackageManager.PERMISSION_GRANTED)
                && (call == PackageManager.PERMISSION_GRANTED)
                && (sms == PackageManager.PERMISSION_GRANTED)
                && (readCallLog == PackageManager.PERMISSION_GRANTED)
                && (writeCallLog == PackageManager.PERMISSION_GRANTED);
    }

    public static void requestPermissions(Activity activity, int REQUEST_CODE_PERMISSION) {
        ActivityCompat.requestPermissions(activity, new String[]{
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.CAMERA,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.CALL_PHONE,
                Manifest.permission.SEND_SMS,
                Manifest.permission.READ_CALL_LOG,
                Manifest.permission.WRITE_CALL_LOG}, REQUEST_CODE_PERMISSION);
    }


    public static void requestPermissions(Fragment fragment, int REQUEST_CODE_PERMISSION) {
        fragment.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_PERMISSION);
    }


//    public static void showSettingPermissionPage(Activity activity ,boolean isCloseApp) {
//        CustomDialog permissionDialog = new CustomDialog(activity);
//        permissionDialog.setOkListener(activity.getResources().getString(R.string.let_go), view -> {
//            permissionDialog.dismiss();
//            Intent intent = new Intent();
//            intent.setDate(Uri.fromParts("package", activity.getPackageName(), null));
//            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
//            if (isCloseApp){
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                activity.startActivity(intent);
//                BaseTransaction.closeDb();
//                activity.finishAffinity();
//            } else {
//                activity.startActivity(intent);
//            }
//        });
//        permissionDialog.setCancelListener(activity.getResources().getString(R.string.dialog_no), new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                permissionDialog.dismiss();
//                if (isCloseApp){
//                    BaseTransaction.closeDb();
//                    activity.finishAffinity();
//                }
//            }
//        });
//        permissionDialog.setIcon(R.drawable.ic_bug_repoart,activity.getResources().getColor(R.color.redColor));
//        permissionDialog.setDialogTitle(activity.getResources().getString(R.string.error_premission));
//        permissionDialog.setColorTitle(activity.getResources().getColor(R.color.primaryTextColor));
//        String text = "Permissions/" + activity.getResources().getString(R.string.app_name) + "/Settings/Apps";
//        permissionDialog.setDescription(activity.getResources().getString(R.string.guide_access) + "\n" + text);
//        permissionDialog.show();
//    }


    public static void checkNetwork(Activity activity, final OnNetworkResponse networkInterface) {
        if (Build.VERSION.SDK_INT < 23 || activity.checkSelfPermission(Manifest.permission.ACCESS_NETWORK_STATE) == PackageManager.PERMISSION_GRANTED) {
            networkPermissionDetermined(activity, true, networkInterface);
        } else {
            setBroadcastReceiver(activity, networkInterface);
        }
    }

    private static void setBroadcastReceiver(Activity activity, final OnNetworkResponse networkInterface) {
        permissionReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    int requestCode = extras.getInt("requestCode");
                    String[] permissions = intent.getStringArrayExtra("permissions");
                    int[] grantResults = intent.getIntArrayExtra("grantResults");
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        networkPermissionDetermined(activity, true, networkInterface);
                    } else {
                        networkPermissionDetermined(activity, false, networkInterface);
                    }
                    activity.unregisterReceiver(permissionReceiver);
                    permissionReceiver = null;
                }
            }
        };
        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("PERMISSION_RECEIVER");
        activity.registerReceiver(permissionReceiver, localIntentFilter);
        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_NETWORK_STATE}, 1);
    }


    private static void networkPermissionDetermined(Activity activity, boolean hasPermission, final OnNetworkResponse networkInterface) {
        if (hasPermission) {
            ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            boolean isConnected = true;
            if (cm != null) {
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
            }
            networkInterface.onConnectionState(isConnected);
        } else {
            networkInterface.onConnectionState(true);
        }
    }

}