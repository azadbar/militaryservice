package com.military.militaryservice.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.database.User;
import com.military.militaryservice.enums.DirectionEnum;
import com.military.militaryservice.enums.UserAccessEnum;
import com.military.militaryservice.maps.Mahale;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Constants {


    public static final int TYPE_PROJECT_CODE = 100;
    public static final int STATE_CODE = 101;
    public static final int ADD_CUSTOMER = 104;
    public static final int HEAT_SOURCE = 105;
    public static final int CITY_CODE = 106;
    public static final int TYPE_OF_CONTROL_SYSTEMS = 107;
    public static final int GENDER_FLOOR = 108;
    public static final int TYPE_SPACE = 109;
    public static final int RESULT_LOAD_IMAGE = 200;
    public static final int REQUEST_IMAGE_CAPTURE = 201;
    public static final int REQUEST_CODE_SEECTED_IMAGE_GALLERY = 202;
    public static final String RECORD = "/Shahed/Record/";
    public static final String IMAGE = "/Shahed/Images/";
    public static final String fileAttachments = "attachments/";
    public static final String fileQuestions = "Questions/";
    public static final String VIDEO = "/Shahed/Videos/";
    public static final String RECORD_ONLY = "/Record/";
    public static final String SHAHED = "/shahed/";
    public static final String CALLRECORD = "/callRecord/";
    public static final int SELECT_PERSON_QUESTION = 203;
    public static final int LOCATION_INTENT = 204;
    public static final int LOCATION_PERMISSION_REQUEST = 297;
    public static Language language = new Language("فارسی", "fa", DirectionEnum.RTL);

    public static final String SELECTED_ICON = "SELECT_ICON";
    public static final int SELECT_ICON = 100;
    public static String URL_API = "http://sgpmojri.ir/a/api/";
    public static String OpenProjectUrl;
    public static boolean isLogin;
    private static User currentUser;


    public static User getUser(Context context) {
        if (currentUser != null) {
            return currentUser;
        }
        AppDatabase database = AppDatabase.getInMemoryDatabase(context);
        List<User> users = database.userDao().getUsers(UserAccessEnum.ADMIN);
        currentUser = users.get(0);
        return currentUser;
    }

    public static void setCurrentUser(User currentUser) {
        Constants.currentUser = currentUser;
    }

    public static Point getScreenSize(WindowManager windowManager) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        return new Point(width, height);
    }


    public static boolean isPackageInstalled(PackageManager packageManager) {
        try {
            packageManager.getPackageInfo("com.farsitel.bazaar", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        } catch (SecurityException e) {
            return true;
        }
    }

    public static String convertNumberToDecimal(double d) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        formatter.applyPattern("#,###.###");
        return formatter.format(d);
    }


    public static void shareTextUrl(Context context, String text) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_TEXT, text);
        context.startActivity(Intent.createChooser(share, "Share link!"));
    }

//    public static void setBackgroundProgress(Context context, ProgressBar progressBar) {
//        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
//            Drawable drawableProgress = DrawableCompat.wrap(progressBar.getIndeterminateDrawable());
//            DrawableCompat.setTint(drawableProgress, ContextCompat.getColor(context, R.color.greenColor));
//            progressBar.setIndeterminateDrawable(DrawableCompat.unwrap(drawableProgress));
//
//        } else {
//            progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(context
//                    , R.color.greenColor), PorterDuff.Mode.SRC_IN);
//        }
//    }

    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public static String convertToEnglishDigits(String value) {
        return value.replace("١", "1").replace("٢", "2").replace("٣", "3").replace("٤", "4").replace("٥", "5")
                .replace("٦", "6").replace("٧", "7").replace("٨", "8").replace("٩", "9").replace("٠", "0")
                .replace("۱", "1").replace("۲", "2").replace("۳", "3").replace("۴", "4").replace("۵", "5")
                .replace("۶", "6").replace("۷", "7").replace("۸", "8").replace("۹", "9").replace("۰", "0");
    }

    public static ArrayList<Mahale> getSearchList() {
        ArrayList<Mahale> entries = new ArrayList<>();
        Mahale mahale = new Mahale("اختیاریه", 35.7882922, 51.4590242);
        entries.add(mahale);
        mahale = new Mahale("افسریه", 35.6545091, 51.4922410);
        entries.add(mahale);
        mahale = new Mahale("اقدسیه", 35.7936512, 51.4874264);
        entries.add(mahale);
        mahale = new Mahale("الهیه", 35.7909727, 51.4289670);
        entries.add(mahale);
        mahale = new Mahale("امانیه", 35.7838734, 51.4193161);
        entries.add(mahale);
        mahale = new Mahale("امیرآباد", 35.7384557, 51.3931696);
        entries.add(mahale);
        mahale = new Mahale("باغ صبا", 35.7196684, 51.4408747);
        entries.add(mahale);
        mahale = new Mahale("بریانک", 35.6769368, 51.3750120);
        entries.add(mahale);
        mahale = new Mahale("بهجت آباد", 35.7198260, 51.4148817);
        entries.add(mahale);
        mahale = new Mahale("بیسیم", 35.6530208, 51.4490165);
        entries.add(mahale);
        mahale = new Mahale("پاسداران", 35.7771539, 51.4710743);
        entries.add(mahale);
        mahale = new Mahale("پونک", 35.7624720, 51.3268858);
        entries.add(mahale);
        mahale = new Mahale("پیروزی", 35.6802344, 51.4826015);
        entries.add(mahale);
        mahale = new Mahale("تجریش", 35.8025239, 51.4320157);
        entries.add(mahale);
        mahale = new Mahale("تهران پارس", 35.7438123, 51.5415621);
        entries.add(mahale);
        mahale = new Mahale("تهران نو", 35.7098352, 51.4986639);
        entries.add(mahale);
        mahale = new Mahale("تهران ویلا", 35.7238779, 51.3674750);
        entries.add(mahale);
        mahale = new Mahale("جماران", 35.8171185, 51.4614804);
        entries.add(mahale);
        mahale = new Mahale("چهارصد دستگاه", 35.7990957, 51.4569196);
        entries.add(mahale);
        mahale = new Mahale("چیذر", 35.7990957, 51.4569196);
        entries.add(mahale);
        mahale = new Mahale("حکیمیه", 35.7379696, 51.5841643);
        entries.add(mahale);
        mahale = new Mahale("خاوران", 35.6666554, 51.4540597);
        entries.add(mahale);
        mahale = new Mahale("خزانه", 35.6532212, 51.3628275);
        entries.add(mahale);
        mahale = new Mahale("داوودیه", 35.7626633, 51.4381727);
        entries.add(mahale);
        mahale = new Mahale("دروس", 35.7734107, 51.4565854);
        entries.add(mahale);
        mahale = new Mahale("دزاشیب", 35.8078244, 51.4532734);
        entries.add(mahale);
        mahale = new Mahale("دولاب", 35.6663635, 51.4611511);
        entries.add(mahale);
        mahale = new Mahale("زعفرانیه", 35.8079932, 51.4210951);
        entries.add(mahale);
        mahale = new Mahale("سعادت آباد", 35.7856932, 51.3792865);
        entries.add(mahale);
        mahale = new Mahale("سلیمانیه", 35.6819476, 51.4573421);
        entries.add(mahale);
        mahale = new Mahale("شمس آباد", 35.7465563, 51.4768629);
        entries.add(mahale);
        mahale = new Mahale("شمیران نو", 35.7537491, 51.5019603);
        entries.add(mahale);
        mahale = new Mahale("صددستگاه", 35.6875621, 51.4762104);
        entries.add(mahale);
        mahale = new Mahale("طیب", 35.6606861, 51.4512122);
        entries.add(mahale);
        mahale = new Mahale("عباس آباد", 35.7298339, 51.4275131);
        entries.add(mahale);
        mahale = new Mahale("فرحزاد", 35.7883262, 51.3485262);
        entries.add(mahale);
        mahale = new Mahale("فردوس", 35.7211410, 51.3199650);
        entries.add(mahale);
        mahale = new Mahale("فرمانیه", 35.8028298, 51.4532702);
        entries.add(mahale);
        mahale = new Mahale("قبا", 35.7615582, 51.4490004);
        entries.add(mahale);
        mahale = new Mahale("قلهک", 35.7712267, 51.4338842);
        entries.add(mahale);
        mahale = new Mahale("قیطریه", 35.7910959, 51.4430862);
        entries.add(mahale);
        mahale = new Mahale("کیانشهر", 35.6348636, 51.4447679);
        entries.add(mahale);
        mahale = new Mahale("مجیدیه", 35.7367795, 51.4714716);
        entries.add(mahale);
        mahale = new Mahale("محمودیه", 35.7959366, 51.4157088);
        entries.add(mahale);
        mahale = new Mahale("میدان اراج", 35.8017223, 51.4869114);
        entries.add(mahale);
        mahale = new Mahale("میدان امام حسین", 35.7018822, 51.4483725);
        entries.add(mahale);
        mahale = new Mahale("میدان امام خمینی", 35.6857082, 51.4215257);
        entries.add(mahale);
        mahale = new Mahale("میدان امامت", 35.7132161, 51.4899281);
        entries.add(mahale);
        mahale = new Mahale("میدان انقلاب", 35.7009724, 51.3915587);
        entries.add(mahale);
        mahale = new Mahale("میدان آزادی", 35.6996786, 51.3381075);
        entries.add(mahale);
        mahale = new Mahale("میدان بروجردی", 35.6187431, 51.4450586);
        entries.add(mahale);
        mahale = new Mahale("میدان بسیج مستضعفین", 35.5971990, 51.4396794);
        entries.add(mahale);
        mahale = new Mahale("میدان بسیج", 35.6371383, 51.4819118);
        entries.add(mahale);
        mahale = new Mahale("میدان تجریش", 35.8069729, 51.4289075);
        entries.add(mahale);

        mahale = new Mahale("میدان حر", 35.6881868, 51.3929835);
        entries.add(mahale);
        mahale = new Mahale("میدان حسن آباد", 35.6861391, 51.4103327);
        entries.add(mahale);
        mahale = new Mahale("میدان خراسان", 35.6653725, 51.4455628);
        entries.add(mahale);
        mahale = new Mahale("میدان رازی", 35.6669714, 51.3960727);
        entries.add(mahale);
        mahale = new Mahale("میدان راه آهن", 35.6592219, 51.3981280);
        entries.add(mahale);
        mahale = new Mahale("میدان رسالت", 35.7367221, 51.4880205);
        entries.add(mahale);
        mahale = new Mahale("یدان سپاه", 35.7066186, 51.4403885);
        entries.add(mahale);
        mahale = new Mahale("میدان شقایق", 35.7194879, 51.5017491);
        entries.add(mahale);
        mahale = new Mahale("میدان شهدا", 35.6897551, 51.4473036);
        entries.add(mahale);
        mahale = new Mahale("میدان شوش", 35.6597109, 51.4308536);
        entries.add(mahale);
        mahale = new Mahale("میدان فتح", 35.6794111, 51.3405373);
        entries.add(mahale);
        mahale = new Mahale("میدان فلسطین", 35.7064226, 51.4037556);
        entries.add(mahale);
        mahale = new Mahale("میدان فلکه ی اول تهران پارس", 35.7267008, 51.5251956);
        entries.add(mahale);
        mahale = new Mahale("میدان فلکه ی سوم تهرا نپارس", 35.7431712, 51.5339792);
        entries.add(mahale);
        mahale = new Mahale("میدان قزوین", 35.6752388, 51.3946134);
        entries.add(mahale);
        mahale = new Mahale("میدان کاج", 35.7818938, 51.3749153);
        entries.add(mahale);
        mahale = new Mahale("میدان مادر", 35.7585208, 51.4429021);
        entries.add(mahale);
        mahale = new Mahale("میدان محمدیه", 35.6676491, 51.4158882);
        entries.add(mahale);
        mahale = new Mahale("میدان منیریه", 35.6812218, 51.4017739);
        entries.add(mahale);
        mahale = new Mahale("میدان نبوت", 35.7302614, 51.4938064);
        entries.add(mahale);
        mahale = new Mahale("میدان نو بنیاد", 35.7914614, 51.4776903);
        entries.add(mahale);
        mahale = new Mahale("میدان هروی", 35.7671655, 51.4762521);
        entries.add(mahale);
        mahale = new Mahale("میدان هفت تیر", 35.7168726, 51.4265996);
        entries.add(mahale);
        mahale = new Mahale("میدان ولی عصر", 35.7116750, 51.4071539);
        entries.add(mahale);
        mahale = new Mahale("نارمک", 35.7497400, 51.5140098);
        entries.add(mahale);
        mahale = new Mahale("نازی آباد", 35.6446809, 51.4031688);
        entries.add(mahale);
        mahale = new Mahale("نیاوران", 35.8162833, 51.4710220);
        entries.add(mahale);
        mahale = new Mahale("ولنجک", 35.8076506, 51.3995519);
        entries.add(mahale);
        mahale = new Mahale("ونک", 35.7601179, 51.4048952);
        entries.add(mahale);
        mahale = new Mahale("یوسف آباد", 35.7317253, 51.4042568);
        entries.add(mahale);
        return entries;
    }
//    public static String priceConvertor(long number, Resources res) {
//
//        double num = number / 10.0;
//        if (num <= 0) {
//            return "";
//        }
//        int digitCount = (int) Math.ceil(log10(num));
//
//        if (digitCount <= 6) {
//            return Constants.convertNumberToDecimal(num);
//        } else if (digitCount < 10) {
//            double result = num / Math.pow(10, 6);
//            return Constants.convertNumberToDecimal(result) + " " + res.getString(R.string.million) + " " + res.getString(R.string.toman);
//        }
//
//        double result = num / Math.pow(10, 9);
//        return Constants.convertNumberToDecimal(result) + " " + res.getString(R.string.billion) + " " + res.getString(R.string.toman);
//
//    }

    public String loadJSONFromAsset(Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("file_name.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

    public static File storage_Dir_Image = new File(
            Environment.getExternalStorageDirectory(),
            "/shahed/Images"
    );

    public static File storage_Dir_file_attachment = new File(
            Environment.getExternalStorageDirectory(),
            "/shahed/Questions"
    );

    public static File storage_Dir_File = new File(
            Environment.getExternalStoragePublicDirectory(
                    Environment.getRootDirectory().getParent()
            ),
            "/shahed/"
    );

    public static File makeFile() {
        if (!Constants.storage_Dir_Image.exists()) {
            Constants.storage_Dir_Image.mkdir();
        }
        return Constants.storage_Dir_Image;

    }
}
