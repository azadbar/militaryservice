package com.military.militaryservice.selectQuestion;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseFragment;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.baseView.BaseToolbar;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.enums.UserAccessEnum;
import com.military.militaryservice.image.AddImageDialog;
import com.military.militaryservice.orderInfo.ProjectResponse;
import com.military.militaryservice.orderInfo.QuestionInfosItem;
import com.military.militaryservice.recordAudio.AudioRecordDialog;
import com.military.militaryservice.recordVideo.AddVideoDialog;
import com.military.militaryservice.utils.Constants;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectListQuestionFragment extends BaseFragment implements SelectListQuestionAdapter.OnItemClickListener,
        AudioRecordDialog.updateListener, AddImageDialog.updateListenerImage, AddVideoDialog.updateListenerVideo {


    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvCenterTitle)
    BaseTextView tvCenterTitle;
    @BindView(R.id.share)
    BaseImageView share;
    @BindView(R.id.toolbar)
    BaseToolbar toolbar;
    @BindView(R.id.rvProjectList)
    RecyclerView rvProjectList;
    @BindView(R.id.imgView)
    BaseImageView imgView;
    @BindView(R.id.tvEmptyView)
    BaseTextView tvEmptyView;
    @BindView(R.id.rootEmptyView)
    BaseRelativeLayout rootEmptyView;
    private SelectListQuestionAdapter adapter;
    private ArrayList<QuestionInfosItem> questions;
    private String name;
    private String lastName;
    private int id;
    private ProjectResponse object;

    private File audioSavePathInDevice;
    private MediaRecorder mediaRecorder;
    AppDatabase database;
    private static final int REQUEST_VOICE_PARAM = 110;
    private static final int REQUEST_IMAGE_PARAM = 120;
    private static final int REQUEST_VIDEO_PARAM = 130;
    private boolean isImage;
    private boolean isVoice;
    private boolean isVideo;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_question_list, container, false);
        ButterKnife.bind(this, view);

        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.ic_arrow_forward);
        imgBack.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        });
        Bundle bundle = getArguments();
        if (bundle != null) {
            id = bundle.getInt("id");
            isImage = bundle.getBoolean("isImage");
            isVoice = bundle.getBoolean("isVoice");
            isVideo = bundle.getBoolean("isVideo");
        }


        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        database = AppDatabase.getInMemoryDatabase(getContext());
        object = database.projectResponseDao().getProject(id);

        SpannableStringBuilder desc_two = new SpannableStringBuilder();
        desc_two.append("محور های پرونده   ");
        int start = desc_two.length();
        desc_two.append(object.getPersonInfo().getFirstName() + " " + object.getPersonInfo().getLastName());
        int end = desc_two.length();
        desc_two.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new RelativeSizeSpan(1.1f), start, end, 0);
        tvCenterTitle.setText(desc_two);

        setAdapter();

    }

    private void setAdapter() {
        adapter = new SelectListQuestionAdapter((ArrayList<QuestionInfosItem>) object.getQuestionInfos(), isImage, isVoice, isVideo, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        rvProjectList.setLayoutManager(layoutManager);
        rvProjectList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }


    @Override
    public void onItemClick(int position, QuestionInfosItem questionInfosItem) {
        if (getActivity() != null) {
            if (Constants.getUser(getActivity()).getUserAccess() != UserAccessEnum.READ){
                if (isImage) {
                    if (getActivity() != null) {
                        FragmentManager fm = getActivity().getSupportFragmentManager();
                        AddImageDialog imageDialog = new AddImageDialog();
                        imageDialog.setListener(this);
                        Bundle bundle = new Bundle();
                        bundle.putInt("id", questionInfosItem.getQuestionInfosId());
                        imageDialog.setArguments(bundle);
                        imageDialog.setListener(this);
                        imageDialog.setTargetFragment(SelectListQuestionFragment.this, REQUEST_IMAGE_PARAM);
                        imageDialog.show(fm, AddImageDialog.class.getName());
                    }
                } else if (isVoice) {
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    AudioRecordDialog audioRecordDialog = new AudioRecordDialog();
                    Bundle bundle = new Bundle();
                    bundle.putInt("id", questionInfosItem.getQuestionInfosId());
                    audioRecordDialog.setArguments(bundle);
                    audioRecordDialog.setTargetFragment(SelectListQuestionFragment.this, REQUEST_VOICE_PARAM);
                    audioRecordDialog.setListener(this);
                    audioRecordDialog.setListener(this);
                    audioRecordDialog.show(fm, AudioRecordDialog.class.getName());
                } else if (isVideo) {
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    AddVideoDialog addVideoDialog = new AddVideoDialog();
                    Bundle bundle = new Bundle();
                    bundle.putInt("id", questionInfosItem.getQuestionInfosId());
                    addVideoDialog.setArguments(bundle);
                    addVideoDialog.setListener(this);
                    addVideoDialog.setTargetFragment(SelectListQuestionFragment.this, REQUEST_VIDEO_PARAM);
                    addVideoDialog.show(fm, AddVideoDialog.class.getName());
                }
            }

        }
    }


    @Override
    public boolean onPopBackStack() {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onInsertOk() {
        onResume();
    }

    @Override
    public void onInsetOk() {
        onResume();
    }

}
