package com.military.militaryservice.selectQuestion;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseActivity;

import java.util.List;

public class SelectQuestionActivity extends BaseActivity {

    private boolean isImage;
    private boolean isVoice;
    private boolean isVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_question);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isImage = bundle.getBoolean("isImage");
            isVoice = bundle.getBoolean("isVoice");
            isVideo = bundle.getBoolean("isVideo");
        }
        SelectListProjectFragment fragment = new SelectListProjectFragment();
        Bundle b = new Bundle();
        b.putBoolean("isImage", isImage);
        b.putBoolean("isVoice", isVoice);
        b.putBoolean("isVideo", isVideo);
        fragment.setArguments(b);
        loadFragment(fragment, SelectListProjectFragment.class.getName());
    }

    private void loadFragment(Fragment fragment, String fragmentTag) {
        FragmentManager fragMgr = getSupportFragmentManager();
        FragmentTransaction fragTrans = fragMgr.beginTransaction();
        fragTrans.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        fragTrans.replace(R.id.framelayout, fragment, fragmentTag);
        fragTrans.commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<Fragment> frags = getSupportFragmentManager().getFragments();
        if (frags != null) {
            for (Fragment f : frags) {
                if (f != null && f.isVisible())
                    f.onActivityResult(requestCode, resultCode, data);
            }
        }
    }
}
