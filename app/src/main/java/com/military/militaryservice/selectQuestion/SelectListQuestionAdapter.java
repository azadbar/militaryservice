package com.military.militaryservice.selectQuestion;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseLinearLayout;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.database.Video;
import com.military.militaryservice.database.Voice;
import com.military.militaryservice.enums.AttachmentType;
import com.military.militaryservice.orderInfo.QuestionInfosItem;
import com.military.militaryservice.resultInfo.FileAttachmentsItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class SelectListQuestionAdapter extends RecyclerView.Adapter<SelectListQuestionAdapter.ViewHolder> {


    private ArrayList<QuestionInfosItem> list;
    private boolean isImage;
    private boolean isVoice;
    private boolean isVideo;
    private final OnItemClickListener listener;


    SelectListQuestionAdapter(ArrayList<QuestionInfosItem> list, boolean isImage, boolean isVoice, boolean isVideo, OnItemClickListener listener) {
        this.list = list;
        this.isImage = isImage;
        this.isVoice = isVoice;
        this.isVideo = isVideo;
        this.listener = listener;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_select_question, parent, false);

        return new ViewHolder(itemView);

    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        QuestionInfosItem object = list.get(position);
        holder.tvCount.setText("محور " + (position + 1));
        holder.tvQuestion.setText(object.getQuestionInfo());
        holder.rlItem.setBackgroundColor(position % 2 == 0 ? res.getColor(R.color.white) : res.getColor(R.color.oddW));

        AppDatabase database = AppDatabase.getInMemoryDatabase(holder.rlItem.getContext());

        if (isImage) {
            List<FileAttachmentsItem> select = database.fileAttachmentDao().select(object.getQuestionInfosId(), AttachmentType.IMAGE.getFormat());
            holder.imgCount.setImageResource(R.drawable.ic_add_a_photo);
            holder.imgCount.setColorFilter(res.getColor(R.color.secondaryBack1));
            holder.count.setText(select.size() + "");
        } else if (isVideo) {
            List<FileAttachmentsItem> select = database.fileAttachmentDao().select(object.getQuestionInfosId(), AttachmentType.VIDEO.getFormat());
            holder.imgCount.setImageResource(R.drawable.ic_video);
            holder.imgCount.setColorFilter(res.getColor(R.color.secondaryBack1));
            holder.count.setText(select.size() + "");
        } else {
            List<FileAttachmentsItem> select = database.fileAttachmentDao().select(object.getQuestionInfosId(), AttachmentType.VOICE.getFormat());
            holder.imgCount.setImageResource(R.drawable.ic_keyboard_voice);
            holder.imgCount.setColorFilter(res.getColor(R.color.secondaryBack1));
            holder.count.setText(select.size() + "");
        }

        holder.bind(object, position, listener);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgDelete)
        BaseImageView imgDelete;
        @BindView(R.id.tvCount)
        BaseTextView tvCount;
        @BindView(R.id.tvQuestion)
        BaseTextView tvQuestion;
        @BindView(R.id.rlItem)
        BaseRelativeLayout rlItem;
        @BindView(R.id.row)
        BaseRelativeLayout row;
        @BindView(R.id.count)
        BaseTextView count;
        @BindView(R.id.btnPhoto)
        BaseLinearLayout btnPhoto;
        @BindView(R.id.imgCount)
        BaseImageView imgCount;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }


        public void bind(final QuestionInfosItem questionInfosItem, int position, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(position, questionInfosItem));
        }

    }


    public interface OnItemClickListener {

        void onItemClick(int position, QuestionInfosItem questionInfosItem);

    }

}
