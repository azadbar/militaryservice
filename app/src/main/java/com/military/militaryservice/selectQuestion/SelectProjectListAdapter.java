package com.military.militaryservice.selectQuestion;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseLinearLayout;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.model.PersonResponse;
import com.military.militaryservice.orderInfo.ProjectResponse;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class SelectProjectListAdapter extends RecyclerView.Adapter<SelectProjectListAdapter.ViewHolder> {


    private ArrayList<ProjectResponse> list;
    private final OnItemClickListener listener;


    SelectProjectListAdapter(ArrayList<ProjectResponse> list, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_select_project, parent, false);

        return new ViewHolder(itemView);

    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        ProjectResponse object = list.get(position);
        holder.tvTitle.setText(" آقای " + object.getPersonInfo().getFirstName() + " " + object.getPersonInfo().getLastName());
        holder.tvFatherName.setText(" فرزند " + object.getPersonInfo().getFatherName());
        holder.rlItem.setBackgroundColor(position % 2 == 0 ? res.getColor(R.color.white) : res.getColor(R.color.oddW));
        holder.bind(object, position, listener);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateReceiptsList(ArrayList<ProjectResponse> newlist) {
        list.clear();
        list.addAll(newlist);
        this.notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.rlItem)
        BaseLinearLayout rlItem;
        @BindView(R.id.row)
        BaseRelativeLayout row;
        @BindView(R.id.tvFatherName)
        BaseTextView tvFatherName;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }


        public void bind(final ProjectResponse projectResponse, int position, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(position, projectResponse));
        }

    }


    public interface OnItemClickListener {
        void onItemClick(int position, ProjectResponse personResponse);
    }

}
