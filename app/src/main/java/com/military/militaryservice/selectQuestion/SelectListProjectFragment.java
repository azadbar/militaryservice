package com.military.militaryservice.selectQuestion;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseFragment;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.model.PersonResponse;
import com.military.militaryservice.orderInfo.ProjectResponse;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectListProjectFragment extends BaseFragment implements SelectProjectListAdapter.OnItemClickListener {

    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvCenterTitle)
    BaseTextView tvCenterTitle;
    @BindView(R.id.rvProjectList)
    RecyclerView rvProjectList;
    @BindView(R.id.tvEmptyView)
    BaseTextView tvEmptyView;
    @BindView(R.id.rootEmptyView)
    BaseRelativeLayout rootEmptyView;
    private ArrayList<ProjectResponse> object;
    private SelectProjectListAdapter adapter;
    private boolean isImage;
    private boolean isVoice;
    private boolean isVideo;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_project_list, container, false);

        ButterKnife.bind(this, view);
        tvCenterTitle.setText("لیست پرونده ها");

        AppDatabase database = AppDatabase.getInMemoryDatabase(getContext());

        Bundle bundle = getArguments();
        if (bundle != null) {
            isImage = bundle.getBoolean("isImage");
            isVoice = bundle.getBoolean("isVoice");
            isVideo = bundle.getBoolean("isVideo");
        }
        object = (ArrayList<ProjectResponse>) database.projectResponseDao().getPersonList();
        if (object == null || object.size() == 0) {
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText(getString(R.string.nothing_project_found));
        } else
            setAdapter();
        return view;
    }

    private void setAdapter() {
        if (adapter == null) {
            adapter = new SelectProjectListAdapter(object, this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
            rvProjectList.setHasFixedSize(true);
            rvProjectList.setLayoutManager(layoutManager);
            rvProjectList.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        } else {
            adapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onItemClick(int position, ProjectResponse projectResponse) {
        SelectListQuestionFragment fragment = new SelectListQuestionFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("id", projectResponse.getId());
        bundle.putBoolean("isImage", isImage);
        bundle.putBoolean("isVoice", isVoice);
        bundle.putBoolean("isVideo", isVideo);
        fragment.setArguments(bundle);
        loadFragment(fragment, SelectListProjectFragment.class.getName());

    }

    private void loadFragment(Fragment fragment, String fragmentTag) {
        if (getActivity() != null) {
            FragmentManager fragMgr = getActivity().getSupportFragmentManager();
            FragmentTransaction fragTrans = fragMgr.beginTransaction();
            fragTrans.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
            fragTrans.add(R.id.framelayout, fragment, fragmentTag);
            fragTrans.addToBackStack(fragmentTag);
            fragTrans.commit();
        }
    }

    @Override
    public boolean onPopBackStack() {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
        return true;
    }


}
