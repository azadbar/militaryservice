package com.military.militaryservice.sendMessage;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseFragment;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.baseView.BaseToolbar;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.database.Message;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_OK;

public class SendMessageFragment extends BaseFragment implements SendMessageAdapter.OnItemClickListener {


    private static final int ADD_CONTACT = 10;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvCenterTitle)
    BaseTextView tvCenterTitle;
    @BindView(R.id.share)
    BaseImageView share;
    @BindView(R.id.toolbar)
    BaseToolbar toolbar;
    @BindView(R.id.rvMessage)
    RecyclerView rvMessage;
    @BindView(R.id.btnAddContact)
    FloatingActionButton btnAddContact;
    @BindView(R.id.imgView)
    BaseImageView imgView;
    @BindView(R.id.tvEmptyView)
    BaseTextView tvEmptyView;
    @BindView(R.id.rootEmptyView)
    BaseRelativeLayout rootEmptyView;
    private Unbinder unbinder;
    private ArrayList<Message> list = new ArrayList<>();
    private SendMessageAdapter adapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_send_message, container, false);

        unbinder = ButterKnife.bind(this, view);
        tvCenterTitle.setText(getResources().getString(R.string.send_message_emergancy));


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        AppDatabase database = AppDatabase.getInMemoryDatabase(getActivity());
        list = (ArrayList<Message>) database.messageDao().getMessageList();
        setDataAdapter();
        if (list.size() == 0) {
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText("هیچ پیامی یافت نشد");
        } else {
            rootEmptyView.setVisibility(View.GONE);
        }
    }

    private void setDataAdapter() {
        adapter = new SendMessageAdapter(list, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvMessage.setHasFixedSize(true);
        rvMessage.setLayoutManager(layoutManager);
        rvMessage.setItemAnimator(new DefaultItemAnimator());
        rvMessage.setAdapter(adapter);

    }

    @OnClick({R.id.imgBack, R.id.btnAddContact})
    public void onViewClicked(View view) {
        Intent intent = null;
        switch (view.getId()) {
            case R.id.imgBack:
                if (getActivity() != null) {
                    getActivity().finish();
                }
                break;
            case R.id.btnAddContact:
//                if (getActivity() != null) {
//                    FragmentManager fm = getActivity().getSupportFragmentManager();
//                    AddContactDialog searchParamDialog = new AddContactDialog();
//                    searchParamDialog.setTargetFragment(SendMessageFragment.this, ADD_CONTACT);
//                    searchParamDialog.show(fm, AddContactDialog.class.getName());
//                }

                intent = new Intent(getActivity(), AddMessageDialog.class);
                startActivity(intent);
                break;

        }
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ADD_CONTACT) {
                onResume();
            }
        }
    }

    @Override
    public void onItemClick(int position, Message message) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + message.getMobile()));
        intent.putExtra("sms_body", message.getMessage());
        startActivity(intent);
    }

    @Override
    public void onItemDelete(int position, Message message) {
        AppDatabase database = AppDatabase.getInMemoryDatabase(getActivity());
        database.messageDao().delete(message);
        removeFromList(message.getId());
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }

        if (list == null || list.size() == 0) {
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText(getString(R.string.nothing_message));
        }
    }


    private void removeFromList(int id) {
        for (Message message : list) {
            if (message.getId() == id) {
                list.remove(message);
            }
        }
    }

}
