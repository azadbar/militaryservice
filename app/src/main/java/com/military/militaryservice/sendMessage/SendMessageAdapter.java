package com.military.militaryservice.sendMessage;

import android.annotation.SuppressLint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.database.Message;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class SendMessageAdapter extends RecyclerView.Adapter<SendMessageAdapter.ViewHolder> {



    private ArrayList<Message> list;
    private final OnItemClickListener listener;


    SendMessageAdapter(ArrayList<Message> list, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_send_message, parent, false);

        return new ViewHolder(itemView);

    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Message message = list.get(position);
        holder.tvMessage.setText(message.getMessage());
//        if (!TextUtils.isEmpty(message.getMobile())) {
//            holder.tvMobile.setText("شماره تماس: " + message.getMobile());
//        } else {
//            holder.tvMobile.setText("شماره تماس تعیین نشده");
//        }

        holder.bind(message, position, listener);
        holder.imgDelete.setOnClickListener(v -> listener.onItemDelete(position, message));
//        holder.tvEdit.setOnClickListener(v -> listener.onItemEdit(position, message));

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateList(ArrayList<Message> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.tvMessage)
        BaseTextView tvMessage;
        @BindView(R.id.imgDelete)
        BaseImageView imgDelete;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }


        public void bind(final Message message, int position, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(position, message));
        }

    }


    public interface OnItemClickListener {

        void onItemClick(int position, Message message);

        void onItemDelete(int position, Message message);


    }

}
