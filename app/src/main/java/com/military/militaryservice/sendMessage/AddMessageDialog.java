package com.military.militaryservice.sendMessage;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.customView.CustomMultiLineEditText;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.database.Contact;
import com.military.militaryservice.database.Message;
import com.military.militaryservice.dialog.CustomDialog;
import com.military.militaryservice.utils.Constants;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddMessageDialog extends DialogFragment implements SendMessageAdapter.OnItemClickListener {


    @BindView(R.id.edtDescription)
    CustomMultiLineEditText edtDescription;
    @BindView(R.id.rvMessage)
    RecyclerView rvMessage;
    @BindView(R.id.btnOk)
    BaseTextView btnOk;
    @BindView(R.id.btnCancel)
    BaseTextView btnCancel;
    private ArrayList<Message> list;
    private SendMessageAdapter adapter;
    private Contact contact;
    private String message;
    private AppDatabase database;
    private updateListActivtyListener listener;

    public void setListener(updateListActivtyListener listener) {
        this.listener = listener;
    }

    public AddMessageDialog() {

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View v = inflater.inflate(R.layout.dialog_add_message, container, false);
        ButterKnife.bind(this, v);

        Bundle bundle = getArguments();
        if (bundle != null) {
            contact = (Contact) bundle.get("contact");
        }

        database = AppDatabase.getInMemoryDatabase(getActivity());
        list = (ArrayList<Message>) database.messageDao().getMessageList();

        if (contact != null && contact.getMessage() != null && !contact.getMessage().isEmpty()) {
            edtDescription.setBody(contact.getMessage());
        }
        setDataAdapter();

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
        if (getActivity() != null && window != null && getContext() != null) {
            WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {
                Point size = Constants.getScreenSize(windowManager);
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                int width = (int) Math.min(size.x * 0.90, getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_width));
                int height = (int) Math.min(size.y * 0.55, getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_fragment_height));
                window.setLayout(width, height);
                window.setGravity(Gravity.CENTER);
            }
        }

    }


    private void setDataAdapter() {
        if (adapter == null) {
            adapter = new SendMessageAdapter(list, this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
            rvMessage.setHasFixedSize(true);
            rvMessage.setLayoutManager(layoutManager);
            rvMessage.setItemAnimator(new DefaultItemAnimator());
            rvMessage.setAdapter(adapter);
        } else {
            adapter.updateList(list);
        }
    }

    private boolean isValidData() {
        ArrayList<String> errorMsgList = new ArrayList<>();

        if (!TextUtils.isEmpty(edtDescription.getError())) {
            errorMsgList.add(edtDescription.getError());
        }


        if (errorMsgList.size() > 0) {
//            showInfoDialog(getResources().getString(R.string.fill_following), errorMsgList);
            return false;
        }
        return true;
    }

    @OnClick({R.id.btnOk, R.id.btnCancel, R.id.imgAddMessage})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnOk:
                if (edtDescription.getError() == null) {
                    database = AppDatabase.getInMemoryDatabase(getContext());
                    database.contactDao().update(edtDescription.getValueString(), contact.getId());
                    Toast.makeText(getContext(), "پیام اضافه شد", Toast.LENGTH_SHORT).show();
                    dismiss();
                    listener.onUpdate();
                } else {
                    Toast.makeText(getContext(), "لطفا یک پیام اضافه کنید", Toast.LENGTH_SHORT).show();
                }


                break;
            case R.id.btnCancel:
                dismiss();
                break;
            case R.id.imgAddMessage:
                if (edtDescription.getError() == null) {
                    AppDatabase database = AppDatabase.getInMemoryDatabase(getContext());
                    Message message = new Message();
                    message.setMessage(edtDescription.getTextVal());
                    message.setDate(new Date());
                    database.messageDao().insertMessage(message);
                    edtDescription.setBody(null);
                    Toast.makeText(getContext(), "پیام اضافه شد", Toast.LENGTH_SHORT).show();
                    list = (ArrayList<Message>) database.messageDao().getMessageList();
                    setDataAdapter();
                } else {
                    Toast.makeText(getContext(), "متن پیام را وارد کنید", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onItemClick(int position, Message message) {

        edtDescription.setBody(message != null ? message.getMessage() : null);

    }

    @Override
    public void onItemDelete(int position, Message message) {
        CustomDialog dialog = new CustomDialog(getContext());
        dialog.setOkListener(getResources().getString(R.string.ok), view1 -> {
            dialog.dismiss();
            AppDatabase database = AppDatabase.getInMemoryDatabase(getActivity());
            database.messageDao().delete(message);
            removeFromList(message.getId());
            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }

            if (adapter != null) {
                adapter.notifyDataSetChanged();
            }


        });
        dialog.setCancelListener(getResources().getString(R.string.cancel), view12 -> dialog.dismiss());
        dialog.setDialogTitle("آیا برای حذف مطمئن هستید؟");
        dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
        dialog.setIcon(R.drawable.ic_delete, getResources().getColor(R.color.redColor));
        dialog.show();
    }

    private void removeFromList(int id) {
        for (Message message : list) {
            if (message.getId() == id) {
                list.remove(message);
            }
        }
    }

    public interface updateListActivtyListener {
        void onUpdate();
    }
}
