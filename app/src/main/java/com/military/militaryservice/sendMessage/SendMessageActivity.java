package com.military.militaryservice.sendMessage;

import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.widget.LinearLayout;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SendMessageActivity extends BaseActivity {

    @BindView(R.id.frameLayout_estate)
    LinearLayout frameLayoutEstate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_message);
        ButterKnife.bind(this);

        SendMessageFragment estateDetailFragment = new SendMessageFragment();
        FragmentManager fragMgr = getSupportFragmentManager();
        FragmentTransaction fragTrans = fragMgr.beginTransaction();
        fragTrans.replace(R.id.frameLayout_estate, estateDetailFragment);
        fragTrans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragTrans.commit();
    }
}

