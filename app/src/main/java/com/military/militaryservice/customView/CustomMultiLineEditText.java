package com.military.militaryservice.customView;

import android.content.Context;
import android.content.res.TypedArray;

import androidx.annotation.Nullable;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseEditText;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.enums.InputTypeEnum;
import com.military.militaryservice.listener.OnEditTextChangeListener;
import com.military.militaryservice.utils.Constants;


public class CustomMultiLineEditText extends LinearLayout implements OnEditTextChangeListener {
    private BaseTextView tvTitle;
    private BaseEditText edtBody;
    private BaseImageView image;
    private String error;
    private InputTypeEnum inputTypeEnum;
    private BaseRelativeLayout root;


    public CustomMultiLineEditText(Context context) {
        this(context, null);
    }

    public CustomMultiLineEditText(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);

    }

    public CustomMultiLineEditText(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    private void initView(Context context, @Nullable AttributeSet attrs) {
        LayoutInflater.from(context).inflate(R.layout.custom_multiline_edit_text, this, true);
        tvTitle = findViewById(R.id.tvTitle);
        edtBody = findViewById(R.id.edtBody);
        image = findViewById(R.id.image);
        root = findViewById(R.id.root);
        this.setLayoutDirection(Constants.language.getLayoutDirection());

        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomMultiLineEditText, 0, 0);
            String titleText = a.getString(R.styleable.CustomMultiLineEditText_tvTitleMulti);
            String bodyValue = a.getString(R.styleable.CustomMultiLineEditText_edtBodyMulti);
            String hint = a.getString(R.styleable.CustomMultiLineEditText_hintMulti);
            Boolean isRequired = a.getBoolean(R.styleable.CustomMultiLineEditText_isRequiredMulti, false);
            if (a.hasValue(R.styleable.CustomMultiLineEditText_inputTypeMulti)) {
                int value = a.getInt(R.styleable.CustomMultiLineEditText_inputTypeMulti, 0);

                if (value >= 0 && value < InputTypeEnum.values().length) {
                    setTypeEnum(InputTypeEnum.values()[value]);
                }
            }
            a.recycle();
            setTextsTitle(titleText);
            setTextValue(bodyValue);
            setTextHint(hint);
            setIsRequired(isRequired);
        }

        //for screoll in srcrolView
        edtBody.setOnTouchListener((v, event) -> {
            if (edtBody.hasFocus()) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction() & MotionEvent.ACTION_MASK){
                    case MotionEvent.ACTION_SCROLL:
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        return true;
                }
            }
            return false;
        });
        edtBody.onEditTextChangeListener = this;

    }

    public void setTextHint(String hint) {
        edtBody.setHint(hint);
    }

    public void setBody(String body) {
        edtBody.setText(body);
    }


    public void setTypeEnum(InputTypeEnum inputTypeEnum) {
        this.edtBody.setInputTypeEnum(inputTypeEnum);
        this.inputTypeEnum = inputTypeEnum;
    }

    public InputTypeEnum getInput() {
        return inputTypeEnum;
    }

    public void setIsRequired(Boolean isRequired) {
        if (isRequired) {
            image.setVisibility(VISIBLE);
            error = getResources().getString(R.string.enter_title, tvTitle.getText().toString().trim());
            image.setImageResource(R.drawable.ic_asterisk);
        }
        this.edtBody.setIsRequired(isRequired);
    }

    public void setTextsTitle(String title) {
        if (title != null && !title.trim().isEmpty()) {
            tvTitle.setVisibility(VISIBLE);
            tvTitle.setText(title);
            edtBody.title = title;
        } else {
            tvTitle.setVisibility(GONE);

        }
    }

    public void setTextValue(String value) {
        edtBody.setText(value);
    }

    public String getTextVal() {
        if (edtBody.getText() == null) {
            return "";
        }
        return edtBody.getText().toString().trim();
    }

    @Override
    public void onGetError(String error) {
        this.error = error;
        if (error != null) {
            image.setVisibility(VISIBLE);
            image.setImageDrawable(getResources().getDrawable(R.drawable.ic_asterisk));
        } else if (!"".equals(edtBody.getTrimedText())) {
            image.setVisibility(GONE);
            image.setImageDrawable(getResources().getDrawable(R.drawable.ic_check_tick));
        } else {
            image.setVisibility(GONE);
        }
    }

    public String getError() {
        return error;
    }

    public String getValue() {
        return edtBody.getValueString();
    }

    public void setTextBody(String text) {
        if (text != null) {
            edtBody.setText(text);
        }
    }

    public String getValueString() {
        return edtBody.getValueString();
    }

    public void disableView() {
        edtBody.setEnabled(false);
        edtBody.setClickable(false);
        edtBody.setFocusable(false);
        root.setBackground(getResources().getDrawable(R.color.disable_edit_text));
    }

    public void enableView() {
        edtBody.setEnabled(true);
        edtBody.setClickable(true);
        edtBody.setFocusable(true);
        edtBody.setFocusableInTouchMode(true);
        root.setBackground(getResources().getDrawable(R.color.white));

    }

    public BaseEditText getEdtBody() {
        return edtBody;
    }


}
