package com.military.militaryservice.customView;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseEditText;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.enums.InputTypeEnum;
import com.military.militaryservice.listener.OnEditTextChangeListener;
import com.military.militaryservice.utils.Constants;


public class CustomEditText extends LinearLayout implements OnEditTextChangeListener {
    BaseTextView tvTitle;
    BaseEditText edtBody;
    BaseImageView image, edtIcon;

    private String error;
    private InputTypeEnum inputTypeEnum;
    boolean passwordIsVisable = false;
    private View root;


    public CustomEditText(Context context) {
        this(context, null);
    }

    public CustomEditText(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);

    }

    public CustomEditText(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs, defStyleAttr);
    }

    private void initView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        LayoutInflater.from(context).inflate(R.layout.custom_edit_text, this, true);
        tvTitle = findViewById(R.id.tvTitle);
        edtBody = findViewById(R.id.edtBody);
        image = findViewById(R.id.image);
        edtIcon = findViewById(R.id.edtIcon);
        root = findViewById(R.id.root);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            this.setLayoutDirection(Constants.language.getLayoutDirection());
        }

        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomEditText, 0, 0);
            String titleText = a.getString(R.styleable.CustomEditText_tvTitle);
            String bodyValue = a.getString(R.styleable.CustomEditText_edtBody);
            String hint = a.getString(R.styleable.CustomEditText_hint);
            Boolean isRequired = a.getBoolean(R.styleable.CustomEditText_isRequired, false);
            Boolean isVisibleEye = a.getBoolean(R.styleable.CustomEditText_isVisibleEye, false);
            int icon = a.getResourceId(R.styleable.CustomEditText_edtIcon, 0);
            if (a.hasValue(R.styleable.CustomEditText_inputType)) {
                int value = a.getInt(R.styleable.CustomEditText_inputType, 0);

                if (value >= 0 && value < InputTypeEnum.values().length) {
                    setInputTypeEnum(InputTypeEnum.values()[value]);
                }
            }

            a.recycle();
            setTextsTitle(titleText);
            setEdtIcon(icon);
//            setTextValue(bodyValue);
            setTextHint(hint);
            setIsRequired(isRequired);
            setVisible(isVisibleEye);
        }
        edtBody.onEditTextChangeListener = this;

    }


    public void setUnit(String unit) {
        this.edtBody.unit = unit;
    }

    public void setTextHint(String hint) {
        edtBody.setHint(hint);
    }

    public void setInputTypeEnum(InputTypeEnum inputTypeEnum) {
        this.inputTypeEnum = inputTypeEnum;
        this.edtBody.setInputTypeEnum(inputTypeEnum);
        if (this.edtBody.getText() != null)
            this.edtBody.setSelection(this.edtBody.getText().length());
    }

    private void setVisible(Boolean isVisibleEye) {
        if (isVisibleEye) {
            image.setVisibility(VISIBLE);
        } else {
            image.setVisibility(GONE);
        }
    }


    public InputTypeEnum getInputTypeEnum() {
        return inputTypeEnum;
    }

    public void setIsRequired(Boolean isRequired) {
        if (isRequired) {
//            image.setVisibility(VISIBLE);
            error = getResources().getString(R.string.enter_title, tvTitle.getText().toString().trim());
//            image.setImageResource(R.drawable.ic_asterisk);
        } else {
//            image.setVisibility(GONE);
            error = null;
        }
        this.edtBody.setIsRequired(isRequired);
    }

    public void setTextsTitle(String title) {
        if (title != null || title.trim().isEmpty()) {
            tvTitle.setVisibility(VISIBLE);
            tvTitle.setText(title);
            edtBody.title = title;
        } else {
            tvTitle.setVisibility(GONE);

        }
    }

    private void setEdtIcon(int icon) {
        if (icon > 0) {
            edtIcon.setVisibility(VISIBLE);
            edtIcon.setImageResource(icon);
        } else {
            edtIcon.setVisibility(GONE);
        }
    }


    public void setBody(String body) {
        edtBody.setText(body);
    }
//    public void setTextValue(String value) {
//        edtBody.setText(value);
//    }

//    public String getTextVal() {
//        if (edtBody.getText() == null) {
//            return "";
//        }
//        return edtBody.getText().toString().trim();
//    }

    @Override
    public void onGetError(String error) {
        setError(error);
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
        if (error != null) {
//            image.setVisibility(VISIBLE);
//            image.setImageDrawable(getResources().getDrawable(R.drawable.ic_asterisk));
        } else if (!"".equals(edtBody.getTrimedText())) {
//            image.setVisibility(VISIBLE);
//            image.setImageDrawable(getResources().getDrawable(R.drawable.ic_check_tick));
        } else {
//            image.setVisibility(GONE);
        }
    }

    public String getValueString() {
        return edtBody.getValueString();
    }

    public int getValueInt() {
        return edtBody.getValueInt();
    }

    public long getValueLong() {
        return edtBody.getValueLong();
    }

    public void showHidePassword() {
        if (!passwordIsVisable) {
            image.setImageResource(R.drawable.ic_eye);
            setInputTypeEnum(InputTypeEnum.PASSWORD_SHOW);
        } else {
            image.setImageResource(R.drawable.ic_eye_off);
            setInputTypeEnum(InputTypeEnum.PASSWORD);
        }

        passwordIsVisable = !passwordIsVisable;
    }

    public BaseImageView getImage() {
        return image;
    }

    public void setTextBody(String text) {
        if (text != null) {
            edtBody.setText(text);
        }
    }

    public void disableView() {
        edtBody.setEnabled(false);
        edtBody.setClickable(false);
        edtBody.setFocusable(false);
        root.setBackground(getResources().getDrawable(R.drawable.disable_edit_text));
    }

}
