package com.military.militaryservice.customView;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.military.militaryservice.R;

import java.util.ArrayList;


/**
 * Created by a.Raghibdoust on 10/11/2017.
 */

public class FilterSpinnerAdapterForKeyValue extends ArrayAdapter<KeyValueObject> {

    private Context context;
    ArrayList<KeyValueObject> list;
    int selectedIndex;
    public FilterSpinnerAdapterForKeyValue(Context ctx, ArrayList<KeyValueObject> itemList) {

        super(ctx, 0, itemList);
        this.context=ctx;
        this.list =itemList;

    }
    public int getSelectedIndex() {
        return selectedIndex;

    }

    public void setSelectedIndex(int index) {
        selectedIndex = index;
        notifyDataSetChanged();

    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {



        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_spinner, parent, false);
        }
        TextView txt=  convertView.findViewById(R.id.spinnertitle);
        txt.setText(list.get(position).getTitle());
        txt.setGravity(Gravity.CENTER);
        int padding = (int) getContext().getResources().getDimension(R.dimen.input_field_padding);
        txt.setPadding(padding, padding, padding, padding);


        txt.setTextColor(Color.parseColor("#1171d0"));
        txt.setSingleLine(true);
        txt.setEllipsize(TextUtils.TruncateAt.END);
        txt.setSingleLine(true);

        return convertView;
    }


    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        String text = list.get(position).getTitle().toString();
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_spinner, parent, false);
        }
        TextView txt= (TextView) convertView.findViewById(R.id.spinnertitle);
        txt.setText(text);



        txt.setPadding(0, 0, 0, 0);
        txt.setGravity(Gravity.CENTER);
        txt.setTextColor(Color.parseColor("#1171d0"));
        txt.setBackgroundColor(Color.parseColor("#FFFFFF"));
        return convertView;

    }


}
