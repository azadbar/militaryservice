package com.military.militaryservice.customView;

/**
 * Created by m.azadbar on 5/8/2018.
 */

public class KeyValueObject {
    int value;
    String title;

    public KeyValueObject(int value, String title) {
        this.value = value;
        this.title = title;
    }

    public int getValue() {
        return value;
    }

    public String getTitle() {
        return title;
    }
}
