package com.military.militaryservice.dialog;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;

import com.bumptech.glide.Glide;
import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseLinearLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.customView.MyJustifiedTextView;
import com.military.militaryservice.fingerPrint.FingerprintHandler;
import com.military.militaryservice.fingerPrint.OnFinishListener;
import com.military.militaryservice.splash.SplashActivity;
import com.military.militaryservice.utils.Constants;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by a.azadbar on 10/7/2017.
 */

@RequiresApi(api = Build.VERSION_CODES.M)
public class FingerPrintDialog extends Dialog implements onUpdateUi {

    @BindView(R.id.icon)
    AppCompatImageView icon;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.tvDescriptioError)
    MyJustifiedTextView tvDescriptioError;
    @BindView(R.id.btnCancel)
    BaseTextView btnCancel;
    @BindView(R.id.btnOk)
    BaseTextView btnOk;
    @BindView(R.id.rlBtn)
    BaseLinearLayout rlBtn;
    @BindView(R.id.call)
    BaseTextView call;

    private String okTitle;
    private View.OnClickListener cancelListener;
    private View.OnClickListener okListener;
    private String cancelTitle;
    private String description;
    private String title;
    private int image;
    private int color;
    private int colorIcon;
    private String callText;
    private SpannableStringBuilder sppanbleDesc;

    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;
    private KeyStore keyStore;
    // Variable used for storing the key in the Android Keystore container
    private static final String KEY_NAME = "androidHive";
    private Cipher cipher;
    private OnFinishListener finishListener;

    public FingerPrintDialog(@NonNull Context context) {
        super(context);
    }

    public void setListener(OnFinishListener listener) {
        finishListener = listener;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        View view = View.inflate(getContext(), R.layout.custom_dialog, null);
        setContentView(view);
        ButterKnife.bind(this, view);
        setCancelable(false);
        if (cancelListener != null) {
            btnCancel.setVisibility(View.VISIBLE);
            btnCancel.setText(cancelTitle);
            btnCancel.setOnClickListener(cancelListener);
        } else {
            btnCancel.setVisibility(View.GONE);
        }
        if (okTitle != null) {
            btnOk.setText(okTitle);
            btnOk.setVisibility(View.VISIBLE);
            btnOk.setOnClickListener(okListener);
        } else {
            btnOk.setVisibility(View.GONE);
        }
        tvTitle.setText(title);
        tvTitle.setTextColor(color);
        if (description != null) {
            tvDescriptioError.setText(description);

        }
        if (sppanbleDesc != null) {
            tvDescriptioError.setText(sppanbleDesc);
        }
        if (callText != null) {
            call.setVisibility(View.VISIBLE);
            call.setText(callText);
        } else {
            call.setVisibility(View.GONE);
        }

        if (image > 0) {
            icon.setVisibility(View.VISIBLE);
            Glide.with(icon.getContext()).load(image).into(icon);
            icon.setColorFilter(colorIcon, PorterDuff.Mode.SRC_ATOP);
        }
        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {
                Point size = Constants.getScreenSize(windowManager);
                int width = (int) Math.min(size.x * 0.9, getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_width));
                window.setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT);
            }
        }


        keyguardManager = (KeyguardManager) getContext().getSystemService(getContext().KEYGUARD_SERVICE);
        fingerprintManager = (FingerprintManager) getContext().getSystemService(getContext().FINGERPRINT_SERVICE);

        if (!fingerprintManager.isHardwareDetected()) {
            tvDescriptioError.setText("دستگاه شما از سنسور اثر انگشت پشتیبانی نخواهد کرد");
        } else {
            // Checks whether fingerprint permission is set on manifest
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
//                textView.setText("Fingerprint authentication permission not enabled");
            } else {
                // Check whether at least one fingerprint is registered
                if (!fingerprintManager.hasEnrolledFingerprints()) {
                    tvDescriptioError.setText("حداقل یک اثر انگشت را در تنظیمات ثبت کنید");
                } else {
                    // Checks whether lock screen security is enabled or not
                    if (!keyguardManager.isKeyguardSecure()) {
                        tvDescriptioError.setText("امنیت قفل صفحه در تنظیمات فعال نیست");
                    } else {
                        generateKey();

                        if (cipherInit()) {
                            FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
                            FingerprintHandler helper = new FingerprintHandler(getContext(), this);
                            helper.startAuth(fingerprintManager, cryptoObject);
                        }
                    }
                }
            }
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    protected void generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }

        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get KeyGenerator instance", e);
        }

        try {
            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean cipherInit() {
        try {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }


    public void setIcon(int image, int color) {
        this.image = image;
        this.colorIcon = color;
    }

    public void setCancelListener(String cancelTitle, View.OnClickListener cancelListener) {
        this.cancelTitle = cancelTitle;
        this.cancelListener = cancelListener;
    }

    public void setOkListener(String okTitle, View.OnClickListener okListener) {
        this.okTitle = okTitle;
        this.okListener = okListener;
    }

    public void setDialogTitle(String title) {
        this.title = title;
    }

    public void setColorTitle(int color) {
        this.color = color;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public void setDescription(SpannableStringBuilder description) {
        this.sppanbleDesc = description;
    }

    public void setCall(String call) {
        this.callText = call;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        dismiss();
    }

    @Override
    public void Update(String s) {
        tvDescriptioError.setText(s);

    }

    @Override
    public void finish() {
        finishListener.onFinish();
    }



}
