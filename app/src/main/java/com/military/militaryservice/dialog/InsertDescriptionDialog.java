package com.military.militaryservice.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.customView.CustomMultiLineEditText;
import com.military.militaryservice.enums.UserAccessEnum;
import com.military.militaryservice.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by a.azadbar on 10/7/2017.
 */

public class InsertDescriptionDialog extends Dialog {


    @BindView(R.id.icon)
    AppCompatImageView icon;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    @BindView(R.id.edtDescription)
    CustomMultiLineEditText edtName;
    @BindView(R.id.btnOk)
    BaseTextView btnOk;
    @BindView(R.id.btnCancel)
    BaseTextView btnCancel;
    private String okTitle;
    private View.OnClickListener cancelListener;
    private View.OnClickListener okListener;
    private String cancelTitle;
    private String title;
    private int image;
    private int color;
    private int colorIcon;
    private String name;

    public InsertDescriptionDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        View view = View.inflate(getContext(), R.layout.custom_insert_description_dialog, null);
        setContentView(view);
        ButterKnife.bind(this, view);
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }

        setCancelable(false);
        if (cancelListener != null) {
            btnCancel.setVisibility(View.VISIBLE);
            btnCancel.setText(cancelTitle);
            btnCancel.setOnClickListener(cancelListener);
        } else {
            btnCancel.setVisibility(View.GONE);
        }
        if (okTitle != null) {
            btnOk.setText(okTitle);
            btnOk.setVisibility(View.VISIBLE);
            btnOk.setOnClickListener(okListener);
        } else {
            btnOk.setVisibility(View.GONE);
        }
        tvTitle.setText(title);
        tvTitle.setTextColor(color);

        if (image > 0) {
            icon.setVisibility(View.VISIBLE);
            icon.setImageResource(image);
            icon.setColorFilter(colorIcon, PorterDuff.Mode.SRC_ATOP);
        }

        if (name != null) {
            edtName.setTextBody(name);
        }
        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {
                Point size = Constants.getScreenSize(windowManager);
                int width = (int) Math.min(size.x * 0.9, getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_width));
                window.setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT);
            }
        }

        if (Constants.getUser(getContext()).getUserAccess() == UserAccessEnum.READ) {
            btnOk.setClickable(false);
            btnOk.setEnabled(false);
            btnOk.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.rippleligh_gray_no_radius));
            btnCancel.setClickable(false);
            btnCancel.setEnabled(false);
            btnCancel.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.rippleligh_gray_no_radius));
        } else {
            btnOk.setClickable(true);
            btnOk.setEnabled(true);
            btnOk.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ripplewhite_no_radius));
            btnCancel.setClickable(true);
            btnCancel.setEnabled(true);
            btnCancel.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ripplewhite_no_radius));
        }
    }

    public void setIcon(int image, int color) {
        this.image = image;
        this.colorIcon = color;
    }

    public void setCancelListener(String cancelTitle, View.OnClickListener cancelListener) {
        this.cancelTitle = cancelTitle;
        this.cancelListener = cancelListener;
    }

    public void setOkListener(String okTitle, View.OnClickListener okListener) {
        this.okTitle = okTitle;
        this.okListener = okListener;
    }

    public void setDialogTitle(String title) {
        this.title = title;
    }

    public void setColorTitle(int color) {
        this.color = color;
    }

    public void setEditTextName(String name) {
        this.name = name;
    }

    public String getEditText() {
        return edtName.getValueString();
    }

    @Override
    public void onBackPressed() {
        dismiss();
    }
}
