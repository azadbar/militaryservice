package com.military.militaryservice.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;

import com.bumptech.glide.Glide;
import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseLinearLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.customView.CustomEditText;
import com.military.militaryservice.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by a.azadbar on 10/7/2017.
 */

public class ValidationUserDialog extends Dialog {


    @BindView(R.id.icon)
    AppCompatImageView icon;
    @BindView(R.id.edtMobile)
    CustomEditText edtMobile;
    @BindView(R.id.edtPassword)
    CustomEditText edtPassword;
    @BindView(R.id.rlLogin)
    BaseLinearLayout rlLogin;
    @BindView(R.id.btnOk)
    BaseTextView btnOk;
    @BindView(R.id.btnCancel)
    BaseTextView btnCancel;
    @BindView(R.id.rlBtn)
    BaseLinearLayout rlBtn;
    @BindView(R.id.tvTitle)
    BaseTextView tvTitle;
    private String okTitle;
    private View.OnClickListener cancelListener;
    private View.OnClickListener okListener;
    private String cancelTitle;
    private String title;
    private int image;
    private int color;
    private int colorIcon;
    private String mobile;
    private String password;

    public ValidationUserDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        View view = View.inflate(getContext(), R.layout.validation_user_dialog, null);
        setContentView(view);
        ButterKnife.bind(this, view);
        setCancelable(false);
        if (cancelListener != null) {
            btnCancel.setVisibility(View.VISIBLE);
            btnCancel.setText(cancelTitle);
            btnCancel.setOnClickListener(cancelListener);
        } else {
            btnCancel.setVisibility(View.GONE);
        }
        if (okTitle != null) {
            btnOk.setText(okTitle);
            btnOk.setVisibility(View.VISIBLE);
            btnOk.setOnClickListener(okListener);
        } else {
            btnOk.setVisibility(View.GONE);
        }
        tvTitle.setText(title);
        tvTitle.setTextColor(color);

        if (!TextUtils.isEmpty(mobile)){
            edtMobile.setBody(mobile);
        }

        if (!TextUtils.isEmpty(password)){
            edtPassword.setBody(password);
        }

        if (image > 0) {
            icon.setVisibility(View.VISIBLE);
            Glide.with(icon.getContext()).load(image).into(icon);
            icon.setColorFilter(colorIcon, PorterDuff.Mode.SRC_ATOP);
        }
        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {
                Point size = Constants.getScreenSize(windowManager);
                int width = (int) Math.min(size.x * 0.9, getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_width));
                window.setLayout(width, WindowManager.LayoutParams.WRAP_CONTENT);
            }
        }

        edtMobile.getImage().setOnClickListener(v -> edtMobile.showHidePassword());
        edtPassword.getImage().setOnClickListener(v -> edtPassword.showHidePassword());
    }

    public void setIcon(int image, int color) {
        this.image = image;
        this.colorIcon = color;
    }

    public void setCancelListener(String cancelTitle, View.OnClickListener cancelListener) {
        this.cancelTitle = cancelTitle;
        this.cancelListener = cancelListener;
    }

    public void setOkListener(String okTitle, View.OnClickListener okListener) {
        this.okTitle = okTitle;
        this.okListener = okListener;
    }

    public void setDialogTitle(String title) {
        this.title = title;
    }

    public void setColorTitle(int color) {
        this.color = color;
    }


    public String getUserName() {
        return edtMobile.getValueString();
    }

    public void setEditTextField(String mobile, String password){
        this.mobile = mobile;
        this.password = password;
    }
    public String getPasswor() {
        return edtPassword.getValueString();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        dismiss();
    }
}
