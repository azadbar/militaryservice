package com.military.militaryservice.setting;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SwitchCompat;

import com.military.militaryservice.R;
import com.military.militaryservice.addUser.AddUserActivity;
import com.military.militaryservice.baseView.BaseActivity;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.changePassword.ChangePasswordActivity;
import com.military.militaryservice.customView.RoundedLoadingView;
import com.military.militaryservice.fingerPrint.EnableFingerPrintActivity;
import com.military.militaryservice.utils.Constants;
import com.military.militaryservice.utils.PreferencesData;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingActivity extends BaseActivity {

    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvCenterTitle)
    BaseTextView tvCenterTitle;
    @BindView(R.id.share)
    BaseImageView share;
    @BindView(R.id.rlChangePassword)
    BaseRelativeLayout rlAccount;
    @BindView(R.id.photoSwitch)
    SwitchCompat photoSwitch;
    @BindView(R.id.rlBackUpZip)
    BaseRelativeLayout rlBackUpZip;
    @BindView(R.id.roundedLoadingView)
    RoundedLoadingView roundedLoadingView;
    @BindView(R.id.toot)
    BaseRelativeLayout toot;
    @BindView(R.id.tvFingerPrint)
    BaseTextView tvFingerPrint;
    private int REQUEST_FINISH_ACTIVITY = 300;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);

        tvCenterTitle.setText("تنظیمات");

        photoSwitch.setChecked(PreferencesData.getBoolean(this, "isCheckPhoto"));
        photoSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                Toast.makeText(SettingActivity.this, "گرفتن عکس با تکان دادن فعال شد", Toast.LENGTH_SHORT).show();
                PreferencesData.saveBoolean(this, "isCheckPhoto", true);
            } else {
                Toast.makeText(SettingActivity.this, "گرفتن عکس با تکان دادن غیرفعال شد", Toast.LENGTH_SHORT).show();
                PreferencesData.saveBoolean(this, "isCheckPhoto", true);
            }
        });

        if (PreferencesData.getFingerPrint(this)) {
            tvFingerPrint.setText("غیر فعال سازی ورود با اثر انگشت");
        } else {
            tvFingerPrint.setText("فعال سازی اثر انگشت");
        }
    }

    @OnClick({R.id.imgBack, R.id.rlChangePassword, R.id.rlBackUpZip, R.id.rlFingerPrint, R.id.rlAddUser})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                finish();
                break;
            case R.id.rlChangePassword:
                Intent intent = new Intent(this, ChangePasswordActivity.class);
                startActivity(intent);
                break;
            case R.id.rlBackUpZip:
                roundedLoadingView.setVisibility(View.VISIBLE);
                new Handler().postDelayed(() -> {
                    File file = new File(Environment.getExternalStorageDirectory(), "");
                    zipFileAtPath(file.getAbsolutePath() + Constants.SHAHED, file.getAbsolutePath() + "/shahed.zip");
                    runOnUiThread(() -> roundedLoadingView.setVisibility(View.GONE));
                    Toast.makeText(SettingActivity.this, "فایل shahed.zip در مسیر فایل منیجر گوشی ذخیره شد", Toast.LENGTH_SHORT).show();
                }, 3000);

                break;
            case R.id.rlFingerPrint:
                if (PreferencesData.getFingerPrint(this)) {
                    PreferencesData.isFingerPrint(this, false);
                    tvFingerPrint.setText("فعال سازی اثر انگشت");
                } else {
                    Intent intent1 = new Intent(SettingActivity.this, EnableFingerPrintActivity.class);
                    startActivityForResult(intent1, REQUEST_FINISH_ACTIVITY);
                }
                break;
            case R.id.rlAddUser:
                intent = new Intent(SettingActivity.this, AddUserActivity.class);
                startActivity(intent);
                break;
        }
    }


    public boolean zipFileAtPath(String sourcePath, String toLocation) {


        final int BUFFER = 2048;

        File sourceFile = new File(sourcePath);
        try {
            BufferedInputStream origin = null;
            FileOutputStream dest = new FileOutputStream(toLocation);
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(
                    dest));
            if (sourceFile.isDirectory()) {
                zipSubFolder(out, sourceFile, sourceFile.getParent().length());
            } else {
                byte data[] = new byte[BUFFER];
                FileInputStream fi = new FileInputStream(sourcePath);
                origin = new BufferedInputStream(fi, BUFFER);
                ZipEntry entry = new ZipEntry(getLastPathComponent(sourcePath));
                entry.setTime(sourceFile.lastModified()); // to keep modification time after unzipping
                out.putNextEntry(entry);
                int count;
                while ((count = origin.read(data, 0, BUFFER)) != -1) {
                    out.write(data, 0, count);
                }
            }
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;

    }

    /*
     *
     * Zips a subfolder
     *
     */

    private void zipSubFolder(ZipOutputStream out, File folder,
                              int basePathLength) throws IOException {

        final int BUFFER = 2048;

        File[] fileList = folder.listFiles();
        BufferedInputStream origin = null;
        for (File file : fileList) {
            if (file.isDirectory()) {
                zipSubFolder(out, file, basePathLength);
            } else {
                byte data[] = new byte[BUFFER];
                String unmodifiedFilePath = file.getPath();
                String relativePath = unmodifiedFilePath
                        .substring(basePathLength);
                FileInputStream fi = new FileInputStream(unmodifiedFilePath);
                origin = new BufferedInputStream(fi, BUFFER);
                ZipEntry entry = new ZipEntry(relativePath);
                entry.setTime(file.lastModified()); // to keep modification time after unzipping
                out.putNextEntry(entry);
                int count;
                while ((count = origin.read(data, 0, BUFFER)) != -1) {
                    out.write(data, 0, count);
                }
                origin.close();
            }
        }
    }

    /*
     * gets the last path component
     *
     * Example: getLastPathComponent("downloads/example/fileToZip");
     * Result: "fileToZip"
     */
    public String getLastPathComponent(String filePath) {
        String[] segments = filePath.split("/");
        if (segments.length == 0)
            return "";
        String lastPathComponent = segments[segments.length - 1];
        return lastPathComponent;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_FINISH_ACTIVITY) {
                Intent in = new Intent();
                setResult(RESULT_OK, in);
                finish();
            }
        }
    }
}
