package com.military.militaryservice.calendar;

import android.content.Context;
import android.graphics.Typeface;

import com.military.militaryservice.R;


public class Font {
    private static Typeface typefaceOnvan;
    public static Typeface IranSans(Context mContext) {
        if (typefaceOnvan == null) {
            typefaceOnvan = Typeface.createFromAsset(mContext.getAssets(),
                    mContext.getString(R.string.font_path));
        }

        return typefaceOnvan;
    }






}
