package com.military.militaryservice.calendar;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.military.militaryservice.R;
import com.military.militaryservice.databinding.ActivityAddEventBinding;
import com.military.militaryservice.persianCalender.utils.PersianCalendar;
import com.military.militaryservice.utils.Constants;

import java.util.Date;

public class AddEventActivity extends AppCompatActivity {

    ActivityAddEventBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_event);

        binding.navIcon.setOnClickListener(view -> finish());
        PersianCalendar persianCalendar = new PersianCalendar();
        long now = System.currentTimeMillis();
        long day = 1000 * 60 * 60 * 24;

        String[] date = new String[60];

        for (int i = 59; i >=0 ; i--) {

            int offset=i-30;
            persianCalendar.setTimeInMillis(now+ (offset*day));
            if (offset!=0) {
                date[i] = (persianCalendar.getPersianDay() + " " + persianCalendar.getPersianMonthName());
            }else {
                date[i] = ("امروز");
            }

        }

        binding.date.setMinValue(0);
        binding.date.setMaxValue(59);
        binding.date.setTypeface(com.military.militaryservice.calendar.Font.IranSans(this));
        binding.date.setDisplayedValues(date);
        binding.date.setValue(30);
        binding.date.setDisplayedValues(date);

        Date date1=new Date(now);
        binding.hour.setMinValue(0);
        binding.hour.setMaxValue(24);
        binding.hour.setTypeface(com.military.militaryservice.calendar.Font.IranSans(this));
        binding.hour.setValue(date1.getHours());

        binding.min.setMinValue(0);
        binding.min.setMaxValue(59);

        binding.min.setTypeface(Font.IranSans(this));
        binding.min.setValue(date1.getMinutes());


        binding.addIcon.setOnClickListener(view -> {
            startActivityForResult(new Intent(this,SelectAlarmIconDialog.class), Constants.SELECT_ICON);
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data!=null){
            if (resultCode== Constants.SELECT_ICON){
                Glide.with(this).load(data.getIntExtra(Constants.SELECTED_ICON,R.mipmap.add_icon)).into(binding.addIcon);
            }
        }
    }
}
