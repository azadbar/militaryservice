package com.military.militaryservice.calendar.github.praytimes;

public enum PrayTime {
    IMSAK, FAJR, SUNRISE, DHUHR, ASR, SUNSET, MAGHRIB, ISHA, MIDNIGHT
}
