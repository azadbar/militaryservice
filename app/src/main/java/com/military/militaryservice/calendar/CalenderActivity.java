package com.military.militaryservice.calendar;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.military.militaryservice.R;
import com.military.militaryservice.calendar.persiancalendar.view.fragment.CalendarFragment;

public class CalenderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender);


        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame,
                CalendarFragment.newInstance(), CalendarFragment.class.getName()).commit();

    }
}
