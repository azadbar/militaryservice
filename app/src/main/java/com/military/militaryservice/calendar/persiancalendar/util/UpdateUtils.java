package com.military.militaryservice.calendar.persiancalendar.util;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;


import com.google.android.apps.dashclock.api.ExtensionData;
import com.military.militaryservice.R;
import com.military.militaryservice.activity.MainActivity;
import com.military.militaryservice.calendar.calendar_utils.CivilDate;
import com.military.militaryservice.calendar.calendar_utils.DateConverter;
import com.military.militaryservice.calendar.calendar_utils.PersianDate;
import com.military.militaryservice.calendar.github.praytimes.Clock;
import com.military.militaryservice.calendar.persiancalendar.Constants;
import com.military.militaryservice.calendar.persiancalendar.service.ApplicationService;

import java.util.Calendar;
import java.util.Date;


public class UpdateUtils {
    private static final int NOTIFICATION_ID = 1001;
    private static UpdateUtils myInstance;
    private Context context;
    private PersianDate pastDate;

    private ExtensionData mExtensionData;

    private UpdateUtils(Context context) {
        this.context = context;
    }

    public static UpdateUtils getInstance(Context context) {
        if (myInstance == null) {
            myInstance = new UpdateUtils(context);
        }
        return myInstance;
    }

    boolean firstTime = true;

    public void update(boolean updateDate) {
        Log.d("UpdateUtils", "update");
        Utils utils = Utils.getInstance(context);
        utils.changeAppLanguage(context);
        if (firstTime) {
            utils.loadLanguageResource();
            firstTime = false;
        }
        Calendar calendar = utils.makeCalendarFromDate(new Date());
        CivilDate civil = new CivilDate(calendar);
        PersianDate persian = utils.getToday();

        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent launchAppPendingIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        //
        // Widgets
        //
        //
        AppWidgetManager manager = AppWidgetManager.getInstance(context);

        String colorInt = utils.getSelectedWidgetTextColor();
        int color = Color.parseColor(colorInt);



        String text1;
        String text2;
        String text3 = "";
        String weekDayName = utils.getWeekDayName(civil);
        String persianDate = utils.dateToString(persian);
        String civilDate = utils.dateToString(civil);
        String date = persianDate + Constants.PERSIAN_COMMA + " " + civilDate;

        String time = utils.getPersianFormattedClock(calendar);
        boolean enableClock = utils.isWidgetClock();

        if (enableClock) {
            text2 = weekDayName + " " + date;
            text1 = time;
            if (utils.iranTime) {
                text3 = "(" + context.getString(R.string.iran_time) + ")";
            }
        } else {
            text1 = weekDayName;
            text2 = date;
        }


        if (enableClock) {
            text2 = weekDayName + " " + persianDate;
            text1 = time;
        } else {
            text1 = weekDayName;
            text2 = persianDate;
        }

        Clock currentClock =
                new Clock(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));

        String owghat;

        if (pastDate == null || !pastDate.equals(persian) || updateDate) {
            Log.d("UpdateUtils", "change date");
            pastDate = persian;

            utils.loadAlarms();

            owghat = utils.getNextOghatTime(currentClock, true);

            String holidays = utils.getEventsTitle(persian, true);


            String events = utils.getEventsTitle(persian, false);
        } else {
            owghat = utils.getNextOghatTime(currentClock, false);
        }




        //
        // Permanent Notification Bar and DashClock Data Extension Update
        //
        //
        String status = utils.getMonthName(persian);

        String title = utils.getWeekDayName(civil) + Constants.PERSIAN_COMMA + " " +
                utils.dateToString(persian);

        String body = utils.dateToString(civil) + Constants.PERSIAN_COMMA + " "
                + utils.dateToString(DateConverter.civilToIslamic(civil, utils.getIslamicOffset()));

        // Prepend a right-to-left mark character to Android with sane text rendering stack
        // to resolve a bug seems some Samsung devices have with characters with weak direction,
        // digits being at the first of string on
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
            title = Constants.RLM + title;
            body = Constants.RLM + body;
        }

        int icon = utils.getDayIconResource(persian.getDayOfMonth());

        ApplicationService applicationService = ApplicationService.getInstance();
        if (applicationService != null && utils.isNotifyDate()) {
            applicationService.startForeground(
                    NOTIFICATION_ID,
                    new NotificationCompat.Builder(context)
                            .setPriority(NotificationCompat.PRIORITY_LOW)
                            .setOngoing(true)
                            .setSmallIcon(icon)
                            .setWhen(0)
                            .setContentIntent(launchAppPendingIntent)
                            .setContentText(utils.shape(body))
                            .setContentTitle(utils.shape(title))
                            .setColor(0xFF607D8B) // permanent services color
                            .build());
        }

        mExtensionData = new ExtensionData().visible(true).icon(icon)
                .status(utils.shape(status))
                .expandedTitle(utils.shape(title))
                .expandedBody(utils.shape(body)).clickIntent(intent);
    }

    public ExtensionData getExtensionData() {
        return mExtensionData;
    }

}
