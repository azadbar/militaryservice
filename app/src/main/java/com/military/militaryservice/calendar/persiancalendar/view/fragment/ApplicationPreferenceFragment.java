package com.military.militaryservice.calendar.persiancalendar.view.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.military.militaryservice.R;
import com.military.militaryservice.calendar.persiancalendar.Constants;
import com.military.militaryservice.calendar.persiancalendar.util.Utils;
import com.military.militaryservice.calendar.persiancalendar.view.preferences.AthanNumericDialog;
import com.military.militaryservice.calendar.persiancalendar.view.preferences.AthanNumericPreference;
import com.military.militaryservice.calendar.persiancalendar.view.preferences.AthanVolumeDialog;
import com.military.militaryservice.calendar.persiancalendar.view.preferences.AthanVolumePreference;
import com.military.militaryservice.calendar.persiancalendar.view.preferences.GPSLocationDialog;
import com.military.militaryservice.calendar.persiancalendar.view.preferences.GPSLocationPreference;
import com.military.militaryservice.calendar.persiancalendar.view.preferences.LocationPreference;
import com.military.militaryservice.calendar.persiancalendar.view.preferences.LocationPreferenceDialog;
import com.military.militaryservice.calendar.persiancalendar.view.preferences.PrayerSelectDialog;
import com.military.militaryservice.calendar.persiancalendar.view.preferences.PrayerSelectPreference;
import com.military.militaryservice.calendar.persiancalendar.view.preferences.ShapedListDialog;
import com.military.militaryservice.calendar.persiancalendar.view.preferences.ShapedListPreference;


/**
 * Preference activity
 *
 * @author ebraminio
 */
public class ApplicationPreferenceFragment extends PreferenceFragmentCompat {
    private Preference categoryAthan;
    private Utils utils;

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        utils = Utils.getInstance(getContext());
        utils.setActivityTitleAndSubtitle(getActivity(), getString(R.string.setting), "");

        addPreferencesFromResource(R.xml.preferences);

        categoryAthan = findPreference(Constants.PREF_KEY_ATHAN);
        updateAthanPreferencesState();

        LocalBroadcastManager.getInstance(getContext()).registerReceiver(preferenceUpdateReceiver,
                new IntentFilter(Constants.LOCAL_INTENT_UPDATE_PREFERENCE));
    }

    private BroadcastReceiver preferenceUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateAthanPreferencesState();
        }
    };

    @Override
    public void onDestroyView() {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(preferenceUpdateReceiver);
        super.onDestroyView();
    }

    public void updateAthanPreferencesState() {
        boolean locationEmpty = utils.getCoordinate() == null;
        categoryAthan.setEnabled(!locationEmpty);
        if (locationEmpty) {
            categoryAthan.setSummary(R.string.athan_disabled_summary);
        } else {
            categoryAthan.setSummary("");
        }
    }

    @Override
    public void onDisplayPreferenceDialog(Preference preference) {
        DialogFragment fragment = null;
        if (preference instanceof PrayerSelectPreference) {
            fragment = new PrayerSelectDialog();
        } else if (preference instanceof AthanVolumePreference) {
            fragment = new AthanVolumeDialog();
        } else if (preference instanceof LocationPreference) {
            fragment = new LocationPreferenceDialog();
        } else if (preference instanceof AthanNumericPreference) {
            fragment = new AthanNumericDialog();
        } else if (preference instanceof GPSLocationPreference) {
            fragment = new GPSLocationDialog();
        } else if (preference instanceof ShapedListPreference) {
            fragment = new ShapedListDialog();
        } else {
            super.onDisplayPreferenceDialog(preference);
        }

        if (fragment != null) {
            Bundle bundle = new Bundle(1);
            bundle.putString("key", preference.getKey());
            fragment.setArguments(bundle);
            fragment.setTargetFragment(this, 0);
            fragment.show(getChildFragmentManager(), fragment.getClass().getName());
        }
    }
}
