package com.military.militaryservice.calendar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.military.militaryservice.R;
import com.military.militaryservice.databinding.SelectIconDialogBinding;
import com.military.militaryservice.utils.Constants;


public class SelectAlarmIconDialog extends Activity implements View.OnClickListener {



    SelectIconDialogBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this, R.layout.select_icon_dialog);

        binding.bag.setOnClickListener(this);
        binding.bir.setOnClickListener(this);
        binding.card.setOnClickListener(this);
        binding.def.setOnClickListener(this);
        binding.meet.setOnClickListener(this);
        binding.money.setOnClickListener(this);
        binding.phone.setOnClickListener(this);
        binding.travel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent=new Intent();
        switch (view.getId()){
            case R.id.bag:
                intent.putExtra(Constants.SELECTED_ICON,R.mipmap.ico_bag);
                setResult(Constants.SELECT_ICON,intent);
                finish();
                break;
            case R.id.money:
                intent.putExtra(Constants.SELECTED_ICON,R.mipmap.ico_money);
                setResult(Constants.SELECT_ICON,intent);
                finish();
                break;
            case R.id.meet:
                intent.putExtra(Constants.SELECTED_ICON,R.mipmap.ico_meet);
                setResult(Constants.SELECT_ICON,intent);
                finish();
                break;
            case R.id.bir:
                intent.putExtra(Constants.SELECTED_ICON,R.mipmap.ico_bir);
                setResult(Constants.SELECT_ICON,intent);
                finish();
                break;
            case R.id.def:
                intent.putExtra(Constants.SELECTED_ICON,R.mipmap.ico_defult);
                setResult(Constants.SELECT_ICON,intent);
                finish();
                break;
            case R.id.phone:
                intent.putExtra(Constants.SELECTED_ICON,R.mipmap.ico_phone);
                setResult(Constants.SELECT_ICON,intent);
                finish();
                break;
            case R.id.travel:
                intent.putExtra(Constants.SELECTED_ICON,R.mipmap.ico_travel);
                setResult(Constants.SELECT_ICON,intent);
                finish();
                break;
            case R.id.card:
                intent.putExtra(Constants.SELECTED_ICON,R.mipmap.ico_card);
                setResult(Constants.SELECT_ICON,intent);
                finish();
                break;

        }
    }
}
