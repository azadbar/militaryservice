package com.military.militaryservice.calendar.persiancalendar.enums;

public enum SeasonEnum {
    SPRING,
    SUMMER,
    FALL,
    WINTER
}
