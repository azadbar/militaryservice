package com.military.militaryservice.calendar.persiancalendar.enums;

/**
 * Calendars Types
 *
 * @author ebraminio
 */
public enum CalendarTypeEnum {
    SHAMSI, ISLAMIC, GREGORIAN
}
