package com.military.militaryservice.addUser;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseActivity;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.customView.CustomEditText;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.database.User;
import com.military.militaryservice.dialog.CustomDialog;
import com.military.militaryservice.enums.UserAccessEnum;
import com.military.militaryservice.utils.Constants;
import com.military.militaryservice.utils.PreferencesData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

public class AddUserActivity extends BaseActivity {

    @BindView(R.id.tableInvoices)
    TableLayout mTableLayout;
    @BindView(R.id.invoices_layout)
    BaseRelativeLayout invoicesLayout;
    @BindView(R.id.edtMobile)
    CustomEditText edtMobile;
    @BindView(R.id.edtPassword)
    CustomEditText edtPassword;
    @BindView(R.id.rbRead)
    RadioButton rbRead;
    @BindView(R.id.rbReadWrite)
    RadioButton rbReadWrite;
    @BindView(R.id.rbAdmin)
    RadioButton rbAdmin;
    @BindView(R.id.standard_radio_group)
    RadioGroup standardRadioGroup;
    @BindView(R.id.btnAddUser)
    BaseTextView btnAddUser;
    @BindView(R.id.btnCancel)
    BaseTextView btnCancel;
    @BindView(R.id.txtState)
    BaseTextView txtState;
    private UserAccessEnum userAccess;
    private AppDatabase database = AppDatabase.getInMemoryDatabase(this);
    private boolean isEdit;
    private int userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        ButterKnife.bind(this);

        mTableLayout.setStretchAllColumns(true);

        edtMobile.getImage().setOnClickListener(v -> edtMobile.showHidePassword());
        edtPassword.getImage().setOnClickListener(v -> edtPassword.showHidePassword());

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }

        standardRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.rbRead:
                    userAccess = UserAccessEnum.READ;
                    break;
                case R.id.rbReadWrite:
                    userAccess = UserAccessEnum.READ_WRITE;
                    break;
                case R.id.rbAdmin:
                    userAccess = UserAccessEnum.ADMIN;
                    break;
            }
        });
        loadData();

        if (PreferencesData.getChangePassword(this)) {
            edtMobile.setBody(Constants.getUser(this).getUsername());
            edtPassword.setBody(Constants.getUser(this).getPassword());
            setRadioButton(Constants.getUser(this));
            userId = Constants.getUser(this).getId();
            int countAdminUser = database.userDao().getCountAdminUser(UserAccessEnum.ADMIN);
            if (countAdminUser == 1) {
                for (int i = 0; i < standardRadioGroup.getChildCount(); i++) {
                    standardRadioGroup.getChildAt(i).setEnabled(false);
                }
            } else {
                for (int i = 0; i < standardRadioGroup.getChildCount(); i++) {
                    standardRadioGroup.getChildAt(i).setEnabled(true);
                }
            }
            isEdit = true;
            btnAddUser.setText("تایید ویرایش");
            txtState.setText("ویرایش");
        }
    }


    private void loadData() {
        int leftRowMargin = 0;
        int topRowMargin = 0;
        int rightRowMargin = 0;
        int bottomRowMargin = 0;
        int textSize = 0, smallTextSize = 0, mediumTextSize = 0;

        textSize = (int) getResources().getDimension(R.dimen.text_size_1);
        smallTextSize = (int) getResources().getDimension(R.dimen.text_size_2);
        mediumTextSize = (int) getResources().getDimension(R.dimen.text_size_2);

        List<User> users = database.userDao().getUsers();


        int rows = users.size();

        TextView textSpacer = null;

        mTableLayout.removeAllViews();

        // -1 means heading row
        for (int i = -1; i < rows; i++) {
            User row = null;
            if (i > -1)
                row = users.get(i);
            else {
                textSpacer = new TextView(this);
                textSpacer.setText("");

            }
            // data columns
            final BaseTextView tv = new BaseTextView(this);
            tv.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                    TableRow.LayoutParams.WRAP_CONTENT));

            tv.setGravity(Gravity.CENTER);

            tv.setPadding(5, 15, 0, 15);
            if (i == -1) {
                tv.setText("ردیف");
                tv.setBackgroundColor(Color.parseColor("#f0f0f0"));
                tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
            } else {
                tv.setBackgroundColor(Color.parseColor("#f8f8f8"));
                tv.setText(String.valueOf(i + 1));
                tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            }

            final BaseTextView tv2 = new BaseTextView(this);
            if (i == -1) {
                tv2.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.WRAP_CONTENT));
                tv2.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
            } else {
                tv2.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                tv2.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            }

            tv2.setGravity(Gravity.CENTER);

            tv2.setPadding(5, 15, 0, 15);
            if (i == -1) {
                tv2.setText("تاریخ ایجاد");
                tv2.setBackgroundColor(Color.parseColor("#f7f7f7"));
            } else {
                tv2.setBackgroundColor(Color.parseColor("#ffffff"));
                tv2.setTextColor(Color.parseColor("#000000"));
                SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy", Locale.ENGLISH);
                Date date = null;
                try {
                    date = format.parse(row.getDate().toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (date != null) {
                    PersianDate datea = new PersianDate(date);
                    PersianDateFormat pd = new PersianDateFormat("j F y");
                    tv2.setText(pd.format(datea) + "");
                } else {
                    tv2.setText(null);
                }


            }


            final LinearLayout layCustomer = new LinearLayout(this);
            layCustomer.setOrientation(LinearLayout.VERTICAL);
            layCustomer.setPadding(0, 10, 0, 10);
            layCustomer.setBackgroundColor(Color.parseColor("#f8f8f8"));

            final BaseTextView tv3 = new BaseTextView(this);
            if (i == -1) {
                tv3.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                tv3.setPadding(5, 5, 0, 5);
                tv3.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
            } else {
                tv3.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                tv3.setPadding(5, 0, 0, 5);
                tv3.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            }

            tv3.setGravity(Gravity.CENTER);
            if (i == -1) {
                tv3.setText("نام کاربری");
                tv3.setBackgroundColor(Color.parseColor("#f0f0f0"));
            } else {
                tv3.setBackgroundColor(Color.parseColor("#f8f8f8"));
                tv3.setTextColor(Color.parseColor("#000000"));
                tv3.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
                tv3.setText(row.getUsername());
            }
            layCustomer.addView(tv3);


//            final LinearLayout layPassword = new LinearLayout(this);
//            layPassword.setOrientation(LinearLayout.VERTICAL);
//            layPassword.setPadding(0, 10, 0, 10);
//            layPassword.setBackgroundColor(Color.parseColor("#f8f8f8"));
//
//            final TextView tv5 = new TextView(this);
//            if (i == -1) {
//                tv5.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
//                        TableRow.LayoutParams.MATCH_PARENT));
//                tv5.setPadding(5, 5, 0, 5);
//                tv5.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
//            } else {
//                tv5.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
//                        TableRow.LayoutParams.MATCH_PARENT));
//                tv5.setPadding(5, 0, 0, 5);
//                tv5.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
//            }
//
//            tv5.setGravity(Gravity.CENTER);
//            if (i == -1) {
//                tv5.setText("رمز عبور");
//                tv5.setBackgroundColor(Color.parseColor("#f0f0f0"));
//            } else {
//                tv5.setBackgroundColor(Color.parseColor("#f8f8f8"));
//                tv5.setTextColor(Color.parseColor("#000000"));
//                tv5.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
//                tv5.setText(row.getUsername());
//            }
//            layCustomer.addView(tv3);
//
//            if (i > -1) {
//                final TextView tv3b = new TextView(this);
//                tv3b.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
//                        TableRow.LayoutParams.WRAP_CONTENT));
//
//                tv3b.setGravity(Gravity.RIGHT);
//                tv3b.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
//                tv3b.setPadding(5, 1, 0, 5);
//                tv3b.setTextColor(Color.parseColor("#aaaaaa"));
//                tv3b.setBackgroundColor(Color.parseColor("#f8f8f8"));
//                tv3b.setText(row.customerAddress);
//                layCustomer.addView(tv3b);
//            }

            final LinearLayout layAmounts = new LinearLayout(this);
            layAmounts.setOrientation(LinearLayout.VERTICAL);
            layAmounts.setGravity(Gravity.RIGHT);
            layAmounts.setPadding(0, 10, 0, 10);
            layAmounts.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));


            final BaseTextView tv4 = new BaseTextView(this);
            if (i == -1) {
                tv4.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.MATCH_PARENT));
                tv4.setPadding(5, 5, 1, 5);
                layAmounts.setBackgroundColor(Color.parseColor("#f7f7f7"));
            } else {
                tv4.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.WRAP_CONTENT));
                tv4.setPadding(5, 0, 1, 5);
                layAmounts.setBackgroundColor(Color.parseColor("#ffffff"));
            }

            tv4.setGravity(Gravity.CENTER);

            if (i == -1) {
                tv4.setText("سطح دسترسی");
                tv4.setBackgroundColor(Color.parseColor("#f7f7f7"));
                tv4.setTextSize(TypedValue.COMPLEX_UNIT_PX, smallTextSize);
            } else {
                tv4.setBackgroundColor(Color.parseColor("#ffffff"));
                tv4.setTextColor(Color.parseColor("#000000"));
                tv4.setText(row.getUserAccess().getAccess());
                tv4.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            }

            layAmounts.addView(tv4);

//
//            if (i > -1) {
//                final TextView tv4b = new TextView(this);
//                tv4b.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
//                        TableRow.LayoutParams.WRAP_CONTENT));
//
//                tv4b.setGravity(Gravity.RIGHT);
//                tv4b.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
//                tv4b.setPadding(2, 2, 1, 5);
//                tv4b.setTextColor(Color.parseColor("#00afff"));
//                tv4b.setBackgroundColor(Color.parseColor("#ffffff"));
//
//                String due = "";
//                if (row.amountDue.compareTo(new BigDecimal(0.01)) == 1) {
//                    due = "Due:" + decimalFormat.format(row.amountDue);
//                    due = due.trim();
//                }
//                tv4b.setText(due);
//                layAmounts.addView(tv4b);
//            }


            // add table row
            final TableRow tr = new TableRow(this);
            tr.setId(i + 1);
            TableLayout.LayoutParams trParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT);
            trParams.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
            tr.setPadding(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
            tr.setLayoutParams(trParams);


            tr.addView(tv);
            tr.addView(tv2);
            tr.addView(layCustomer);
            tr.addView(layAmounts);

            if (i > -1) {

                User finalRow = row;
                tr.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        TableRow tr = (TableRow) v;
                        //do whatever action is needed
                        userId = finalRow.getId();
                        edtMobile.setBody(finalRow.getUsername());
                        edtPassword.setBody(finalRow.getPassword());
                        setRadioButton(finalRow);
                        if (finalRow.getUserAccess() == UserAccessEnum.ADMIN) {
                            if (getUserAdmin(users) == 1) {
                                for (int i = 0; i < standardRadioGroup.getChildCount(); i++) {
                                    standardRadioGroup.getChildAt(i).setEnabled(false);
                                }
                            }
                        } else {
                            for (int i = 0; i < standardRadioGroup.getChildCount(); i++) {
                                standardRadioGroup.getChildAt(i).setEnabled(true);
                            }
                        }
                        isEdit = true;
                        btnAddUser.setText("تایید ویرایش");
                        txtState.setText("ویرایش");


                    }
                });
                tr.setOnLongClickListener(v -> {
                    if (finalRow.getUserAccess() == UserAccessEnum.ADMIN) {
                        if (getUserAdmin(users) == 1) {
                            Toast toast = Toast.makeText(AddUserActivity.this,
                                    "امکان حذف برای کاربر ادمین وجود ندارد", Toast.LENGTH_SHORT);
                            View view = toast.getView();
                            view.getBackground().setColorFilter(getResources().getColor(R.color.redColor), PorterDuff.Mode.SRC_IN);
                            toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 350);
                            toast.show();
                        } else {
                            deleteUserDialog(finalRow);
                        }
                    } else {
                        deleteUserDialog(finalRow);
                    }
                    return false;
                });


            }
            mTableLayout.addView(tr, trParams);

            if (i > -1) {

                // add separator row
                final TableRow trSep = new TableRow(this);
                TableLayout.LayoutParams trParamsSep = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                        TableLayout.LayoutParams.WRAP_CONTENT);
                trParamsSep.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);

                trSep.setLayoutParams(trParamsSep);
                TextView tvSep = new TextView(this);
                TableRow.LayoutParams tvSepLay = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.WRAP_CONTENT);
                tvSepLay.span = 4;
                tvSep.setLayoutParams(tvSepLay);
                tvSep.setBackgroundColor(Color.parseColor("#d9d9d9"));
                tvSep.setHeight(1);

                trSep.addView(tvSep);
                mTableLayout.addView(trSep, trParamsSep);
            }


        }
    }

    private void deleteUserDialog(User finalRow) {
        CustomDialog dialog = new CustomDialog(AddUserActivity.this);
        dialog.setOkListener(getResources().getString(R.string.ok), view1 -> {
            dialog.dismiss();
            database.userDao().delete(finalRow);
            clearView();
        });
        dialog.setCancelListener(getResources().getString(R.string.cancel), view12 -> dialog.dismiss());
        dialog.setDialogTitle("آیا برای حذف مطمئن هستید؟");
        dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
        dialog.setIcon(R.drawable.ic_delete, getResources().getColor(R.color.redColor));
        dialog.show();
    }

    private int getUserAdmin(List<User> users) {
        int userAdmin = 0;
        for (User u : users) {
            if (u.getUserAccess() == UserAccessEnum.ADMIN) {
                userAdmin++;
            }
        }
        return userAdmin;
    }

    private void setRadioButton(User finalRow) {
        switch (finalRow.getUserAccess()) {
            case READ:
                rbRead.setChecked(true);
                break;
            case READ_WRITE:
                rbReadWrite.setChecked(true);
                break;
            case ADMIN:
                rbAdmin.setChecked(true);
                break;
        }
        userAccess = finalRow.getUserAccess();
    }

    @OnClick({R.id.btnAddUser, R.id.btnCancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnAddUser:
                if (isValidData()) {
                    User user = new User();
                    user.setUsername(edtMobile.getValueString());
                    user.setPassword(edtPassword.getValueString());
                    user.setUserAccess(userAccess);
                    user.setDate(new Date());
                    if (isEdit) {
                        if (!TextUtils.equals(user.getPassword(),Constants.getUser(this).getPassword())){
                            database.userDao().update(user.getPassword(), user.getUsername(), user.getUserAccess(), user.getDate(), userId);
                            isEdit = false;
                            PreferencesData.isChangePassword(this, false);
                            Toast.makeText(this, "ویرایش با موفقیت انجام شد", Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(this, "لطفا رمز عبور را تغییر دهید", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        database.userDao().insert(user);
                    }
                }
                clearView();
                Constants.hideKeyboard(this);
                break;
            case R.id.btnCancel:
                if (edtMobile.getValueString() != null || edtPassword.getValueString() != null) {
                    clearView();
                }else {
                    finish();
                }
                break;
        }

    }


    private void clearView() {
        loadData();
        isEdit = false;
        edtMobile.setBody(null);
        edtPassword.setBody(null);
        userAccess = null;
        standardRadioGroup.clearCheck();
        btnAddUser.setText("ثبت");
        txtState.setText("ثبت");
        for (int i = 0; i < standardRadioGroup.getChildCount(); i++) {
            standardRadioGroup.getChildAt(i).setEnabled(true);
        }
    }

    private boolean isValidData() {

        database = AppDatabase.getInMemoryDatabase(this);
        List<User> users = database.userDao().getUsers();
        ArrayList<String> userName = new ArrayList<>();
        for (int i = 0; i < users.size(); i++) {
            userName.add(users.get(i).getUsername());
        }

        ArrayList<String> errorMsgList = new ArrayList<>();

        if (!isEdit) {
            if (userName.contains(edtMobile.getValueString())) {
                String message = "نام کاربری با این مشخصات ثبت شده است";
                errorMsgList.add(message);
            }
        }


        if (edtMobile.getValueString() == null) {
            errorMsgList.add("نام کاربری را وارد کنید");
        }

        if (edtPassword.getValueString() == null) {
            errorMsgList.add("رمز عبور را وارد کنید");
        }

        if (userAccess == null) {
            String message = "لطفا دسترسی کاربر را مشخص کنید";
            errorMsgList.add(message);
        }

        if (errorMsgList.size() > 0) {
            showInfoDialog(getString(R.string.fill_following), errorMsgList);
            return false;
        }
        return true;
    }

}
