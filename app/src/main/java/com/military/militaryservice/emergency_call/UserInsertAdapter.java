package com.military.militaryservice.emergency_call;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseCardView;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.database.Contact;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserInsertAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final int maxImageCount;

    @BindView(R.id.cvRegisterWithoutPhotos)
    BaseCardView cvRegisterWithoutPhotos;


    private Context context;
    private ArrayList<Contact> list;
    private final OnItemClickListener listener;
    private final DeleteOnItemClickListener deleteOnItemClickListener;


    public UserInsertAdapter(Context context, ArrayList<Contact> list, int maxImageCount, OnItemClickListener listener, DeleteOnItemClickListener deleteOnItemClickListener) {
        this.list = list;
        this.listener = listener;
        this.maxImageCount = maxImageCount;
        this.context = context;
        this.deleteOnItemClickListener = deleteOnItemClickListener;
    }


    @Override
    public int getItemViewType(int position) {
        if (list.size() == maxImageCount) {
            return 2;
        }
        if (position == 0) {
            return 1;
        }
        return 2;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1:
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_add_user, parent, false);
//                itemView.getLayoutParams().height = (int) (cellWidth / Constants.imageRatio);
                return new addImageViewHolder(itemView);
            default:
                View itemView1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false);
//                itemView1.getLayoutParams().height = (int) (cellWidth / Constants.imageRatio);
                return new ViewHolderImageList(itemView1);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        switch (holder.getItemViewType()) {
            case 1:
                addImageViewHolder viewHolder0 = (addImageViewHolder) holder;
                ((addImageViewHolder) holder).bind(position, listener);
                break;

            default:
                ViewHolderImageList viewHolder2 = (ViewHolderImageList) holder;


                Contact contact;
                if (list.size() == maxImageCount) {
                    contact = list.get(position);
                } else {
                    contact = list.get(position - 1);
                }
                ((ViewHolderImageList) holder).bind(position, contact, deleteOnItemClickListener);
                viewHolder2.userName.setText(contact.getName());
//
//                Glide.with(context).load(imagPath)
//                        .placeholder(R.drawable.placeholder)
//                        .centerCrop()
//                        .skipMemoryCache(true)
//                        .diskCacheStrategy(DiskCacheStrategy.NONE)
//                        .override(200, 200)
//                        .into(((ViewHolderImageList) holder).imgAdd);

                break;
        }

    }


    @Override
    public int getItemCount() {
        if (list.size() < maxImageCount) {
            return list.size() + 1;
        }
        return list.size();
    }


    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public interface DeleteOnItemClickListener {
        void onItemClickDelete(int position, Contact contact);
    }

    static class addImageViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgAdd)
        BaseImageView imgAdd;
        @BindView(R.id.text)
        BaseTextView text;
        @BindView(R.id.rlAddImage)
        BaseRelativeLayout rlAddImage;

        addImageViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(position));
        }

    }

    static class ViewHolderImageList extends RecyclerView.ViewHolder {

        @BindView(R.id.userName)
        BaseTextView userName;
        @BindView(R.id.imgDelete)
        BaseImageView imgDelete;


        ViewHolderImageList(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, Contact contact, final DeleteOnItemClickListener listener) {
            imgDelete.setOnClickListener(v -> listener.onItemClickDelete(position, contact));
        }
    }

}
