package com.military.militaryservice.emergency_call;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.github.rubensousa.bottomsheetbuilder.BottomSheetBuilder;
import com.github.rubensousa.bottomsheetbuilder.BottomSheetMenuDialog;
import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.customView.CustomEditText;
import com.military.militaryservice.customView.CustomMultiLineEditText;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.database.Contact;
import com.military.militaryservice.dialog.InfoListDialog;
import com.military.militaryservice.utils.Constants;
import com.military.militaryservice.utils.EqualSpacingItemDecoration;
import com.military.militaryservice.utils.PermissionHandler;
import com.military.militaryservice.utils.PreferencesData;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

public class AddContentDialog extends DialogFragment implements UserInsertAdapter.OnItemClickListener, UserInsertAdapter.DeleteOnItemClickListener, ShowUserDialog.onPassUserListener {

    @BindView(R.id.recordVoice)
    SwitchCompat recordVoice;
    @BindView(R.id.edtDescription)
    CustomMultiLineEditText edtDescription;
    @BindView(R.id.edtTellPhone)
    CustomEditText edtTellPhone;
    @BindView(R.id.switchDefault)
    SwitchCompat switchDefault;
    @BindView(R.id.imgCheckPhone)
    BaseImageView imgCheckPhone;
    @BindView(R.id.imgCheckTellPhone)
    BaseImageView imgCheckTellPhone;
    @BindView(R.id.rvAddUser)
    RecyclerView rvAddUser;
    private int REQUEST_CODE_PERMISSION = 2;
    @BindView(R.id.profile)
    CircleImageView profile;
    @BindView(R.id.edtName)
    CustomEditText edtName;
    @BindView(R.id.edtMobile)
    CustomEditText edtMobile;
    @BindView(R.id.btnOk)
    BaseTextView btnOk;
    @BindView(R.id.btnCancel)
    BaseTextView btnCancel;
    @BindView(R.id.rlSelectContact)
    BaseImageView rlSelectContact;

    private Uri image;
    private String cNumber;
    private boolean isEdit;
    private Contact contact;
    private boolean isDefaultContact = false;
    private boolean isChecked;
    private boolean isDefaultContactTellPhone;
    private AppDatabase database;
    private boolean isDefaultCount;
    private UserInsertAdapter adapter;
    private int maxUserCount = 10;
    private ArrayList<Contact> userList = new ArrayList<>();
//    private AddMessageDialog.updateListActivtyListener listener;

//    public void setListener(AddMessageDialog.updateListActivtyListener listener) {
//        this.listener = listener;
//    }

    public AddContentDialog() {

    }

    @Override
    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
        if (getActivity() != null && window != null && getContext() != null) {
            WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {
                Point size = Constants.getScreenSize(windowManager);
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                int width = (int) Math.min(size.x * 0.90, getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_width));
//                int height = (int) Math.min(size.y * 0.65, getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_fragment_height));
                window.setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
                window.setGravity(Gravity.CENTER);
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View v = inflater.inflate(R.layout.activity_add_content, container, false);
        ButterKnife.bind(this, v);
        setCancelable(false);
        Bundle bundle = getArguments();
        if (bundle != null) {
            isEdit = bundle.getBoolean("isEdit");
            contact = (Contact) bundle.get("contact");
        }

        if (isEdit && contact != null) {
//            tvCenterTitle.setText("ویرایش مخاطب");
            btnOk.setText("ثبت");
            edtName.setBody(contact.getName());
            edtMobile.setBody(contact.getMobile());
            edtTellPhone.setBody(contact.getTellPhone());
            edtDescription.setBody(contact.getMessage());
            isChecked = contact.isDefault();
            if (isChecked) {
                switchDefault.setChecked(contact.isDefault());
                imgCheckPhone.setVisibility(View.VISIBLE);
                imgCheckTellPhone.setVisibility(View.VISIBLE);
            }
            if (!TextUtils.isEmpty(contact.getMobile())&&TextUtils.equals(contact.getMobile(),contact.getDefaultPhone())) {
                isDefaultContact = true;
                phoneCheck();
            }

            if (!TextUtils.isEmpty(contact.getTellPhone())&& TextUtils.equals(contact.getTellPhone(),contact.getDefaultPhone())) {
                isDefaultContactTellPhone = true;
                tellPhoneCheck();
            }
            if (contact.getImage() != null) {
                image = Uri.parse(contact.getImage());
                Glide.with(getContext()).load(contact.getImage()).into(profile);

            }
            userList.addAll(contact.getSendMessageist());
        } else {
            btnOk.setText("ثبت");
//            tvCenterTitle.setText("ثبت");
        }

        recordVoice.setChecked(PreferencesData.getBoolean(getContext(), "isCheckPhoto"));
        recordVoice.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                Toast.makeText(getContext(), "ضبط صدا فعال شد", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), "ضبط صدا غیرفعال شد", Toast.LENGTH_SHORT).show();
            }

        });

//        switchDefault.setChecked(PreferencesData.getBoolean(this, "isCheckPhoto"));

//        if (!isDefaultCount) {
        switchDefault.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
//                Toast.makeText(getContext(), "مخاطب به عنوان پیش فرض فعال شد", Toast.LENGTH_SHORT).show();
                imgCheckPhone.setVisibility(View.VISIBLE);
                imgCheckTellPhone.setVisibility(View.VISIBLE);
                database.contactDao().updateDefaultField(false);

            } else {
//                Toast.makeText(getContext(), "مخاطب به عنوان پیش فرض غیرفعال شد", Toast.LENGTH_SHORT).show();
                imgCheckPhone.setVisibility(View.GONE);
                imgCheckTellPhone.setVisibility(View.GONE);
            }
            this.isChecked = isChecked;
        });
//        }
//        else {
//            switchDefault.setEnabled(false);
//
//        }
        isDefaultCount = false;
        database = AppDatabase.getInMemoryDatabase(getContext());
        for (Contact c : database.contactDao().getContactList()) {
            if (c.isDefault()) {
                isDefaultCount = true;
                break;
            }
        }

        setUserContact();
        return v;
    }

    private void setUserContact() {
        int coulumCount = getResources().getInteger(R.integer.coloum_count_register_estate);
        int offset = getResources().getDimensionPixelSize(R.dimen.coulem_offset_recycle_view_in_register_estate);
        int cellWidth = getResources().getDimensionPixelSize(R.dimen.defualt_width_image_image_recycle_view_register_estate);
        if (getContext() != null) {

            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {

                int screenWidth = Constants.getScreenSize(windowManager).x;
                cellWidth = (int) ((screenWidth - (coulumCount + 1) * (offset * 2)) / (double) (coulumCount));
            }
        }
        rvAddUser.setHasFixedSize(true);
        adapter = new UserInsertAdapter(getActivity(), userList, maxUserCount, this, this);
        LinearLayoutManager layoutManager = new GridLayoutManager(getActivity(), 1, RecyclerView.HORIZONTAL, false);
        rvAddUser.setLayoutManager(layoutManager);
//        rvAddUser.addItemDecoration(new EqualSpacingItemDecoration(offset, EqualSpacingItemDecoration.GRID));
        rvAddUser.setAdapter(adapter);
    }

    @OnClick({R.id.profile, R.id.btnOk, R.id.btnCancel, R.id.rlSelectContact, R.id.imgCheckPhone, R.id.imgCheckTellPhone})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.profile:
                if (PermissionHandler.hasAllPermissions(getActivity())) {
                    showDialogForImageSelection();
                } else {
                    PermissionHandler.requestPermissions(this, REQUEST_CODE_PERMISSION);
                }
                break;
            case R.id.btnOk:
                if (isValidData()) {
                    AppDatabase database = AppDatabase.getInMemoryDatabase(getContext());
                    Contact contact = new Contact();
                    contact.setName(edtName.getValueString());
                    contact.setMobile(edtMobile.getValueString());
                    contact.setMessage(edtDescription.getValueString());
                    contact.setTellPhone(edtTellPhone.getValueString());

                    contact.setDefault(isChecked);
                    if (isDefaultContact)
                        contact.setDefaultPhone(edtMobile.getValueString());
                    if (isDefaultContactTellPhone)
                        contact.setDefaultPhone(edtTellPhone.getValueString());
                    contact.setDate(new Date());
                    if (image != null && !TextUtils.isEmpty(image.toString()))
                        contact.setImage(image.toString());
                    contact.setSendMessageist(userList);
                    if (isEdit) {
//                        database.contactDao().updateObject(edtName.getValueString(), edtMobile.getValueString(), image.toString(), contact.getId());
                        contact.setId(this.contact.getId());
                        database.contactDao().updateTour(contact);
                        Toast.makeText(getActivity(), "مخاطب ویرایش شد", Toast.LENGTH_SHORT).show();
//                        listener.onUpdate();
                    } else {
                        database.contactDao().insertContact(contact);
                        Toast.makeText(getContext(), "مخاطب اضافه شد", Toast.LENGTH_SHORT).show();
                    }

                    Intent intent1 = new Intent(getContext(), AddContentDialog.class);
                    if (getTargetFragment() != null) {
                        getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, intent1);
                    }
                    dismiss();
                }
                break;

            case R.id.btnCancel:
                dismiss();
                break;
            case R.id.rlSelectContact:
                if (PermissionHandler.hasAllPermissions(getContext())) {
                    Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    startActivityForResult(intent, 1);
                } else {
                    PermissionHandler.requestPermissions(this, REQUEST_CODE_PERMISSION);
                }
                break;
            case R.id.imgCheckPhone:
                isDefaultContact = !isDefaultContact;
                isDefaultContactTellPhone = false;
                phoneCheck();
                break;
            case R.id.imgCheckTellPhone:
                isDefaultContactTellPhone = !isDefaultContactTellPhone;
                isDefaultContact = false;
                tellPhoneCheck();
                break;

        }
    }

    private void phoneCheck() {
        if (isDefaultContact) {
            imgCheckPhone.setImageResource(R.drawable.ic_check_box_primary);
            imgCheckPhone.setColorFilter(getResources().getColor(R.color.redColor), PorterDuff.Mode.SRC_ATOP);
            imgCheckTellPhone.setImageResource(R.drawable.ic_check_box_outline_primary);
            imgCheckTellPhone.setColorFilter(getResources().getColor(R.color.secondryTextColor), PorterDuff.Mode.SRC_ATOP);
        } else {
            imgCheckPhone.setImageResource(R.drawable.ic_check_box_outline_primary);
            imgCheckPhone.setColorFilter(getResources().getColor(R.color.secondryTextColor), PorterDuff.Mode.SRC_ATOP);
            imgCheckTellPhone.setImageResource(R.drawable.ic_check_box_outline_primary);
            imgCheckTellPhone.setColorFilter(getResources().getColor(R.color.secondryTextColor), PorterDuff.Mode.SRC_ATOP);
        }
    }

    private void tellPhoneCheck() {
        if (isDefaultContactTellPhone) {
            imgCheckTellPhone.setImageResource(R.drawable.ic_check_box_primary);
            imgCheckTellPhone.setColorFilter(getResources().getColor(R.color.redColor), PorterDuff.Mode.SRC_ATOP);
            imgCheckPhone.setImageResource(R.drawable.ic_check_box_outline_primary);
            imgCheckPhone.setColorFilter(getResources().getColor(R.color.secondryTextColor), PorterDuff.Mode.SRC_ATOP);
        } else {
            imgCheckTellPhone.setImageResource(R.drawable.ic_check_box_outline_primary);
            imgCheckTellPhone.setColorFilter(getResources().getColor(R.color.secondryTextColor), PorterDuff.Mode.SRC_ATOP);
            imgCheckPhone.setImageResource(R.drawable.ic_check_box_outline_primary);
            imgCheckPhone.setColorFilter(getResources().getColor(R.color.secondryTextColor), PorterDuff.Mode.SRC_ATOP);
        }
    }

    private void showDialogForImageSelection() {
        BottomSheetMenuDialog dialog = new BottomSheetBuilder(getActivity(), R.style.AppTheme_BottomSheetDialog)
                .setMode(BottomSheetBuilder.MODE_LIST)
                .setMenu(R.menu.menu_bottom_sheet)
                .setItemClickListener(item -> {
                    switch (item.getItemId()) {
                        case R.id.btnTakePhoto:
                            openCameraTake();
                            break;
                        case R.id.btnImageGallery:
                            showDialogImage();
                            break;
                    }
                })
                .createDialog();
        dialog.show();
    }

    private void openCameraTake() {
        Intent photoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(photoIntent, Constants.REQUEST_IMAGE_CAPTURE);
    }

    private void showDialogImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), Constants.RESULT_LOAD_IMAGE);
    }

    private boolean isValidData() {
        ArrayList<String> errorMsgList = new ArrayList<>();

        if (!TextUtils.isEmpty(edtName.getError())) {
            errorMsgList.add(edtName.getError());
        }

        if (!TextUtils.isEmpty(edtMobile.getError())) {
            errorMsgList.add(edtMobile.getError());
        }

//        if (isChecked) {
////            if (isDefaultContact || isDefaultContactTellPhone) {
////
////            } else {
////                Toast.makeText(getContext(), "لطفا شماره پیش فرض انتخاب کنید", Toast.LENGTH_SHORT).show();
////                return false;
////            }
////        }
        if (!isDefaultCount) {
            if (isChecked) {
                if (isDefaultContact || isDefaultContactTellPhone) {
                } else {
                    errorMsgList.add("لطفا شماره پیش فرض انتخاب کنید");
                }
            } else {
                errorMsgList.add("لطفا این مخاطب را بعنوان پیش فرض انتخاب کنید");
            }
        }

        if (errorMsgList.size() > 0) {
            showInfoDialog("به موارد زیر توجه کنید", errorMsgList);
            return false;
        }
        return true;
    }


    public void showInfoDialog(String title, ArrayList<String> errorMsgList) {
        InfoListDialog infoListDialog = new InfoListDialog(getContext());
        infoListDialog.errorMsg = errorMsgList;
        infoListDialog.title = title;
        infoListDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.RESULT_LOAD_IMAGE && data != null) {
                image = bitmapImage(data);
                Glide.with(getContext()).load(data.getData()).into(profile);
            } else if (requestCode == Constants.REQUEST_IMAGE_CAPTURE) {
                image = bitmapImage(getImageUri((Bitmap) data.getExtras().get("data")));
                Glide.with(getContext()).load(image.toString()).into(profile);
            }
            if (requestCode == 1) {
                Uri contactData = data.getData();
                Cursor c = getActivity().managedQuery(contactData, null, null, null, null);
                if (c.moveToFirst()) {


                    String id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

                    String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                    if (c.getString(c.getColumnIndex(ContactsContract.Contacts.PHOTO_URI)) != null)
                        image = Uri.parse(c.getString(c.getColumnIndex(ContactsContract.Contacts.PHOTO_URI)));
                    else image = null;
                    if (image != null)
                        Glide.with(getContext()).load(image).into(profile);
                    else Glide.with(getContext()).load(R.drawable.ic_profile).into(profile);

                    if (hasPhone.equalsIgnoreCase("1")) {
                        Cursor phones = getActivity().getContentResolver().query(
                                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                                null, null);
                        phones.moveToFirst();
                        cNumber = phones.getString(phones.getColumnIndex("data1"));
                        System.out.println("number is:" + cNumber);
                    }
                    String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    edtName.setBody(name);
                    edtMobile.setBody(cNumber != null ? cNumber.replace(" ", "").replace("+98", "0") : "");

                }
            }
        }
    }


    public Uri getImageUri(Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(getContext().getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private Uri bitmapImage(Intent data) {
        final Uri resultUri = data.getData();
//        InputStream imageStream = null;
        File myImg = null;
        try {
            Bitmap selectedBitmap = BitmapFactory.decodeStream(
                    getActivity().getContentResolver().openInputStream(resultUri));

            File dir = Constants.storage_Dir_Image;
            if (!dir.exists()) {
                dir.mkdirs();
            }

            myImg = new File(dir, "temp" + System.currentTimeMillis() + ".png");
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            OutputStream stream = null;
            stream = new FileOutputStream(myImg);
            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            stream.flush();
            stream.close();


//            selectedBitmap.compress(Bitmap.CompressFormat.PNG, 90, bos);
//            byte[] bitmapdata = bos.toByteArray();
//            FileOutputStream fos = new FileOutputStream(myImg);
//            fos.write(bitmapdata);
//            fos.flush();
//            fos.close();

//            imageStream = getActivity().getContentResolver().openInputStream(resultUri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
        return Uri.parse(myImg.getAbsolutePath());
    }

    private Uri bitmapImage(Uri data) {

//        InputStream imageStream = null;
        File myImg = null;
        try {
            Bitmap selectedBitmap = BitmapFactory.decodeStream(
                    getActivity().getContentResolver().openInputStream(data));

            File dir = Constants.storage_Dir_Image;
            if (!dir.exists()) {
                dir.mkdirs();
            }

            myImg = new File(dir, "temp" + System.currentTimeMillis() + ".png");
            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            OutputStream stream = null;
            stream = new FileOutputStream(myImg);
            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            stream.flush();
            stream.close();


//            selectedBitmap.compress(Bitmap.CompressFormat.PNG, 90, bos);
//            byte[] bitmapdata = bos.toByteArray();
//            FileOutputStream fos = new FileOutputStream(myImg);
//            fos.write(bitmapdata);
//            fos.flush();
//            fos.close();

//            imageStream = getActivity().getContentResolver().openInputStream(resultUri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
        return Uri.parse(myImg.getAbsolutePath());
    }


    @Override
    public void onItemClick(int position) {
        ShowUserDialog dialog = new ShowUserDialog(getContext(), this);
        dialog.setDialogTitle("لطفا از شماره های زیر انتخاب کنید");
        dialog.show();
    }

    @Override
    public void onItemClickDelete(int position, Contact contact) {
        if (userList.size() == maxUserCount) {
            userList.remove(position);
        } else {
            userList.remove(position - 1);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onPassUser(int position, Contact contact) {

//        if (userList.contains(contact)) {
            userList.add(contact);
            adapter.notifyDataSetChanged();
//        }
//        else {
//            Toast.makeText(getContext(), "این شماره قبلا اضافه شده است", Toast.LENGTH_SHORT).show();
//        }
    }
}


