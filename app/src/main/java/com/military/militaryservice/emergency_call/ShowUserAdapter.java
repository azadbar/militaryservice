package com.military.militaryservice.emergency_call;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.database.Contact;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class ShowUserAdapter extends RecyclerView.Adapter<ShowUserAdapter.ViewHolder> {


    private ArrayList<Contact> list;
    private OnItemClickListener listener;


    public ShowUserAdapter(ArrayList<Contact> list, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_show_user_info, parent, false);

        return new ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Contact object = list.get(position);
        if (object != null){
            holder.tvTitle.setText(object.getName());
        }

        holder.bind(position,object,listener);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.root)
        BaseRelativeLayout root;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, Contact object, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClickMax(position,object));
        }

    }

    public interface OnItemClickListener {
        void onItemClickMax(int position, Contact contact);
    }

}
