package com.military.militaryservice.emergency_call;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseFragment;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.baseView.BaseToolbar;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.database.Contact;
import com.military.militaryservice.dialog.CustomDialog;
import com.military.militaryservice.enums.UserAccessEnum;
import com.military.militaryservice.sendMessage.AddMessageDialog;
import com.military.militaryservice.utils.Constants;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_OK;

public class AddContactFragment extends BaseFragment implements ContactAdapter.OnItemClickListener, AddMessageDialog.updateListActivtyListener {


    private static final int ADD_CONTACT = 10;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvCenterTitle)
    BaseTextView tvCenterTitle;
    @BindView(R.id.share)
    BaseImageView share;
    @BindView(R.id.toolbar)
    BaseToolbar toolbar;
    @BindView(R.id.rvContect)
    RecyclerView rvContect;
    @BindView(R.id.btnAddContact)
    FloatingActionButton btnAddContact;
    @BindView(R.id.imgView)
    BaseImageView imgView;
    @BindView(R.id.tvEmptyView)
    BaseTextView tvEmptyView;
    @BindView(R.id.rootEmptyView)
    BaseRelativeLayout rootEmptyView;
    private Unbinder unbinder;
    private ArrayList<Contact> list = new ArrayList<>();
    private ContactAdapter adapter;
    private AppDatabase database;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_emergancy, container, false);

        unbinder = ButterKnife.bind(this, view);
        tvCenterTitle.setText(getResources().getString(R.string.emergancy_call));


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        database = AppDatabase.getInMemoryDatabase(getActivity());
        ArrayList<Contact> contactList = (ArrayList<Contact>) database.contactDao().getContactList();
        list.clear();
        for (Contact c : contactList) {
            if (c.isDefault()) {
                list.add(0, c);
            } else {
                list.add(c);
            }
        }
        setDataAdapter();
        if (list.size() == 0) {
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText("هیچ مخاطبی یافت نشد");
        } else {
            rootEmptyView.setVisibility(View.GONE);
        }
    }

    private void setDataAdapter() {
        if (adapter == null) {
            adapter = new ContactAdapter(list, this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
            rvContect.setHasFixedSize(true);
            rvContect.setLayoutManager(layoutManager);
            rvContect.setItemAnimator(new DefaultItemAnimator());
            rvContect.setAdapter(adapter);
        } else {
            adapter.setList(list);
        }


    }

    @OnClick({R.id.imgBack, R.id.btnAddContact})
    public void onViewClicked(View view) {
        Intent intent = null;
        switch (view.getId()) {
            case R.id.imgBack:
                getActivity().finish();
                break;
            case R.id.btnAddContact:
                if (getActivity() != null) {
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    AddContentDialog searchParamDialog = new AddContentDialog();
//                    searchParamDialog.setListener(this);
                    searchParamDialog.setTargetFragment(AddContactFragment.this, ADD_CONTACT);
                    searchParamDialog.show(fm, AddContentDialog.class.getName());
                }
//
//                intent = new Intent(getActivity(), AddContentDialog.class);
//                startActivity(intent);
                break;

        }
    }

    @Override
    public void onItemClick(int position, Contact contact) {
//        try {
//            MediaRecorder recorder = new MediaRecorder();
//            recorder.reset();
//            recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
//            recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
//            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
//
//            File dir = new File(Environment.getExternalStorageDirectory(), Constants.CALLRECORD);
//            if (!dir.exists()) {
//                dir.mkdirs();
//            }
//            String pathSaveIn = "temp" + System.currentTimeMillis() + "recorded_audio.wav";
//            File audioSavePathInDevice = new File(dir, pathSaveIn);
//            recorder.setOutputFile(audioSavePathInDevice.getAbsolutePath());
//            try {
//                recorder.prepare();
//            } catch (java.io.IOException e) {
//                recorder = null;
//                return;
//            }
//            recorder.start();
        if (Constants.getUser(getContext()).getUserAccess() != UserAccessEnum.READ) {
            if (contact.isDefault()) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + contact.getDefaultPhone().trim()));
                startActivity(intent);
            } else {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + contact.getMobile().trim()));
                startActivity(intent);
            }
        }


//        } catch (ActivityNotFoundException act) {
//
//        }


    }

    @Override
    public void onItemDelete(int position, Contact contact) {
        if (Constants.getUser(getContext()).getUserAccess() != UserAccessEnum.READ){
            CustomDialog dialog = new CustomDialog(getContext());
            dialog.setOkListener(getResources().getString(R.string.ok), view1 -> {
                removeFromList(contact.getId());
                dialog.dismiss();
            });
            dialog.setCancelListener(getResources().getString(R.string.cancel), view12 -> dialog.dismiss());
            dialog.setDialogTitle("آیا برای حذف مطمئن هستید؟");
            dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
            dialog.setIcon(R.drawable.ic_delete, getResources().getColor(R.color.redColor));
            dialog.show();
        }


    }

    @Override
    public void onItemEdit(int position, Contact contact) {
        if (Constants.getUser(getContext()).getUserAccess() != UserAccessEnum.READ) {
            FragmentManager fm = getActivity().getSupportFragmentManager();
            AddContentDialog dialog = new AddContentDialog();
            dialog.setTargetFragment(AddContactFragment.this, ADD_CONTACT);
            Bundle bundle = new Bundle();
            bundle.putBoolean("isEdit", true);
            bundle.putSerializable("contact", contact);
            dialog.setArguments(bundle);
            dialog.show(fm, AddContentDialog.class.getName());
        }

    }

    @Override
    public void onItemMessage(int position, Contact contact) {
        if (Constants.getUser(getContext()).getUserAccess() != UserAccessEnum.READ) {
            try {
                SmsManager smsManager = SmsManager.getDefault();
                AppDatabase database = AppDatabase.getInMemoryDatabase(getContext());
                if (contact != null && contact.getMessage() != null)
                    smsManager.sendTextMessage(contact.getMobile(), null, contact.getMessage(), null, null);
                else
                    Toast.makeText(getContext(), "لطفا یک پیام اضافه کنید", Toast.LENGTH_SHORT).show();
                Toast.makeText(getContext().getApplicationContext(), "پیام ارسال شد", Toast.LENGTH_LONG).show();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    @Override
    public void onItemAddMessage(int position, Contact contact) {
        if (Constants.getUser(getContext()).getUserAccess() != UserAccessEnum.READ) {
            FragmentManager fm = getActivity().getSupportFragmentManager();
            AddMessageDialog dialog = new AddMessageDialog();
            dialog.setListener(this);
            Bundle bundle = new Bundle();
            bundle.putSerializable("contact", contact);
            dialog.setArguments(bundle);
            dialog.show(fm, AddMessageDialog.class.getName());
        }


    }

    private void removeFromList(int id) {
        for (Contact contact : list) {
            if (contact.getId() == id) {
                list.remove(contact);
                database.contactDao().delete(contact);
                break;
            }
        }
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }

        if (list == null || list.size() == 0) {
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText(getString(R.string.nothing_found_contact));
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ADD_CONTACT) {
                onResume();
            }
        }
    }

    @Override
    public void onUpdate() {
        onResume();
    }
}
