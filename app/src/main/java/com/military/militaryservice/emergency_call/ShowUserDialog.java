package com.military.militaryservice.emergency_call;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseLinearLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.database.Contact;
import com.military.militaryservice.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ShowUserDialog extends Dialog implements ShowUserAdapter.OnItemClickListener{


    public ArrayList<Contact> user;
    public String title;
    @BindView(R.id.tvShow)
    BaseTextView tvShow;
    @BindView(R.id.rvShowError)
    RecyclerView rvShowError;
    @BindView(R.id.llError)
    BaseLinearLayout llError;
    @BindView(R.id.rlError)
    BaseLinearLayout rlError;
    @BindView(R.id.btnOk)
    BaseTextView btnOk;
    @BindView(R.id.rlBtn)
    BaseLinearLayout rlBtn;
    private AppDatabase database;
    private onPassUserListener listener;

    public ShowUserDialog(@NonNull Context context,onPassUserListener listener) {
        super(context);
        this.listener = listener;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        View view = View.inflate(getContext(), R.layout.info_list_dialog, null);
        setContentView(view);
        ButterKnife.bind(this, view);
        database = AppDatabase.getInMemoryDatabase(getContext());
        user = (ArrayList<Contact>) database.contactDao().getContactList();


        if (getWindow() != null) {
            Window window = getWindow();
            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Point size = Constants.getScreenSize(windowManager);
                int width = (int) Math.min(size.x * 0.90, getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_width));
                int maxHeight = getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_show_info);
                int cellHeight = getContext().getResources().getDimensionPixelSize(R.dimen.defult_height_dilog_info);
                int heightDialog = (cellHeight) * (3 + user.size());
                if (heightDialog > maxHeight) {
                    heightDialog = maxHeight;
                } else if (heightDialog <= (cellHeight * 3)) {
                    heightDialog = cellHeight * 4;
                }
                window.setLayout(width, heightDialog);
                window.setGravity(Gravity.CENTER);
            }

        }


        rlBtn.setVisibility(View.GONE);
        btnOk.setText(getContext().getString(R.string.ok));
        tvShow.setText(title);
        setAdapter();

    }


    private void setAdapter() {

        ShowUserAdapter adapter = new ShowUserAdapter(user,this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvShowError.setLayoutManager(layoutManager);
        rvShowError.setAdapter(adapter);

    }


    @OnClick({R.id.btnOk})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnOk:
                dismiss();
                break;

        }
    }


    @Override
    public void onItemClickMax(int position, Contact object) {
        listener.onPassUser(position, object);
        dismiss();
    }

    public interface onPassUserListener{
        void onPassUser(int position, Contact contact);
    }

    public void setDialogTitle(String title) {
        this.title = title;
    }


}
