package com.military.militaryservice.emergency_call;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.database.Contact;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder> {


    private ArrayList<Contact> list;
    private final OnItemClickListener listener;


    ContactAdapter(ArrayList<Contact> list, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_emergency_emergancy_call, parent, false);

        return new ViewHolder(itemView);

    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        Contact contact = list.get(position);
        holder.tvTitle.setText(contact.getName());
        holder.tvMobile.setText(contact.getMobile());
        holder.tvDescription.setText(contact.getMessage());

        if (contact.getImage() != null) {
            Glide.with(holder.tvTitle.getContext()).load(contact.getImage()).into(holder.imageMag);
        }else {
            Glide.with(holder.tvTitle.getContext()).load(R.drawable.ic_profile).into(holder.imageMag);
        }
        holder.bind(contact, position, listener);
        holder.imgDelete.setOnClickListener(v -> listener.onItemDelete(position, contact));

        holder.imgEdit.setOnClickListener(v -> listener.onItemEdit(position, contact));

        holder.imgMessage.setOnClickListener(v -> listener.onItemMessage(position, contact));

        holder.imgAddMessage.setOnClickListener(v -> listener.onItemAddMessage(position, contact));

        if (contact.isDefault()) {
            holder.rl.setBackgroundColor(res.getColor(R.color.dark_text_second));
        } else {
            holder.rl.setBackgroundColor(res.getColor(R.color.backgroundItem));
        }

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setList(ArrayList<Contact> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageMag)
        CircleImageView imageMag;
        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.tvMobile)
        BaseTextView tvMobile;
        @BindView(R.id.tvDescription)
        BaseTextView tvDescription;
        @BindView(R.id.imgDelete)
        BaseImageView imgDelete;
        @BindView(R.id.imgEdit)
        BaseImageView imgEdit;
        @BindView(R.id.imgCall)
        BaseImageView imgCall;
        @BindView(R.id.imgMessage)
        BaseImageView imgMessage;
        @BindView(R.id.imgAddMessage)
        BaseImageView imgAddMessage;
        @BindView(R.id.rl)
        BaseRelativeLayout rl;
        @BindView(R.id.imgDefault)
        BaseImageView imgDefault;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }


        public void bind(final Contact contact, int position, final OnItemClickListener listener) {
            imgCall.setOnClickListener(v -> listener.onItemClick(position, contact));
        }

    }


    public interface OnItemClickListener {

        void onItemClick(int position, Contact contact);

        void onItemDelete(int position, Contact contact);

        void onItemEdit(int position, Contact contact);

        void onItemMessage(int position, Contact contact);

        void onItemAddMessage(int position, Contact contact);
    }

}
