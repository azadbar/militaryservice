package com.military.militaryservice.emergency_call;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.widget.LinearLayout;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddContectActivity extends BaseActivity {

    @BindView(R.id.frameLayout_estate)
    LinearLayout frameLayoutEstate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contect);
        ButterKnife.bind(this);

        AddContactFragment estateDetailFragment = new AddContactFragment();
        FragmentManager fragMgr = getSupportFragmentManager();
        FragmentTransaction fragTrans = fragMgr.beginTransaction();
        fragTrans.replace(R.id.frameLayout_estate, estateDetailFragment);
        fragTrans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragTrans.commit();
    }


}

