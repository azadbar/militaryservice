package com.military.militaryservice.fingerPrint;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseActivity;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.customView.CustomEditText;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.splash.SplashActivity;
import com.military.militaryservice.utils.PreferencesData;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EnableFingerPrintActivity extends BaseActivity {

    @BindView(R.id.btnFingerPrint)
    BaseImageView btnFingerPrint;
    @BindView(R.id.edtMobile)
    CustomEditText edtMobile;
    @BindView(R.id.edtPassword)
    CustomEditText edtPassword;
    @BindView(R.id.btnLogin)
    BaseTextView btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enable_finger_print);
        ButterKnife.bind(this);

        edtPassword.getImage().setOnClickListener(view -> edtPassword.showHidePassword());
        edtMobile.getImage().setOnClickListener(view -> edtMobile.showHidePassword());

    }

    @OnClick(R.id.btnLogin)
    public void onViewClicked() {
        if (isValidData()) {
            PreferencesData.isFingerPrint(this, true);
            Toast.makeText(this, "ورود با اثر انگشت فعال شد", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(EnableFingerPrintActivity.this, SplashActivity.class);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private boolean isValidData() {
        ArrayList<String> errorMsgList = new ArrayList<>();

        AppDatabase database = AppDatabase.getInMemoryDatabase(this);
        if (!TextUtils.equals(edtMobile.getValueString(), database.userDao().getLogin().getUsername())) {
            String message = getResources().getString(R.string.wrong_username);
            errorMsgList.add(message);
        }

        if (!TextUtils.equals(edtPassword.getValueString(), database.userDao().getLogin().getPassword())) {
            String message = getResources().getString(R.string.wrong_password);
            errorMsgList.add(message);
        }

        if (edtMobile.getError() != null) {
            errorMsgList.add(edtMobile.getError());
        }

        if (edtPassword.getError() != null) {
            errorMsgList.add(edtPassword.getError());
        }

        if (errorMsgList.size() > 0) {
            showInfoDialog(getString(R.string.fill_following), errorMsgList);
            return false;
        }
        return true;
    }

}
