package com.military.militaryservice.fingerPrint;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import com.military.militaryservice.activity.MainActivity;
import com.military.militaryservice.dialog.FingerPrintDialog;
import com.military.militaryservice.dialog.onUpdateUi;

/**
 * Created by whit3hawks on 11/16/16.
 */
@RequiresApi(api = Build.VERSION_CODES.M)
public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {

    private Context context;
    private onUpdateUi listener;

    // Constructor
    public FingerprintHandler(Context mContext, onUpdateUi listener) {
        context = mContext;
        this.listener = listener;
    }

    public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {
        CancellationSignal cancellationSignal = new CancellationSignal();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }

    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
//        this.update("Fingerprint Authentication error\n" + errString);
    }

    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        this.update("لطفا دوباره امتحان کنید\n" + helpString);
    }

    @Override
    public void onAuthenticationFailed() {
        this.update("تأیید اثر انگشت انجام نشد");
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {

        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
        listener.finish();

    }

    private void update(String e) {
        listener.Update(e);
    }

}
