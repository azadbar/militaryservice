package com.military.militaryservice.fingerPrint;

public interface OnFinishListener {

    void onFinish();
}
