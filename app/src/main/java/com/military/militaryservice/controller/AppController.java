package com.military.militaryservice.controller;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;


public class AppController extends MultiDexApplication {

    private static Activity CurrentActivity = null;
    private static Context CurrentContext;


    public void onCreate() {
        super.onCreate();
//        Fabric.with(this, new Crashlytics());//TODO Enable fabric
//        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder().setDefaultFontPath("fonts/IRANSansMobile(FaNum).ttf").setFontAttrId(R.attr.fontPath).build());


//        Base.initialize(this);
    }


    @Override
    protected void attachBaseContext(Context base) {
        MultiDex.install(this);
        super.attachBaseContext(base);
    }
    @Override
    public void onTerminate() {
        super.onTerminate();
    }


    public static void setActivityContext(Activity activity, Context context) {
        CurrentActivity = activity;
        CurrentContext = context;
    }

    public static Activity getCurrentActivity() {
        return CurrentActivity;
    }

    public static Context getCurrentContext() {
        return CurrentContext;
    }

}
