package com.military.militaryservice.recordVideo;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.customView.CustomMultiLineEditText;
import com.military.militaryservice.database.Address;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.dialog.CustomDialog;
import com.military.militaryservice.dialog.InfoListDialog;
import com.military.militaryservice.enums.AttachmentType;
import com.military.militaryservice.maps.AddressAdapter;
import com.military.militaryservice.maps.OfflineActivity;
import com.military.militaryservice.projectList.show.PreviewVideoActivity;
import com.military.militaryservice.resultInfo.FileAttachmentsItem;
import com.military.militaryservice.resultInfo.LocationDTO;
import com.military.militaryservice.utils.Constants;
import com.military.militaryservice.utils.EqualSpacingItemDecoration;
import com.military.militaryservice.utils.PermissionHandler;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

/**
 * Created by a.azadbar on 10/7/2017.
 */

public class AddVideoDialog extends DialogFragment implements VideoInsertAdapter.OnItemClickListener, VideoInsertAdapter.DeleteOnItemClickListener, AddressAdapter.OnItemClickListener, VideoInsertAdapter.OnItemClickPlayListener {


    private static final int REQUEST_VIDEO_CAPTURE = 1;
    //    @BindView(R.id.edtName)
//    CustomEditText edtName;
    @BindView(R.id.imgAddress)
    BaseImageView imgAddress;
    @BindView(R.id.rlBtnAddAddress)
    BaseRelativeLayout rlBtnAddAddress;
    @BindView(R.id.edtDescription)
    CustomMultiLineEditText edtDescription;
    @BindView(R.id.rvSelectedVideo)
    RecyclerView rvSelectedVideo;
    @BindView(R.id.btnOk)
    BaseTextView btnOk;
    @BindView(R.id.btnCancel)
    BaseTextView btnCancel;
    private int REQUEST_CODE_PERMISSION = 2;
    private VideoInsertAdapter adapter;

    Unbinder unbinder;
    private Uri imageUri;
    private ArrayList<FileAttachmentsItem> fileAttachmentsItems = new ArrayList<>();
    private int maxImageCount = 50;
    private int id;
    private ArrayList<Address> address = new ArrayList<>();
    private AddressAdapter addressAddpter;
    private FileAttachmentsItem fileAttachmentsItem;
    private boolean isEdit;
    private AppDatabase database;
    private updateListenerVideo listener;
    private FileAttachmentsItem selectedVideo;
    private int selectedPosition;
    private FileAttachmentsItem fileSelected;
    private int oldPosition;
    private long identifier;
    private double latitude;
    private double longitude;
    private String locationAddressItem;
    private boolean doubleClick = false;
    private int position;

    public void setListener(updateListenerVideo listener) {
        this.listener = listener;
    }

    public AddVideoDialog() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_video_dialog, container, false);
        unbinder = ButterKnife.bind(this, v);
        setCancelable(false);

        database = AppDatabase.getInMemoryDatabase(getActivity());

        Bundle bundle = getArguments();
        if (bundle != null) {
            id = bundle.getInt("id");
            position = bundle.getInt("position");
            isEdit = bundle.getBoolean("isEdit");
            if (isEdit) {
                fileAttachmentsItem = (FileAttachmentsItem) bundle.getSerializable("file");
                identifier = fileAttachmentsItem.getIdentifier();
            } else {
                List<FileAttachmentsItem> selectVideo = database.fileAttachmentDao().select(id, AttachmentType.VIDEO.getFormat());
                if (selectVideo.size() > 0) {
                    fileAttachmentsItems.addAll(selectVideo);
                }

            }
        }

        if (isEdit) {
            fileAttachmentsItem.setDisable(true);
            fileAttachmentsItems.add(fileAttachmentsItem);
            fileSelected = fileAttachmentsItem;
            if (fileAttachmentsItem.getGeoLocationDTO() != null) {
                latitude = fileAttachmentsItem.getGeoLocationDTO().getLatitude();
                longitude = fileAttachmentsItem.getGeoLocationDTO().getLongitude();
                locationAddressItem = fileAttachmentsItem.getGeoLocationDTO().getAddress();
            }
            for (int i = 0; i < fileAttachmentsItems.size(); i++) {
                edtDescription.setBody(fileAttachmentsItems.get(i).getDescription());
//                edtName.setBody(videos.get(i).getNameVideo());
//                Address address = new Address();
//                address.setLatitude(fileAttachmentsItems.get(i).getLat());
//                address.setLongitude(fileAttachmentsItems.get(i).getLang());
//                address.setQuestionIdentifier(fileAttachmentsItems.get(i).getQuestionIdentifier());
//                address.setDate(fileAttachmentsItems.get(i).getDate());
//                address.setAddress(fileAttachmentsItems.get(i).getVideoLocation());
//                this.address.add(address);
            }
        }

        setImageCellSize();

        edtDescription.getEdtBody().addTextChangedListener(new TextWatcher() {
            private Timer timer = new Timer();
            private final long DELAY = 300; // milliseconds

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (timer != null) {
                    timer.cancel();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                timer.cancel();
                timer = new Timer();
                timer.schedule(
                        new TimerTask() {
                            @Override
                            public void run() {
                                if (getActivity() != null) {
                                    getActivity().runOnUiThread(() -> {
                                        if (isEdit) {
                                            database.fileAttachmentDao().setDescription(edtDescription.getValueString(), fileAttachmentsItem.getIdentifier());
                                            makeJson();
                                            listener.onInsertOk();
                                        } else {
                                            if (fileSelected != null) {
                                                database.fileAttachmentDao().setDescription(edtDescription.getValueString(), fileSelected.getIdentifier());
                                                makeJson();
                                                listener.onInsertOk();
                                            } else {
                                                Toast.makeText(getActivity(), "لطفا یک فایل انتخاب کنید", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                                }
                            }
                        },
                        DELAY
                );
            }
        });
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
        if (getActivity() != null && window != null && getContext() != null) {
            WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {
                Point size = Constants.getScreenSize(windowManager);
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                int width = (int) Math.min(size.x * 0.90, getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_width));
                int height = (int) Math.min(size.y * 0.60, getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_fragment_height));
                window.setLayout(width, height);
                window.setGravity(Gravity.CENTER);
            }
        }

        if (address.size() > 0) {
//            setDatAddressAdapter(address);
        }

    }

//    private void setDatAddressAdapter(ArrayList<Address> address) {
//        if (addressAddpter == null) {
//            addressAddpter = new AddressAdapter(address, this);
//            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
//            recycleAddress.setAdapter(addressAddpter);
//            recycleAddress.setLayoutManager(layoutManager);
//            recycleAddress.setHasFixedSize(true);
//        } else {
//            addressAddpter.setList(address);
//        }
//    }

    private void setImageCellSize() {
        int coulumCount = getResources().getInteger(R.integer.coloum_count_register_estate);
        int offset = getResources().getDimensionPixelSize(R.dimen.coulem_offset_recycle_view_in_register_estate);
        int cellWidth = getResources().getDimensionPixelSize(R.dimen.defualt_width_image_image_recycle_view_register_estate);
        if (getContext() != null) {

            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {

                int screenWidth = Constants.getScreenSize(windowManager).x;
//                cellWidth = (int) ((screenWidth - (coulumCount + 1) * (offset * 2)) / (double) (coulumCount));
            }
        }
        rvSelectedVideo.setHasFixedSize(true);
        adapter = new VideoInsertAdapter(getActivity(), fileAttachmentsItems, cellWidth, maxImageCount, this, this, this, isEdit);
        LinearLayoutManager layoutManager = new GridLayoutManager(getActivity(), coulumCount, RecyclerView.VERTICAL, false);
        rvSelectedVideo.setLayoutManager(layoutManager);
        rvSelectedVideo.addItemDecoration(new EqualSpacingItemDecoration(offset, EqualSpacingItemDecoration.GRID));
        rvSelectedVideo.setAdapter(adapter);
    }


    @OnClick({R.id.btnOk, R.id.btnCancel, R.id.imgAddVideo, R.id.imgInsertDesc, R.id.imgAddress, R.id.imgClose})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgAddVideo:
                if (getActivity() != null) {
                    if (PermissionHandler.hasAllPermissions(getActivity())) {
                        if (fileAttachmentsItems.size() < maxImageCount) {
                            openCameraTake();
                        }
                    } else {
                        PermissionHandler.requestPermissions(AddVideoDialog.this, REQUEST_CODE_PERMISSION);
                    }
                }
                break;
            case R.id.btnOk:

                if (fileAttachmentsItems.size() > 0) {

                    for (int i = 0; i < fileAttachmentsItems.size(); i++) {

                        if (isEdit) {
                            database.fileAttachmentDao().insert(fileAttachmentsItems.get(i));
                            database.fileAttachmentDao().updateTour(fileAttachmentsItems.get(i));
                            Toast.makeText(getActivity(), "فیلم مورد نظر بروز شد", Toast.LENGTH_SHORT).show();
                            Intent intent1 = new Intent(getContext(), AddVideoDialog.class);
                            if (getTargetFragment() != null) {
                                getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, intent1);
                            }
                        } else {
                            Toast.makeText(getActivity(), "فیلم مورد نظر اضافه شد", Toast.LENGTH_SHORT).show();
                            database.fileAttachmentDao().insert(fileAttachmentsItems.get(i));
                            makeJson();
                        }
                        listener.onInsertOk();
                        dismiss();
                    }
                }
                break;
            case R.id.btnCancel:
                dismiss();
                break;
            case R.id.imgAddress:
                Intent intent = new Intent(getContext(), OfflineActivity.class);
                if (fileSelected != null) {
                    intent.putExtra("lat", latitude > 0 ? latitude : 0);
                    intent.putExtra("long", longitude > 0 ? longitude : 0);
                    intent.putExtra("bodyEditTxt", locationAddressItem != null ? locationAddressItem : 0);
                    intent.putExtra("isQuestion", true);
                } else {
                    intent.putExtra("isQuestion", false);
                }
                startActivityForResult(intent, Constants.LOCATION_INTENT);
                break;

            case R.id.imgInsertDesc:
                if (edtDescription.getValueString() != null) {
                    if (fileSelected != null) {
                        fileAttachmentsItems.get(selectedPosition).setDescription(edtDescription.getValueString());
                        database.fileAttachmentDao().setDescription(edtDescription.getValueString(), fileSelected.getIdentifier());
                        Toast.makeText(getContext(), "پیام اضافه شد", Toast.LENGTH_SHORT).show();
                        edtDescription.setBody(null);
//                        edtDescription.setTextHint("توضیح را وارد کنید");
                    } else {
                        Toast.makeText(getContext(), "یکی از فایل های زیر را انتخاب کنید", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(getContext(), "متن پیام را وارد کنید", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.imgClose:
                dismiss();
                break;
        }
    }

    private void makeJson() {
        if (isEdit) {
            JSONObject json = new JSONObject();
            try {
                json.put("identifier", fileAttachmentsItem.getIdentifier());
                json.put("filename", fileAttachmentsItem.getFilename());
                json.put("description", database.fileAttachmentDao().getDescription(fileAttachmentsItem != null ? fileAttachmentsItem.getIdentifier() : 0) != null ?
                        database.fileAttachmentDao().getDescription(fileAttachmentsItem.getIdentifier()).getDescription() : "");
                json.put("format", AttachmentType.VIDEO.getFormat());
                Writer output = null;
                File jsonFile = new File(Environment.getExternalStorageDirectory() + "" +
                        Constants.SHAHED + Constants.fileAttachments + fileAttachmentsItem.getFilename(),
                        "/" + fileAttachmentsItem.getFilename() + ".json");
                output = new BufferedWriter(new FileWriter(jsonFile));
                output.write(json.toString());
                output.close();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            JSONObject json = new JSONObject();
            try {
                json.put("identifier", fileSelected.getIdentifier());
                json.put("filename", fileSelected.getFilename());
                json.put("description", database.fileAttachmentDao().getDescription(fileSelected != null ? fileSelected.getIdentifier() : 0) != null ?
                        database.fileAttachmentDao().getDescription(fileSelected.getIdentifier()).getDescription() : "");
                json.put("format", AttachmentType.VIDEO.getFormat());
                Writer output = null;
                File jsonFile = new File(Environment.getExternalStorageDirectory() + "" +
                        Constants.SHAHED + Constants.fileAttachments + fileSelected.getFilename(),
                        "/" + fileSelected.getFilename() + ".json");
                output = new BufferedWriter(new FileWriter(jsonFile));
                output.write(json.toString());
                output.close();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private boolean isValidData() {
        ArrayList<String> errorMsgList = new ArrayList<>();

//        if (!TextUtils.isEmpty(edtName.getError())) {
//            errorMsgList.add(edtName.getError());
//        }

        if (errorMsgList.size() > 0) {
            showInfoDialog(getActivity().getString(R.string.fill_following), errorMsgList);
            return false;
        }
        return true;
    }


    public void showInfoDialog(String title, ArrayList<String> errorMsgList) {
        if (getActivity() != null) {
            InfoListDialog infoListDialog = new InfoListDialog(getActivity());
            infoListDialog.errorMsg = errorMsgList;
            infoListDialog.title = title;
            infoListDialog.show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_VIDEO_CAPTURE) {
                getImageFromCamera(data);
            } else if (requestCode == Constants.LOCATION_INTENT) {
                Toast.makeText(getContext(), "آدرس ثبت شد", Toast.LENGTH_SHORT).show();
                LocationDTO add = (LocationDTO) data.getSerializableExtra("result");
                LocationDTO address = new LocationDTO();
                address.setLatitude(add.getLatitude());
                address.setLongitude(add.getLongitude());
                address.setAddress(add.getAddress());
                address.setDate(new Date());
                if (isEdit) {
                    database.fileAttachmentDao().setLocation(address.getLatitude(), address.getLongitude(), address.getAddress(), fileSelected.getIdentifier());
                    makeJson();
                    listener.onInsertOk();
                } else {
                    if (fileSelected != null) {
                        database.fileAttachmentDao().setLocation(address.getLatitude(), address.getLongitude(), address.getAddress(), fileSelected.getIdentifier());
                        makeJson();
                        listener.onInsertOk();
                    } else {
                        Toast.makeText(getActivity(), "لطفا یک فایل انتخاب کنید", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }

        if (resultCode == RESULT_CANCELED) {
            try {

            } catch (Exception ec) {

            }
        }
    }


    private void getImageFromCamera(Intent data) {

        try {

            AssetFileDescriptor videoAsset = btnOk.getContext().getContentResolver().openAssetFileDescriptor(data.getData(), "r");
            FileInputStream fis = videoAsset.createInputStream();
            long currentTime = System.currentTimeMillis();
            File dir = new File(Constants.storage_Dir_file_attachment + "/" + position + "/" + Constants.fileAttachments, currentTime + "");
            if (!dir.exists()) {
                dir.mkdirs();
            }
            String pathSaveIn = currentTime + "video.mp4";
            File file = new File(dir, pathSaveIn);

            FileOutputStream fos = new FileOutputStream(file);

            byte[] buf = new byte[2048];
            int len;
            while ((len = fis.read(buf)) > 0) {
                fos.write(buf, 0, len);
            }
            fis.close();
            fos.close();

            if (isEdit) {
                database.fileAttachmentDao().delete(fileAttachmentsItem);
                File fdelete = new File(Environment.getExternalStorageDirectory(), fileAttachmentsItem.getFileFolder());
                if (fdelete.isDirectory()) {
                    String[] children = fdelete.list();
                    for (int i = 0; i < children.length; i++) {
                        new File(fdelete, children[i]).delete();
                    }
                }
                try {
                    FileUtils.deleteDirectory(fdelete);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            fileAttachmentsItem = new FileAttachmentsItem();
            fileAttachmentsItem.setDate(new Date());
            fileAttachmentsItem.setIdentifier(currentTime);
            fileAttachmentsItem.setFilename(currentTime + "");
            fileAttachmentsItem.setFileAddress(Constants.SHAHED + Constants.fileQuestions + position + "/" + Constants.fileAttachments + currentTime + "/" + pathSaveIn);
            fileAttachmentsItem.setFileFolder(Constants.SHAHED + Constants.fileQuestions + position + "/" + Constants.fileAttachments + currentTime);
            fileAttachmentsItem.setAttachmentType(AttachmentType.VIDEO);
            fileAttachmentsItem.setQuestionIdentifier(id);
            fileAttachmentsItem.setFormat(AttachmentType.VIDEO.getFormat());
            fileAttachmentsItem.setUserId(Constants.getUser(getContext()).getId());
//            fileAttachmentsItem.setDisable(true);

            if (isEdit) {
                if (fileAttachmentsItems.size() > 0) {
                    fileAttachmentsItems.clear();
                    fileAttachmentsItems.add(fileAttachmentsItem);
                    Collections.reverse(fileAttachmentsItems);
                    Toast.makeText(getActivity(), "فیلم مورد ویرایش شد", Toast.LENGTH_SHORT).show();
                    database.fileAttachmentDao().insert(fileAttachmentsItem);
                    fileSelected = fileAttachmentsItem;
                    makeJson();
                } else {
                    fileAttachmentsItems.add(fileAttachmentsItem);
                    Collections.reverse(fileAttachmentsItems);
                    Toast.makeText(getActivity(), "فیلم مورد نظر اضافه شد", Toast.LENGTH_SHORT).show();
                    database.fileAttachmentDao().insert(fileAttachmentsItem);
                    fileSelected = fileAttachmentsItem;
                    makeJson();
                }

            } else {
                fileAttachmentsItems.add(fileAttachmentsItem);
                Collections.reverse(fileAttachmentsItems);
                Toast.makeText(getActivity(), "فیلم مورد نظر اضافه شد", Toast.LENGTH_SHORT).show();
                database.fileAttachmentDao().insert(fileAttachmentsItem);
                fileSelected = fileAttachmentsItem;
                makeJson();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        adapter.notifyDataSetChanged();
        fileSelected = fileAttachmentsItem;
        selectedPosition = 0;
        adapter.setSelectedVideo(selectedPosition, fileSelected);

    }


    private void openCameraTake() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
        }
    }


    @Override
    public void onItemClickDelete(int position, FileAttachmentsItem file) {

        CustomDialog dialog = new CustomDialog(getActivity());
        dialog.setOkListener(getResources().getString(R.string.ok), view1 -> {
            dialog.dismiss();
            removeFromListAndFile(position, file.getIdentifier());
        });
        dialog.setCancelListener(getResources().getString(R.string.cancel), view12 -> dialog.dismiss());
        dialog.setDialogTitle("آیا برای حذف مطمئن هستید؟");
        dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
        dialog.setIcon(R.drawable.ic_delete, getResources().getColor(R.color.redColor));
        dialog.show();

        adapter.notifyDataSetChanged();
    }

    private void removeFromListAndFile(int position, long id) {
        for (FileAttachmentsItem file : fileAttachmentsItems) {
            if (file.getIdentifier() == id) {
                if (fileAttachmentsItems.size() == maxImageCount) {
                    fileAttachmentsItems.remove(position);
                } else {
                    fileAttachmentsItems.remove(position - 1);
                }
                database.fileAttachmentDao().delete(file);
                File fdelete = new File(Environment.getExternalStorageDirectory(), file.getFileFolder());
                if (fdelete.isDirectory()) {
                    String[] children = fdelete.list();
                    for (int i = 0; i < children.length; i++) {
                        new File(fdelete, children[i]).delete();
                    }
                }
                try {
                    FileUtils.deleteDirectory(fdelete);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
        }

        if (adapter != null) {
            adapter.notifyDataSetChanged();
            listener.onInsertOk();
        }
    }

    @Override
    public void onItemAddress(int position, LocationDTO address) {

    }

    @Override
    public void onItemDelete(int position, LocationDTO address) {
        if (getActivity() != null) {
            CustomDialog dialog = new CustomDialog(getActivity());
            dialog.setOkListener(getResources().getString(R.string.ok), view1 -> {
                dialog.dismiss();
                removeFromAddres(address.getId());
            });
            dialog.setCancelListener(getResources().getString(R.string.cancel), view12 -> dialog.dismiss());
            dialog.setDialogTitle("آیا برای حذف مطمئن هستید؟");
            dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
            dialog.setIcon(R.drawable.ic_delete, getResources().getColor(R.color.redColor));
            dialog.show();
        }
    }

    private void removeFromAddres(int id) {
        for (Address address : address) {
            if (address.getId() == id) {
                this.address.remove(address);
                break;
            }
        }

        if (adapter != null) {
            adapter.notifyDataSetChanged();
            listener.onInsertOk();
        }
    }


    @Override
    public void onItemClick(int position, FileAttachmentsItem file) {

        if (doubleClick) {
            Intent intent = new Intent(getContext(), PreviewVideoActivity.class);
            intent.putExtra("video", file.getFileAddress());
            getContext().startActivity(intent);
        } else {
            doubleClick = true;
            new Handler().postDelayed(() -> doubleClick = false, 1000);

            this.fileSelected = file;
            if (fileAttachmentsItems.size() == maxImageCount) {
                selectedVideo = fileAttachmentsItems.get(position);
                selectedPosition = position;
            } else {
                selectedVideo = fileAttachmentsItems.get(position - 1);
                selectedPosition = position - 1;
            }

            adapter.setSelectedVideo(selectedPosition, selectedVideo);

            AppDatabase database = AppDatabase.getInMemoryDatabase(getContext());

            if (oldPosition == position) {
//            database.fileAttachmentDao().setDescription(edtDescription.getValueString(), fileSelected.getIdentifier());
            } else {
                FileAttachmentsItem description = database.fileAttachmentDao().getDescription(fileSelected.getIdentifier());
                if (description != null) {
                    edtDescription.setBody(description.getDescription());
                    if (description.getGeoLocationDTO() != null) {
                        latitude = description.getGeoLocationDTO().getLatitude();
                        longitude = description.getGeoLocationDTO().getLongitude();
                        locationAddressItem = description.getGeoLocationDTO().getAddress();
                    } else {
                        latitude = 0.0;
                        longitude = 0.0;
                        locationAddressItem = "";
                    }
                }
            }

            this.oldPosition = position;
        }


//        if (this.address != null) {
//            videos.get(selectedPosition).setLat(this.address.get(0).getLatitude());
//            videos.get(selectedPosition).setLang(this.address.get(0).getLongitude());
//        }

    }

    @Override
    public void onItemClick(int position) {
        if (getActivity() != null) {
            if (PermissionHandler.hasAllPermissions(getActivity())) {
                if (fileAttachmentsItems.size() < maxImageCount) {
                    openCameraTake();
                }
            } else {
                PermissionHandler.requestPermissions(AddVideoDialog.this, REQUEST_CODE_PERMISSION);
            }
        }
    }

    public interface updateListenerVideo {
        void onInsertOk();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(getActivity(), getTheme()) {
            @Override
            public void onBackPressed() {
                dismiss();
            }
        };
    }
}
