package com.military.militaryservice.recordVideo;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.image.ImageInsertAdapter;
import com.military.militaryservice.projectList.show.PreviewActivity;
import com.military.militaryservice.projectList.show.PreviewVideoActivity;
import com.military.militaryservice.resultInfo.FileAttachmentsItem;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class VideoInsertAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final int cellWidth;
    private final int maxImageCount;
    private Context context;
    private ArrayList<FileAttachmentsItem> list;
    private final OnItemClickListener listener;
    private final DeleteOnItemClickListener deleteOnItemClickListener;
    private final OnItemClickPlayListener playListener;
    private boolean isEdit;
    private FileAttachmentsItem videoSelected;


    public VideoInsertAdapter(Context context, ArrayList<FileAttachmentsItem> list, int cellWidth, int maxImageCount, OnItemClickListener listener, DeleteOnItemClickListener deleteOnItemClickListener, OnItemClickPlayListener playListener, boolean isEdit) {
        this.list = list;
        this.listener = listener;
        this.cellWidth = cellWidth;
        this.maxImageCount = maxImageCount;
        this.context = context;
        this.deleteOnItemClickListener = deleteOnItemClickListener;
        this.playListener = playListener;
        this.isEdit = isEdit;
    }


    @Override
    public int getItemViewType(int position) {
        if (list.size() == maxImageCount) {
            return 2;
        }
        if (position == 0) {
            return 1;
        }
        return 2;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1:
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_add_video, parent, false);
                itemView.getLayoutParams().height = cellWidth;
                itemView.getLayoutParams().width = cellWidth;
                return new addImageViewHolder(itemView);
            default:
                View itemView1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video_cell, parent, false);
                itemView1.getLayoutParams().height = cellWidth;
                itemView1.getLayoutParams().width = cellWidth;
                return new ViewHolderImageList(itemView1);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        switch (holder.getItemViewType()) {
            case 1:
                addImageViewHolder viewHolder0 = (addImageViewHolder) holder;
                if (isEdit) {
                    ((VideoInsertAdapter.addImageViewHolder) holder).text.setText("ویرایش فیلم");
                } else {
                    ((VideoInsertAdapter.addImageViewHolder) holder).text.setText("افزودن فیلم");
                }
                ((addImageViewHolder) holder).bind(position, listener);
                break;

            default:
                ViewHolderImageList viewHolder2 = (ViewHolderImageList) holder;

                FileAttachmentsItem fileAttachmentsItem;

                if (list.size() == maxImageCount) {
                    fileAttachmentsItem = list.get(position);
                } else {
                    fileAttachmentsItem = list.get(position - 1);
                }


                viewHolder2.setFileAttachment(fileAttachmentsItem);
//                ((ViewHolderImageList) holder).doubleClick(position, fileAttachmentsItem, deleteOnItemClickListener);

                ((ViewHolderImageList) holder).bind(position, fileAttachmentsItem, deleteOnItemClickListener);

                ((ViewHolderImageList) holder).onItemClick(position, playListener, fileAttachmentsItem);

                File file = new File(Environment.getExternalStorageDirectory(), fileAttachmentsItem.getFileAddress());


                ((ViewHolderImageList) holder).rlAddImage.post(() -> {
                    int width = ((ViewHolderImageList) holder).rlAddImage.getWidth();// this will give you cell width dynamically
                    int height = ((ViewHolderImageList) holder).rlAddImage.getHeight();// this will give you cell height dynamically
                    ViewGroup.LayoutParams params = ((ViewHolderImageList) holder).rlAddImage.getLayoutParams();
                    params.height = height;
                    params.width = width;
                    ((ViewHolderImageList) holder).rlAddImage.setLayoutParams(params);
                    ((ViewHolderImageList) holder).imgAdd.setVideoURI(Uri.fromFile(file.getAbsoluteFile()));
                    ((ViewHolderImageList) holder).imgAdd.setOnPreparedListener(mp -> {
                        mp.setVolume(0f, 0f);
                        mp.setLooping(false);
                    });
                    ((ViewHolderImageList) holder).imgAdd.start();
                    ((ViewHolderImageList) holder).imgAdd.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                        @Override
                        public boolean onError(MediaPlayer mp, int what, int extra) {
                            Log.d("video", "setOnErrorListener ");
                            return true;
                        }
                    });
                });

                if (fileAttachmentsItem.isDisable()) {
                    ((ViewHolderImageList) holder).disable.setBackgroundColor(((ViewHolderImageList) holder).disable.getContext().getResources().getColor(R.color.transparent));
                } else {
                    ((ViewHolderImageList) holder).disable.setBackgroundColor(((ViewHolderImageList) holder).disable.getContext().getResources().getColor(R.color.disable));

                }
                break;
        }

    }


    @Override
    public int getItemCount() {
        if (list.size() < maxImageCount) {
            return list.size() + 1;
        }
        return list.size();
    }

    public void setSelectedVideo(int position, FileAttachmentsItem file) {
        this.videoSelected = file;
        list.get(position).setDisable(true);
        for (int i = 0; i < list.size(); i++) {
            if (position != i)
                list.get(i).setDisable(false);
        }
        notifyDataSetChanged();
    }


    public interface OnItemClickPlayListener {
        void onItemClick(int position, FileAttachmentsItem fileAttachmentsItem);
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public interface DeleteOnItemClickListener {
        void onItemClickDelete(int position, FileAttachmentsItem fileAttachmentsItem);
    }

    static class addImageViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgAdd)
        BaseImageView imgAdd;
        @BindView(R.id.rlAddImage)
        BaseRelativeLayout rlAddImage;
        @BindView(R.id.text)
        BaseTextView text;

        addImageViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(position));
        }

    }

    static class ViewHolderImageList extends RecyclerView.ViewHolder {
        private final GestureDetector dg;

        @BindView(R.id.imgDelete)
        BaseImageView imgDelete;
        @BindView(R.id.imgAdd)
        VideoView imgAdd;
        @BindView(R.id.rlAddImage)
        CardView rlAddImage;
        @BindView(R.id.disable)
        BaseRelativeLayout disable;
        private FileAttachmentsItem fileAttachment;

        ViewHolderImageList(View view) {
            super(view);
            ButterKnife.bind(this, view);

            dg = new GestureDetector(rlAddImage.getContext(), new GestureDetector.OnGestureListener() {
                @Override
                public boolean onDown(MotionEvent e) {
                    return false;
                }

                @Override
                public void onShowPress(MotionEvent e) {

                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return false;
                }

                @Override
                public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                    return false;
                }

                @Override
                public void onLongPress(MotionEvent e) {

                }

                @Override
                public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                    return false;
                }
            });
            dg.setOnDoubleTapListener(new GestureDetector.OnDoubleTapListener() {
                @Override
                public boolean onDoubleTap(MotionEvent e) {
                    Intent intent = new Intent(itemView.getContext(), PreviewVideoActivity.class);
                    intent.putExtra("video", fileAttachment.getFileAddress());
                    itemView.getContext().startActivity(intent);
                    return false;
                }

                @Override
                public boolean onDoubleTapEvent(MotionEvent e) {
                    // if the second tap hadn't been released and it's being moved

                    return false;
                }

                @Override
                public boolean onSingleTapConfirmed(MotionEvent e) {
                    // TODO Auto-generated method stub
                    return false;
                }

            });
        }

        public void bind(int position, FileAttachmentsItem fileAttachmentsItem, final DeleteOnItemClickListener listener) {
            imgDelete.setOnClickListener(v -> listener.onItemClickDelete(position, fileAttachmentsItem));
        }

        public void onItemClick(int position, OnItemClickPlayListener listener, FileAttachmentsItem fileAttachmentsItem) {
            itemView.setOnClickListener(v -> listener.onItemClick(position, fileAttachmentsItem));
        }

        public void doubleClick(int position, FileAttachmentsItem fileAttachmentsItem, DeleteOnItemClickListener deleteOnItemClickListener) {
            rlAddImage.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    dg.onTouchEvent(event);
                    return false;
                }
            });
        }

        public void setFileAttachment(FileAttachmentsItem fileAttachmentsItem) {
            this.fileAttachment = fileAttachmentsItem;
        }

    }

}
