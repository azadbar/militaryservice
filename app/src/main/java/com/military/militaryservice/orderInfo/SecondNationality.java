package com.military.militaryservice.orderInfo;

import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SecondNationality implements Serializable {

    @SerializedName("id")
    private int SecondNationalityId;
    @SerializedName("manualId")
    private int SecondNationalityManualId;
//    private String lastUpdateDateTime;
//    private String code;
//
//    private String value;
//    private String creationDateTime;


    public int getSecondNationalityId() {
        return SecondNationalityId;
    }

    public void setSecondNationalityId(int secondNationalityId) {
        SecondNationalityId = secondNationalityId;
    }

    public int getSecondNationalityManualId() {
        return SecondNationalityManualId;
    }

    public void setSecondNationalityManualId(int secondNationalityManualId) {
        SecondNationalityManualId = secondNationalityManualId;
    }

//    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }

//    public String getCode() {
//        return code;
//    }
//
//    public void setCode(String code) {
//        this.code = code;
//    }
//
//
//    public String getValue() {
//        return value;
//    }
//
//    public void setValue(String value) {
//        this.value = value;
//    }

//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }
}
