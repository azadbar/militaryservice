package com.military.militaryservice.orderInfo;

import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Unit implements Serializable {

    @PrimaryKey
    @SerializedName("id")
    private int unitId;
    @SerializedName("manualIdId")
    private int unitManualId;
//    private String lastUpdateDateTime;
    private String code;
    private String value;
//    private String creationDateTime;

    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public int getUnitManualId() {
        return unitManualId;
    }

    public void setUnitManualId(int unitManualId) {
        this.unitManualId = unitManualId;
    }

//    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }
}
