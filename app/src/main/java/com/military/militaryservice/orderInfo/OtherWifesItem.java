package com.military.militaryservice.orderInfo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OtherWifesItem implements Serializable {

    @SerializedName("id")
    private int otherWifesId;
    @SerializedName("manualId")
    private int otherWifesManualId;
//    private String lastUpdateDateTime;
    private String locationInfo;
    private String description;
    private String fileAttachment;
//    private String creationDateTime;

    public int getOtherWifesId() {
        return otherWifesId;
    }

    public void setOtherWifesId(int otherWifesId) {
        this.otherWifesId = otherWifesId;
    }

    public int getOtherWifesManualId() {
        return otherWifesManualId;
    }

    public void setOtherWifesManualId(int otherWifesManualId) {
        this.otherWifesManualId = otherWifesManualId;
    }

//    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }

    public String getLocationInfo() {
        return locationInfo;
    }

    public void setLocationInfo(String locationInfo) {
        this.locationInfo = locationInfo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getFileAttachment() {
        return fileAttachment;
    }

    public void setFileAttachment(String fileAttachment) {
        this.fileAttachment = fileAttachment;
    }

//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }
}
