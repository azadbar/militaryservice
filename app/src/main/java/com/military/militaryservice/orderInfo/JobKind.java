package com.military.militaryservice.orderInfo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class JobKind implements Serializable {

    @SerializedName("id")
    private int jobKindId;
    @SerializedName("manualId")
    private int jobKindManualId;
    //    private String lastUpdateDateTime;
    private String code;
    @SerializedName("value")
    private String jobKindValue;
//    private String creationDateTime;


    public String getJobKindValue() {
        return jobKindValue;
    }

    public void setJobKindValue(String jobKindValue) {
        this.jobKindValue = jobKindValue;
    }

    public int getJobKindId() {
        return jobKindId;
    }

    public void setJobKindId(int jobKindId) {
        this.jobKindId = jobKindId;
    }

    public int getJobKindManualId() {
        return jobKindManualId;
    }

    public void setJobKindManualId(int jobKindManualId) {
        this.jobKindManualId = jobKindManualId;
    }

    //    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }
//
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public String getValue() {
        return jobKindValue;
    }

    public void setValue(String value) {
        this.jobKindValue = value;
    }

//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }
}
