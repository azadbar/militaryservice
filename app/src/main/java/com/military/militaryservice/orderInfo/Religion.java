package com.military.militaryservice.orderInfo;


import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Religion implements Serializable {

    @SerializedName("id")
    private int religionId;
    @SerializedName("manualId")
    private int religionManualId;
//    private String lastUpdateDateTime;
    private String code;
    private String value;
//    private String creationDateTime;

    public int getReligionId() {
        return religionId;
    }

    public void setReligionId(int religionId) {
        this.religionId = religionId;
    }

    public int getReligionManualId() {
        return religionManualId;
    }

    public void setReligionManualId(int religionManualId) {
        this.religionManualId = religionManualId;
    }

//    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }
}
