package com.military.militaryservice.orderInfo;

import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Province implements Serializable {

    @PrimaryKey
    @SerializedName("id")
    private int provinceId;
    @SerializedName("manualId")
    private int provinceManualId;
//    private String lastUpdateDateTime;
    @SerializedName("name")
    private String provinceName;
//    private String creationDateTime;


    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public int getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }

    public int getProvinceManualId() {
        return provinceManualId;
    }

    public void setProvinceManualId(int provinceManualId) {
        this.provinceManualId = provinceManualId;
    }

//    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }

    public String getName() {
        return provinceName;
    }

    public void setName(String name) {
        this.provinceName = name;
    }


//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }
}
