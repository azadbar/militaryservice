package com.military.militaryservice.orderInfo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ExtraWorkAddressInfosItem implements Serializable {

    @SerializedName("id")
    private int extraWorkAddressInfosId;
    @SerializedName("manualId")
    private int extraWorkAddressInfosManualId;
//    private String lastUpdateDateTime;
    private String locationInfo;
    private String description;
    private String fileAttachment;
//    private String creationDateTime;

    public int getExtraWorkAddressInfosId() {
        return extraWorkAddressInfosId;
    }

    public void setExtraWorkAddressInfosId(int extraWorkAddressInfosId) {
        this.extraWorkAddressInfosId = extraWorkAddressInfosId;
    }

    public int getExtraWorkAddressInfosManualId() {
        return extraWorkAddressInfosManualId;
    }

    public void setExtraWorkAddressInfosManualId(int extraWorkAddressInfosManualId) {
        this.extraWorkAddressInfosManualId = extraWorkAddressInfosManualId;
    }

//    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }

    public String getLocationInfo() {
        return locationInfo;
    }

    public void setLocationInfo(String locationInfo) {
        this.locationInfo = locationInfo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getFileAttachment() {
        return fileAttachment;
    }

    public void setFileAttachment(String fileAttachment) {
        this.fileAttachment = fileAttachment;
    }

//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }
}
