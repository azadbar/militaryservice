package com.military.militaryservice.orderInfo;

import androidx.room.Embedded;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class HomeInfo implements Serializable {

    @SerializedName("id")
    private int homeInfoId;
    @SerializedName("manualId")
    private int homeInfoManualId;
//    private String lastUpdateDateTime;
    @Embedded(prefix = "homeInfoAddressInfo_")
    private AddressInfo addressInfo;
    private String extraDescription;
    @TypeConverters(ExtraHomeAddressDataConverter.class)
    private List<ExtraHomeAddressInfosItem> extraHomeAddressInfos;
//    private String creationDateTime;


    public int getHomeInfoId() {
        return homeInfoId;
    }

    public void setHomeInfoId(int homeInfoId) {
        this.homeInfoId = homeInfoId;
    }

    public int getHomeInfoManualId() {
        return homeInfoManualId;
    }

    public void setHomeInfoManualId(int homeInfoManualId) {
        this.homeInfoManualId = homeInfoManualId;
    }

//    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }

    public AddressInfo getAddressInfo() {
        return addressInfo;
    }

    public void setAddressInfo(AddressInfo addressInfo) {
        this.addressInfo = addressInfo;
    }

    public String getExtraDescription() {
        return extraDescription;
    }

    public void setExtraDescription(String extraDescription) {
        this.extraDescription = extraDescription;
    }

    public List<ExtraHomeAddressInfosItem> getExtraHomeAddressInfos() {
        return extraHomeAddressInfos;
    }

    public void setExtraHomeAddressInfos(List<ExtraHomeAddressInfosItem> extraHomeAddressInfos) {
        this.extraHomeAddressInfos = extraHomeAddressInfos;
    }


//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }
}