package com.military.militaryservice.orderInfo;

import androidx.room.Embedded;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PersonInfo implements Serializable {

    @PrimaryKey
    @SerializedName("id")
    private int personInfoId;
    @SerializedName("manualId")
    private int personInfoManualId;
    private String lastName;
    private String fatherName;
    private String nationalCode;
    private String habits;
//    private String wifeInfo;
    private String protection;
    @Embedded(prefix = "personInfoWorkInfo_")
    private WorkInfo workInfo;
    private String sufficiency;
    @Embedded(prefix = "personInfoEyeColor_")
    private EyeColor eyeColor;
    private String faceStyle;
    private String nikName;
    //    private String creationDateTime;
    @Embedded(prefix = "personInfoHeight_")
    private Height height;
    //    private String lastUpdateDateTime;
    private String mobileNumbers;
    @Embedded(prefix = "personInfoMarriedStatus_")
    private MarriedStatus marriedStatus;
    @Embedded(prefix = "personInfoWeight_")
    private Weight weight;
    @Embedded(prefix = "personInfoSecondNationality_")
    private SecondNationality secondNationality;
    @Embedded(prefix = "personInfoSecondHomeInfo_")
    private HomeInfo homeInfo;
    private String otherInformation;
    @Embedded(prefix = "personInfoSecondReligion_")
    private Religion religion;
    private String firstName;
    private int numberOfChildren;
    @Embedded(prefix = "personInfoSecondNationality_")
    private Nationality nationality;
    @Embedded(prefix = "personInfoSecondBulk_")
    private Bulk bulk;
    private String headStyle;


    public int getPersonInfoId() {
        return personInfoId;
    }

    public void setPersonInfoId(int personInfoId) {
        this.personInfoId = personInfoId;
    }

    public int getPersonInfoManualId() {
        return personInfoManualId;
    }

    public void setPersonInfoManualId(int personInfoManualId) {
        this.personInfoManualId = personInfoManualId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getNationalCode() {
        return nationalCode;
    }

    public void setNationalCode(String nationalCode) {
        this.nationalCode = nationalCode;
    }

    public String getHabits() {
        return habits;
    }

    public void setHabits(String habits) {
        this.habits = habits;
    }

//    public String getWifeInfo() {
//        return wifeInfo;
//    }
//
//    public void setWifeInfo(String wifeInfo) {
//        this.wifeInfo = wifeInfo;
//    }

    public String getProtection() {
        return protection;
    }

    public void setProtection(String protection) {
        this.protection = protection;
    }

    public WorkInfo getWorkInfo() {
        return workInfo;
    }

    public void setWorkInfo(WorkInfo workInfo) {
        this.workInfo = workInfo;
    }

    public String getSufficiency() {
        return sufficiency;
    }

    public void setSufficiency(String sufficiency) {
        this.sufficiency = sufficiency;
    }

    public EyeColor getEyeColor() {
        return eyeColor;
    }

    public void setEyeColor(EyeColor eyeColor) {
        this.eyeColor = eyeColor;
    }

    public String getFaceStyle() {
        return faceStyle;
    }

    public void setFaceStyle(String faceStyle) {
        this.faceStyle = faceStyle;
    }

    public String getNikName() {
        return nikName;
    }

    public void setNikName(String nikName) {
        this.nikName = nikName;
    }


//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }

    public Height getHeight() {
        return height;
    }

    public void setHeight(Height height) {
        this.height = height;
    }

//    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }

    public String getMobileNumbers() {
        return mobileNumbers;
    }

    public void setMobileNumbers(String mobileNumbers) {
        this.mobileNumbers = mobileNumbers;
    }

    public MarriedStatus getMarriedStatus() {
        return marriedStatus;
    }

    public void setMarriedStatus(MarriedStatus marriedStatus) {
        this.marriedStatus = marriedStatus;
    }

    public Weight getWeight() {
        return weight;
    }

    public void setWeight(Weight weight) {
        this.weight = weight;
    }

    public SecondNationality getSecondNationality() {
        return secondNationality;
    }

    public void setSecondNationality(SecondNationality secondNationality) {
        this.secondNationality = secondNationality;
    }

    public HomeInfo getHomeInfo() {
        return homeInfo;
    }

    public void setHomeInfo(HomeInfo homeInfo) {
        this.homeInfo = homeInfo;
    }

    public String getOtherInformation() {
        return otherInformation;
    }

    public void setOtherInformation(String otherInformation) {
        this.otherInformation = otherInformation;
    }

    public Religion getReligion() {
        return religion;
    }

    public void setReligion(Religion religion) {
        this.religion = religion;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getNumberOfChildren() {
        return numberOfChildren;
    }

    public void setNumberOfChildren(int numberOfChildren) {
        this.numberOfChildren = numberOfChildren;
    }

    public Nationality getNationality() {
        return nationality;
    }

    public void setNationality(Nationality nationality) {
        this.nationality = nationality;
    }

    public Bulk getBulk() {
        return bulk;
    }

    public void setBulk(Bulk bulk) {
        this.bulk = bulk;
    }

    public String getHeadStyle() {
        return headStyle;
    }

    public void setHeadStyle(String headStyle) {
        this.headStyle = headStyle;
    }
}
