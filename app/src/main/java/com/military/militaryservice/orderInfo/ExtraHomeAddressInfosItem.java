package com.military.militaryservice.orderInfo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ExtraHomeAddressInfosItem implements Serializable {

    @SerializedName("id")
    private int extraHomeAddressInfosId;
    @SerializedName("manualId")
    private int extraHomeAddressInfosManualId;
//    private String lastUpdateDateTime;
    private String locationInfo;
    private String description;

    private String fileAttachment;
//    private String creationDateTime;

    public int getExtraHomeAddressInfosId() {
        return extraHomeAddressInfosId;
    }

    public void setExtraHomeAddressInfosId(int extraHomeAddressInfosId) {
        this.extraHomeAddressInfosId = extraHomeAddressInfosId;
    }

    public int getExtraHomeAddressInfosManualId() {
        return extraHomeAddressInfosManualId;
    }

    public void setExtraHomeAddressInfosManualId(int extraHomeAddressInfosManualId) {
        this.extraHomeAddressInfosManualId = extraHomeAddressInfosManualId;
    }

//    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }

    public String getLocationInfo() {
        return locationInfo;
    }

    public void setLocationInfo(String locationInfo) {
        this.locationInfo = locationInfo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getFileAttachment() {
        return fileAttachment;
    }

    public void setFileAttachment(String fileAttachment) {
        this.fileAttachment = fileAttachment;
    }

//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }
}
