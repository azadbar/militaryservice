package com.military.militaryservice.orderInfo;

import androidx.room.Embedded;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SupplementaryInfo implements Serializable {


    @SerializedName("id")
    private int supplementaryInfoId;
    @SerializedName("manualId")
    private int supplementaryInfoManualId;

    //    private String lastUpdateDateTime;
    private String extraResearch;
    private String judicialHistoryDescription;
    private String supplementaryInfoExtraInfos;
    private String chargeType;
//    @Embedded(prefix = "supplementaryInfoSupplementaryInfo_")
//    private SupplementaryInfo vehicleInfo;
    private String placeExtraInfo;
    private String fileAttachments;
    private String individualCombatAbility;
    private String typeOfFile;
    private String judicialHistory;
    private String effectiveCases;
    @TypeConverters(StampingGroundsDataConverter.class)
    private List<StampingGroundsItem> stampingGrounds;
    private String communicationInfo;
    private String furtherDetails;
    @Embedded(prefix = "supplementaryInfoRelatedVehicleInfo_")
    private RelatedVehicleInfo relatedVehicleInfo;
    private String morality;
    private String armedStatus;
    private String policeAwareness;
//    private String creationDateTime;


    public int getSupplementaryInfoId() {
        return supplementaryInfoId;
    }

    public void setSupplementaryInfoId(int supplementaryInfoId) {
        this.supplementaryInfoId = supplementaryInfoId;
    }

    public int getSupplementaryInfoManualId() {
        return supplementaryInfoManualId;
    }

    public void setSupplementaryInfoManualId(int supplementaryInfoManualId) {
        this.supplementaryInfoManualId = supplementaryInfoManualId;
    }

//    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }

    public String getExtraResearch() {
        return extraResearch;
    }

    public void setExtraResearch(String extraResearch) {
        this.extraResearch = extraResearch;
    }

    public String getJudicialHistoryDescription() {
        return judicialHistoryDescription;
    }

    public void setJudicialHistoryDescription(String judicialHistoryDescription) {
        this.judicialHistoryDescription = judicialHistoryDescription;
    }

    public String getSupplementaryInfoExtraInfos() {
        return supplementaryInfoExtraInfos;
    }

    public void setSupplementaryInfoExtraInfos(String supplementaryInfoExtraInfos) {
        this.supplementaryInfoExtraInfos = supplementaryInfoExtraInfos;
    }

    public String getChargeType() {
        return chargeType;
    }

    public void setChargeType(String chargeType) {
        this.chargeType = chargeType;
    }

//    public SupplementaryInfo getVehicleInfo() {
//        return vehicleInfo;
//    }
//
//    public void setVehicleInfo(SupplementaryInfo vehicleInfo) {
//        this.vehicleInfo = vehicleInfo;
//    }

    public String getPlaceExtraInfo() {
        return placeExtraInfo;
    }

    public void setPlaceExtraInfo(String placeExtraInfo) {
        this.placeExtraInfo = placeExtraInfo;
    }

    public String getFileAttachments() {
        return fileAttachments;
    }

    public void setFileAttachments(String fileAttachments) {
        this.fileAttachments = fileAttachments;
    }

    public String getIndividualCombatAbility() {
        return individualCombatAbility;
    }

    public void setIndividualCombatAbility(String individualCombatAbility) {
        this.individualCombatAbility = individualCombatAbility;
    }

    public String getTypeOfFile() {
        return typeOfFile;
    }

    public void setTypeOfFile(String typeOfFile) {
        this.typeOfFile = typeOfFile;
    }

    public String getJudicialHistory() {
        return judicialHistory;
    }

    public void setJudicialHistory(String judicialHistory) {
        this.judicialHistory = judicialHistory;
    }

    public String getEffectiveCases() {
        return effectiveCases;
    }

    public void setEffectiveCases(String effectiveCases) {
        this.effectiveCases = effectiveCases;
    }

    public List<StampingGroundsItem> getStampingGrounds() {
        return stampingGrounds;
    }

    public void setStampingGrounds(List<StampingGroundsItem> stampingGrounds) {
        this.stampingGrounds = stampingGrounds;
    }


    public String getCommunicationInfo() {
        return communicationInfo;
    }

    public void setCommunicationInfo(String communicationInfo) {
        this.communicationInfo = communicationInfo;
    }

    public String getFurtherDetails() {
        return furtherDetails;
    }

    public void setFurtherDetails(String furtherDetails) {
        this.furtherDetails = furtherDetails;
    }

    public RelatedVehicleInfo getRelatedVehicleInfo() {
        return relatedVehicleInfo;
    }

    public void setRelatedVehicleInfo(RelatedVehicleInfo relatedVehicleInfo) {
        this.relatedVehicleInfo = relatedVehicleInfo;
    }

    public String getMorality() {
        return morality;
    }

    public void setMorality(String morality) {
        this.morality = morality;
    }

    public String getArmedStatus() {
        return armedStatus;
    }

    public void setArmedStatus(String armedStatus) {
        this.armedStatus = armedStatus;
    }


    public String getPoliceAwareness() {
        return policeAwareness;
    }

    public void setPoliceAwareness(String policeAwareness) {
        this.policeAwareness = policeAwareness;
    }

//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }
}