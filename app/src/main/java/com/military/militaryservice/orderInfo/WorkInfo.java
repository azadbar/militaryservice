package com.military.militaryservice.orderInfo;

import androidx.room.Embedded;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class WorkInfo implements Serializable {

    @SerializedName("id")
    private int workInfoId;
    @SerializedName("manualId")
    private int workInfoManualId;
//    private String lastUpdateDateTime;
    @Embedded(prefix = "workInfoAddressInfo_")
    private AddressInfo addressInfo;
    private String jobWithDetails;
    private String responsibility;
    @Embedded(prefix = "workInfoJobKind_")
    private JobKind jobKind;
    private String workTimeFrom;
    private String workTimeTo;
//    private String creationDateTime;
    @TypeConverters(ExtraWorkAddressDataConverter.class)
    private List<ExtraWorkAddressInfosItem> extraWorkAddressInfos;



    public int getWorkInfoId() {
        return workInfoId;
    }

    public void setWorkInfoId(int workInfoId) {
        this.workInfoId = workInfoId;
    }

    public int getWorkInfoManualId() {
        return workInfoManualId;
    }

    public void setWorkInfoManualId(int workInfoManualId) {
        this.workInfoManualId = workInfoManualId;
    }

//    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }

    public AddressInfo getAddressInfo() {
        return addressInfo;
    }

    public void setAddressInfo(AddressInfo addressInfo) {
        this.addressInfo = addressInfo;
    }

    public String getJobWithDetails() {
        return jobWithDetails;
    }

    public void setJobWithDetails(String jobWithDetails) {
        this.jobWithDetails = jobWithDetails;
    }

    public String getResponsibility() {
        return responsibility;
    }

    public void setResponsibility(String responsibility) {
        this.responsibility = responsibility;
    }

    public JobKind getJobKind() {
        return jobKind;
    }

    public void setJobKind(JobKind jobKind) {
        this.jobKind = jobKind;
    }

    public String getWorkTimeFrom() {
        return workTimeFrom;
    }

    public void setWorkTimeFrom(String workTimeFrom) {
        this.workTimeFrom = workTimeFrom;
    }


    public String getWorkTimeTo() {
        return workTimeTo;
    }

    public void setWorkTimeTo(String workTimeTo) {
        this.workTimeTo = workTimeTo;
    }

//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }

    public List<ExtraWorkAddressInfosItem> getExtraWorkAddressInfos() {
        return extraWorkAddressInfos;
    }

    public void setExtraWorkAddressInfos(List<ExtraWorkAddressInfosItem> extraWorkAddressInfos) {
        this.extraWorkAddressInfos = extraWorkAddressInfos;
    }
}