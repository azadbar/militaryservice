package com.military.militaryservice.orderInfo;

import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Bulk implements Serializable {

    @SerializedName("id")
    private int bulkId;
    @SerializedName("manualId")
    private int bulkManualId;
//    private String lastUpdateDateTime;
    private String code;
    private String value;
//    private String creationDateTime;

    public int getBulkId() {
        return bulkId;
    }

    public void setBulkId(int bulkId) {
        this.bulkId = bulkId;
    }

    public int getBulkManualId() {
        return bulkManualId;
    }

    public void setBulkManualId(int bulkManualId) {
        this.bulkManualId = bulkManualId;
    }

//    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }
}
