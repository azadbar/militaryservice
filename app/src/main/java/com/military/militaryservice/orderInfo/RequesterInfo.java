package com.military.militaryservice.orderInfo;

import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RequesterInfo implements Serializable {

    @SerializedName("id")
    private int requesterInfoId;
    @SerializedName("manualId")
    private int requesterInfoManualId;
//    private String lastUpdateDateTime;
    private String requesterMobileNumber;
    private String requesterCode;
    private String requesterInfoExtraInfos;
    private String requesterIdentifier;
    private String requesterJobPhoneNumber;
//    private String creationDateTime;

    public int getRequesterInfoId() {
        return requesterInfoId;
    }

    public void setRequesterInfoId(int requesterInfoId) {
        this.requesterInfoId = requesterInfoId;
    }

    public int getRequesterInfoManualId() {
        return requesterInfoManualId;
    }

    public void setRequesterInfoManualId(int requesterInfoManualId) {
        this.requesterInfoManualId = requesterInfoManualId;
    }

//    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }

    public String getRequesterMobileNumber() {
        return requesterMobileNumber;
    }

    public void setRequesterMobileNumber(String requesterMobileNumber) {
        this.requesterMobileNumber = requesterMobileNumber;
    }

    public String getRequesterCode() {
        return requesterCode;
    }

    public void setRequesterCode(String requesterCode) {
        this.requesterCode = requesterCode;
    }

    public String getRequesterInfoExtraInfos() {
        return requesterInfoExtraInfos;
    }

    public void setRequesterInfoExtraInfos(String requesterInfoExtraInfos) {
        this.requesterInfoExtraInfos = requesterInfoExtraInfos;
    }

    public String getRequesterIdentifier() {
        return requesterIdentifier;
    }

    public void setRequesterIdentifier(String requesterIdentifier) {
        this.requesterIdentifier = requesterIdentifier;
    }


    public String getRequesterJobPhoneNumber() {
        return requesterJobPhoneNumber;
    }

    public void setRequesterJobPhoneNumber(String requesterJobPhoneNumber) {
        this.requesterJobPhoneNumber = requesterJobPhoneNumber;
    }

//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }
}
