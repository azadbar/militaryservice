package com.military.militaryservice.orderInfo;

import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class QuestionInfosItem implements Serializable {

    @SerializedName("id")
    private int questionInfosId;
    @SerializedName("manualId")
    private int questionInfosManualId;
//    private String lastUpdateDateTime;
//    private String questionResultInfo;
    private String questionIdentifier;
    private String questionInfo;
    private String user;
//    private String creationDateTime;

    public int getQuestionInfosId() {
        return questionInfosId;
    }

    public void setQuestionInfosId(int questionInfosId) {
        this.questionInfosId = questionInfosId;
    }

    public int getQuestionInfosManualId() {
        return questionInfosManualId;
    }

    public void setQuestionInfosManualId(int questionInfosManualId) {
        this.questionInfosManualId = questionInfosManualId;
    }

//    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }

//    public String getQuestionResultInfo() {
//        return questionResultInfo;
//    }
//
//    public void setQuestionResultInfo(String questionResultInfo) {
//        this.questionResultInfo = questionResultInfo;
//    }

    public String getQuestionIdentifier() {
        return questionIdentifier;
    }

    public void setQuestionIdentifier(String questionIdentifier) {
        this.questionIdentifier = questionIdentifier;
    }


    public String getQuestionInfo() {
        return questionInfo;
    }

    public void setQuestionInfo(String questionInfo) {
        this.questionInfo = questionInfo;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }
}
