package com.military.militaryservice.orderInfo;

import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MarriedStatus implements Serializable {

    @SerializedName("id")
    private int marriedStatusId;
    @SerializedName("manualId")
    private int marriedStatusManualId;
//    private Serializable lastUpdateDateTime;
    private String code;

    private String value;
//    private String creationDateTime;


    public int getMarriedStatusId() {
        return marriedStatusId;
    }

    public void setMarriedStatusId(int marriedStatusId) {
        this.marriedStatusId = marriedStatusId;
    }

    public int getMarriedStatusManualId() {
        return marriedStatusManualId;
    }

    public void setMarriedStatusManualId(int marriedStatusManualId) {
        this.marriedStatusManualId = marriedStatusManualId;
    }

//    public Serializable getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(Serializable lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }
//
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }
}
