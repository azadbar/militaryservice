package com.military.militaryservice.orderInfo;

import androidx.room.Embedded;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class City implements Serializable {

    @PrimaryKey
    @SerializedName("id")
    private int cityId;
    @SerializedName("manualId")
    private int cityManualId;
    //    private String lastUpdateDateTime;
    @Embedded(prefix = "cityProvince_")
    private Province province;
    @SerializedName("name")
    private String cityName;
    @Embedded(prefix = "cityCounty_")
    private County county;
//    private String creationDateTime;


    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public int getCityManualId() {
        return cityManualId;
    }

    public void setCityManualId(int cityManualId) {
        this.cityManualId = cityManualId;
    }

//    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public String getName() {
        return cityName;
    }

    public void setName(String name) {
        this.cityName = name;
    }

    public County getCounty() {
        return county;
    }

    public void setCounty(County county) {
        this.county = county;
    }


//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }
}
