package com.military.militaryservice.orderInfo;

import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StampingGroundsItem implements Serializable {


    @SerializedName("id")
    private int stampingGroundsId;
    @SerializedName("manualId")
    private int stampingGroundsManualId;
//    private String lastUpdateDateTime;
//    private String locationInfo;
    private String description;
//    private String fileAttachment;
//    private String creationDateTime;

    public int getStampingGroundsId() {
        return stampingGroundsId;
    }

    public void setStampingGroundsId(int stampingGroundsId) {
        this.stampingGroundsId = stampingGroundsId;
    }

    public int getStampingGroundsManualId() {
        return stampingGroundsManualId;
    }

    public void setStampingGroundsManualId(int stampingGroundsManualId) {
        this.stampingGroundsManualId = stampingGroundsManualId;
    }

//    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }

//    public String getLocationInfo() {
//        return locationInfo;
//    }
//
//    public void setLocationInfo(String locationInfo) {
//        this.locationInfo = locationInfo;
//    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


//    public String getFileAttachment() {
//        return fileAttachment;
//    }
//
//    public void setFileAttachment(String fileAttachment) {
//        this.fileAttachment = fileAttachment;
//    }

//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }
}
