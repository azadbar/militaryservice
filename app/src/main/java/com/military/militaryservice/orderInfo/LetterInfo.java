package com.military.militaryservice.orderInfo;

import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LetterInfo implements Serializable {

    @SerializedName("id")
    private int letterInfoId;
    @SerializedName("manualId")
    private int manualIdManualId;
//    private String lastUpdateDateTime;
    private String letterDate;
    private String issuerPlace;

//    private String creationDateTime;
    private String letterNumber;


    public int getLetterInfoId() {
        return letterInfoId;
    }

    public void setLetterInfoId(int letterInfoId) {
        this.letterInfoId = letterInfoId;
    }

    public int getManualIdManualId() {
        return manualIdManualId;
    }

    public void setManualIdManualId(int manualIdManualId) {
        this.manualIdManualId = manualIdManualId;
    }

//    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }

    public String getLetterDate() {
        return letterDate;
    }

    public void setLetterDate(String letterDate) {
        this.letterDate = letterDate;
    }

    public String getIssuerPlace() {
        return issuerPlace;
    }

    public void setIssuerPlace(String issuerPlace) {
        this.issuerPlace = issuerPlace;
    }


//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }
//
    public String getLetterNumber() {
        return letterNumber;
    }

    public void setLetterNumber(String letterNumber) {
        this.letterNumber = letterNumber;
    }
}
