package com.military.militaryservice.orderInfo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Owner implements Serializable {

    @SerializedName("id")
    private int ownerId;
    @SerializedName("manualId")
    private int ownerIdManualId;
    private String lastUpdateDateTime;
    private String code;
    private String value;
    private String creationDateTime;

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public int getOwnerIdManualId() {
        return ownerIdManualId;
    }

    public void setOwnerIdManualId(int ownerIdManualId) {
        this.ownerIdManualId = ownerIdManualId;
    }

    public String getLastUpdateDateTime() {
        return lastUpdateDateTime;
    }

    public void setLastUpdateDateTime(String lastUpdateDateTime) {
        this.lastUpdateDateTime = lastUpdateDateTime;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(String creationDateTime) {
        this.creationDateTime = creationDateTime;
    }
}
