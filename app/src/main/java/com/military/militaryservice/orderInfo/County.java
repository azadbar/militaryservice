package com.military.militaryservice.orderInfo;

import androidx.room.Embedded;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class County implements Serializable {

    @SerializedName("id")
    private int countyId;
    @SerializedName("manualId")
    private int countyManualId;
    //    private String lastUpdateDateTime;
    @Embedded(prefix = "countyProvince_")
    private Province province;
    @SerializedName("name")
    private String countyName;

//    private String creationDateTime;


    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public int getCountyId() {
        return countyId;
    }

    public void setCountyId(int countyId) {
        this.countyId = countyId;
    }

    public int getCountyManualId() {
        return countyManualId;
    }

    public void setCountyManualId(int countyManualId) {
        this.countyManualId = countyManualId;
    }

//    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public String getName() {
        return countyName;
    }

    public void setName(String name) {
        this.countyName = name;
    }


//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }
}
