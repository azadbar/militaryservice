package com.military.militaryservice.orderInfo;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.io.Serializable;
import java.util.List;

@Entity(tableName = "ProjectResponse")
public class ProjectResponse implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private int manualId;
    private String lastUpdateDateTime;
    @TypeConverters(QuestionInfosDataConverter.class)
    private List<QuestionInfosItem> questionInfos;
    @Embedded(prefix = "projectResponseRequesterInfo_")
    private RequesterInfo requesterInfo;
    private String complementryReport;
    private String orderIdentifier;
    @Embedded(prefix = "projectResponseSupplementaryInfo_")
    private SupplementaryInfo supplementaryInfo;
    @Embedded(prefix = "projectResponseLetterInfo_")
    private LetterInfo letterInfo;
    @Embedded(prefix = "projectResponsePersonInfo_")
    private PersonInfo personInfo;
    private String receiverUsers;
    private String requesterUser;
    private String registerUser;
    private String creationDateTime;
    private String orderAssignmentInfos;

    public String getLastUpdateDateTime() {
        return lastUpdateDateTime;
    }

    public void setLastUpdateDateTime(String lastUpdateDateTime) {
        this.lastUpdateDateTime = lastUpdateDateTime;
    }

    public List<QuestionInfosItem> getQuestionInfos() {
        return questionInfos;
    }

    public void setQuestionInfos(List<QuestionInfosItem> questionInfos) {
        this.questionInfos = questionInfos;
    }

    public RequesterInfo getRequesterInfo() {
        return requesterInfo;
    }

    public void setRequesterInfo(RequesterInfo requesterInfo) {
        this.requesterInfo = requesterInfo;
    }

    public String getComplementryReport() {
        return complementryReport;
    }

    public void setComplementryReport(String complementryReport) {
        this.complementryReport = complementryReport;
    }

    public String getOrderIdentifier() {
        return orderIdentifier;
    }

    public void setOrderIdentifier(String orderIdentifier) {
        this.orderIdentifier = orderIdentifier;
    }

    public SupplementaryInfo getSupplementaryInfo() {
        return supplementaryInfo;
    }

    public void setSupplementaryInfo(SupplementaryInfo supplementaryInfo) {
        this.supplementaryInfo = supplementaryInfo;
    }

    public LetterInfo getLetterInfo() {
        return letterInfo;
    }

    public void setLetterInfo(LetterInfo letterInfo) {
        this.letterInfo = letterInfo;
    }

    public PersonInfo getPersonInfo() {
        return personInfo;
    }

    public void setPersonInfo(PersonInfo personInfo) {
        this.personInfo = personInfo;
    }

    public String getReceiverUsers() {
        return receiverUsers;
    }

    public void setReceiverUsers(String receiverUsers) {
        this.receiverUsers = receiverUsers;
    }

    public int getManualId() {
        return manualId;
    }

    public void setManualId(int manualId) {
        this.manualId = manualId;
    }

    public String getRequesterUser() {
        return requesterUser;
    }

    public void setRequesterUser(String requesterUser) {
        this.requesterUser = requesterUser;
    }

    public String getRegisterUser() {
        return registerUser;
    }

    public void setRegisterUser(String registerUser) {
        this.registerUser = registerUser;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(String creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    public String getOrderAssignmentInfos() {
        return orderAssignmentInfos;
    }

    public void setOrderAssignmentInfos(String orderAssignmentInfos) {
        this.orderAssignmentInfos = orderAssignmentInfos;
    }
}