package com.military.militaryservice.orderInfo;

import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class VehicleInfo implements Serializable {

    @PrimaryKey
    @SerializedName("id")
    private int vehicleInfoId;
    @SerializedName("manualId")
    private int vehicleInfoManualId;

    private String owner;
//    private String lastUpdateDateTime;
//    private String secondPartVehicleNumber;
    @SerializedName("color")
    private String vehicleInfColor;
    @SerializedName("firstPartVehicleNumber")
    private String vehicFirstPartVehicleNumber;
    @SerializedName("thirdPartVehicleNumber")
    private String vehicleThirdPartVehicleNumber;
    @SerializedName("forthPartVehicleNumber")
    private String vehicleForthPartVehicleNumber;
    private String model;
    @SerializedName("type")
    private String vehicleType;
//    private String creationDateTime;


    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleInfColor() {
        return vehicleInfColor;
    }

    public void setVehicleInfColor(String vehicleInfColor) {
        this.vehicleInfColor = vehicleInfColor;
    }

    public String getVehicFirstPartVehicleNumber() {
        return vehicFirstPartVehicleNumber;
    }

    public void setVehicFirstPartVehicleNumber(String vehicFirstPartVehicleNumber) {
        this.vehicFirstPartVehicleNumber = vehicFirstPartVehicleNumber;
    }

    public String getVehicleThirdPartVehicleNumber() {
        return vehicleThirdPartVehicleNumber;
    }

    public void setVehicleThirdPartVehicleNumber(String vehicleThirdPartVehicleNumber) {
        this.vehicleThirdPartVehicleNumber = vehicleThirdPartVehicleNumber;
    }

    public String getVehicleForthPartVehicleNumber() {
        return vehicleForthPartVehicleNumber;
    }

    public void setVehicleForthPartVehicleNumber(String vehicleForthPartVehicleNumber) {
        this.vehicleForthPartVehicleNumber = vehicleForthPartVehicleNumber;
    }

    public int getVehicleInfoId() {
        return vehicleInfoId;
    }

    public void setVehicleInfoId(int vehicleInfoId) {
        this.vehicleInfoId = vehicleInfoId;
    }

    public int getVehicleInfoManualId() {
        return vehicleInfoManualId;
    }

    public void setVehicleInfoManualId(int vehicleInfoManualId) {
        this.vehicleInfoManualId = vehicleInfoManualId;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }


//    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }

//    public String getSecondPartVehicleNumber() {
//        return secondPartVehicleNumber;
//    }
//
//    public void setSecondPartVehicleNumber(String secondPartVehicleNumber) {
//        this.secondPartVehicleNumber = secondPartVehicleNumber;
//    }

    public String getColor() {
        return vehicleInfColor;
    }

    public void setColor(String color) {
        this.vehicleInfColor = color;
    }

    public String getFirstPartVehicleNumber() {
        return vehicFirstPartVehicleNumber;
    }

    public void setFirstPartVehicleNumber(String firstPartVehicleNumber) {
        this.vehicFirstPartVehicleNumber = firstPartVehicleNumber;
    }

    public String getThirdPartVehicleNumber() {
        return vehicleThirdPartVehicleNumber;
    }

    public void setThirdPartVehicleNumber(String thirdPartVehicleNumber) {
        this.vehicleThirdPartVehicleNumber = thirdPartVehicleNumber;
    }

    public String getForthPartVehicleNumber() {
        return vehicleForthPartVehicleNumber;
    }

    public void setForthPartVehicleNumber(String forthPartVehicleNumber) {
        this.vehicleForthPartVehicleNumber = forthPartVehicleNumber;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }


    public String getType() {
        return vehicleType;
    }

    public void setType(String type) {
        this.vehicleType = type;
    }

//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }
}
