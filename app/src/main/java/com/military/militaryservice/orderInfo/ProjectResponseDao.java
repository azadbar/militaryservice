package com.military.militaryservice.orderInfo;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.military.militaryservice.model.PersonResponse;

import java.util.ArrayList;
import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface ProjectResponseDao {

    @Query("select * from  projectresponse")
    List<ProjectResponse> getPersonList();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllList(ArrayList<ProjectResponse> persons);

    @Insert(onConflict = REPLACE)
    void insertAddress(ProjectResponse persons);


    @Query("select * from projectresponse where id =:id")
    ProjectResponse getProject(int id);

    @Query("DELETE FROM projectresponse")
    public void nukeTable();


    @Delete
    void delete(ProjectResponse persons);

    @Query("delete from projectresponse where orderIdentifier =:id")
    void deleteItem(int id);

    @Update
    int updatePerson(ProjectResponse projectResponse);

}
