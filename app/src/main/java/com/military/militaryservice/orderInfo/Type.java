package com.military.militaryservice.orderInfo;

import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Type implements Serializable {

    @PrimaryKey
    @SerializedName("id")
    private int typeId;
    @SerializedName("manualIdId")
    private int typeManualId;
//    private String lastUpdateDateTime;
    private String code;
    private String value;
//    private String creationDateTime;

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public int getTypeManualId() {
        return typeManualId;
    }

    public void setTypeManualId(int typeManualId) {
        this.typeManualId = typeManualId;
    }

//    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }
}
