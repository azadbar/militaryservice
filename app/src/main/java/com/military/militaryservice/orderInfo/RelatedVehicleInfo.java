package com.military.militaryservice.orderInfo;

import androidx.room.Embedded;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RelatedVehicleInfo implements Serializable {

    @SerializedName("id")
    private int relatedVehicleInfoId;
    @SerializedName("manualId")
    private int relatedVehicleInfoManualId;
    @Embedded(prefix = "relatedVehicleInfoOwner_")
    private Owner owner;
    //    private String lastUpdateDateTime;
//    private String secondPartVehicleNumber;
    private String color;
    private String firstPartVehicleNumber;
    private String thirdPartVehicleNumber;
    private String forthPartVehicleNumber;
    @SerializedName("model")
    private String relatedVehicleInfoModel;
    @SerializedName("type")
    private String relatedVehicleInfoType;
//    private String creationDateTime;


    public String getRelatedVehicleInfoModel() {
        return relatedVehicleInfoModel;
    }

    public void setRelatedVehicleInfoModel(String relatedVehicleInfoModel) {
        this.relatedVehicleInfoModel = relatedVehicleInfoModel;
    }

    public String getRelatedVehicleInfoType() {
        return relatedVehicleInfoType;
    }

    public void setRelatedVehicleInfoType(String relatedVehicleInfoType) {
        this.relatedVehicleInfoType = relatedVehicleInfoType;
    }

    public int getRelatedVehicleInfoId() {
        return relatedVehicleInfoId;
    }

    public void setRelatedVehicleInfoId(int relatedVehicleInfoId) {
        this.relatedVehicleInfoId = relatedVehicleInfoId;
    }

    public int getRelatedVehicleInfoManualId() {
        return relatedVehicleInfoManualId;
    }

    public void setRelatedVehicleInfoManualId(int relatedVehicleInfoManualId) {
        this.relatedVehicleInfoManualId = relatedVehicleInfoManualId;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }


//    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }

//    public String getSecondPartVehicleNumber() {
//        return secondPartVehicleNumber;
//    }
//
//    public void setSecondPartVehicleNumber(String secondPartVehicleNumber) {
//        this.secondPartVehicleNumber = secondPartVehicleNumber;
//    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getFirstPartVehicleNumber() {
        return firstPartVehicleNumber;
    }

    public void setFirstPartVehicleNumber(String firstPartVehicleNumber) {
        this.firstPartVehicleNumber = firstPartVehicleNumber;
    }

    public String getThirdPartVehicleNumber() {
        return thirdPartVehicleNumber;
    }

    public void setThirdPartVehicleNumber(String thirdPartVehicleNumber) {
        this.thirdPartVehicleNumber = thirdPartVehicleNumber;
    }

    public String getForthPartVehicleNumber() {
        return forthPartVehicleNumber;
    }

    public void setForthPartVehicleNumber(String forthPartVehicleNumber) {
        this.forthPartVehicleNumber = forthPartVehicleNumber;
    }

    public String getModel() {
        return relatedVehicleInfoModel;
    }

    public void setModel(String model) {
        this.relatedVehicleInfoModel = model;
    }


    public String getType() {
        return relatedVehicleInfoType;
    }

    public void setType(String type) {
        this.relatedVehicleInfoType = type;
    }

//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }
}
