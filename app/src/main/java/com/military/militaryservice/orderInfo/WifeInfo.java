package com.military.militaryservice.orderInfo;

import androidx.room.Embedded;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class WifeInfo implements Serializable {

    @PrimaryKey
    @SerializedName("id")
    private int wifeInfoId;
    @SerializedName("manualId")
    private int wifeInfoManualId;
//    private String lastUpdateDateTime;
    @Embedded(prefix = "wifeInfoPersonInfo_")
    private PersonInfo personInfo;
//    private String creationDateTime;
    @TypeConverters(OtherWifesDataConverter.class)
    private List<OtherWifesItem> otherWifes;

    public int getWifeInfoId() {
        return wifeInfoId;
    }

    public void setWifeInfoId(int wifeInfoId) {
        this.wifeInfoId = wifeInfoId;
    }

    public int getWifeInfoManualId() {
        return wifeInfoManualId;
    }

    public void setWifeInfoManualId(int wifeInfoManualId) {
        this.wifeInfoManualId = wifeInfoManualId;
    }

//    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }


    public PersonInfo getPersonInfo() {
        return personInfo;
    }

    public void setPersonInfo(PersonInfo personInfo) {
        this.personInfo = personInfo;
    }

//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }

    public List<OtherWifesItem> getOtherWifes() {
        return otherWifes;
    }

    public void setOtherWifes(List<OtherWifesItem> otherWifes) {
        this.otherWifes = otherWifes;
    }
}