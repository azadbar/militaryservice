package com.military.militaryservice.orderInfo;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

public class ExtraHomeAddressDataConverter {

    private static Gson gson = new Gson();

    @TypeConverter
    public static List<ExtraHomeAddressInfosItem> stringToList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<ExtraHomeAddressInfosItem>>() {
        }.getType();
        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String ListToString(List<ExtraHomeAddressInfosItem> questionInfosItems) {
        return gson.toJson(questionInfosItems);
    }
}
