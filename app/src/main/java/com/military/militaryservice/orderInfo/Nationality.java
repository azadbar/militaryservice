package com.military.militaryservice.orderInfo;

import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Nationality implements Serializable {

    @SerializedName("id")
    private int nationalityId;
    @SerializedName("manualId")
    private int nationalityManualId;
//    private String lastUpdateDateTime;
//    private String code;
//    private String value;
//    private String creationDateTime;

    public int getNationalityId() {
        return nationalityId;
    }

    public void setNationalityId(int nationalityId) {
        this.nationalityId = nationalityId;
    }

    public int getNationalityManualId() {
        return nationalityManualId;
    }

    public void setNationalityManualId(int nationalityManualId) {
        this.nationalityManualId = nationalityManualId;
    }

//    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }

//    public String getCode() {
//        return code;
//    }
//
//    public void setCode(String code) {
//        this.code = code;
//    }


//    public String getValue() {
//        return value;
//    }
//
//    public void setValue(String value) {
//        this.value = value;
//    }

//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }
}
