package com.military.militaryservice.orderInfo;

import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class EyeColor implements Serializable {

    @SerializedName("id")
    private int eyeColorId;
    @SerializedName("manualId")
    private int eyeColorManualId;
//    private String lastUpdateDateTime;
    private String code;
    private String value;
//    private String creationDateTime;

    public int getEyeColorId() {
        return eyeColorId;
    }

    public void setEyeColorId(int eyeColorId) {
        this.eyeColorId = eyeColorId;
    }

    public int getEyeColorManualId() {
        return eyeColorManualId;
    }

    public void setEyeColorManualId(int eyeColorManualId) {
        this.eyeColorManualId = eyeColorManualId;
    }

//    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }
//
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }
}
