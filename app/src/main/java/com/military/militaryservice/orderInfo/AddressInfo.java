package com.military.militaryservice.orderInfo;

import androidx.room.Embedded;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AddressInfo implements Serializable {

    @SerializedName("id")
    private int addressInfoId;
    @SerializedName("manualId")
    private int addressInfoManualId;

//    private String lastUpdateDateTime;
//    private String locationInfo;
    private String moreDescription;
    @Embedded(prefix = "addressInfoCity_")
    private City city;
    private String subStreet;
    @Embedded(prefix = "addressInfoCounty_")
    private County county;
    private String unitNumber;
    private String mainStreet;
    @Embedded(prefix = "addressInfoType_")
    private Type type;
    private String phoneNumbers;
    private String placeNumber;
    @Embedded(prefix = "addressInfoUnit_")
    private Unit unit;
    @Embedded(prefix = "addressInfoProvince_")
    private Province province;
    private String alley;
//    private String creationDateTime;


    public int getAddressInfoId() {
        return addressInfoId;
    }

    public void setAddressInfoId(int addressInfoId) {
        this.addressInfoId = addressInfoId;
    }

    public int getAddressInfoManualId() {
        return addressInfoManualId;
    }

    public void setAddressInfoManualId(int addressInfoManualId) {
        this.addressInfoManualId = addressInfoManualId;
    }

//    public String getLastUpdateDateTime() {
//        return lastUpdateDateTime;
//    }
//
//    public void setLastUpdateDateTime(String lastUpdateDateTime) {
//        this.lastUpdateDateTime = lastUpdateDateTime;
//    }

//    public String getLocationInfo() {
//        return locationInfo;
//    }
//
//    public void setLocationInfo(String locationInfo) {
//        this.locationInfo = locationInfo;
//    }

    public String getMoreDescription() {
        return moreDescription;
    }

    public void setMoreDescription(String moreDescription) {
        this.moreDescription = moreDescription;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getSubStreet() {
        return subStreet;
    }

    public void setSubStreet(String subStreet) {
        this.subStreet = subStreet;
    }

    public County getCounty() {
        return county;
    }

    public void setCounty(County county) {
        this.county = county;
    }

    public String getUnitNumber() {
        return unitNumber;
    }

    public void setUnitNumber(String unitNumber) {
        this.unitNumber = unitNumber;
    }

    public String getMainStreet() {
        return mainStreet;
    }

    public void setMainStreet(String mainStreet) {
        this.mainStreet = mainStreet;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(String phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public String getPlaceNumber() {
        return placeNumber;
    }

    public void setPlaceNumber(String placeNumber) {
        this.placeNumber = placeNumber;
    }


    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }


    public String getAlley() {
        return alley;
    }

    public void setAlley(String alley) {
        this.alley = alley;
    }

//    public String getCreationDateTime() {
//        return creationDateTime;
//    }
//
//    public void setCreationDateTime(String creationDateTime) {
//        this.creationDateTime = creationDateTime;
//    }
}
