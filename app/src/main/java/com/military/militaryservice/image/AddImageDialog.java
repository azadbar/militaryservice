package com.military.militaryservice.image;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.rubensousa.bottomsheetbuilder.BottomSheetBuilder;
import com.github.rubensousa.bottomsheetbuilder.BottomSheetMenuDialog;
import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseLinearLayout;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.customView.CustomMultiLineEditText;
import com.military.militaryservice.database.Address;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.dialog.CustomDialog;
import com.military.militaryservice.enums.AttachmentType;
import com.military.militaryservice.maps.AddressAdapter;
import com.military.militaryservice.maps.OfflineActivity;
import com.military.militaryservice.projectList.show.PreviewActivity;
import com.military.militaryservice.resultInfo.FileAttachmentsItem;
import com.military.militaryservice.resultInfo.LocationDTO;
import com.military.militaryservice.utils.Constants;
import com.military.militaryservice.utils.EqualSpacingItemDecoration;
import com.military.militaryservice.utils.PermissionHandler;
import com.yalantis.ucrop.UCrop;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_OK;

/**
 * Created by a.azadbar on 10/7/2017.
 */

public class AddImageDialog extends DialogFragment implements ImageInsertAdapter.OnItemClickListener, ImageInsertAdapter.DeleteOnItemClickListener, AddressAdapter.OnItemClickListener {


    @BindView(R.id.tvHeadet)
    BaseTextView tvHeadet;
    //    @BindView(R.id.edtName)
//    CustomEditText edtName;
    @BindView(R.id.edtDescription)
    CustomMultiLineEditText edtDescription;
    @BindView(R.id.rvSelectedPic)
    RecyclerView rvSelectedPic;
    @BindView(R.id.llError)
    BaseRelativeLayout llError;
    @BindView(R.id.rlError)
    BaseLinearLayout rlError;
    @BindView(R.id.btnOk)
    BaseTextView btnOk;
    @BindView(R.id.btnCancel)
    BaseTextView btnCancel;
    @BindView(R.id.rlBtn)
    BaseLinearLayout rlBtn;
    private int REQUEST_CODE_PERMISSION = 2;
    private ImageInsertAdapter adapter;

    Unbinder unbinder;
    private Uri imageUri;
    private ArrayList<FileAttachmentsItem> fileAttachmentsItems = new ArrayList<>();
    private int maxImageCount = 500;
    private int id;
    private ArrayList<Address> address = new ArrayList<>();
    private AddressAdapter addressAddpter;
    //    private Image image;
    private boolean isEdit;
    private AppDatabase database;
    private updateListenerImage listener;
    private int selectedPosition;
    private FileAttachmentsItem selectedVoice;

    private FileAttachmentsItem attachmentsItem;
    private FileAttachmentsItem fileSelected;
    private int oldPosition;
    private long identifier;
    private String editText;
    private double latitude;
    private double longitude;
    private String locationAddressItem;
    private boolean doubleClick = false;
    private int position;


    public void setListener(updateListenerImage listener) {
        this.listener = listener;
    }

    public AddImageDialog() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_image_dialog, container, false);
        unbinder = ButterKnife.bind(this, v);
        setCancelable(false);

        database = AppDatabase.getInMemoryDatabase(getActivity());

        Bundle bundle = getArguments();
        if (bundle != null) {
            id = bundle.getInt("id");
            position = bundle.getInt("position");
            isEdit = bundle.getBoolean("isEdit");
            if (isEdit) {
                attachmentsItem = (FileAttachmentsItem) bundle.getSerializable("file");
                identifier = attachmentsItem.getIdentifier();
            } else {
                List<FileAttachmentsItem> selectImage = database.fileAttachmentDao().select(id, AttachmentType.IMAGE.getFormat());
                if (selectImage.size() > 0) {
                    fileAttachmentsItems.addAll(selectImage);
                }
            }
        }


        if (isEdit) {
            attachmentsItem.setDisable(true);
            fileAttachmentsItems.add(attachmentsItem);
            fileSelected = attachmentsItem;
            if (attachmentsItem.getGeoLocationDTO() != null) {
                latitude = attachmentsItem.getGeoLocationDTO().getLatitude();
                longitude = attachmentsItem.getGeoLocationDTO().getLongitude();
                locationAddressItem = attachmentsItem.getGeoLocationDTO().getAddress();
            }
            for (int i = 0; i < fileAttachmentsItems.size(); i++) {
                edtDescription.setBody(fileAttachmentsItems.get(i).getDescription());
//                edtName.setBody(images.get(i).getNameImage());
//                Address address = new Address();
//                address.setLatitude(images.get(i).getLat());
//                address.setLongitude(images.get(i).getLang());
//                address.setQuestionIdentifier(images.get(i).getQuestionIdentifier());
//                address.setDate(images.get(i).getDate());
//                address.setAddress(images.get(i).getImageLocation());
//                this.address.add(address);
            }
        }

        setImageCellSize();


        edtDescription.getEdtBody().addTextChangedListener(new TextWatcher() {
            private Timer timer = new Timer();
            private final long DELAY = 300; // milliseconds

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (timer != null) {
                    timer.cancel();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                timer.cancel();
                timer = new Timer();
                timer.schedule(
                        new TimerTask() {
                            @Override
                            public void run() {
                                if (getActivity() != null) {
                                    getActivity().runOnUiThread(() -> {
                                        if (isEdit) {
                                            database.fileAttachmentDao().setDescription(edtDescription.getValueString(), attachmentsItem.getIdentifier());
//                                            makeJson();
                                            listener.onInsetOk();
                                        } else {
                                            if (fileSelected != null) {
                                                database.fileAttachmentDao().setDescription(edtDescription.getValueString(), fileSelected.getIdentifier());
//                                                makeJson();
                                                listener.onInsetOk();
                                            } else {
                                                Toast.makeText(getActivity(), "لطفا یک فایل انتخاب کنید", Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                    });
                                }
                            }
                        },
                        DELAY
                );
            }
        });
        return v;
    }


    @Override
    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
        if (getActivity() != null && window != null && getContext() != null) {
            WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {
                Point size = Constants.getScreenSize(windowManager);
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                int width = (int) Math.min(size.x * 0.90, getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_width));
                int height = (int) Math.min(size.y * 0.60, getContext().getResources().getDimensionPixelSize(R.dimen.max_dialog_fragment_height));
                window.setLayout(width, height);
                window.setGravity(Gravity.CENTER);
            }
        }

        if (address.size() > 0) {
//            setDatAddressAdapter(address);
        }

    }

//    private void setDatAddressAdapter(ArrayList<Address> address) {
//        if (addressAddpter == null) {
//            addressAddpter = new AddressAdapter(address, this);
//            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
//            recycleAddress.setAdapter(addressAddpter);
//            recycleAddress.setLayoutManager(layoutManager);
//            recycleAddress.setHasFixedSize(true);
//        } else {
//            addressAddpter.setList(address);
//        }
//    }

    private void setImageCellSize() {
        int coulumCount = getResources().getInteger(R.integer.coloum_count_register_estate);
        int offset = getResources().getDimensionPixelSize(R.dimen.coulem_offset_recycle_view_in_register_estate);
        int cellWidth = getResources().getDimensionPixelSize(R.dimen.defualt_width_image_image_recycle_view_register_estate);
        if (getContext() != null) {

            WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {

                int screenWidth = Constants.getScreenSize(windowManager).x;
//                cellWidth = (int) ((screenWidth - (coulumCount + 1) * (offset * 2)) / (double) (coulumCount));
            }
        }
        rvSelectedPic.setHasFixedSize(true);
        adapter = new ImageInsertAdapter(getActivity(), fileAttachmentsItems, cellWidth, maxImageCount, this, this, isEdit);
        LinearLayoutManager layoutManager = new GridLayoutManager(getActivity(), coulumCount, RecyclerView.VERTICAL, false);
        rvSelectedPic.setLayoutManager(layoutManager);
        rvSelectedPic.setVerticalScrollBarEnabled(true);
        rvSelectedPic.addItemDecoration(new EqualSpacingItemDecoration(offset, EqualSpacingItemDecoration.GRID));
        rvSelectedPic.setAdapter(adapter);


    }


    @OnClick({R.id.btnOk, R.id.btnCancel, R.id.rlBtnAddAddress, R.id.imgAddImage, R.id.imgInsertDesc, R.id.imgClose})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgAddImage:
                if (getActivity() != null) {
                    if (PermissionHandler.hasAllPermissions(getActivity())) {
                        if (fileAttachmentsItems.size() < maxImageCount) {
                            showDialogForImageSelection();
                        }
                    } else {
                        PermissionHandler.requestPermissions(AddImageDialog.this, REQUEST_CODE_PERMISSION);
                    }
                }
                break;
            case R.id.btnOk:

                if (fileAttachmentsItems.size() > 0) {

                    for (int i = 0; i < fileAttachmentsItems.size(); i++) {
                        Intent intent1 = new Intent(getContext(), AddImageDialog.class);
                        if (isEdit) {
                            database.fileAttachmentDao().insert(fileAttachmentsItems.get(i));
                            database.fileAttachmentDao().updateTour(fileAttachmentsItems.get(i));
                            Toast.makeText(getActivity(), "عکس مورد نظر بروز شد", Toast.LENGTH_SHORT).show();

                            if (getTargetFragment() != null) {
                                getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, intent1);
                            }
                        } else {
                            if (getTargetFragment() != null) {
                                getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, intent1);
                            }
                            Toast.makeText(getActivity(), "عکس مورد نظر اضافه شد", Toast.LENGTH_SHORT).show();
                            database.fileAttachmentDao().insert(fileAttachmentsItems.get(i));
//                            makeJson();
                        }
                        listener.onInsetOk();
                        dismiss();
                    }
                }
                break;
            case R.id.btnCancel:
                dismiss();
                break;
            case R.id.rlBtnAddAddress:
                Intent intent = new Intent(getContext(), OfflineActivity.class);
                if (fileSelected != null) {
                    intent.putExtra("lat", latitude > 0 ? latitude : 0);
                    intent.putExtra("long", longitude > 0 ? longitude : 0);
                    intent.putExtra("bodyEditTxt", locationAddressItem != null ? locationAddressItem : 0);
                    intent.putExtra("isQuestion", true);
                } else {
                    intent.putExtra("isQuestion", false);
                }
                startActivityForResult(intent, Constants.LOCATION_INTENT);
                break;
            case R.id.imgInsertDesc:
                if (edtDescription.getValueString() != null) {
                    if (fileSelected != null) {
                        fileAttachmentsItems.get(selectedPosition).setDescription(edtDescription.getValueString());
                        database.fileAttachmentDao().setDescription(edtDescription.getValueString(), fileSelected.getIdentifier());
                        Toast.makeText(getContext(), "پیام اضافه شد", Toast.LENGTH_SHORT).show();
                        edtDescription.setBody(null);
//                        edtDescription.setTextHint("توضیح را وارد کنید");
                    } else {
                        Toast.makeText(getContext(), "یکی از فایل های زیر را انتخاب کنید", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(getContext(), "متن پیام را وارد کنید", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.imgClose:
                dismiss();
                break;
        }
    }


    @Override
    public void onItemClick(int position) {
        if (getContext() != null) {
            if (PermissionHandler.hasAllPermissions(getContext())) {
                if (fileAttachmentsItems.size() < maxImageCount) {
                    showDialogForImageSelection();
                }
            } else {
                PermissionHandler.requestPermissions(AddImageDialog.this, REQUEST_CODE_PERMISSION);
            }
        }
    }

    private void showDialogForImageSelection() {
        BottomSheetMenuDialog dialog = new BottomSheetBuilder(getActivity(), R.style.AppTheme_BottomSheetDialog)
                .setMode(BottomSheetBuilder.MODE_LIST)
                .setMenu(R.menu.menu_bottom_sheet)
                .setItemClickListener(item -> {
                    switch (item.getItemId()) {
                        case R.id.btnTakePhoto:
                            openCameraTake();
                            break;
                        case R.id.btnImageGallery:
                            openGalleryPhotos();
                            break;
                    }
                })
                .createDialog();
        dialog.show();
    }

    private void openCameraTake() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "دلتا");
        values.put(MediaStore.Images.Media.DESCRIPTION, "عکس خود را انتخاب کنید");
        imageUri = getContext().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, Constants.REQUEST_IMAGE_CAPTURE);
    }

    private void openGalleryPhotos() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, Constants.REQUEST_CODE_SEECTED_IMAGE_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_CODE_SEECTED_IMAGE_GALLERY) {
//                getImageFromGallery(data);
                if (data != null) {
//            try {
                    Uri selectedImage = data.getData();
                    String destinationFileName = "temp" + System.currentTimeMillis() + ".png";
                    if (selectedImage != null)
                        UCrop.of(selectedImage, Uri.fromFile(new File(getContext().getCacheDir(), destinationFileName))).withAspectRatio(1, 1)
                                .withMaxResultSize(1024, 1024)
                                .start(getActivity());
//                    getImageFromGallery(data);
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
//                uploadImageCameraRequest((encodeImage(bitmap)));
//            }
//            catch (IOException e) {
//                e.printStackTrace();
//            }
                }
            } else if (requestCode == Constants.REQUEST_IMAGE_CAPTURE) {
//                getImageFromCamera(data);
                String destinationFileName = "temp" + System.currentTimeMillis() + ".png";
                UCrop.of(imageUri, Uri.fromFile(new File(getContext().getCacheDir(), destinationFileName))).withAspectRatio(1, 1)
                        .withMaxResultSize(1024, 1024)
                        .start(getActivity());

            } else if (requestCode == Constants.LOCATION_INTENT) {
                Toast.makeText(getContext(), "آدرس ثبت شد", Toast.LENGTH_SHORT).show();
                LocationDTO add = (LocationDTO) data.getSerializableExtra("result");
                LocationDTO address = new LocationDTO();
                address.setLatitude(add.getLatitude());
                address.setLongitude(add.getLongitude());
                address.setAddress(add.getAddress());
                address.setDate(new Date());
                if (isEdit) {
                    database.fileAttachmentDao().setLocation(address.getLatitude(), address.getLongitude(), address.getAddress(), fileSelected.getIdentifier());
//                    makeJson();
                    listener.onInsetOk();
                } else {
                    if (fileSelected != null) {
                        database.fileAttachmentDao().setLocation(address.getLatitude(), address.getLongitude(), address.getAddress(), fileSelected.getIdentifier());
//                        makeJson();
                        listener.onInsetOk();
                    } else {
                        Toast.makeText(getActivity(), "لطفا یک فایل انتخاب کنید", Toast.LENGTH_SHORT).show();
                    }
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                getImageFromGallery(data);
            } else {
//                final Throwable cropError = UCrop.getError(data);
            }
        }


    }

    private void getImageFromGallery(Intent data) {

        long currentTime = System.currentTimeMillis();

        File dir = new File(Constants.storage_Dir_file_attachment + "/" + position + "/" + Constants.fileAttachments, currentTime + "");
        if (!dir.exists()) {
            dir.mkdirs();
        }


        String pathSaveIn = currentTime + "image.png";
        final Uri resultUri = UCrop.getOutput(data);
        InputStream imageStream = null;
        try {
            imageStream = getContext().getContentResolver().openInputStream(resultUri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
        File file = new File(dir, pathSaveIn);
        try {
            FileOutputStream out = new FileOutputStream(file);
            selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (isEdit) {
            database.fileAttachmentDao().delete(attachmentsItem);
            File fdelete = new File(Environment.getExternalStorageDirectory(), attachmentsItem.getFileFolder());
            if (fdelete.isDirectory()) {
                String[] children = fdelete.list();
                for (int i = 0; i < children.length; i++) {
                    new File(fdelete, children[i]).delete();
                }
            }
            try {
                FileUtils.deleteDirectory(fdelete);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        attachmentsItem = new FileAttachmentsItem();
        attachmentsItem.setIdentifier(currentTime);
        attachmentsItem.setDate(new Date());
        attachmentsItem.setFilename(currentTime + "");
        attachmentsItem.setFileAddress(Constants.SHAHED + Constants.fileQuestions + position + "/" + Constants.fileAttachments + currentTime + "/" + pathSaveIn);
        attachmentsItem.setFileFolder(Constants.SHAHED + Constants.fileQuestions + position + "/" + Constants.fileAttachments + currentTime);
        attachmentsItem.setAttachmentType(AttachmentType.IMAGE);
        attachmentsItem.setQuestionIdentifier(id);
        attachmentsItem.setFormat(AttachmentType.IMAGE.getFormat());
        attachmentsItem.setUserId(Constants.getUser(getContext()).getId());
//        attachmentsItem.setDisable(true);

        if (isEdit) {
            if (fileAttachmentsItems.size() > 0) {
                fileAttachmentsItems.clear();

                fileAttachmentsItems.add(attachmentsItem);
                Collections.reverse(fileAttachmentsItems);
                Toast.makeText(getActivity(), "عکس مورد ویرایش شد", Toast.LENGTH_SHORT).show();
                database.fileAttachmentDao().insert(attachmentsItem);
                fileSelected = attachmentsItem;
//                makeJson();
            } else {
                fileAttachmentsItems.add(attachmentsItem);
                Collections.reverse(fileAttachmentsItems);
                Toast.makeText(getActivity(), "عکس مورد نظر اضافه شد", Toast.LENGTH_SHORT).show();
                database.fileAttachmentDao().insert(attachmentsItem);
                fileSelected = attachmentsItem;
//                makeJson();
            }

        } else {
            fileAttachmentsItems.add(attachmentsItem);
            Collections.reverse(fileAttachmentsItems);
            Toast.makeText(getActivity(), "عکس مورد نظر اضافه شد", Toast.LENGTH_SHORT).show();
            database.fileAttachmentDao().insert(attachmentsItem);
            fileSelected = attachmentsItem;
//            makeJson();
        }

        adapter.notifyDataSetChanged();
        fileSelected = attachmentsItem;
        selectedPosition = 0;
        adapter.setSelectedImage(selectedPosition, fileSelected);

    }

//    private void makeJson() {
//        if (isEdit) {
//            JSONObject json = new JSONObject();
//            try {
//                json.put("identifier", attachmentsItem.getIdentifier());
//                json.put("filename", attachmentsItem.getFilename());
//                json.put("description", database.fileAttachmentDao().getDescription(attachmentsItem != null ? attachmentsItem.getIdentifier() : 0) != null ?
//                        database.fileAttachmentDao().getDescription(attachmentsItem.getIdentifier()).getDescription() : "");
//                json.put("format", AttachmentType.IMAGE.getFormat());
//                Writer output = null;
//                File jsonFile = new File(Environment.getExternalStorageDirectory() + "" +
//                        Constants.SHAHED + Constants.fileAttachments + attachmentsItem.getFilename(),
//                        "/" + attachmentsItem.getFilename() + ".json");
//                output = new BufferedWriter(new FileWriter(jsonFile));
//                output.write(json.toString());
//                output.close();
//            } catch (JSONException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        } else {
//            JSONObject json = new JSONObject();
//            try {
//                json.put("identifier", fileSelected.getIdentifier());
//                json.put("filename", fileSelected.getFilename());
//                json.put("description", database.fileAttachmentDao().getDescription(fileSelected != null ? fileSelected.getIdentifier() : 0) != null ?
//                        database.fileAttachmentDao().getDescription(fileSelected.getIdentifier()).getDescription() : "");
//                json.put("format", AttachmentType.IMAGE.getFormat());
//                Writer output = null;
//                File jsonFile = new File(Environment.getExternalStorageDirectory() + "" +
//                        Constants.SHAHED + Constants.fileAttachments + fileSelected.getFilename(),
//                        "/" + fileSelected.getFilename() + ".json");
//                output = new BufferedWriter(new FileWriter(jsonFile));
//                output.write(json.toString());
//                output.close();
//            } catch (JSONException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//
//    }

    private void getImageFromCamera(Intent data) {

        long currentTime = System.currentTimeMillis();
        File dir = new File(Constants.storage_Dir_file_attachment + "/" + position + "/" + Constants.fileAttachments, currentTime + "");
        if (!dir.exists()) {
            dir.mkdirs();
        }

        String pathSaveIn = System.currentTimeMillis() + "image.png";
        final Uri resultUri = imageUri;
        InputStream imageStream = null;
        try {
            imageStream = getActivity().getContentResolver().openInputStream(resultUri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
        File file = new File(dir, pathSaveIn);
        try {
            FileOutputStream out = new FileOutputStream(file);
            selectedImage.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        attachmentsItem = new FileAttachmentsItem();
        attachmentsItem.setIdentifier(currentTime);
        attachmentsItem.setDate(new Date());
        attachmentsItem.setFilename(currentTime + "");
        attachmentsItem.setFileAddress(Constants.SHAHED + Constants.fileQuestions  + position + "/" + Constants.fileAttachments + currentTime + "/" + pathSaveIn);
        attachmentsItem.setFileFolder(Constants.SHAHED + Constants.fileQuestions + position + "/" + Constants.fileAttachments + currentTime);
        attachmentsItem.setAttachmentType(AttachmentType.IMAGE);
        attachmentsItem.setQuestionIdentifier(id);
        attachmentsItem.setFormat(AttachmentType.IMAGE.getFormat());
        attachmentsItem.setUserId(Constants.getUser(getContext()).getId());

        if (isEdit) {
            fileAttachmentsItems.clear();
            File fdelete = new File(Environment.getExternalStorageDirectory(), attachmentsItem.getFileFolder());
            if (fdelete.isDirectory()) {
                String[] children = fdelete.list();
                for (int i = 0; i < children.length; i++) {
                    new File(fdelete, children[i]).delete();
                }
            }
            try {
                FileUtils.deleteDirectory(fdelete);
            } catch (IOException e) {
                e.printStackTrace();
            }
            fileAttachmentsItems.add(attachmentsItem);
            Collections.reverse(fileAttachmentsItems);
            Toast.makeText(getActivity(), "عکس مورد ویرایش شد", Toast.LENGTH_SHORT).show();
            database.fileAttachmentDao().updateObject(attachmentsItem.getDate(), attachmentsItem.getFilename(),
                    attachmentsItem.getFileAddress(), attachmentsItem.getFileFolder(),
                    attachmentsItem.getAttachmentType(), attachmentsItem.getUserId(), identifier);
//            makeJson();
        } else {
            fileAttachmentsItems.add(attachmentsItem);
            Collections.reverse(fileAttachmentsItems);
            Toast.makeText(getActivity(), "عکس مورد نظر اضافه شد", Toast.LENGTH_SHORT).show();
            database.fileAttachmentDao().insert(attachmentsItem);
//            makeJson();
        }

        adapter.notifyDataSetChanged();

    }


    @Override
    public void onItemClickDelete(int position, FileAttachmentsItem image) {

        CustomDialog dialog = new CustomDialog(getActivity());
        dialog.setOkListener(getResources().getString(R.string.ok), view1 -> {
            dialog.dismiss();
            removeFromListAndFile(position, image.getIdentifier());
        });
        dialog.setCancelListener(getResources().getString(R.string.cancel), view12 -> dialog.dismiss());
        dialog.setDialogTitle("آیا برای حذف مطمئن هستید؟");
        dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
        dialog.setIcon(R.drawable.ic_delete, getResources().getColor(R.color.redColor));
        dialog.show();

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onIemClick(int position, FileAttachmentsItem fileSelected) {
        this.fileSelected = fileSelected;

        if (doubleClick) {
            Intent intent = new Intent(getContext(), PreviewActivity.class);
            intent.putExtra("currentPosition", position - 1);
            intent.putExtra("id", id);
            getContext().startActivity(intent);
        } else {
            doubleClick = true;
            new Handler().postDelayed(() -> doubleClick = false, 1000);

            if (fileAttachmentsItems.size() == maxImageCount) {
                selectedVoice = fileAttachmentsItems.get(position);
                selectedPosition = position;
            } else {
                selectedVoice = fileAttachmentsItems.get(position - 1);
                selectedPosition = position - 1;
            }

            adapter.setSelectedImage(selectedPosition, selectedVoice);

            AppDatabase database = AppDatabase.getInMemoryDatabase(getContext());

            if (oldPosition == position) {
//            if (isEdit) {
//                database.fileAttachmentDao().setDescription(edtDescription.getValueString(), identifier);
//            } else {
//                database.fileAttachmentDao().setDescription(edtDescription.getValueString(), fileSelected.getIdentifier());
//            }
//            Toast.makeText(getContext(), "پیام اضافه شد", Toast.LENGTH_SHORT).show();
            } else {
                FileAttachmentsItem description = database.fileAttachmentDao().getDescription(fileSelected.getIdentifier());
                if (description != null) {
                    edtDescription.setBody(description.getDescription());
                    if (description.getGeoLocationDTO() != null) {
                        latitude = description.getGeoLocationDTO().getLatitude();
                        longitude = description.getGeoLocationDTO().getLongitude();
                        locationAddressItem = description.getGeoLocationDTO().getAddress();
                    } else {
                        latitude = 0.0;
                        longitude = 0.0;
                        locationAddressItem = "";
                    }
                }
            }

            this.oldPosition = position;
        }


//        List<FileAttachmentsItem> fileAttachments = database.fileAttachmentDao().getFileAttachments();
//        if (fileAttachments.get(selectedPosition).getDescription() != null) {
//            edtDescription.setBody(fileAttachments.get(selectedPosition).getDescription());
//        } else {
//            edtDescription.setBody(null);
//        }


//        if (this.address != null) {
//            fileAttachmentsItems.get(selectedPosition).setLat(this.address.get(0).getLatitude());
//            fileAttachmentsItems.get(selectedPosition).setLang(this.address.get(0).getLongitude());
//        }
    }


    private void removeFromListAndFile(int position, long id) {
        for (FileAttachmentsItem fileAttachmentsItem : fileAttachmentsItems) {
            if (fileAttachmentsItem.getIdentifier() == id) {
                if (fileAttachmentsItems.size() == maxImageCount) {
                    fileAttachmentsItems.remove(position);
                } else {
                    fileAttachmentsItems.remove(position - 1);
                }
                database.fileAttachmentDao().delete(fileAttachmentsItem);
                File fdelete = new File(Environment.getExternalStorageDirectory(), fileAttachmentsItem.getFileFolder());
                if (fdelete.isDirectory()) {
                    String[] children = fdelete.list();
                    for (int i = 0; i < children.length; i++) {
                        new File(fdelete, children[i]).delete();
                    }
                }
                try {
                    FileUtils.deleteDirectory(fdelete);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
        }

        if (adapter != null) {
            adapter.notifyDataSetChanged();
            listener.onInsetOk();
        }

    }

    @Override
    public void onItemAddress(int position, LocationDTO address) {

    }

    @Override
    public void onItemDelete(int position, LocationDTO address) {
        if (getActivity() != null) {
            CustomDialog dialog = new CustomDialog(getActivity());
            dialog.setOkListener(getResources().getString(R.string.ok), view1 -> {
                dialog.dismiss();
                removeFromAddres(address.getId());
            });
            dialog.setCancelListener(getResources().getString(R.string.cancel), view12 -> dialog.dismiss());
            dialog.setDialogTitle("آیا برای حذف مطمئن هستید؟");
            dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
            dialog.setIcon(R.drawable.ic_delete, getResources().getColor(R.color.redColor));
            dialog.show();
        }
    }

    private void removeFromAddres(int id) {
        for (Address address : address) {
            if (address.getId() == id) {
                this.address.remove(address);
                break;
            }
        }

        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }


    public interface updateListenerImage {
        void onInsetOk();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(getActivity(), getTheme()) {
            @Override
            public void onBackPressed() {
                dismiss();
            }
        };
    }
}
