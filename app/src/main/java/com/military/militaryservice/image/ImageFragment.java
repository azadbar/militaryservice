package com.military.militaryservice.image;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseFragment;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.baseView.BaseToolbar;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.database.Image;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_OK;

public class ImageFragment extends BaseFragment implements ImagesAdapter.OnItemClickListener {


    private static final int ADD_IMAGE = 11;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvCenterTitle)
    BaseTextView tvCenterTitle;
    @BindView(R.id.share)
    BaseImageView share;
    @BindView(R.id.toolbar)
    BaseToolbar toolbar;
    @BindView(R.id.rvImages)
    RecyclerView rvImages;
    @BindView(R.id.btnAddImage)
    FloatingActionButton btnAddImage;
    @BindView(R.id.imgView)
    BaseImageView imgView;
    @BindView(R.id.tvEmptyView)
    BaseTextView tvEmptyView;
    @BindView(R.id.rootEmptyView)
    BaseRelativeLayout rootEmptyView;
    private Unbinder unbinder;
    private ArrayList<Image> list = new ArrayList<>();
    private ImagesAdapter adapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_images, container, false);

        unbinder = ButterKnife.bind(this, view);
        tvCenterTitle.setText(getResources().getString(R.string.add_image));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        AppDatabase database = AppDatabase.getInMemoryDatabase(getActivity());
        list = (ArrayList<Image>) database.imageDao().getImages();
        setDataAdapter();
        if (list.size() == 0) {
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText(getResources().getString(R.string.no_image_found));
        } else {
            rootEmptyView.setVisibility(View.GONE);
        }
    }

    private void setDataAdapter() {
        adapter = new ImagesAdapter(list, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        rvImages.setHasFixedSize(true);
        rvImages.setLayoutManager(layoutManager);
        rvImages.setItemAnimator(new DefaultItemAnimator());
        rvImages.setAdapter(adapter);

    }

    @OnClick({R.id.imgBack, R.id.btnAddImage})
    public void onViewClicked(View view) {
        Intent intent = null;
        switch (view.getId()) {
            case R.id.imgBack:
                getActivity().finish();
                break;
            case R.id.btnAddImage:
                if (getActivity() != null) {
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    AddImageDialog searchParamDialog = new AddImageDialog();
                    searchParamDialog.setTargetFragment(ImageFragment.this, ADD_IMAGE);
                    searchParamDialog.show(fm, AddImageDialog.class.getName());
                }
                break;

        }
    }

    @Override
    public void onItemClick(int position, Image image) {
//        try {
//            Intent intent = new Intent(Intent.ACTION_DIAL);
//            intent.setDate(Uri.parse("tel:" + contact.getMobile().trim()));
//            startActivity(intent);
//        } catch (ActivityNotFoundException act) {
//
//        }
    }

    @Override
    public void onItemDelete(int position, Image image) {
        AppDatabase database = AppDatabase.getInMemoryDatabase(getActivity());
        database.imageDao().delete(image);
        removeFromList(image.getId());
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }

        if (list == null || list.size() == 0) {
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText(getString(R.string.nothing_found));
        }
    }

    private void removeFromList(int id) {
        for (Image image : list) {
            if (image.getId() == id) {
                list.remove(image);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ADD_IMAGE) {
                onResume();
            }
        }
    }


}
