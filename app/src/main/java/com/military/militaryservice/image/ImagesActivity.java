package com.military.militaryservice.image;

import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.widget.LinearLayout;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImagesActivity extends BaseActivity {

    @BindView(R.id.frameLayout_estate)
    LinearLayout frameLayoutEstate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_images);
        ButterKnife.bind(this);

        ImageFragment estateDetailFragment = new ImageFragment();
        FragmentManager fragMgr = getSupportFragmentManager();
        FragmentTransaction fragTrans = fragMgr.beginTransaction();
        fragTrans.replace(R.id.frameLayout_estate, estateDetailFragment);
        fragTrans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragTrans.commit();
    }
}

