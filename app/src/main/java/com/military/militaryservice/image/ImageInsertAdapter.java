package com.military.militaryservice.image;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.projectList.show.PreviewActivity;
import com.military.militaryservice.resultInfo.FileAttachmentsItem;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class ImageInsertAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private final int cellWidth;
    private final int maxImageCount;
    private Context context;
    private ArrayList<FileAttachmentsItem> list;
    private final OnItemClickListener listener;
    private final DeleteOnItemClickListener deleteOnItemClickListener;
    private boolean isEdit;
    private FileAttachmentsItem voiceSelected;


    public ImageInsertAdapter(Context context, ArrayList<FileAttachmentsItem> list, int cellWidth, int maxImageCount, OnItemClickListener listener, DeleteOnItemClickListener deleteOnItemClickListener, boolean isEdit) {
        this.list = list;
        this.listener = listener;
        this.cellWidth = cellWidth;
        this.maxImageCount = maxImageCount;
        this.context = context;
        this.deleteOnItemClickListener = deleteOnItemClickListener;
        this.isEdit = isEdit;
    }


    @Override
    public int getItemViewType(int position) {
        if (list.size() == maxImageCount) {
            return 2;
        }
        if (position == 0) {
            return 1;
        }
        return 2;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1:
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_add_image, parent, false);
                itemView.getLayoutParams().height = cellWidth;
                itemView.getLayoutParams().width = cellWidth;
                return new addImageViewHolder(itemView);
            default:
                View itemView1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_cell, parent, false);
                itemView1.getLayoutParams().height = cellWidth;
                itemView1.getLayoutParams().width = cellWidth;
                return new ViewHolderImageList(itemView1);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        switch (holder.getItemViewType()) {
            case 1:
                addImageViewHolder viewHolder0 = (addImageViewHolder) holder;
                if (isEdit) {
                    ((addImageViewHolder) holder).text.setText("ویرایش عکس");
                } else {
                    ((addImageViewHolder) holder).text.setText("افزودن عکس");

                }
                ((addImageViewHolder) holder).bind(position, listener);
                break;

            default:

                FileAttachmentsItem fileAttachmentsItem;

                ViewHolderImageList viewHolder2 = (ViewHolderImageList) holder;


                if (list.size() == maxImageCount) {
                    fileAttachmentsItem = list.get(position);
                } else {
                    fileAttachmentsItem = list.get(position - 1);
                }

                viewHolder2.setFileAttachment(fileAttachmentsItem);
                ((ViewHolderImageList) holder).bind(position, fileAttachmentsItem, deleteOnItemClickListener);

                ((ViewHolderImageList) holder).itemClick(position, fileAttachmentsItem, deleteOnItemClickListener);

//                ((ViewHolderImageList) holder).doubleClick(position, fileAttachmentsItem, deleteOnItemClickListener);


                File file = new File(Environment.getExternalStorageDirectory(), fileAttachmentsItem.getFileAddress());


                Glide.with(context)
                        .load(file.getAbsolutePath())
                        .apply(new RequestOptions().override(200, 200))
                        .into(((ViewHolderImageList) holder).imgAdd);

                if (fileAttachmentsItem.isDisable()) {
                    ((ViewHolderImageList) holder).disable.setBackgroundColor(((ViewHolderImageList) holder).rlAddImage.getContext().getResources().getColor(R.color.transparent));
                } else {
                    ((ViewHolderImageList) holder).disable.setBackgroundColor(((ViewHolderImageList) holder).rlAddImage.getContext().getResources().getColor(R.color.disable));
                }
                break;
        }

    }


    @Override
    public int getItemCount() {
        if (list.size() < maxImageCount) {
            return list.size() + 1;
        }
        return list.size();
    }


    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public interface DeleteOnItemClickListener {
        void onItemClickDelete(int position, FileAttachmentsItem image);

        void onIemClick(int position, FileAttachmentsItem image);
    }

    static class addImageViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgAdd)
        BaseImageView imgAdd;
        @BindView(R.id.rlAddImage)
        BaseRelativeLayout rlAddImage;
        @BindView(R.id.text)
        BaseTextView text;

        addImageViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(position));
        }

    }

    static class ViewHolderImageList extends RecyclerView.ViewHolder {
        private final GestureDetector dg;
        @BindView(R.id.imgDelete)
        BaseImageView imgDelete;
        @BindView(R.id.imgAdd)
        BaseImageView imgAdd;
        @BindView(R.id.rlAddImage)
        CardView rlAddImage;
        @BindView(R.id.disable)
        BaseRelativeLayout disable;
        private FileAttachmentsItem fileAttachment;
        private int position;

        ViewHolderImageList(View view) {
            super(view);
            ButterKnife.bind(this, view);


            dg = new GestureDetector(rlAddImage.getContext(), new GestureDetector.OnGestureListener() {
                @Override
                public boolean onDown(MotionEvent e) {
                    return false;
                }

                @Override
                public void onShowPress(MotionEvent e) {

                }

                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return false;
                }

                @Override
                public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                    return false;
                }

                @Override
                public void onLongPress(MotionEvent e) {

                }

                @Override
                public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                    return false;
                }
            });
            dg.setOnDoubleTapListener(new GestureDetector.OnDoubleTapListener() {
                @Override
                public boolean onDoubleTap(MotionEvent e) {

                    Intent intent = new Intent(itemView.getContext(), PreviewActivity.class);
                    intent.putExtra("currentPosition", position - 1);
                    itemView.getContext().startActivity(intent);
//                    Toast.makeText(itemView.getContext(), "not work", Toast.LENGTH_SHORT).show();
                    return true;
                }

                @Override
                public boolean onDoubleTapEvent(MotionEvent e) {
                    // if the second tap hadn't been released and it's being moved

                    return false;
                }

                @Override
                public boolean onSingleTapConfirmed(MotionEvent e) {
                    // TODO Auto-generated method stub
                    return false;
                }

            });
        }

        public void bind(int position, FileAttachmentsItem fileAttachmentsItem, final DeleteOnItemClickListener listener) {
            imgDelete.setOnClickListener(v -> listener.onItemClickDelete(position, fileAttachmentsItem));
        }

        public void itemClick(int position, FileAttachmentsItem fileAttachmentsItem, final DeleteOnItemClickListener listener) {
            rlAddImage.setOnClickListener(v -> listener.onIemClick(position, fileAttachmentsItem));
        }

        public void doubleClick(int positio, FileAttachmentsItem fileAttachmentsItem, DeleteOnItemClickListener deleteOnItemClickListener) {
            rlAddImage.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    fileAttachment = fileAttachmentsItem;
                    position = positio;
                    return dg.onTouchEvent(event);
                }
            });
        }

        public void setFileAttachment(FileAttachmentsItem fileAttachmentsItem) {
            this.fileAttachment = fileAttachmentsItem;
        }
    }

    public void setSelectedImage(int selectedPosition, FileAttachmentsItem fileAttachmentsItem) {
        this.voiceSelected = fileAttachmentsItem;
        list.get(selectedPosition).setDisable(true);
        for (int i = 0; i < list.size(); i++) {
            if (selectedPosition != i)
                list.get(i).setDisable(false);
        }
        notifyDataSetChanged();
    }

    public void setDisable(int selectedPosition, boolean isDisable) {
        list.get(selectedPosition).setDisable(isDisable);
        for (int i = 0; i < list.size(); i++) {
            if (selectedPosition != i)
                list.get(i).setDisable(isDisable);
        }
        notifyDataSetChanged();
    }
}
