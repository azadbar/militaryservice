package com.military.militaryservice.image;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.bumptech.glide.Glide;
import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.database.Image;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ViewHolder> {


    private ArrayList<Image> list;
    private final OnItemClickListener listener;


    ImagesAdapter(ArrayList<Image> list, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_images, parent, false);

        return new ViewHolder(itemView);

    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        Image image = list.get(position);
        holder.tvTitle.setText(image.getNamePerson());
//        holder.tvAddress.setText(image.getAddress());
//
//        Glide.with(holder.imageMag.getContext()).load(new File(image.getPath())).into(holder.imageMag);

        holder.bind(image, position, listener);
        holder.imgDelete.setOnClickListener(v -> listener.onItemDelete(position, image));
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageMag)
        BaseImageView imageMag;
        @BindView(R.id.framelayout)
        FrameLayout framelayout;
        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.tvAddress)
        BaseTextView tvAddress;
        @BindView(R.id.imgDelete)
        BaseImageView imgDelete;
        @BindView(R.id.llItems)
        BaseRelativeLayout llItems;
        @BindView(R.id.emergancy)
        CardView emergancy;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }


        public void bind(final Image image, int position, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(position, image));
        }

    }


    public interface OnItemClickListener {

        void onItemClick(int position, Image image);

        void onItemDelete(int position, Image image);

    }

}
