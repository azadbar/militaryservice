package com.military.militaryservice.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.io.Serializable;
import java.util.Date;

@Entity(tableName = "Voice")
public class Voice implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "questionIdentifier")
    private int questionIdentifier;
    private String nameVoice;
    private String voiceAddress;
    private String description;
    @TypeConverters(DateTypeConverter.class)
    private Date date;
    private double lat;
    private double lang;
    private String voiceLoaction;

    //local
    private boolean isSelected;
    private boolean disable;

    public boolean isDisable() {
        return disable;
    }

    public void setDisable(boolean disable) {
        this.disable = disable;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getVoiceLoaction() {
        return voiceLoaction;
    }

    public void setVoiceLoaction(String voiceLoaction) {
        this.voiceLoaction = voiceLoaction;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLang() {
        return lang;
    }

    public void setLang(double lang) {
        this.lang = lang;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVoiceAddress() {
        return voiceAddress;
    }

    public void setVoiceAddress(String voiceAddress) {
        this.voiceAddress = voiceAddress;
    }

    public int getQuestionIdentifier() {
        return questionIdentifier;
    }

    public void setQuestionIdentifier(int questionIdentifier) {
        this.questionIdentifier = questionIdentifier;
    }

    public String getNameVoice() {
        return nameVoice;
    }

    public void setNameVoice(String nameVoice) {
        this.nameVoice = nameVoice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
