package com.military.militaryservice.database;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ImageDao {

    @Query("select * from  image order by date Desc")
    List<Image> getImages();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllList(List<Image> images);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Image image);


    @Query("DELETE FROM image")
    public void nukeTable();


    @Delete
    void delete(Image image);

    @Query("select * from image where questionIdentifier =:questionIdentifier order by date DESC")
    List<Image> select(int questionIdentifier);


    @Update
    void updateTour(Image image);
}
