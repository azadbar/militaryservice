package com.military.militaryservice.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.military.militaryservice.resultInfo.FileAttachmentDao;
import com.military.militaryservice.model.PersonResponsDao;
import com.military.militaryservice.model.PersonResponse;
import com.military.militaryservice.orderInfo.ProjectResponse;
import com.military.militaryservice.orderInfo.ProjectResponseDao;
import com.military.militaryservice.resultInfo.FileAttachmentsItem;
import com.military.militaryservice.resultInfo.LocationDTO;
import com.military.militaryservice.resultInfo.ResultResponse;
import com.military.militaryservice.resultInfo.ResultsResponseDao;


@Database(entities = { Contact.class, User.class,
        PersonResponse.class,
        Image.class, Message.class, Voice.class,
        Video.class, Description.class, ProjectResponse.class,
        ResultResponse.class, FileAttachmentsItem.class, LocationDTO.class}, version = 1, exportSchema = false)


public abstract class AppDatabase extends RoomDatabase {

    public static AppDatabase getInMemoryDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class,
                            AppDatabase.class.getName())
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    private static AppDatabase INSTANCE;

    public abstract AddressDao addressDao();

    public abstract ContactDao contactDao();

    public abstract UserDao userDao();


    public abstract PersonResponsDao personResponsDao();


    public abstract ImageDao imageDao();

    public abstract MessageDao messageDao();

    public abstract VoiceDao voiceDao();

    public abstract VideoDao videoDao();

    public abstract ProjectResponseDao projectResponseDao();

    public abstract ResultsResponseDao resultsResponseDao();

    public abstract FileAttachmentDao fileAttachmentDao();


    public abstract DescriptionDao descriptionDao();

    public static void destroyInstance() {
        INSTANCE = null;
    }

}