package com.military.militaryservice.database;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface DescriptionDao {

    @Query("select * from  Description where questionIdentifier =:questionIdentifier")
    Description getDescription(int questionIdentifier);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllList(Description description);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Description description);


    @Query("DELETE FROM description")
    public void nukeTable();


    @Delete
    void delete(Description description);

    @Query("select * from description where questionIdentifier =:questionIdentifier ")
    Description select(int questionIdentifier);

    @Query("update description SET description =:description  where questionIdentifier =:questionIdentifier")
    void update(String description, int questionIdentifier);

    @Update
    void updateTour(Description description);
}
