package com.military.militaryservice.database;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.military.militaryservice.resultInfo.LocationDTO;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface AddressDao {

    @Query("select * from  locationdto order by date Desc")
    List<LocationDTO> getAddressList();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllList(List<LocationDTO> countries);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAddress(LocationDTO address);



    @Query("DELETE FROM locationdto")
    public void nukeTable();


    @Delete
    void delete(LocationDTO address);

}
