package com.military.militaryservice.database;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface ContactDao {

    @Query("select * from  contact order by date Desc")
    List<Contact> getContactList();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllList(List<Contact> contacts);

    @Insert(onConflict = REPLACE)
    void insertContact(Contact contact);


    @Query("DELETE FROM contact")
    public void nukeTable();


    @Delete
    void delete(Contact contact);

    @Update
    void updateTour(Contact contact);

    @Query("UPDATE Contact set name =:name,mobile =:mobile,image =:image where id=:id")
    int updateObject(String name, String mobile, String image, int id);

    @Query("select * from contact")
    Contact getMessage();

    @Query("UPDATE Contact set message =:message where id=:id")
    void update(String message, int id);

    @Query("update Contact set isDefault =:isDefault")
    void updateDefaultField(boolean isDefault);

}
