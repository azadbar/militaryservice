package com.military.militaryservice.database;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.TypeConverters;
import androidx.room.Update;

import com.military.militaryservice.enums.UserAccessEnum;

import java.util.Date;
import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface UserDao {

    @Query("select * from User")
    User getLogin();

    @Query("select * from  user")
    List<User> getUsers();

    @TypeConverters(UserAccessEnum.class)
    @Query("select * from  user where userAccess =:userAccess")
    List<User> getUsers(UserAccessEnum userAccess);

    @Query("select * from  user where id =:id")
    User getUser(int id);

    @TypeConverters(UserAccessEnum.class)
    @Query("select COUNT(userAccess) from  user where userAccess =:userAccess ")
    int getCountAdminUser(UserAccessEnum userAccess);


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(User user);

    @Insert(onConflict = REPLACE)
    void insertLogin(User user);

    @Update
    void update(User user);

    @TypeConverters({DateTypeConverter.class, UserAccessEnum.class})
    @Query("UPDATE User SET password=:password, username =:username,userAccess=:userAccess,date=:date  WHERE id =:id")
    void update(String password, String username, UserAccessEnum userAccess, Date date, int id);

    @Query("UPDATE User SET password=:password, username =:username WHERE id =:id")
    void update(String password, String username, int id);


    @Query("DELETE FROM User")
    public void nukeTable();


    @Delete
    void delete(User user);
}
