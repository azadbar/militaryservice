package com.military.militaryservice.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import com.military.militaryservice.enums.AttachmentType;
import com.military.militaryservice.enums.UserAccessEnum;

import java.io.Serializable;
import java.util.Date;

@Entity(tableName = "User")
public class User implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;
    private String username;
    private String password;
    @TypeConverters(UserAccessEnum.class)
    private UserAccessEnum userAccess;
    @TypeConverters(DateTypeConverter.class)
    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public UserAccessEnum getUserAccess() {
        return userAccess;
    }

    public void setUserAccess(UserAccessEnum userAccess) {
        this.userAccess = userAccess;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
