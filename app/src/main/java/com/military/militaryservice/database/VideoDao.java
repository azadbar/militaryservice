package com.military.militaryservice.database;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface VideoDao {

    @Query("select * from  video order by date Desc")
    List<Video> getImages();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllList(List<Video> videos);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Video video);


    @Query("DELETE FROM video")
    public void nukeTable();


    @Delete
    void delete(Video video);

    @Query("select * from video where questionIdentifier =:questionIdentifier order by date DESC")
    List<Video> select(int questionIdentifier);

    @Update
    void updateTour(Video video);
}
