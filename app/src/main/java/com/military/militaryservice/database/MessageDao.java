package com.military.militaryservice.database;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface MessageDao {

    @Query("select * from message order by date DESC")
    List<Message> getMessageList();


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Message message);

    @Insert(onConflict = REPLACE)
    void insertMessage(Message message);

    @Query("DELETE FROM message")
    public void nukeTable();


    @Delete
    void delete(Message message);

    @Update
    void updateTour(Message message);

    @Query("select * from message where idCall =:idCall order by date DESC")
    Message getMessage(int idCall);

    @Query("update contact SET  message =:message WHERE id =:idCall")
    void update(String message, int idCall);

}
