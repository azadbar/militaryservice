package com.military.militaryservice.database;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface VoiceDao {

    @Query("select * from voice order by date DESC")
    List<Voice> getVoice();


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Voice voice);

    @Insert(onConflict = REPLACE)
    void insertMessage(Voice voice);

    @Query("DELETE FROM voice")
    public void nukeTable();


    @Delete
    void delete(Voice voice);

    @Update
    void updateTour(Voice voice);

    @Query("update voice SET questionIdentifier =:questionIdentifier  WHERE id =:id")
    void update( int questionIdentifier,int id);

    @Query("select * from voice where questionIdentifier =:questionIdentifier order By date DESC")
    List<Voice> select(int questionIdentifier);

    @Query("update voice SET description =:desc  WHERE id =:id")
    void updateMessageVoice(String desc, int id);
}
