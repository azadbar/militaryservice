package com.military.militaryservice.splash;

import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.bumptech.glide.Glide;
import com.military.militaryservice.R;
import com.military.militaryservice.activity.MainActivity;
import com.military.militaryservice.baseView.BaseActivity;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseLinearLayout;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.customView.CustomEditText;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.database.User;
import com.military.militaryservice.dialog.FingerPrintDialog;
import com.military.militaryservice.enums.UserAccessEnum;
import com.military.militaryservice.fingerPrint.OnFinishListener;
import com.military.militaryservice.utils.Constants;
import com.military.militaryservice.utils.GPSTracker;
import com.military.militaryservice.utils.PermissionHandler;
import com.military.militaryservice.utils.PreferencesData;

import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.crypto.Cipher;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@RequiresApi(api = Build.VERSION_CODES.M)
public class SplashActivity extends BaseActivity implements OnFinishListener {

    @BindView(R.id.logo)
    BaseImageView logo;
    @BindView(R.id.textAnim)
    BaseImageView textAnim;

    private static final int REQUEST_CODE_PERMISSION = 2;
    @BindView(R.id.edtMobile)
    CustomEditText edtMobile;
    @BindView(R.id.edtPassword)
    CustomEditText edtPassword;
    @BindView(R.id.image)
    BaseImageView background;
    @BindView(R.id.rlLogin)
    BaseLinearLayout rlLogin;
    @BindView(R.id.rootView)
    BaseRelativeLayout rootView;
    @BindView(R.id.scroll)
    ScrollView scroll;
    @BindView(R.id.btnLogin)
    BaseTextView btnLogin;
    @BindView(R.id.btnFingerPrint)
    BaseImageView btnFingerPrint;
    @BindView(R.id.tvEnableFingerPrint)
    BaseTextView tvEnableFingerPrint;

    private KeyStore keyStore;
    // Variable used for storing the key in the Android Keystore container
    private static final String KEY_NAME = "androidHive";
    private Cipher cipher;
    private TextView textView;
    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);


        Glide.with(this).load(R.drawable.shahedd).into(logo);
        Glide.with(this).load(R.drawable.text_logo).into(textAnim);

        textAnim.setVisibility(View.INVISIBLE);
        Animation bottomUp = AnimationUtils.loadAnimation(this,
                R.anim.buottom_up);
        textAnim.startAnimation(bottomUp);
        textAnim.setVisibility(View.VISIBLE);


        //login
        rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = rootView.getRootView().getHeight() - rootView.getHeight();
                if (heightDiff > 100) {
                    scrollToView(scroll, textAnim);
                } else {
                }
            }
        });
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            rlLogin.setVisibility(View.VISIBLE);
            AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(2500);
            rlLogin.startAnimation(anim);
        }, 1600);


        if (PermissionHandler.hasAllPermissions(this)) {

        } else {
            PermissionHandler.requestPermissions(this, REQUEST_CODE_PERMISSION);
        }
        if (PreferencesData.getIsFirst(this)) {
            AppDatabase database = AppDatabase.getInMemoryDatabase(this);
            User user = new User();
            user.setUsername("admin");
            user.setPassword("admin");
            user.setUserAccess(UserAccessEnum.ADMIN);
            user.setDate(new Date());
            database.userDao().insert(user);
            PreferencesData.isFirst(this, false);
        }
        edtPassword.getImage().setOnClickListener(view -> edtPassword.showHidePassword());
        edtMobile.getImage().setOnClickListener(view -> edtMobile.showHidePassword());

//        keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
//        fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

        // Check whether the device has a Fingerprint sensor.


        if (PreferencesData.getFingerPrint(this)) {
            btnFingerPrint.setVisibility(View.VISIBLE);
            tvEnableFingerPrint.setVisibility(View.VISIBLE);
            FingerPrintDialog dialog = new FingerPrintDialog(this);
            dialog.setCancelListener("انصراف", v -> dialog.dismiss());
            dialog.setDescription("انگشت خود را بر روی حسگر اثر انگشت قرار دهید");
            dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
            dialog.setIcon(R.drawable.finger, getResources().getColor(R.color.transparent));
            dialog.setListener(this);
            dialog.show();

        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //Fingerprint API only available on from Android 6.0 (M)
                FingerprintManager fingerprintManager = (FingerprintManager) getSystemService(Context.FINGERPRINT_SERVICE);
                if (!fingerprintManager.isHardwareDetected()) {
                    btnFingerPrint.setVisibility(View.GONE);
                    tvEnableFingerPrint.setVisibility(View.GONE);
                } else if (!fingerprintManager.hasEnrolledFingerprints()) {
                    // User hasn't enrolled any fingerprints to authenticate with
                } else {
                    btnFingerPrint.setVisibility(View.VISIBLE);
                    tvEnableFingerPrint.setVisibility(View.VISIBLE);
                }
            }else {
                btnFingerPrint.setVisibility(View.GONE);
                tvEnableFingerPrint.setVisibility(View.GONE);
            }

        }

//        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
//        boolean gps_enabled = false;
//        boolean network_enabled = false;
//
//        try {
//            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
//        } catch(Exception ex) {}
//
//        try {
//            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
//        } catch(Exception ex) {}
//
//        if(!gps_enabled && !network_enabled) {
//            // notify user
//            new AlertDialog.Builder(this)
//                    .setMessage("لوکشین فعال نیست")
//                    .setPositiveButton("روشن کردن gps" , new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
//                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
//                        }
//                    }).show();
//
//        }
        GPSTracker gps = new GPSTracker(SplashActivity.this);

        double latitudeD = gps.getLatitude();
        double longitudeD = gps.getLongitude();

    }


    private void scrollToView(final ScrollView scrollViewParent, final View view) {
        // Get deepChild Offset
        Point childOffset = new Point();
        getDeepChildOffset(scrollViewParent, view.getParent(), view, childOffset);
        // Scroll to child.
        scrollViewParent.smoothScrollTo(0, childOffset.y);
    }

    private void getDeepChildOffset(final ViewGroup mainParent, final ViewParent parent, final View child, final Point accumulatedOffset) {
        ViewGroup parentGroup = (ViewGroup) parent;
        accumulatedOffset.x += child.getLeft();
        accumulatedOffset.y += child.getTop();
        if (parentGroup.equals(mainParent)) {
            return;
        }
        getDeepChildOffset(mainParent, parentGroup.getParent(), parentGroup, accumulatedOffset);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.btnLogin, R.id.btnFingerPrint})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                if (isValidData()) {
                    Intent intent = new Intent(this, MainActivity.class);
                    startActivity(intent);
//            PreferencesData.isLogin(this, true);
                    finish();
                }
                break;
            case R.id.btnFingerPrint:
                FingerPrintDialog dialog = new FingerPrintDialog(this);
                dialog.setCancelListener("انصراف", v -> dialog.dismiss());
                dialog.setDescription("انگشت خود را بر روی حسگر اثر انگشت قرار دهید");
                dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
                dialog.setIcon(R.drawable.finger, getResources().getColor(R.color.transparent));
                dialog.setListener(this);
                dialog.show();

                break;
        }

    }


    private boolean isValidData() {
        ArrayList<String> errorMsgList = new ArrayList<>();

        AppDatabase database = AppDatabase.getInMemoryDatabase(this);
        List<User> users = database.userDao().getUsers();
        ArrayList<String> userName = new ArrayList<>();
        for (int i = 0; i < users.size(); i++) {
            userName.add(users.get(i).getUsername());
        }
        if (userName.contains(edtMobile.getValueString())) {
            for (User u : users) {
                if (TextUtils.equals(u.getUsername(), edtMobile.getValueString())) {
                    if (!TextUtils.equals(edtPassword.getValueString(), u.getPassword())) {
                        String message = getResources().getString(R.string.wrong_password);
                        errorMsgList.add(message);
                    } else {
                        Constants.setCurrentUser(u);

                    }
                }
            }
        } else {
            String message = "نام کاربری شما در سیستم ثبت نشده است";
            errorMsgList.add(message);
        }


        if (edtMobile.getError() != null) {
            errorMsgList.add(edtMobile.getError());
        }

        if (edtPassword.getError() != null) {
            errorMsgList.add(edtPassword.getError());
        }

        if (errorMsgList.size() > 0) {
            showInfoDialog(getString(R.string.fill_following), errorMsgList);
            return false;
        }
        return true;
    }


    @Override
    public void onFinish() {
        finish();
    }


    @Override
    protected void onShowKeyboard(int keyboardHeight) {
        // do things when keyboard is shown
        View targetView = findViewById(R.id.textAnim);
        targetView.getParent().requestChildFocus(targetView,targetView);
    }

    @Override
    protected void onHideKeyboard() {
        // do things when keyboard is hidden
    }
}
