package com.military.militaryservice.baseView;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.military.militaryservice.R;
import com.military.militaryservice.database.User;
import com.military.militaryservice.enums.FontTypeEnum;
import com.military.militaryservice.enums.UserAccessEnum;
import com.military.militaryservice.utils.Constants;


public class BaseCardView extends CardView {


    public BaseCardView(Context context) {
        this(context, null);
    }

    public BaseCardView(Context context, @Nullable AttributeSet attrs) {
        this(context,attrs, 0);

    }

    public BaseCardView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
     super(context,attrs,defStyleAttr);
     initView(context,attrs,defStyleAttr);

    }

    private void initView(Context context, AttributeSet attrs, int defStyleAttr) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            this.setTextDirection(Constants.language.setTextDirec());
        }

        setUserAccess(context);
    }

    private void setUserAccess(Context context) {
        User user = Constants.getUser(context);
        if (user.getUserAccess() == UserAccessEnum.READ){
            this.setEnabled(false);
            this.setClickable(false);
            this.setFocusable(false);
            this.setCardBackgroundColor(getResources().getColor(R.color.disable));
        }else {
            this.setEnabled(true);
            this.setClickable(true);
            this.setFocusable(true);
            this.setCardBackgroundColor(getResources().getColor(R.color.white));
        }
    }

}
