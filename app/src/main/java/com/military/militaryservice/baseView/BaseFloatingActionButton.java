package com.military.militaryservice.baseView;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.util.AttributeSet;

import androidx.annotation.Nullable;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.military.militaryservice.R;
import com.military.militaryservice.database.User;
import com.military.militaryservice.enums.UserAccessEnum;
import com.military.militaryservice.utils.Constants;


public class BaseFloatingActionButton extends FloatingActionButton {


    public BaseFloatingActionButton(Context context) {
        this(context, null);
    }

    public BaseFloatingActionButton(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);

    }

    public BaseFloatingActionButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs, defStyleAttr);

    }

    private void initView(Context context, AttributeSet attrs, int defStyleAttr) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            this.setTextDirection(Constants.language.setTextDirec());
        }

        setUserAccess(context);
    }

    private void setUserAccess(Context context) {
        User user = Constants.getUser(context);
        if (user.getUserAccess() == UserAccessEnum.READ) {
            this.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#CC4D4D4D")));
//            this.setClickable(false);
//            this.setFocusable(false);
            this.setEnabled(false);
        } else {
            this.setBackgroundColor(getResources().getColor(R.color.colorAccent));
//            this.setClickable(true);
//            this.setFocusable(true);
            this.setEnabled(true);
        }
    }

}
