package com.military.militaryservice.detail;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.military.militaryservice.R;
import com.military.militaryservice.orderInfo.QuestionInfosItem;

import java.util.List;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            QuestionInfosItem questionInfosItem = (QuestionInfosItem) bundle.getSerializable("questionInfosItem");
            if (questionInfosItem != null) {
                DetailFragment detailFragment = new DetailFragment();
                Bundle b = new Bundle();
                b.putSerializable("questionInfosItem", questionInfosItem);
                detailFragment.setArguments(b);
                loadFragment(detailFragment, DetailFragment.class.getName());
            }
        }

    }

    private void loadFragment(Fragment fragment, String fragmentTag) {
        FragmentManager fragMgr = getSupportFragmentManager();
        FragmentTransaction fragTrans = fragMgr.beginTransaction();
        fragTrans.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        fragTrans.replace(R.id.framelayout, fragment, fragmentTag);
        fragTrans.commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<Fragment> frags = getSupportFragmentManager().getFragments();
        if (frags != null) {
            for (Fragment f : frags) {
                if (f != null && f.isVisible())
                    f.onActivityResult(requestCode, resultCode, data);
            }
        }
    }
}
