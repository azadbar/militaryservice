package com.military.militaryservice.detail;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.military.militaryservice.R;
import com.military.militaryservice.audio.AudioRecorderActivity;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.baseView.BaseToolbar;
import com.military.militaryservice.customView.CustomMultiLineEditText;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.database.Description;
import com.military.militaryservice.dialog.CustomDialog;
import com.military.militaryservice.enums.AttachmentType;
import com.military.militaryservice.image.AddImageDialog;
import com.military.militaryservice.maps.OfflineActivity;
import com.military.militaryservice.orderInfo.QuestionInfosItem;
import com.military.militaryservice.projectList.show.ImageShowAdapter;
import com.military.militaryservice.projectList.show.PreviewActivity;
import com.military.militaryservice.projectList.show.PreviewVideoActivity;
import com.military.militaryservice.projectList.show.VideoShowAdapter;
import com.military.militaryservice.projectList.show.VoiceShowAdapter;
import com.military.militaryservice.recordAudio.AudioRecordDialog;
import com.military.militaryservice.recordVideo.AddVideoDialog;
import com.military.militaryservice.resultInfo.FileAttachmentsItem;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

public class DetailFragment extends Fragment implements VoiceShowAdapter.OnItemClickListener, ImageShowAdapter.OnItemClickListener,
        VideoShowAdapter.OnItemClickListener, AudioRecordDialog.updateListener, AddImageDialog.updateListenerImage, AddVideoDialog.updateListenerVideo {

    private static final int REQUEST_EDIT_VOICE = 2;
    private static final int REQUEST_EDIT_IMAGE = 3;
    private static final int REQUEST_EDIT_VIDEO = 4;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvCenterTitle)
    BaseTextView tvCenterTitle;
    @BindView(R.id.share)
    BaseImageView share;
    @BindView(R.id.toolbar)
    BaseToolbar toolbar;
    @BindView(R.id.voiceRecycle)
    RecyclerView voiceRecycle;
    @BindView(R.id.imageRecycle)
    RecyclerView imageRecycle;
    @BindView(R.id.videoRecycle)
    RecyclerView videoRecycle;
    @BindView(R.id.tvEmptyView)
    BaseTextView tvEmptyView;
    @BindView(R.id.rootEmptyView)
    BaseRelativeLayout rootEmptyView;
    @BindView(R.id.emptyVoice)
    BaseTextView emptyVoice;
    @BindView(R.id.emptyImage)
    BaseTextView emptyImage;
    @BindView(R.id.emptyVideo)
    BaseTextView emptyVideo;
    @BindView(R.id.edtDescription)
    CustomMultiLineEditText edtDescription;
    @BindView(R.id.editDesc)
    BaseTextView editDesc;
    @BindView(R.id.btnVoice)
    BaseRelativeLayout btnVoice;
    @BindView(R.id.btnImage)
    BaseRelativeLayout btnImage;
    @BindView(R.id.btnVideo)
    BaseRelativeLayout btnVideo;
    @BindView(R.id.btnSave)
    BaseTextView btnSave;
    @BindView(R.id.cvDesc)
    CardView cvDesc;
    @BindView(R.id.imgVoiceColapse)
    BaseImageView imgVoiceColapse;
    @BindView(R.id.imgImageColapse)
    BaseImageView imgImageColapse;
    @BindView(R.id.imgVideoColaps)
    BaseImageView imgVideoColaps;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    private QuestionInfosItem questionInfosItem;
    private int id;
    private AppDatabase database;
    private ArrayList<FileAttachmentsItem> voicesFileAttachments;
    private ArrayList<FileAttachmentsItem> imageFileAttachmentsItems;
    private ArrayList<FileAttachmentsItem> videoFileAttachments;
    private VoiceShowAdapter voiceAdpter;
    private ImageShowAdapter imageShowAdapter;
    private VideoShowAdapter videoShowAdapter;
    private boolean isShowVoice;
    private boolean isShowVideo;
    private boolean isShowImage;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_detail_question, container, false);
        ButterKnife.bind(this, view);
        Bundle bundle = getArguments();
        if (bundle != null) {
            questionInfosItem = (QuestionInfosItem) bundle.getSerializable("questionInfosItem");
        }

        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.ic_arrow_forward);
        imgBack.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        imgBack.setOnClickListener(v -> getActivity().finish());

        if (questionInfosItem != null) {
            id = questionInfosItem.getQuestionInfosId();
            tvCenterTitle.setText(questionInfosItem.getQuestionInfo());
        }

        scrollView.scrollTo(0, 0);
        scrollView.post(() -> {
            if (scrollView != null) {
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            }
        });
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        database = AppDatabase.getInMemoryDatabase(getActivity());
        if (database.descriptionDao().getDescription(id) != null && database.descriptionDao().getDescription(id).getDescription() != null) {
            disableView();
        } else {
            enableView();
        }
        edtDescription.setBody(database.descriptionDao().getDescription(id) == null ? "" : database.descriptionDao().getDescription(id).getDescription());
        voicesFileAttachments = (ArrayList<FileAttachmentsItem>) database.fileAttachmentDao().select(id, AttachmentType.VOICE.getFormat());
        imageFileAttachmentsItems = (ArrayList<FileAttachmentsItem>) database.fileAttachmentDao().select(id, AttachmentType.IMAGE.getFormat());
        videoFileAttachments = (ArrayList<FileAttachmentsItem>) database.fileAttachmentDao().select(id, AttachmentType.VIDEO.getFormat());
        if (voicesFileAttachments.size() > 0) {
            emptyVoice.setVisibility(View.GONE);
            setVoiceAdapter();
        } else {
            emptyVoice.setVisibility(View.GONE);
        }

        if (imageFileAttachmentsItems.size() > 0) {
            emptyImage.setVisibility(View.GONE);
            setImageAdapter();
        } else {
            emptyImage.setVisibility(View.GONE);

        }

        if (videoFileAttachments.size() > 0) {
            emptyVideo.setVisibility(View.GONE);
            setVideoAdapter();
        } else {
            emptyVideo.setVisibility(View.GONE);
        }

        if (voicesFileAttachments.size() == 0 && imageFileAttachmentsItems.size() == 0 && videoFileAttachments.size() == 0) {
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText("هیچ فایلی به این سوال اضافه نشده است");
        } else {
            rootEmptyView.setVisibility(View.GONE);
        }
    }


    private void setVoiceAdapter() {
        if (voiceAdpter == null) {
            voiceAdpter = new VoiceShowAdapter(voicesFileAttachments, this, true);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
            voiceRecycle.setLayoutManager(layoutManager);
            voiceRecycle.setAdapter(voiceAdpter);
            voiceRecycle.setHasFixedSize(true);
        } else {
            voiceAdpter.setList(voicesFileAttachments);
        }
    }

    private void setImageAdapter() {
        if (imageShowAdapter == null) {
            imageShowAdapter = new ImageShowAdapter(imageFileAttachmentsItems, this, true);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
            imageRecycle.setLayoutManager(layoutManager);
            imageRecycle.setAdapter(imageShowAdapter);
            imageRecycle.setHasFixedSize(true);
        } else {
            imageShowAdapter.setList(imageFileAttachmentsItems);
        }
    }

    private void setVideoAdapter() {
        if (videoShowAdapter == null) {
            videoShowAdapter = new VideoShowAdapter(videoFileAttachments, this, true);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
            videoRecycle.setLayoutManager(layoutManager);
            videoRecycle.setAdapter(videoShowAdapter);
            videoRecycle.setHasFixedSize(true);
        } else {
            videoShowAdapter.setList(videoFileAttachments);
        }
    }


    @Override
    public void onItemClickVoice(int position, FileAttachmentsItem image) {

    }

    @Override
    public void onDeleteItemVoice(int position, FileAttachmentsItem voice) {
        CustomDialog dialog = new CustomDialog(getContext());
        dialog.setOkListener(getResources().getString(R.string.ok), view1 -> {
            dialog.dismiss();
            removeFromListAndFile(voice.getIdentifier());
        });
        dialog.setCancelListener(getResources().getString(R.string.cancel), view12 -> dialog.dismiss());
        dialog.setDialogTitle("آیا برای حذف مطمئن هستید؟");
        dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
        dialog.setIcon(R.drawable.ic_delete, getResources().getColor(R.color.redColor));
        dialog.show();
    }

    private void removeFromListAndFile(long id) {
        for (FileAttachmentsItem voice : voicesFileAttachments) {
            if (voice.getIdentifier() == id) {
                voicesFileAttachments.remove(voice);
                database.fileAttachmentDao().delete(voice);
                File fdelete = new File(Environment.getExternalStorageDirectory(), voice.getFileFolder());
                if (fdelete.isDirectory()) {
                    String[] children = fdelete.list();
                    for (int i = 0; i < children.length; i++) {
                        new File(fdelete, children[i]).delete();
                    }
                }
                try {
                    FileUtils.deleteDirectory(fdelete);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
        if (voiceAdpter != null) {
            voiceAdpter.notifyDataSetChanged();
        }

        if (voicesFileAttachments.size() > 0) {
            setVoiceAdapter();
        }
    }

    @Override
    public void onLocationVoice(int position, FileAttachmentsItem file) {
        Intent intent = new Intent(getContext(), OfflineActivity.class);
        intent.putExtra("lat", file.getGeoLocationDTO() != null ? file.getGeoLocationDTO().getLatitude() : 0);
        intent.putExtra("long", file.getGeoLocationDTO() != null ? file.getGeoLocationDTO().getLongitude() :0);
        intent.putExtra("bodyEditTxt", file.getGeoLocationDTO() != null ? file.getGeoLocationDTO().getAddress() : 0);
        intent.putExtra("isQuestion", true);
        startActivity(intent);
    }

    @Override
    public void onEditItemVoice(int position, FileAttachmentsItem voice) {
        if (getActivity() != null) {
            FragmentManager fm = getActivity().getSupportFragmentManager();
            AudioRecordDialog audioRecordDialog = new AudioRecordDialog();
            Bundle bundle = new Bundle();
            bundle.putInt("id", questionInfosItem.getQuestionInfosId());
            bundle.putBoolean("isEdit", true);
            bundle.putSerializable("file", voice);
            audioRecordDialog.setArguments(bundle);
            audioRecordDialog.setTargetFragment(DetailFragment.this, REQUEST_EDIT_VOICE);
            audioRecordDialog.setListener(this);
            audioRecordDialog.show(fm, AudioRecordDialog.class.getName());
        }
    }

    @Override
    public void onItemPlay(int position, FileAttachmentsItem voice) {
        Intent intent = new Intent(getContext(), AudioRecorderActivity.class);
        intent.putExtra("isPlay", true);
        File file = new File(Environment.getExternalStorageDirectory(), voice.getFileAddress());
        intent.putExtra("filePath", file.getAbsolutePath());
        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_EDIT_VOICE) {
                onResume();
            } else if (requestCode == REQUEST_EDIT_IMAGE) {
                onResume();
            } else if (requestCode == REQUEST_EDIT_VIDEO) {
                onResume();
            }
        }
    }

    @Override
    public void onDeleteItem(int position, FileAttachmentsItem file) {
        CustomDialog dialog = new CustomDialog(getContext());
        dialog.setOkListener(getResources().getString(R.string.ok), view1 -> {
            dialog.dismiss();
            removeImageFromListAndFile(file.getIdentifier());
        });
        dialog.setCancelListener(getResources().getString(R.string.cancel), view12 -> dialog.dismiss());
        dialog.setDialogTitle("آیا برای حذف مطمئن هستید؟");
        dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
        dialog.setIcon(R.drawable.ic_delete, getResources().getColor(R.color.redColor));
        dialog.show();
    }

    @Override
    public void onLocation(int position, FileAttachmentsItem file) {
        Intent intent = new Intent(getContext(), OfflineActivity.class);
        intent.putExtra("lat", file.getGeoLocationDTO() != null ? file.getGeoLocationDTO().getLatitude() : 0);
        intent.putExtra("long", file.getGeoLocationDTO() != null ? file.getGeoLocationDTO().getLongitude() :0);
        intent.putExtra("bodyEditTxt", file.getGeoLocationDTO() != null ? file.getGeoLocationDTO().getAddress() : 0);
        intent.putExtra("isQuestion", true);
        startActivity(intent);
    }

    @Override
    public void onEditItem(int position, FileAttachmentsItem file) {

        if (getActivity() != null) {
            FragmentManager fm = getActivity().getSupportFragmentManager();
            AddImageDialog addImageDialog = new AddImageDialog();
            Bundle bundle = new Bundle();
            bundle.putInt("id", questionInfosItem.getQuestionInfosId());
            bundle.putBoolean("isEdit", true);
            bundle.putSerializable("file", file);
            addImageDialog.setArguments(bundle);
            addImageDialog.setTargetFragment(DetailFragment.this, REQUEST_EDIT_IMAGE);
            addImageDialog.setListener(this);
            addImageDialog.show(fm, AudioRecordDialog.class.getName());
        }

    }

    @Override
    public void onShowImage(int position, FileAttachmentsItem file) {
        if (imageFileAttachmentsItems.size() > 0) {
            Intent intent = new Intent(getContext(), PreviewActivity.class);
            intent.putExtra("image", file.getFileAddress());
            intent.putExtra("id", id);
            startActivity(intent);
        }
    }


    private void removeImageFromListAndFile(long id) {
        for (FileAttachmentsItem file : imageFileAttachmentsItems) {
            if (file.getIdentifier() == id) {
                imageFileAttachmentsItems.remove(file);
                database.fileAttachmentDao().delete(file);
                File fdelete = new File(Environment.getExternalStorageDirectory(), file.getFileFolder());
                if (fdelete.isDirectory()) {
                    String[] children = fdelete.list();
                    for (int i = 0; i < children.length; i++) {
                        new File(fdelete, children[i]).delete();
                    }
                }
                try {
                    FileUtils.deleteDirectory(fdelete);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
        if (imageShowAdapter != null) {
            imageShowAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDeleteItemVideo(int position, FileAttachmentsItem file) {
        CustomDialog dialog = new CustomDialog(getContext());
        dialog.setOkListener(getResources().getString(R.string.ok), view1 -> {
            dialog.dismiss();
            removeVidoeFromListAndFile(file.getIdentifier());
        });
        dialog.setCancelListener(getResources().getString(R.string.cancel), view12 -> dialog.dismiss());
        dialog.setDialogTitle("آیا برای حذف مطمئن هستید؟");
        dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
        dialog.setIcon(R.drawable.ic_delete, getResources().getColor(R.color.redColor));
        dialog.show();
    }

    private void removeVidoeFromListAndFile(long id) {
        for (FileAttachmentsItem video : videoFileAttachments) {
            if (video.getIdentifier() == id) {
                videoFileAttachments.remove(video);
                database.fileAttachmentDao().delete(video);
                File fdelete = new File(Environment.getExternalStorageDirectory(), video.getFileFolder());
                if (fdelete.isDirectory()) {
                    String[] children = fdelete.list();
                    for (int i = 0; i < children.length; i++) {
                        new File(fdelete, children[i]).delete();
                    }
                }
                try {
                    FileUtils.deleteDirectory(fdelete);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
        if (videoShowAdapter != null) {
            videoShowAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onLocationVideo(int position, FileAttachmentsItem file) {
        Intent intent = new Intent(getContext(), OfflineActivity.class);
        intent.putExtra("lat", file.getGeoLocationDTO() != null ? file.getGeoLocationDTO().getLatitude() : 0);
        intent.putExtra("long", file.getGeoLocationDTO() != null ? file.getGeoLocationDTO().getLongitude() :0);
        intent.putExtra("bodyEditTxt", file.getGeoLocationDTO() != null ? file.getGeoLocationDTO().getAddress() : 0);
        intent.putExtra("isQuestion", true);
        startActivity(intent);
    }

    @Override
    public void onEditItemVideo(int position, FileAttachmentsItem video) {
        if (getActivity() != null) {
            FragmentManager fm = getActivity().getSupportFragmentManager();
            AddVideoDialog addVideoDialog = new AddVideoDialog();
            Bundle bundle = new Bundle();
            bundle.putInt("id", questionInfosItem.getQuestionInfosId());
            bundle.putBoolean("isEdit", true);
            bundle.putSerializable("file", video);
            addVideoDialog.setListener(this);
            addVideoDialog.setArguments(bundle);
            addVideoDialog.setTargetFragment(DetailFragment.this, REQUEST_EDIT_VIDEO);
            addVideoDialog.show(fm, AudioRecordDialog.class.getName());
        }
    }

    @Override
    public void onShowImageVideo(int position, FileAttachmentsItem video) {
        if (videoFileAttachments.size() > 0) {
            Intent intent = new Intent(getContext(), PreviewVideoActivity.class);
            intent.putExtra("video", video.getFileAddress());
            startActivity(intent);
        }
    }


    @Override
    public void onInsertOk() {
        onResume();
    }

    @Override
    public void onInsetOk() {
        onResume();
    }

    @OnClick({R.id.btnVoice, R.id.btnImage, R.id.btnVideo, R.id.editDesc, R.id.btnSave})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnVoice:
                if (voicesFileAttachments.size() > 0) {
                    if (isShowVoice) {
                        voiceRecycle.setVisibility(View.VISIBLE);
                        imgVoiceColapse.setImageResource(R.drawable.ic_keyboard_arrow_down);

                    } else {
                        voiceRecycle.setVisibility(View.GONE);
                        imgVoiceColapse.setImageResource(R.drawable.ic_keyboard_arrow_up_black);
                    }

                    isShowVoice = !isShowVoice;
                }

                break;
            case R.id.btnImage:
                if (imageFileAttachmentsItems.size() > 0) {
                    if (isShowImage) {
                        imageRecycle.setVisibility(View.VISIBLE);
                        imgImageColapse.setImageResource(R.drawable.ic_keyboard_arrow_down);
                    } else {
                        imageRecycle.setVisibility(View.GONE);
                        imgImageColapse.setImageResource(R.drawable.ic_keyboard_arrow_up_black);

                    }

                    isShowImage = !isShowImage;
                }

                break;
            case R.id.btnVideo:
                if (videoFileAttachments.size() > 0) {
                    if (isShowVideo) {
                        videoRecycle.setVisibility(View.VISIBLE);
                        imgVideoColaps.setImageResource(R.drawable.ic_keyboard_arrow_down);
                    } else {
                        videoRecycle.setVisibility(View.GONE);
                        imgVideoColaps.setImageResource(R.drawable.ic_keyboard_arrow_up_black);
                    }

                    isShowVideo = !isShowVideo;
                }
                break;
            case R.id.editDesc:
//                Description description = new Description();
//                description.setDescription(edtDescription.getValueString());
//                database.descriptionDao().update(edtDescription.getValueString(), id);
//                Toast.makeText(getContext(), "توضیح بروز شد", Toast.LENGTH_SHORT).show();

                enableView();

                break;
            case R.id.btnSave :
                Description description1 = new Description();
                if (edtDescription.getValueString() != null) {
                    description1.setDescription(edtDescription.getValueString());

                    if (database.descriptionDao().getDescription(id) == null) {
                        description1.setQuestionIdentifier(id);
                        database.descriptionDao().insert(description1);
                        Toast.makeText(getContext(), "توضیح جدید اضافه شد", Toast.LENGTH_SHORT).show();
                    } else {
                        description1.setDescription(edtDescription.getValueString());
                        database.descriptionDao().update(edtDescription.getValueString(), id);
                        Toast.makeText(getContext(), "توضیح بروز شد", Toast.LENGTH_SHORT).show();
                    }
                    disableView();
                } else {
                    Toast.makeText(getActivity(), "توضیح را وارد کنید", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    private void disableView() {
        btnSave.setEnabled(false);
        btnSave.setFocusable(false);
        btnSave.setBackgroundColor(getResources().getColor(R.color.secondryTextColor));
        editDesc.setBackgroundColor(getResources().getColor(R.color.redColor));
        editDesc.setFocusable(true);
        editDesc.setEnabled(true);
        edtDescription.disableView();
        edtDescription.setBackgroundColor(getResources().getColor(R.color.secondryTextColor));

    }

    private void enableView() {
        btnSave.setEnabled(true);
        btnSave.setFocusable(true);
        btnSave.setBackgroundColor(getResources().getColor(R.color.secondaryBack1));
        edtDescription.enableView();
        editDesc.setFocusable(false);
        editDesc.setEnabled(true);
        editDesc.setBackgroundColor(getResources().getColor(R.color.secondryTextColor));
        edtDescription.enableView();
    }

}
