package com.military.militaryservice.newCalender.persiancalendar.entities;

import com.military.militaryservice.newCalender.persiancalendar.calendar.CivilDate;

public class GregorianCalendarEvent extends AbstractEvent<CivilDate> {
    public GregorianCalendarEvent(CivilDate date, String title, boolean holiday) {
        this.date = date;
        this.title = title;
        this.holiday = holiday;
    }
}
