package com.military.militaryservice.newCalender.persiancalendar.entities;

import com.military.militaryservice.newCalender.persiancalendar.calendar.IslamicDate;

public class IslamicCalendarEvent extends AbstractEvent<IslamicDate> {
    public IslamicCalendarEvent(IslamicDate date, String title, boolean holiday) {
        this.date = date;
        this.title = title;
        this.holiday = holiday;
    }
}
