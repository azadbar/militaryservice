package com.military.militaryservice.newCalender.persiancalendar.ui.preferences.locationathan.location

import android.view.View
import androidx.lifecycle.ViewModel

class LocationAdapterModel constructor(val city: String, val country: String, val callback: View.OnClickListener) : ViewModel()
