package com.military.militaryservice.newCalender.persiancalendar.di

import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.military.militaryservice.newCalender.persiancalendar.MainApplication
import com.military.militaryservice.newCalender.persiancalendar.ui.MainActivityy
import com.military.militaryservice.newCalender.persiancalendar.ui.calendar.CalendarFragment
import com.military.militaryservice.newCalender.persiancalendar.ui.calendar.month.DaysPaintResources
import javax.inject.Inject
import javax.inject.Singleton


@PerFragment
class CalendarFragmentDependency @Inject
constructor(activity: MainActivityy) {
    val daysPaintResources: DaysPaintResources = DaysPaintResources(activity)

    @Inject
    lateinit var calendarFragment: CalendarFragment
        internal set
}

@Singleton
class AppDependency @Inject
constructor(app: MainApplication) {
    val sharedPreferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(app)
}

@PerActivity
class MainActivityDependency @Inject
constructor() {
    @Inject
    lateinit var mainActivity: MainActivityy
        internal set
}

//
//@PerChildFragment
//public final class MonthFragmentDependency {
//    @Inject
//    public MonthFragmentDependency() {
//    }
//}
