package com.military.militaryservice.newCalender.persiancalendar

import android.content.Intent
import com.google.android.apps.dashclock.api.DashClockExtension
import com.google.android.apps.dashclock.api.ExtensionData
import com.military.militaryservice.newCalender.persiancalendar.ui.MainActivityy
import com.military.militaryservice.newCalender.persiancalendar.utils.Utils

class DashClockUpdate : DashClockExtension() {

    override fun onUpdateData(reason: Int) {
        setUpdateWhenScreenOn(true)
        val mainCalendar = Utils.getMainCalendar()
        val jdn = Utils.getTodayJdn()
        val date = Utils.getDateFromJdnOfCalendar(mainCalendar, jdn)
        publishUpdate(ExtensionData().visible(true)
                .icon(Utils.getDayIconResource(date.dayOfMonth))
                .status(Utils.getMonthName(date))
                .expandedTitle(Utils.dayTitleSummary(date))
                .expandedBody(Utils.dateStringOfOtherCalendars(jdn, Utils.getSpacedComma()))
                .clickIntent(Intent(applicationContext, MainActivityy::class.java)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)))
    }
}
