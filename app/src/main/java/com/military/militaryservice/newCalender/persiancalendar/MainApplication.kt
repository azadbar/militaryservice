package com.military.militaryservice.newCalender.persiancalendar

import android.content.Context
import androidx.multidex.MultiDex
import com.military.militaryservice.newCalender.persiancalendar.di.DaggerAppComponent
import com.military.militaryservice.newCalender.persiancalendar.utils.Utils

import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class MainApplication : DaggerApplication()  {
    override fun onCreate() {
        super.onCreate()
//        ReleaseDebugDifference.mainApplication(this)
        Utils.initUtils(applicationContext)
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> = DaggerAppComponent.factory().create(this)


    override fun attachBaseContext(base: Context) {
        MultiDex.install(this)
        super.attachBaseContext(base)
    }
}
