package com.military.militaryservice.newCalender.persiancalendar.entities;

import com.military.militaryservice.newCalender.persiancalendar.calendar.PersianDate;

public class PersianCalendarEvent extends AbstractEvent<PersianDate> {
    public PersianCalendarEvent(PersianDate date, String title, boolean holiday) {
        this.date = date;
        this.title = title;
        this.holiday = holiday;
    }
}
