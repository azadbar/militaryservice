package com.military.militaryservice.model;

import com.military.militaryservice.enums.AttachmentType;

import java.io.Serializable;
import java.util.ArrayList;

public class Attachment implements Serializable {

    private int id;
    private String name;
    private String fileAddrerss;
    private AttachmentType attachmentTypes;
    private ArrayList<FileLocation> fileLocations;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFileAddrerss() {
        return fileAddrerss;
    }

    public void setFileAddrerss(String fileAddrerss) {
        this.fileAddrerss = fileAddrerss;
    }

    public AttachmentType getAttachmentTypes() {
        return attachmentTypes;
    }

    public void setAttachmentTypes(AttachmentType attachmentTypes) {
        this.attachmentTypes = attachmentTypes;
    }

    public ArrayList<FileLocation> getFileLocations() {
        return fileLocations;
    }

    public void setFileLocations(ArrayList<FileLocation> fileLocations) {
        this.fileLocations = fileLocations;
    }
}
