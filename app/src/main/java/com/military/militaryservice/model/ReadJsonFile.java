package com.military.militaryservice.model;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.military.militaryservice.orderInfo.ProjectResponse;
import com.military.militaryservice.utils.PreferencesData;

import java.io.File;
import java.io.FileInputStream;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;

public class ReadJsonFile {
    public ProjectResponse ReadFile(Context context, String path) {
        ProjectResponse object = null;
        try {
            File yourFile = new File("", path);
            FileInputStream stream = new FileInputStream(yourFile);
            String jsonStr = null;
            try {
                FileChannel fc = stream.getChannel();
                MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());

                jsonStr = Charset.defaultCharset().decode(bb).toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                stream.close();
            }
            /*  String jsonStr = "{\n\"data\": [\n    {\n        \"id\": \"1\",\n        \"title\": \"Farhan Shah\",\n        \"duration\": 10\n    },\n    {\n        \"id\": \"2\",\n        \"title\": \"Noman Shah\",\n        \"duration\": 10\n    },\n    {\n        \"id\": \"3\",\n        \"title\": \"Ahmad Shah\",\n        \"duration\": 10\n    },\n    {\n        \"id\": \"4\",\n        \"title\": \"Mohsin Shah\",\n        \"duration\": 10\n    },\n    {\n        \"id\": \"5\",\n        \"title\": \"Haris Shah\",\n        \"duration\": 10\n    }\n  ]\n\n}\n";
             */

            // Getting data JSON Array nodes
            // do what do you want on your interface

            PreferencesData.saveString(context, "json", jsonStr);
            String mJsonString = jsonStr;
            JsonParser parser = new JsonParser();
            JsonElement mJson = parser.parse(mJsonString);

            Gson gson = new Gson();

//            object = gson.fromJson(mJson, new TypeToken<ArrayList<ProjectResponse>>() {
//            }.getType());
            object = gson.fromJson(mJson, ProjectResponse.class);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return object;

    }
}