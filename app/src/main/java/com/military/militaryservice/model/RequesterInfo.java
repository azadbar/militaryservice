package com.military.militaryservice.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
public class RequesterInfo implements Serializable {

	private String requesterCode;
	private String requesterMobileNumber;
	private String requesterIdentifier;
	private String letterNumber;


	public String getRequesterCode() {
		return requesterCode;
	}

	public String getRequesterMobileNumber() {
		return requesterMobileNumber;
	}

	public String getRequesterIdentifier() {
		return requesterIdentifier;
	}

	public String getLetterNumber() {
		return letterNumber;
	}

	public void setRequesterCode(String requesterCode) {
		this.requesterCode = requesterCode;
	}

	public void setRequesterMobileNumber(String requesterMobileNumber) {
		this.requesterMobileNumber = requesterMobileNumber;
	}

	public void setRequesterIdentifier(String requesterIdentifier) {
		this.requesterIdentifier = requesterIdentifier;
	}

	public void setLetterNumber(String letterNumber) {
		this.letterNumber = letterNumber;
	}
}
