package com.military.militaryservice.model;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;

public class PersonInfo implements Serializable {


    private String firstName;
    private String lastName;
    private String nickName;
    private String fatherName;
    private String nationalCode;
    private String mobileNumbers;
    private String workAddress;
    private int personImageId;
    @Embedded
    private PersonImage personImage;
    private String homeAddress;


    public int getPersonImageId() {
        return personImageId;
    }

    public void setPersonImageId(int personImageId) {
        this.personImageId = personImageId;
    }


    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getNickName() {
        return nickName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public String getNationalCode() {
        return nationalCode;
    }

    public String getMobileNumbers() {
        return mobileNumbers;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public PersonImage getPersonImage() {
        return personImage;
    }

    public String getHomeAddress() {
        return homeAddress;
    }


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public void setNationalCode(String nationalCode) {
        this.nationalCode = nationalCode;
    }

    public void setMobileNumbers(String mobileNumbers) {
        this.mobileNumbers = mobileNumbers;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public void setPersonImage(PersonImage personImage) {
        this.personImage = personImage;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }
}
