package com.military.militaryservice.model;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface PersonResponsDao {

    @Query("select * from  personresponse")
    List<PersonResponse> getPersonList();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllList(List<PersonResponse> persons);

    @Insert(onConflict = REPLACE)
    void insertAddress(PersonResponse persons);


    @Query("select * from personresponse where orderIdentifier =:id")
    PersonResponse getPerson(int id);

    @Query("DELETE FROM personresponse")
    public void nukeTable();


    @Delete
    void delete(PersonResponse persons);

    @Query("delete from personresponse where orderIdentifier =:id")
    void deleteItem(int id);

    @Update
    int updatePerson(PersonResponse personResponse);

}
