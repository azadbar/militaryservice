package com.military.militaryservice.model;

/**
 * Created by m.azadbar on 5/8/2018.
 */

public class MyMenuItem {

    private int imageColor;
    private int id;
    private String title;
    private int drawableId;
    private String loitteAnim;
    private boolean isAnim;
    private int backColor;

    public MyMenuItem(int id, String title, int drawableId, String loitteAnim, boolean isAnim, int backColor, int imageColor) {
        this.id = id;
        this.title = title;
        this.drawableId = drawableId;
        this.loitteAnim = loitteAnim;
        this.isAnim = isAnim;
        this.backColor = backColor;
        this.imageColor = imageColor;

    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getDrawableId() {
        return drawableId;
    }

    public int getBackColor() {
        return backColor;
    }

    public int getImageColor() {
        return imageColor;
    }

    public String getLoitteAnim() {
        return loitteAnim;
    }

    public boolean isAnim() {
        return isAnim;
    }
}
