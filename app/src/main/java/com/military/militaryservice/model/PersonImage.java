package com.military.militaryservice.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

public class PersonImage implements Serializable {

	private String name;
	private String format;
	private String base64content;



	public String getName() {
		return name;
	}

	public String getFormat() {
		return format;
	}

	public String getBase64content() {
		return base64content;
	}


	public void setName(String name) {
		this.name = name;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public void setBase64content(String base64content) {
		this.base64content = base64content;
	}
}
