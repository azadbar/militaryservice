package com.military.militaryservice.model;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.Relation;
import androidx.room.TypeConverters;

import com.military.militaryservice.orderInfo.QuestionInfosItem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity(tableName = "PersonResponse")
public class PersonResponse implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "orderIdentifier")
    private int orderIdentifier;
    private String registerDate;
    private String registerTime;
    private int personInfoId;
    @Embedded
    private PersonInfo personInfo;
    private int requesterInfoId;
    @Embedded
    private RequesterInfo requesterInfo;
    private int questionInfosId;
    @TypeConverters(DataConverter.class)
    private List<QuestionInfosItem> questionInfos = null;

    public int getOrderIdentifier() {
        return orderIdentifier;
    }

    public String getRegisterDate() {
        return registerDate;
    }

    public String getRegisterTime() {
        return registerTime;
    }

    public int getPersonInfoId() {
        return personInfoId;
    }

    public PersonInfo getPersonInfo() {
        return personInfo;
    }

    public int getRequesterInfoId() {
        return requesterInfoId;
    }

    public RequesterInfo getRequesterInfo() {
        return requesterInfo;
    }

    public int getQuestionInfosId() {
        return questionInfosId;
    }

    public List<QuestionInfosItem> getQuestionInfos() {
        return questionInfos;
    }


    public void setOrderIdentifier(int orderIdentifier) {
        this.orderIdentifier = orderIdentifier;
    }

    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    public void setRegisterTime(String registerTime) {
        this.registerTime = registerTime;
    }

    public void setPersonInfoId(int personInfoId) {
        this.personInfoId = personInfoId;
    }

    public void setPersonInfo(PersonInfo personInfo) {
        this.personInfo = personInfo;
    }

    public void setRequesterInfoId(int requesterInfoId) {
        this.requesterInfoId = requesterInfoId;
    }

    public void setRequesterInfo(RequesterInfo requesterInfo) {
        this.requesterInfo = requesterInfo;
    }

    public void setQuestionInfosId(int questionInfosId) {
        this.questionInfosId = questionInfosId;
    }

    public void setQuestionInfos(List<QuestionInfosItem> questionInfos) {
        this.questionInfos = questionInfos;
    }
}