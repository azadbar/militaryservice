package com.military.militaryservice.model;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.military.militaryservice.orderInfo.QuestionInfosItem;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

public class DataConverter {

    private static Gson gson = new Gson();

    @TypeConverter
    public static List<QuestionInfosItem> stringToList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<QuestionInfosItem>>() {
        }.getType();
        return gson.fromJson(data,listType);
    }

    @TypeConverter
    public static String ListToString(List<QuestionInfosItem> questionInfosItems) {
        return gson.toJson(questionInfosItems);
    }
}
