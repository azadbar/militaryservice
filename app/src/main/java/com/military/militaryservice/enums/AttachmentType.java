package com.military.militaryservice.enums;

import android.content.res.Resources;

import androidx.room.TypeConverter;

public enum AttachmentType {
    VIDEO(0),
    IMAGE(1),
    VOICE(2);

    private int metodType;
    private String format;

    AttachmentType(int method) {
        this.metodType = method;

    }


    public int getMetodType() {
        return metodType;
    }

    public String getFormat() {
        switch (this) {
            case VIDEO:
                return "mp4";
            case IMAGE:
                return "png";
            default:
                return "wav";
        }
    }


    @TypeConverter
    public static AttachmentType getDownloadStatus(int numeral){
        for(AttachmentType ds : values()){
            if(ds.metodType == numeral){
                return ds;
            }
        }
        return null;
    }

    @TypeConverter
    public static Integer getDownloadStatusInt(AttachmentType status){
         return status.metodType;
    }

    public void setMetodType(int metodType) {
        this.metodType = metodType;
    }
}
