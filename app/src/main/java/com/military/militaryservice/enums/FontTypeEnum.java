package com.military.militaryservice.enums;


public enum FontTypeEnum {
    REGULAR,
    Light,
    MEDIUM,
    BOLD
}

