package com.military.militaryservice.enums;

import androidx.room.TypeConverter;

public enum UserAccessEnum {
    READ(0),
    READ_WRITE(1),
    ADMIN(2);

    private int metodType;

    UserAccessEnum(int i) {
        this.metodType = i;
    }

    public String getAccess() {
        switch (this) {
            case READ:
                return "فقط خواندن";
            case READ_WRITE:
                return "خواندن و نوشتن";
            default:
                return "مدیر";
        }
    }

    @TypeConverter
    public static UserAccessEnum getDownloadStatus(int numeral) {
        for (UserAccessEnum ds : values()) {
            if (ds.metodType == numeral) {
                return ds;
            }
        }
        return null;
    }

    @TypeConverter
    public static Integer getDownloadStatusInt(UserAccessEnum status) {
        return status.metodType;
    }

    public void setMetodType(int metodType) {
        this.metodType = metodType;
    }

}
