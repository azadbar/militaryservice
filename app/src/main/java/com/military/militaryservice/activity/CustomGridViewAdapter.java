package com.military.militaryservice.activity;

import android.content.Context;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.model.MyMenuItem;

import java.util.ArrayList;

public class CustomGridViewAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<MyMenuItem> list;
    private int height;

    public CustomGridViewAdapter(Context context, ArrayList<MyMenuItem> list, int height) {
        mContext = context;
        this.list = list;
        this.height = height;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        View gridViewAndroid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            MyMenuItem obj = list.get(i);

            gridViewAndroid = new View(mContext);
            gridViewAndroid = inflater.inflate(R.layout.item_menu_main_circle, null);
            TextView txtTitle =  gridViewAndroid.findViewById(R.id.txtTitle);
            ImageView imageView =  gridViewAndroid.findViewById(R.id.imgMenu);
            LottieAnimationView lottie =  gridViewAndroid.findViewById(R.id.lottie);
            RelativeLayout root =  gridViewAndroid.findViewById(R.id.root);

            txtTitle.setText(obj.getTitle());

            if (obj.isAnim()) {
                lottie.setVisibility(View.VISIBLE);
                imageView.setVisibility(View.GONE);

                lottie.setAnimation(obj.getLoitteAnim());
                lottie.playAnimation();
                lottie.loop(true);

            } else {
                lottie.setVisibility(View.GONE);
                imageView.setVisibility(View.VISIBLE);

                imageView.setImageResource(obj.getDrawableId());
                imageView.setColorFilter(obj.getImageColor(), PorterDuff.Mode.SRC_ATOP);
            }
//            bind(list.get(position), listener);
//
//            itemView.post(() -> {
//                int cellWidth = holder.itemView.getWidth();// this will give you cell width dynamically
//                int cellHeight = holder.itemView.getHeight();// this will give you cell height dynamically
//                ViewGroup.LayoutParams params = holder.shape.getLayoutParams();
//                params.height = cellWidth;
//                params.width = cellWidth;
//                holder.shape.setLayoutParams(params);
//
//            });

//            holder.shape.post(() -> {
//                int cellWidth = holder.shape.getWidth();// this will give you cell width dynamically
//                int cellHeight = holder.shape.getHeight();// this will give you cell height dynamically
//                ViewGroup.LayoutParams params = holder.lottie.getLayoutParams();
//                params.height = (int) ((cellWidth) * 2/3.0);
//                params.width = (int) ((cellWidth) * 2/3.0);
//                holder.lottie.setLayoutParams(params);
//
//            });
            setFadeAnimation(txtTitle);

            // Set height and width constraints for the image view
            root.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height));
            // Set Padding for images
            root.setPadding(1, 1, 1, 1);

        } else {
            gridViewAndroid = (View) convertView;
        }

        return gridViewAndroid;
    }

    private void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }
}