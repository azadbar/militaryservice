package com.military.militaryservice.activity;

import android.content.Intent;
import android.os.Bundle;

import com.military.militaryservice.baseView.BaseActivity;
import com.military.militaryservice.splash.SplashActivity;
import com.military.militaryservice.utils.Constants;

public class StartActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        if (PreferencesData.getIsLogin(this)){
//            Intent intent = new Intent(this, MainActivity.class);
//            startActivity(intent);
//        }else {
//        if (!Constants.isLogin) {
//            Intent intent = new Intent(this, SplashActivity.class);
//            Constants.isLogin = true;
//            startActivity(intent);
//            finish();
//        } else {
//            Intent intent = new Intent(this, MainActivity.class);
//            startActivity(intent);
//        }

        Intent intent = new Intent(this, SplashActivity.class);
        Constants.isLogin = true;
        startActivity(intent);
        finish();
//        }
    }
}
