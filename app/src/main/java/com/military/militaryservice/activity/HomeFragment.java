package com.military.militaryservice.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.CallLog;
import android.provider.Settings;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.military.militaryservice.BuildConfig;
import com.military.militaryservice.R;
import com.military.militaryservice.addUser.AddUserActivity;
import com.military.militaryservice.baseView.BaseFragment;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.customView.RoundedLoadingView;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.database.Contact;
import com.military.militaryservice.database.User;
import com.military.militaryservice.dialog.CustomDialog;
import com.military.militaryservice.dialog.ValidationUserDialog;
import com.military.militaryservice.emergency_call.AddContectActivity;
import com.military.militaryservice.enums.UserAccessEnum;
import com.military.militaryservice.maps.MapsListActivity;
import com.military.militaryservice.model.MyMenuItem;
import com.military.militaryservice.model.ReadJsonFile;
import com.military.militaryservice.orderInfo.ProjectResponse;
import com.military.militaryservice.projectList.ListProjectActivity;
import com.military.militaryservice.qrCde.ScanActivity;
import com.military.militaryservice.resultInfo.FileAttachmentsItem;
import com.military.militaryservice.resultInfo.QuestionWithResultsItem;
import com.military.militaryservice.resultInfo.ResultResponse;
import com.military.militaryservice.selectQuestion.SelectQuestionActivity;
import com.military.militaryservice.sendMessage.SendMessageActivity;
import com.military.militaryservice.splash.SplashActivity;
import com.military.militaryservice.utils.Constants;
import com.military.militaryservice.utils.PermissionHandler;
import com.military.militaryservice.utils.PreferencesData;
import com.military.militaryservice.utils.ShakeDetector;
import com.obsez.android.lib.filechooser.ChooserDialog;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

public class HomeFragment extends BaseFragment implements MenuAdapter.OnItemClickListener, LocationListener {

    public static final String IMAGE_RESOURCE_ID = "iconResourceID";
    public static final String ITEM_NAME = "itemName";
    private static final int REQUEST_CODE_PERMISSION = 100;
    private static final long DELAY = 5000;
    @BindView(R.id.roundedLoadingView)
    RoundedLoadingView roundedLoadingView;
    //    @BindView(R.id.root)
//    BaseRelativeLayout root;
//    @BindView(R.id.imgBack)
//    BaseImageView imgBack;
//    @BindView(R.id.tvCenterTitle)
//    BaseTextView tvCenterTitle;
    @BindView(R.id.image)
    AppCompatImageView image;
    //    @BindView(R.id.imgLogo)
//    BaseImageView imgLogo;
//    @BindView(R.id.imgHelp)
//    BaseImageView imgHelp;
    @BindView(R.id.unistall)
    BaseImageView unistall;
    @BindView(R.id.grid)
    GridView grid;
    private MenuAdapter adapter;
    private ProjectResponse personResponse;
    private ArrayList<ProjectResponse> personResponseList = new ArrayList<>();
    private boolean doubleBackToExitPressedOnce;
    private int shakeCount;

    private String pathSaveIn;
    private boolean inImage = true;
    //    private MagicalCamera magicalTakePhoto;
    private AppDatabase database;
    private ResultResponse resultResponse;
    private ArrayList<QuestionWithResultsItem> questionWithResultsItemArrayList = new ArrayList<>();
    private KProgressHUD kProgressHUD;
    private int REQUEST_FINISH_ACTIVITY = 300;
    private double latitud;
    private double longitud;
    //    Location location;
//    LocationManager locationManager;
//    boolean GpsStatus = false;
    Criteria criteria;
    String Holder;
//    private static GoogleApiClient mGoogleApiClient;
//    private static final int ACCESS_FINE_LOCATION_INTENT_ID = 3;
//    private static final String BROADCAST_ACTION = "android.location.PROVIDERS_CHANGED";
//    private static final int REQUEST_CHECK_SETTINGS = 0x1;

    // location updates interval - 10sec
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    // fastest updates interval - 5 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 5000;
    private static final int REQUEST_CHECK_SETTINGS = 100;
    // bunch of location related apis
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;
    // boolean flag to toggle the ui
    private Boolean mRequestingLocationUpdates;
    private String mLastUpdateTime;
    private static final String BROADCAST_ACTION = "android.location.PROVIDERS_CHANGED";
    private static final String FILIGHT_MODE_ACTION = "\"android.intent.action.AIRPLANE_MODE\"";
    private ArrayList<String> fileList;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle bundle) {

        View view = inflater.inflate(R.layout.home_fragment, container, false);
        ButterKnife.bind(this, view);
//        imgBack.setColorFilter(getResources().getColor(R.color.white));
//        imgLogo.setVisibility(View.VISIBLE);
//        Glide.with(getActivity()).load(R.drawable.shahed).into(imgLogo);
//        imgHelp.setVisibility(View.VISIBLE);
//        tvCenterTitle.setVisibility(View.GONE);
        setMainMenu();

        SensorManager mSensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        Sensor mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        ShakeDetector mShakeDetector = new ShakeDetector();
        mShakeDetector.setOnShakeListener(count -> HomeFragment.this.shakeCount = count);

        mSensorManager.registerListener(mShakeDetector, mAccelerometer, SensorManager.SENSOR_DELAY_UI);


        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                //TODO location permissions are granted code here your feature
            }
        };
//        locationPermissions.askPermissions(runnable);

        shakeMethod();

        File dir = new File(Constants.storage_Dir_file_attachment, "");
        if (!dir.exists()) {
            dir.mkdirs();
        }
//        Toast.makeText(getContext(), Constants.getUser(getContext()).getUsername() + "\n" + Constants.getUser(getContext()).getPassword()
//                + "\n" + Constants.getUser(getContext()).getId(), Toast.LENGTH_SHORT).show();
        //change username and password
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (PreferencesData.getChangePassword(getContext()) && Constants.getUser(getContext()).getUserAccess() == UserAccessEnum.ADMIN) {
                    CustomDialog dialog = new CustomDialog(getActivity());
                    dialog.setOkListener(getResources().getString(R.string.ok), view1 -> {

                        Intent intent = new Intent(getContext(), AddUserActivity.class);
                        startActivity(intent);
                        dialog.dismiss();

                    });
                    dialog.setCancelListener(getResources().getString(R.string.cancel), view12 -> dialog.dismiss());
                    dialog.setDescription("شما بعنوان مدیر برنامه وارد شدید لطفا رمز عبور خود را تغییر دهید");
                    dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
                    dialog.setIcon(R.drawable.ic_lock_black_24dp, getResources().getColor(R.color.redColor));
                    dialog.show();
                }
            }
        }, DELAY);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (PreferencesData.getChangePasswordUser(getContext()) &&
                        (Constants.getUser(getContext()).getUserAccess() == UserAccessEnum.READ || Constants.getUser(getContext()).getUserAccess() == UserAccessEnum.READ_WRITE)) {
                    CustomDialog dialog = new CustomDialog(getActivity());
                    dialog.setOkListener(getResources().getString(R.string.ok), view1 -> {
                        dialog.dismiss();
                        ValidationUserDialog dialog1 = new ValidationUserDialog(getContext());
                        dialog1.setEditTextField(Constants.getUser(getContext()).getUsername(), Constants.getUser(getContext()).getPassword());
                        dialog1.setIcon(R.drawable.ic_lock_black_24dp, getResources().getColor(R.color.secondaryBack1));
                        dialog1.setDialogTitle("لطفا رمز عبور خود را تغییر دهید");
                        dialog1.setColorTitle(getResources().getColor(R.color.primaryTextColor));
                        dialog1.setOkListener("ویرایش", v -> {
                            if (!TextUtils.equals(dialog1.getUserName(), null)) {
                                if (!TextUtils.equals(dialog1.getPasswor(), null)) {
                                    User user = new User();
                                    user.setId(Constants.getUser(getContext()).getId());
                                    user.setUsername(dialog1.getUserName());
                                    user.setPassword(dialog1.getPasswor());
                                    user.setUserAccess(Constants.getUser(getContext()).getUserAccess());
                                    user.setDate(new Date());
                                    database = AppDatabase.getInMemoryDatabase(getContext());
                                    database.userDao().update(user.getPassword(), user.getUsername(), user.getUserAccess(), user.getDate(), user.getId());
                                    PreferencesData.isChangePasswordUser(getContext(), false);
                                    dialog1.dismiss();
                                    Toast.makeText(getContext(), "ویرایش با موفقیت انجام شد", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getContext(), "رمز عبور را وارد کنید", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getContext(), "نام کاربری را وارد کنید", Toast.LENGTH_SHORT).show();
                            }
                        });
                        dialog1.setCancelListener("لغو", v -> dialog1.dismiss());
                        dialog1.show();
                        dialog.dismiss();

                    });
                    dialog.setCancelListener(getResources().getString(R.string.cancel), view12 -> dialog.dismiss());
                    dialog.setDescription("شما بعنوان کاربر عادی وارد برنامه شدید لطفا رمز عبور خود را تغییر دهید");
                    dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
                    dialog.setIcon(R.drawable.ic_lock_black_24dp, getResources().getColor(R.color.redColor));
                    dialog.show();
                }
            }
        }, DELAY);


//        initGoogleAPIClient();
//        showSettingDialog();
        init();
        startCheckLocation();

        return view;
    }

    private void startCheckLocation() {
        Dexter.withActivity(getActivity())
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        CustomDialog dialog = new CustomDialog(getActivity());
                        dialog.setOkListener(getActivity().getResources().getString(R.string.ok), view -> {
                            dialog.dismiss();
                            mRequestingLocationUpdates = true;
                            startLocationUpdates();
                        });
                        dialog.setIcon(R.drawable.ic_add_location, getActivity().getResources().getColor(R.color.green));
                        dialog.setDialogTitle("سیستم موقعیت یاب");
                        dialog.setColorTitle(getActivity().getResources().getColor(R.color.primaryTextColor));
                        dialog.setDescription("موقعیت مکانی شما غیرفعال است. آیا مایلید آن را فعال کنید؟");
                        dialog.show();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if (response.isPermanentlyDenied()) {
                            // open device settings when the permission is
                            // denied permanently
                            openSettings();
                            Toast.makeText(getContext(), "Denied", Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                        Toast.makeText(getContext(), "token", Toast.LENGTH_SHORT).show();

                    }
                }).check();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        restoreValuesFromBundle(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("is_requesting_updates", mRequestingLocationUpdates);
        outState.putParcelable("last_known_location", mCurrentLocation);
        outState.putString("last_updated_on", mLastUpdateTime);
    }

    private void shakeMethod() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            private Contact contact;

            @Override
            public void run() {
                if (getActivity() != null) {
                    getActivity().runOnUiThread(() -> {
                        AppDatabase database = AppDatabase.getInMemoryDatabase(getContext());
                        database.contactDao().getContactList();
                        for (Contact c : database.contactDao().getContactList()) {
                            if (c.isDefault()) {
                                contact = c;
                            }
                        }
                        if (shakeCount > 1 && shakeCount <= 4) {
//                            magicalTakePhoto = new MagicalCamera(getActivity(), 80, locationPermissions);
//                            magicalTakePhoto.takePhoto();
                            if (contact != null) {
                                Intent intent = new Intent(Intent.ACTION_CALL);
                                intent.setData(Uri.parse("tel:" + contact.getDefaultPhone().trim()));
                                startActivity(intent);
//                                getContext().getContentResolver().delete(android.provider.CallLog.Calls.CONTENT_URI, null, null) ;
//                                deleteAnEntryFromCallLog("09119316040");
//                                String queryString="NUMBER="+contact.getDefaultPhone().trim();
//                                getContext().getContentResolver().delete(CallLog.Calls.CONTENT_URI,queryString,null);
//                                Uri uri = Uri.parse("content://call_log/calls");
//                                int d  = getContext().getContentResolver().delete(uri, null, null);
                            }
//                        } else if (shakeCount > 3 && shakeCount < 15) {
                            try {
                                if (contact != null) {
//                                    getLocation();
//                                        if (Holder != null) {
//                                            if (ActivityCompat.checkSelfPermission(getContext(),
//                                                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
//                                                    && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                                                return;
//                                            }
//                                            location = locationManager.getLastKnownLocation(Holder);
//                                            locationManager.requestLocationUpdates(Holder, 1200, 7, HomeFragment.this);
                                    SmsManager smsManager = SmsManager.getDefault();
                                    String link = "";
                                    if (mCurrentLocation != null)
                                        link = "http://maps.google.com/maps?q=" + mCurrentLocation.getLatitude() + "," + mCurrentLocation.getLongitude() + "?z=22&q=" + mCurrentLocation.getLatitude() + "," + mCurrentLocation.getLongitude();

                                    ArrayList<String> parts = smsManager.divideMessage(contact.getMessage() + "\nموقعیت فعلی من در لینک زیر می باشد:\n" + link);
                                    smsManager.sendMultipartTextMessage(contact.getDefaultPhone(), null, parts, null, null);
                                    if (contact.getSendMessageist().size() > 0) {
                                        for (int i = 0; i < contact.getSendMessageist().size(); i++) {
                                            smsManager.sendMultipartTextMessage(contact.getSendMessageist().get(i).getMobile(), null, parts, null, null);
                                        }
                                    }
                                    Toast.makeText(getContext().getApplicationContext(), "پیام ارسال شد" + link, Toast.LENGTH_LONG).show();
//                                        }
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                        shakeCount = 0;
                    });
                }
            }
        }, 0, 5 * 1000);
    }

    private void init() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mSettingsClient = LocationServices.getSettingsClient(getActivity());

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location is received
                mCurrentLocation = locationResult.getLastLocation();

//                updateLocationUI();
            }
        };
        mRequestingLocationUpdates = false;
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    private void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(getActivity(), new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {

//                        Toast.makeText(getActivity(), "Started location updates!", Toast.LENGTH_SHORT).show();

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());

//                        updateLocationUI();
                    }
                })
                .addOnFailureListener(getActivity(), new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
//                                String errorMessage = "Location settings are inadequate, and cannot be " +
//                                        "fixed here. Fix in Settings.";
//                                Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
                        }

//                        updateLocationUI();
                    }
                });
    }

    private void restoreValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("is_requesting_updates")) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean("is_requesting_updates");
            }

            if (savedInstanceState.containsKey("last_known_location")) {
                mCurrentLocation = savedInstanceState.getParcelable("last_known_location");
            }

            if (savedInstanceState.containsKey("last_updated_on")) {
//                mLastUpdateTime = savedInstanceState.getString("last_updated_on");
            }
        }

//        updateLocationUI();
    }


    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package",
                BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(gpsLocationReceiver, new IntentFilter(BROADCAST_ACTION));//Register broadcast receiver to check the status of GPS
        if (mRequestingLocationUpdates && checkPermissions()) {
            startLocationUpdates();
        }


    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private BroadcastReceiver gpsLocationReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            //If Action is Location
            if (intent.getAction().matches(BROADCAST_ACTION)) {
                LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                //Check if GPS is turned ON or OFF
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    Log.e("About GPS", "GPS is Enabled in your device");
//                    updateGPSStatus("GPS is Enabled in your device");
                } else {
                    //If GPS turned OFF show Location Dialog
                    new Handler().postDelayed(sendUpdatesToUI, 10);
                    // showSettingDialog();
//                    updateGPSStatus("GPS is Disabled in your device");
                    Log.e("About GPS", "GPS is Disabled in your device");
                }

            }
        }
    };


    public static boolean isAirplaneModeOn(Context context) {
        return Settings.Global.getInt(context.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
    }

    private Runnable sendUpdatesToUI = new Runnable() {
        public void run() {
//            CustomDialog dialog = new CustomDialog(getActivity());
//            dialog.setOkListener(getActivity().getResources().getString(R.string.ok), view -> {
//                dialog.dismiss();
//                startCheckLocation();
//            });
//            dialog.setIcon(R.drawable.ic_add_location, getActivity().getResources().getColor(R.color.green));
//            dialog.setDialogTitle("سیستم موقعیت یاب");
//            dialog.setColorTitle(getActivity().getResources().getColor(R.color.primaryTextColor));
//            dialog.setDescription("موقعیت مکانی شما غیرفعال است. آیا مایلید آن را فعال کنید؟");
//            dialog.show();
            startCheckLocation();
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (gpsLocationReceiver != null)
            getContext().unregisterReceiver(gpsLocationReceiver);
    }


    public void deleteAnEntryFromCallLog(String number) {
        try {
            Uri CALLLOG_URI = Uri.parse("content://call_log/calls");
            getContext().getContentResolver().delete(CALLLOG_URI, CallLog.Calls.NUMBER + "=?", new String[]{number});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setMainMenu() {
//        adapter = new MenuAdapter(getActivity(), getMainList(), this);
////
////        int culomnCount = 3;
////        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), culomnCount, RecyclerView.VERTICAL, false);
////        myMenu.setLayoutManager(layoutManager);
////        myMenu.addItemDecoration(new EqualSpacingItemDecoration(getResources().getDimensionPixelSize(R.dimen.margin_18dp), EqualSpacingItemDecoration.GRID));
////        myMenu.setAdapter(adapter);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels / 3;
        int height = (displayMetrics.heightPixels -
                (getResources().getDimensionPixelSize(R.dimen.height_size_home_fragment)
                        + getResources().getDimensionPixelSize(R.dimen.height_size_home_fragment)
                        + getResources().getDimensionPixelSize(R.dimen.height_size_home_fragment))) / 3;

        CustomGridViewAdapter adapter = new CustomGridViewAdapter(getActivity(), getMainList(), height);
        grid.setColumnWidth(width);
        grid.setAdapter(adapter);
        grid.setVerticalScrollBarEnabled(false);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = null;
                switch (position) {
                    case 0:
                        if (PermissionHandler.hasAllPermissions(getActivity())) {
//                    ReadJsonFile readJsonFile = new ReadJsonFile();
//                    personResponse = readJsonFile.ReadFile(HomeFragment.this, Environment.getExternalStorageDirectory() + "/Download/test.json");


                            new ChooserDialog(getActivity())
                                    .withStartFile(Environment.getExternalStorageDirectory().getAbsolutePath())
                                    .withFilterRegex(false, false, ".*\\.(json)")
                                    .withChosenListener((path, pathFile) -> {
                                        Toast toast = Toast.makeText(getActivity(),
                                                "پرونده با موفقیت بارگذاری شد", Toast.LENGTH_SHORT);
                                        View v = toast.getView();
                                        v.getBackground().setColorFilter(getResources().getColor(R.color.redColor), PorterDuff.Mode.SRC_IN);
                                        toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 350);
                                        toast.show();
                                        ReadJsonFile readJsonFile = new ReadJsonFile();
                                        personResponse = readJsonFile.ReadFile(getActivity(), pathFile.getPath());
                                        personResponseList.add(personResponse);
                                        AppDatabase database = AppDatabase.getInMemoryDatabase(getActivity());
                                        database.projectResponseDao().insertAllList(personResponseList);

                                    })
                                    // to handle the back key pressed or clicked outside the dialog:
                                    .withOnCancelListener(dialog -> {
                                        Log.d("CANCEL", "CANCEL");
                                        dialog.cancel(); // MUST have
                                    })
                                    .build()
                                    .show();
                        } else {
                            PermissionHandler.requestPermissions(getActivity(), REQUEST_CODE_PERMISSION);
                        }
                        break;
                    case 1:
                        database = AppDatabase.getInMemoryDatabase(getActivity());
                        if (database.projectResponseDao().getPersonList() != null) {
                            intent = new Intent(getActivity(), ListProjectActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getActivity(), "لطفا یک پرونده ایجاد کنید", Toast.LENGTH_SHORT).show();
                        }

                        break;
                    case 2:
                        intent = new Intent(getActivity(), MapsListActivity.class);
                        startActivity(intent);
                        break;
                    case 3:
                        intent = new Intent(getActivity(), SelectQuestionActivity.class);
                        intent.putExtra("isVideo", true);
                        startActivity(intent);
                        break;
                    case 4:
                        intent = new Intent(getActivity(), SelectQuestionActivity.class);
                        intent.putExtra("isImage", true);
                        startActivity(intent);
                        break;
                    case 5:
                        intent = new Intent(getActivity(), SelectQuestionActivity.class);
                        intent.putExtra("isVoice", true);
                        startActivity(intent);
//                String queryString = "NUMBER=" + "09119316040";
//                getContext().getContentResolver().delete(CallLog.Calls.CONTENT_URI, queryString, null);
//                Cursor cursor = getContext().getContentResolver().query(CallLog.Calls.CONTENT_URI, null, CallLog.Calls.NUMBER + " = ? ", new String[]{"09119306040"}, "");
//                int idOfRowToDelete= cursor.getInt(cursor.getColumnIndex(CallLog.Calls._ID));
//                getContext().getContentResolver().delete(Uri.withAppendedPath(CallLog.Calls.CONTENT_URI, String.valueOf(idOfRowToDelete)), "", null);
                        break;

                    case 6:
                        intent = new Intent(getActivity(), AddContectActivity.class);
                        startActivity(intent);

                        break;
                    case 7:
                        ValidationUserDialog dialog = new ValidationUserDialog(getContext());
                        dialog.setIcon(R.drawable.ic_exit_to_app, getResources().getColor(R.color.secondaryBack1));
                        dialog.setDialogTitle("نام کاربری و رمز عبور خود را وارد کنید");
                        dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
                        dialog.setOkListener("ورود", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                database = AppDatabase.getInMemoryDatabase(getActivity());
                                List<User> users = database.userDao().getUsers();
                                ArrayList<String> userName = new ArrayList<>();
                                for (int i = 0; i < users.size(); i++) {
                                    userName.add(users.get(i).getUsername());
                                }
                                if (userName.contains(dialog.getUserName())) {
                                    for (User u : users) {
                                        if (TextUtils.equals(u.getUsername(), dialog.getUserName())) {
                                            if (TextUtils.equals(dialog.getPasswor(), u.getPassword())) {
                                                if (u.getUserAccess() == UserAccessEnum.ADMIN) {
                                                    Intent intent = new Intent(getContext(), AddUserActivity.class);
                                                    startActivity(intent);
                                                    dialog.dismiss();
                                                } else {
                                                    Toast.makeText(getContext(), "نام کاربری شما به این بخش دسترسی ندارد", Toast.LENGTH_SHORT).show();
                                                }
                                            } else {
                                                Toast.makeText(getContext(), getResources().getString(R.string.wrong_password), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }
                                } else {
                                    Toast.makeText(getContext(), "نام کاربری شما در سیستم ثبت نشده است", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
//
                        dialog.show();
                        break;
                    case 8:
                        if (Constants.getUser(getContext()).getUserAccess() != UserAccessEnum.READ) {
                            kProgressHUD = KProgressHUD.create(getActivity())
                                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                                    .setLabel("لطفا منتظر بمانید")
                                    .setDetailsLabel("در حال ایجاد فایل پشتیبان")
                                    .setCancellable(false)
                                    .setAnimationSpeed(2)
                                    .setDimAmount(0.8f)
                                    .setBackgroundColor(getResources().getColor(R.color.secondaryBack1))
                                    .show();
                            File fdelete = new File(Environment.getExternalStorageDirectory(), "/shahed/" + "orderResults.json");
                            if (fdelete.exists()) {
                                fdelete.delete();
                            }
                            File fdeleteZip = new File(Environment.getExternalStorageDirectory(), "/Download/" + "resultInfo.zip");
                            if (fdeleteZip.exists()) {
                                fdeleteZip.delete();
                            }
                            new Handler().postDelayed(() -> {
                                database = AppDatabase.getInMemoryDatabase(getActivity());
                                List<ProjectResponse> personList = database.projectResponseDao().getPersonList();
                                if (personList.size() > 0) {
                                    for (int i = 0; i < personList.size(); i++) {
                                        resultResponse = new ResultResponse();
                                        resultResponse.setOrderIdentifier(personList.get(i).getOrderIdentifier());
                                        if (personList.get(i).getQuestionInfos().size() > 0) {
                                            for (int j = 0; j < personList.get(i).getQuestionInfos().size(); j++) {
                                                QuestionWithResultsItem question = new QuestionWithResultsItem();
                                                question.setId(personList.get(i).getQuestionInfos().get(j).getQuestionInfosId());
                                                question.setQuestionIdentifier(personList.get(i).getQuestionInfos().get(j).getQuestionIdentifier());
                                                question.setQuestion(personList.get(i).getQuestionInfos().get(j).getQuestionInfo());
                                                question.setGeoLocations(new ArrayList<>());
//                                question.setQuestionIdentifier(personList.get(i).getQuestionInfos().get(j).getQuestionIdentifier());
                                                question.setResult(database.descriptionDao().getDescription(personList.get(i).getQuestionInfos().get(j)
                                                        .getQuestionInfosId()) != null ? database.descriptionDao().getDescription(personList.get(i).getQuestionInfos().get(j)
                                                        .getQuestionInfosId()).getDescription() : "");
                                                List<FileAttachmentsItem> select = database.fileAttachmentDao().select(personList.get(i).getQuestionInfos().get(j).getQuestionInfosId());
                                                if (select.size() > 0) {
                                                    question.setFileAttachments((ArrayList<FileAttachmentsItem>) select);
                                                }
                                                questionWithResultsItemArrayList.add(question);
                                            }
                                            resultResponse.setQuestionWithResults(questionWithResultsItemArrayList);
                                        }
                                        resultResponse.setComplementaryReport("");
                                        if (resultResponse != null) {
                                            makeJson();
                                        }
                                    }
                                    personList.clear();
                                    resultResponse = new ResultResponse();
                                    questionWithResultsItemArrayList.clear();
                                }
                                File file = new File(Environment.getExternalStorageDirectory(), Constants.SHAHED);
                                File file1 = new File(Environment.getExternalStorageDirectory(), "/Download/resultInfo.zip");
                                 fileList = new ArrayList<>();
                                compressDirectory(file.getAbsolutePath(), file1.getAbsolutePath());

//                                File file = new File(Environment.getExternalStorageDirectory(), Constants.SHAHED);
//                                File file1 = new File(Environment.getExternalStorageDirectory(), "/Download/resultInfo.zip");
//                                zipFileAtPath(file.getAbsolutePath() + Constants.SHAHED, file.getAbsolutePath() + "/Download/resultInfo.zip");
//                                zip(file,file1);
                                getActivity().runOnUiThread(() -> kProgressHUD.dismiss());
//                    Toast.makeText(getActivity(), "فایل resultInfo.zip در مسیر فایل منیجر گوشی ذخیره شد", Toast.LENGTH_SHORT).show();
                                CustomDialog permissionDialog = new CustomDialog(getActivity());
                                permissionDialog.setOkListener(getActivity().getResources().getString(R.string.ok), v -> {
                                    permissionDialog.dismiss();
                                });
                                permissionDialog.setIcon(R.drawable.ic_check_circle, getActivity().getResources().getColor(R.color.green));
                                permissionDialog.setDialogTitle(getActivity().getResources().getString(R.string.success_store));
                                permissionDialog.setColorTitle(getActivity().getResources().getColor(R.color.primaryTextColor));

                                permissionDialog.setDescription(getActivity().getResources().getString(R.string.guide_access));
                                permissionDialog.show();

                            }, 1000);

                        }else {
                            Toast.makeText(getContext(), "شما به این قسمت دسترسی ندارید", Toast.LENGTH_SHORT).show();
                        }
                        break;

                }

            }
        });

//        grid.setOnTouchListener(new View.OnTouchListener() {
//
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                return event.getAction() == MotionEvent.ACTION_MOVE;
//            }
//        });
    }

    private ArrayList<MyMenuItem> getMainList() {
        ArrayList<MyMenuItem> myMenuItems = new ArrayList<>();
        myMenuItems.add(new MyMenuItem(1, "ایجاد پرونده", R.drawable.ic_add_ducument, "document.json", true, getResources().getColor(R.color.white), getResources().getColor(R.color.primaryTextColor)));
        myMenuItems.add(new MyMenuItem(2, "لیست پرونده ها", R.drawable.ic_list_ducumnet, "list.json", true, getResources().getColor(R.color.white), getResources().getColor(R.color.primaryTextColor)));
        myMenuItems.add(new MyMenuItem(7, "آدرس منتخب", R.drawable.ic_add_location, "location.json", true, getResources().getColor(R.color.white), getResources().getColor(R.color.primaryTextColor)));
        myMenuItems.add(new MyMenuItem(4, "ثبت ویدیو", R.drawable.ic_video, "video.json", true, getResources().getColor(R.color.white), getResources().getColor(R.color.primaryTextColor)));
        myMenuItems.add(new MyMenuItem(5, "ثبت عکس", R.drawable.ic_add_a_photo, "take_photo.json", true, getResources().getColor(R.color.white), getResources().getColor(R.color.primaryTextColor)));
        myMenuItems.add(new MyMenuItem(6, "ضبط صدا", R.drawable.ic_keyboard_voice, "voice.json", true, getResources().getColor(R.color.white), getResources().getColor(R.color.primaryTextColor)));
//        myMenuItems.add(new MyMenuItem(9, "حذف اضطراری", R.drawable.ic_emergancy_delete, getResources().getColor(R.color.white), getResources().getColor(R.color.redColor)));
        myMenuItems.add(new MyMenuItem(8, getResources().getString(R.string.emergancy_call), R.drawable.ic_emergancy_call, "call.json", true, getResources().getColor(R.color.white), getResources().getColor(R.color.redColor)));
//        myMenuItems.add(new MyMenuItem(10, "پیام اضطراری", R.drawable.ic_emargancy_message, "message.json", true, getResources().getColor(R.color.white), getResources().getColor(R.color.redColor)));
//        myMenuItems.add(new MyMenuItem(12, "تغییر رمز", R.drawable.reset_password, getResources().getColor(R.color.white), getResources().getColor(R.color.redColor)));
        myMenuItems.add(new MyMenuItem(11, "تنظیمات", R.drawable.ic_settings_applications_black_48dp, "setting.json", true, getResources().getColor(R.color.white), getResources().getColor(R.color.redColor)));
        myMenuItems.add(new MyMenuItem(12, "پشتیبان گیری", R.drawable.ic_backup_black_24dp, "back.json", true, getResources().getColor(R.color.white), getResources().getColor(R.color.redColor)));

        return myMenuItems;
    }

    @Override
    public void onItemClick(MyMenuItem item) {

    }


    private void makeJson() {
        JSONObject json = new JSONObject();
        try {
            json.put("orderIdentifier", resultResponse.getOrderIdentifier());
            if (resultResponse.getQuestionWithResults().size() > 0) {
                JSONArray jArray = new JSONArray();
                for (int i = 0; i < resultResponse.getQuestionWithResults().size(); i++) {
                    JSONObject jGroup = new JSONObject();
                    jGroup.put("id", resultResponse.getQuestionWithResults().get(i).getId());
                    jGroup.put("question", resultResponse.getQuestionWithResults().get(i).getQuestion());
                    jGroup.put("questionIdentifier", resultResponse.getQuestionWithResults().get(i).getQuestionIdentifier());
                    jGroup.put("result", resultResponse.getQuestionWithResults().get(i).getResult());
                    if (resultResponse.getQuestionWithResults().get(i).getFileAttachments() != null && resultResponse.getQuestionWithResults().get(i).getFileAttachments().size() > 0) {
                        JSONArray jAttachment = new JSONArray();
                        for (int j = 0; j < resultResponse.getQuestionWithResults().get(i).getFileAttachments().size(); j++) {
                            JSONObject jGroupAttchment = new JSONObject();
                            jGroupAttchment.put("identifier", resultResponse.getQuestionWithResults().get(i).getFileAttachments().get(j).getIdentifier());
                            jGroupAttchment.put("description", resultResponse.getQuestionWithResults().get(i).getFileAttachments().get(j).getDescription());
                            jGroupAttchment.put("name", resultResponse.getQuestionWithResults().get(i).getFileAttachments().get(j).getFilename());
                            jGroupAttchment.put("format", resultResponse.getQuestionWithResults().get(i).getFileAttachments().get(j).getFormat());
                            if (resultResponse.getQuestionWithResults().get(i).getFileAttachments().get(j).getGeoLocationDTO() != null) {
                                JSONObject jGroupLocation = new JSONObject();
                                jGroupLocation.put("latitude", resultResponse.getQuestionWithResults().get(i).getFileAttachments().get(j).getGeoLocationDTO().getLatitude());
                                jGroupLocation.put("longitude", resultResponse.getQuestionWithResults().get(i).getFileAttachments().get(j).getGeoLocationDTO().getLongitude());
                                jGroupLocation.put("description", resultResponse.getQuestionWithResults().get(i).getFileAttachments().get(j).getGeoLocationDTO().getDescription());
                                jGroupLocation.put("label", resultResponse.getQuestionWithResults().get(i).getFileAttachments().get(j).getGeoLocationDTO().getAddress());
                                jGroupAttchment.put("geoLocationDTO", jGroupLocation);
                            }
                            jAttachment.put(jGroupAttchment);
                        }
                        jGroup.put("fileAttachments", jAttachment);
                    }
                    jArray.put(jGroup);
                }
                json.put("questionWithResults", jArray);
            }
            json.put("complementaryReport", resultResponse.getComplementaryReport());
            Writer output = null;
            File jsonFile = new File(Constants.storage_Dir_File,
                    "orderResults.json");
            output = new BufferedWriter(new FileWriter(jsonFile));
            output.write(json.toString());
            output.close();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void compressDirectory(String dir, String zipFile) {
        File directory = new File(dir);
        getFileList(directory);

        try (FileOutputStream fos = new FileOutputStream(zipFile);
             ZipOutputStream zos = new ZipOutputStream(fos)) {

            for (String filePath : fileList) {
                System.out.println("Compressing: " + filePath);

                // Creates a zip entry.
                String name = filePath.substring(
                        directory.getAbsolutePath().length() + 1,
                        filePath.length());

                ZipEntry zipEntry = new ZipEntry(name);
                zos.putNextEntry(zipEntry);

                // Read file content and write to zip output stream.
                try (FileInputStream fis = new FileInputStream(filePath)) {
                    byte[] buffer = new byte[1024];
                    int length;
                    while ((length = fis.read(buffer)) > 0) {
                        zos.write(buffer, 0, length);
                    }

                    // Close the zip entry.
                    zos.closeEntry();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get files list from the directory recursive to the sub directory.
     */
    private void getFileList(File directory) {
        File[] files = directory.listFiles();
        if (files != null && files.length > 0) {
            for (File file : files) {
                if (file.isFile()) {
                    fileList.add(file.getAbsolutePath());
                } else {
                    getFileList(file);
                }
            }
        }

    }


/*    public static boolean zip(File sourceFile, File zipFile) {
        List<File> fileList = getSubFiles(sourceFile, true);
        ZipOutputStream zipOutputStream = null;
        try {
            zipOutputStream = new ZipOutputStream(new FileOutputStream(zipFile));
            int bufferSize = 1024;
            byte[] buf = new byte[bufferSize];
            ZipEntry zipEntry;
            for(int i = 0; i < fileList.size(); i++) {
                File file = fileList.get(i);
                zipEntry = new ZipEntry(sourceFile.toURI().relativize(file.toURI()).getPath());
                zipOutputStream.putNextEntry(zipEntry);
                if (!file.isDirectory()) {
                    InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
                    int readLength;
                    while ((readLength = inputStream.read(buf, 0, bufferSize)) != -1) {
                        zipOutputStream.write(buf, 0, readLength);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
        }
        return true;
    }

    public static List<File> getSubFiles(File baseDir, boolean isContainFolder) {
        List<File> fileList = new ArrayList<>();
        File[] tmpList = baseDir.listFiles();
        for (File file : tmpList) {
            if (file.isFile()) {
                fileList.add(file);
            }
            if (file.isDirectory()) {
                if (isContainFolder) {
                    fileList.add(file); //key code
                }
                fileList.addAll(getSubFiles(file,false));
            }
        }
        return fileList;
    }*/



//    public boolean zipFileAtPath(String sourcePath, String toLocation) {
//
//
//        final int BUFFER = 2048;
//
//        File sourceFile = new File(sourcePath);
//        try {
//            BufferedInputStream origin = null;
//            FileOutputStream dest = new FileOutputStream(toLocation);
//            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(
//                    dest));
//            if (sourceFile.isDirectory()) {
//                zipSubFolder(out, sourceFile, sourceFile.getParent().length());
//            } else {
//                byte data[] = new byte[BUFFER];
//                FileInputStream fi = new FileInputStream(sourcePath);
//                origin = new BufferedInputStream(fi, BUFFER);
//                ZipEntry entry = new ZipEntry(getLastPathComponent(sourcePath));
//                entry.setTime(sourceFile.lastModified()); // to keep modification time after unzipping
//                out.putNextEntry(entry);
//                int count;
//                while ((count = origin.read(data, 0, BUFFER)) != -1) {
//                    out.write(data, 0, count);
//                }
//            }
//            out.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//        return true;
//
//    }
//
//    /*
//     *
//     * Zips a subfolder
//     *
//     */
//
//    private void zipSubFolder(ZipOutputStream out, File folder,
//                              int basePathLength) throws IOException {
//
//        final int BUFFER = 2048;
//
//        File[] fileList = folder.listFiles();
//        BufferedInputStream origin = null;
//        for (File file : fileList) {
//            if (file.isDirectory()) {
//                zipSubFolder(out, file, basePathLength);
//            } else {
//                byte data[] = new byte[BUFFER];
//                String unmodifiedFilePath = file.getPath();
//                String relativePath = unmodifiedFilePath
//                        .substring(basePathLength);
//                FileInputStream fi = new FileInputStream(unmodifiedFilePath);
//                origin = new BufferedInputStream(fi, BUFFER);
//                ZipEntry entry = new ZipEntry(relativePath);
//                entry.setTime(file.lastModified()); // to keep modification time after unzipping
//                out.putNextEntry(entry);
//                int count;
//                while ((count = origin.read(data, 0, BUFFER)) != -1) {
//                    out.write(data, 0, count);
//                }
//                origin.close();
//            }
//        }
//    }

    /*
     * gets the last path component
     *
     * Example: getLastPathComponent("downloads/example/fileToZip");
     * Result: "fileToZip"
     */
    public String getLastPathComponent(String filePath) {
        String[] segments = filePath.split("/");
        if (segments.length == 0)
            return "";
        String lastPathComponent = segments[segments.length - 1];
        return lastPathComponent;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CODE_PERMISSION) {
            boolean hasAllPermission = true;
            for (int grantResult : grantResults) {
                if (grantResult != PackageManager.PERMISSION_GRANTED) {
                    hasAllPermission = false;
                    break;
                }
            }
            if (hasAllPermission) {
//                ReadJsonFile readJsonFile = new ReadJsonFile();
//                ArrayList<ProjectResponse> personResponse = readJsonFile.ReadFile(this);
            } else {
                PermissionHandler.requestPermissions(this, REQUEST_CODE_PERMISSION);
            }
        }
    }


    @OnClick({R.id.exit, R.id.unistall, R.id.QrCode})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.exit:
                CustomDialog dialog = new CustomDialog(getActivity());
                dialog.setOkListener(getResources().getString(R.string.ok), view1 -> {
                    dialog.dismiss();
//                    PreferencesData.isLogin(getActivity(), false);
                    Constants.isLogin = false;
                    Intent intent1 = new Intent(getActivity(), SplashActivity.class);
                    startActivity(intent1);
                    getActivity().finish();
                });
                dialog.setCancelListener(getResources().getString(R.string.cancel), view12 -> dialog.dismiss());
                dialog.setDialogTitle("آیا برای خروج اطمینان دارید؟");
                dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
                dialog.setIcon(R.drawable.ic_exit_to_app, getResources().getColor(R.color.redColor));
                dialog.show();
                break;


            case R.id.unistall:

                CustomDialog dialog2 = new CustomDialog(getActivity());
                dialog2.setOkListener(getResources().getString(R.string.ok), view1 -> {
                    File fdelete = new File(Environment.getExternalStorageDirectory(), "/shahed");
                    if (fdelete.isDirectory()) {
                        String[] children = fdelete.list();
                        for (int i = 0; i < children.length; i++) {
                            new File(fdelete, children[i]).delete();
                        }
                    }
                    try {
                        FileUtils.deleteDirectory(fdelete);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Intent intent = new Intent("android.intent.action.DELETE");
                    intent.setData(Uri.parse("package:" + "com.military.militaryservice"));
                    startActivity(intent);
                    dialog2.dismiss();
                });
                dialog2.setCancelListener(getResources().getString(R.string.cancel), view12 -> dialog2.dismiss());
                dialog2.setDialogTitle("آیا برای حذف اطمینان دارید؟");
                dialog2.setColorTitle(getResources().getColor(R.color.primaryTextColor));
                dialog2.setIcon(R.drawable.ic_exit_to_app, getResources().getColor(R.color.redColor));
                dialog2.show();

                break;
            case R.id.QrCode:
                startActivity(new Intent(getActivity(), ScanActivity.class));
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_FINISH_ACTIVITY) {
                Intent intent = new Intent(getActivity(), SplashActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        }

        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case RESULT_OK:
                        // Nothing to do. startLocationupdates() gets called in onResume again.
                        break;
                    case Activity.RESULT_CANCELED:
//                        mRequestingLocationUpdates = false;
                        Toast toast = Toast.makeText(getActivity(),
                                "برای ادامه کار gps باید روشن باشد.", Toast.LENGTH_SHORT);
                        View view = toast.getView();
                        view.getBackground().setColorFilter(getResources().getColor(R.color.redColor), PorterDuff.Mode.SRC_IN);
                        toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 350);
                        toast.show();
                        startCheckLocation();
                        break;
                }
                break;
        }

    }

    @Override
    public void onLocationChanged(Location location) {
        latitud = location.getLatitude();
        longitud = location.getLatitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


}
