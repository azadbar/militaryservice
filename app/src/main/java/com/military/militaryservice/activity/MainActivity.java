package com.military.militaryservice.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.bumptech.glide.Glide;
import com.military.militaryservice.R;
import com.military.militaryservice.addUser.AddUserActivity;
import com.military.militaryservice.baseView.BaseActivity;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.calendar.CalenderActivity;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.database.User;
import com.military.militaryservice.dialog.CustomDialog;
import com.military.militaryservice.dialog.ValidationUserDialog;
import com.military.militaryservice.enums.UserAccessEnum;
import com.military.militaryservice.model.DrawerItem;
import com.military.militaryservice.newCalender.persiancalendar.ui.MainActivityy;
import com.military.militaryservice.persianCalender.date.DatePickerDialog;
import com.military.militaryservice.setting.SettingActivity;
import com.military.militaryservice.splash.SplashActivity;
import com.military.militaryservice.utils.Constants;
import com.military.militaryservice.utils.CustomTypefaceSpan;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener {

    @BindView(R.id.imgHelp)
    BaseImageView imgHelp;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.imgLogo)
    BaseImageView imgLogo;
    @BindView(R.id.tvCenterTitle)
    BaseTextView tvCenterTitle;
    @BindView(R.id.share)
    BaseImageView share;
    //    @BindView(R.id.toolbar)
//    BaseToolbar toolbar;
    @BindView(R.id.menu)
    BaseImageView menu;


    public static int navItemIndex = 0;
    private static final String TAG_MAG = "mag";
    private static final String TAG_ABOUT_DELTA = "aboutDelta";
    private static final String TAG_SHOW_ADS = "showAds";
    private static final String TAG_ADS = "ads";
    private static final String TAG_CONTACT_US = "contactUs";
    public static String CURRENT_TAG = TAG_MAG;

    @BindView(R.id.content_frame)
    FrameLayout contentFrame;
    @BindView(R.id.left_drawer)
    ListView mDrawerList;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.rlDrawer)
    BaseRelativeLayout rlDrawer;

    private Handler mHandler;
    private Date mEndDate;
    private Date mStartDate;

    private boolean shouldLoadHomeFragOnBackPress = true;
    private boolean doubleBackToExitPressedOnce = false;

    List<DrawerItem> dataList;
    private CustomDrawerAdapter adapter;
    private AppDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        imgBack.setColorFilter(getResources().getColor(R.color.white));
        imgLogo.setVisibility(View.VISIBLE);
        Glide.with(this).load(R.drawable.shahed).into(imgLogo);
        imgHelp.setVisibility(View.VISIBLE);
        tvCenterTitle.setVisibility(View.GONE);
        menu.setVisibility(View.VISIBLE);

        mEndDate = new Date();
        mStartDate = new Date();

        mHandler = new Handler();
//        setUpNavigationView();
//        setFont();

//        if (savedInstanceState == null) {
//            navItemIndex = 0;
//            CURRENT_TAG = TAG_MAG;
////            loadHomeFragment();
//        }

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
                Gravity.RIGHT);

        dataList = new ArrayList<DrawerItem>();

        dataList.add(new DrawerItem("صفحه اصلی", R.drawable.ic_home));
        dataList.add(new DrawerItem("تقویم", R.drawable.ic_perm_contact_calendar));
        dataList.add(new DrawerItem("تنظیمات", R.drawable.ic_settings_black));
        dataList.add(new DrawerItem("راهنما", R.drawable.ic_info_outline));
        dataList.add(new DrawerItem("درباره ما", R.drawable.ic_group));

        adapter = new CustomDrawerAdapter(this, R.layout.custom_drawer_item,
                dataList);

        mDrawerList.setAdapter(adapter);


        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        if (savedInstanceState == null) {
            SelectItem(0);
        }

        menu.setOnClickListener(v -> {
            mDrawerLayout.openDrawer(Gravity.RIGHT);
        });

        rlDrawer.setOnClickListener(v -> mDrawerLayout.closeDrawer(Gravity.RIGHT));

    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/IRANSansMobile(FaNum).ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    public void onBackPressedd() {

        CustomDialog dialog = new CustomDialog(this);
        dialog.setOkListener(getResources().getString(R.string.ok), view1 -> {
            dialog.dismiss();
//            PreferencesData.isLogin(this, false);
            Constants.isLogin = false;
            Intent intent1 = new Intent(this, SplashActivity.class);
            startActivity(intent1);
            finish();
        });
        dialog.setCancelListener(getResources().getString(R.string.cancel), view12 -> dialog.dismiss());
        dialog.setDialogTitle("آیا برای خروج اطمینان دارید؟");
        dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
        dialog.setIcon(R.drawable.ic_exit_to_app, getResources().getColor(R.color.redColor));
        dialog.show();

    }

    @OnClick(R.id.imgHelp)
    public void onViewClicked() {
        CustomDialog dialog1 = new CustomDialog(this);
        dialog1.setOkListener("متوجه شدم", v -> dialog1.dismiss());
        dialog1.setIcon(R.drawable.shahed, getResources().getColor(R.color.transparent));
        dialog1.setDialogTitle("مرکز تحقیقات متعال");
        SpannableStringBuilder desc_two = new SpannableStringBuilder();
        desc_two.append("برنامه اندرویدی شاهد یک برنامه برای جمع آوری اطلاعات مورد نیاز سامانه تحت وب شهاب می باشد.در این برنامه شما می توانید بر مبنای");
        int start = desc_two.length();
        desc_two.append(" پرونده اصلی، اطلاعات صوتی، تصویری و موقعیتی مرتبط");
        int end = desc_two.length();
        desc_two.append("را جمع آوری نموده و به پرونده اصلی ضمیمه نمایید. برخی امکانات فرعی به تناسب نیاز کاربر، در این برنامه پیش بینی شده است.");
        desc_two.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.redColor)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new RelativeSizeSpan(1.0f), start, end, 0);
        dialog1.setDescription(desc_two);
        dialog1.setColorTitle(getResources().getColor(R.color.primaryTextColor));
        dialog1.show();
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
//        String date = "You picked the following date: "+dayOfMonth+"/"+(monthOfYear+1)+"/"+year;
//        Toast.makeText(getApplicationContext(), date, Toast.LENGTH_SHORT).show();
    }

    private class DrawerItemClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            SelectItem(position);

        }
    }

    public void SelectItem(int possition) {

        Fragment fragment = null;
        Bundle args = new Bundle();
        switch (possition) {
            case 0:
                fragment = new HomeFragment();
                args.putString(HomeFragment.ITEM_NAME, dataList.get(possition)
                        .getItemName());
                args.putInt(HomeFragment.IMAGE_RESOURCE_ID, dataList.get(possition)
                        .getImgResID());
                break;
            case 1:
//                PersianCalendar now = new PersianCalendar();
//                DatePickerDialog datePickerDialog = DatePickerDialog.newInstance((view, year, monthOfYear, dayOfMonth) ->
//                                Toast.makeText(MainActivity.this, "" + year + "/" + monthOfYear + "/" + dayOfMonth, Toast.LENGTH_SHORT).show(), now.getPersianYear(),
//                        now.getPersianMonth(),
//                        now.getPersianDay());
//
//                datePickerDialog.setThemeDark(true);
//                datePickerDialog.show(getFragmentManager(), "tpd");
//                fragment = new FragmentOne();
//                args.putString(FragmentOne.ITEM_NAME, dataList.get(possition)
//                        .getItemName());
//                args.putInt(FragmentOne.IMAGE_RESOURCE_ID, dataList.get(possition)
//                        .getImgResID());

//                getSupportFragmentManager().beginTransaction().replace(R.id.content_frame,
//                        CalendarFragment.newInstance(), CalendarFragment.class.getName()).commit();

                Intent intent2 = new Intent(this, MainActivityy.class);
                startActivity(intent2);

                break;

            case 2:
                ValidationUserDialog dialog = new ValidationUserDialog(this);
                dialog.setIcon(R.drawable.ic_exit_to_app, getResources().getColor(R.color.secondaryBack1));
                dialog.setDialogTitle("نام کاربری و رمز عبور خود را وارد کنید");
                dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
                dialog.setOkListener("ورود", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        database = AppDatabase.getInMemoryDatabase(MainActivity.this);
                        List<User> users = database.userDao().getUsers();
                        ArrayList<String> userName = new ArrayList<>();
                        for (int i = 0; i < users.size(); i++) {
                            userName.add(users.get(i).getUsername());
                        }
                        if (userName.contains(dialog.getUserName())) {
                            for (User u : users) {
                                if (TextUtils.equals(u.getUsername(), dialog.getUserName())) {
                                    if (TextUtils.equals(dialog.getPasswor(), u.getPassword())) {
                                        if (u.getUserAccess() == UserAccessEnum.ADMIN) {
                                            Intent intent = new Intent(MainActivity.this, AddUserActivity.class);
                                            startActivity(intent);
                                            dialog.dismiss();
                                        } else {
                                            Toast.makeText(MainActivity.this, "نام کاربری شما به این بخش دسترسی ندارد", Toast.LENGTH_SHORT).show();
                                        }
                                    } else {
                                        Toast.makeText(MainActivity.this, getResources().getString(R.string.wrong_password), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        } else {
                            Toast.makeText(MainActivity.this, "نام کاربری شما در سیستم ثبت نشده است", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
//
                dialog.show();

                break;
            case 3:
                CustomDialog dialog1 = new CustomDialog(this);
                dialog1.setOkListener("متوجه شدم", v -> dialog1.dismiss());
                dialog1.setIcon(R.drawable.shahed, getResources().getColor(R.color.transparent));
                dialog1.setDialogTitle("مرکز تحقیقات متعال");
                SpannableStringBuilder desc_two = new SpannableStringBuilder();
                desc_two.append("برنامه اندرویدی شاهد یک برنامه برای جمع آوری اطلاعات مورد نیاز سامانه تحت وب شهاب می باشد.در این برنامه شما می توانید بر مبنای");
                int start = desc_two.length();
                desc_two.append(" پرونده اصلی، اطلاعات صوتی،تصویری و موقعیتی مرتبط ");
                int end = desc_two.length();
                desc_two.append("را جمع آوری نموده و به پرونده اصلی ضمیمه نمایید. برخی امکانات فرعی نیز به تناسب نیاز کاربر، ‌در این برنامه پیش بینی شده است.");
                desc_two.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.redColor)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                desc_two.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                desc_two.setSpan(new RelativeSizeSpan(1.0f), start, end, 0);
                dialog1.setDescription(desc_two);
                dialog1.setColorTitle(getResources().getColor(R.color.primaryTextColor));
                dialog1.show();
                break;
            case 4:
                CustomDialog dialog2 = new CustomDialog(this);
                dialog2.setOkListener("متوجه شدم", v -> dialog2.dismiss());
                dialog2.setIcon(R.drawable.logo_trance, getResources().getColor(R.color.transparent));
                dialog2.setDialogTitle("");
                dialog2.setDescription("این برنامه توسط مرکز تحقیقات عالی الکترونیک (متعال) طراحی و توسعه یافته است.");
                dialog2.setColorTitle(getResources().getColor(R.color.primaryTextColor));
                dialog2.setCall("شماره تماس: 23112589_021");
                dialog2.show();
                break;
            default:
                break;
        }

        if (fragment != null) {
            fragment.setArguments(args);
            FragmentManager frgManager = getSupportFragmentManager();
            frgManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
        } else {
            Log.e("AlarmActivitiy", "Error in creating fragment");
        }
        mDrawerList.setItemChecked(possition, true);
        setTitle(dataList.get(possition).getItemName());
        mDrawerLayout.closeDrawer(Gravity.RIGHT);


    }

    @Override
    public void onBackPressed() {
        onBackPressedd();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
            List<Fragment> frags = getSupportFragmentManager().getFragments();
            if (frags != null) {
                for (Fragment f : frags) {
                    if (f != null && f.isVisible())
                        f.onActivityResult(requestCode, resultCode, data);
                }
            }
    }
}
