package com.military.militaryservice.activity;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.model.MyMenuItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {


    private Activity context;
    private ArrayList<MyMenuItem> list;
    private final OnItemClickListener listener;

    public MenuAdapter(Activity context, ArrayList<MyMenuItem> list, OnItemClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu_main_circle, parent, false);


        return new ViewHolder(itemView);

    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        MyMenuItem obj = list.get(position);

        holder.txtTitle.setText(obj.getTitle());

        if (obj.isAnim()) {
            holder.lottie.setVisibility(View.VISIBLE);
            holder.imageView.setVisibility(View.GONE);

            holder.lottie.setAnimation(obj.getLoitteAnim());
            holder.lottie.playAnimation();
            holder.lottie.loop(true);

        } else {
            holder.lottie.setVisibility(View.GONE);
            holder.imageView.setVisibility(View.VISIBLE);

            holder.imageView.setImageResource(obj.getDrawableId());
            holder.imageView.setColorFilter(obj.getImageColor(), PorterDuff.Mode.SRC_ATOP);
        }
        holder.bind(list.get(position), listener);

        holder.itemView.post(() -> {
            int cellWidth = holder.itemView.getWidth();// this will give you cell width dynamically
            int cellHeight = holder.itemView.getHeight();// this will give you cell height dynamically
            ViewGroup.LayoutParams params = holder.shape.getLayoutParams();
            params.height = cellWidth;
            params.width = cellWidth;
            holder.shape.setLayoutParams(params);

        });

        holder.shape.post(() -> {
            int cellWidth = holder.shape.getWidth();// this will give you cell width dynamically
            int cellHeight = holder.shape.getHeight();// this will give you cell height dynamically
            ViewGroup.LayoutParams params = holder.lottie.getLayoutParams();
            params.height = (int) ((cellWidth) * 2/3.0);
            params.width = (int) ((cellWidth) * 2/3.0);
            holder.lottie.setLayoutParams(params);

        });
        setFadeAnimation(holder.txtTitle);
    }


    private void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }


    @Override
    public int getItemCount() {

        return list.size();
    }


    public interface OnItemClickListener {
        void onItemClick(MyMenuItem item);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.imgMenu)
        ImageView imageView;
        @BindView(R.id.lottie)
        LottieAnimationView lottie;
        @BindView(R.id.shape)
        BaseRelativeLayout shape;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(final MyMenuItem item, final OnItemClickListener listener) {

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }
}
