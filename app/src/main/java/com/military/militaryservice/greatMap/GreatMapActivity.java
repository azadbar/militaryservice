package com.military.militaryservice.greatMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.military.militaryservice.R;

import java.io.File;

//sample for running map
@SuppressLint("JavascriptInterface")
public class GreatMapActivity extends Activity {

    //	private Marker localmarker;
    public static double lat, lng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.great_map_activity);

        // launch TileHost
        // ///////////////////
        WebView webView = (WebView) findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient()
        {
            @Override public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                view.loadUrl(url);
                return true;
            }
        });

        try
        {
            File sdcard = new File(Environment.getExternalStorageDirectory() + File.separator + File.separator + "GreatMaps1" + File.separator);
            TileHost.Db = sdcard.getAbsolutePath() + File.separator + "Data.gmdb";
            Thread t = new Thread(new TileHost());
            t.setDaemon(true);
            t.start();
            new File(sdcard.getAbsolutePath() + File.separator + "gmap.html");
            webView.loadUrl("file:///android_asset/www/GreatMaps1/gmapmain.html");
        }
        catch (Exception e)
        {
            System.out.println("TileHost error: " + e);
        }
        // ///////////////////
    }




}
