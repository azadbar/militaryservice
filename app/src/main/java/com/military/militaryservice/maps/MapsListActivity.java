package com.military.militaryservice.maps;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.resultInfo.LocationDTO;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MapsListActivity extends AppCompatActivity implements AddressAdapter.OnItemClickListener {

    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvCenterTitle)
    BaseTextView tvCenterTitle;
    @BindView(R.id.rvAddress)
    RecyclerView rvAddress;
    @BindView(R.id.add_address)
    FloatingActionButton addAddress;
    @BindView(R.id.tvEmptyView)
    BaseTextView tvEmptyView;
    @BindView(R.id.rootEmptyView)
    BaseRelativeLayout rootEmptyView;
    private AddressAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_list);
        ButterKnife.bind(this);

        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.ic_arrow_forward);
        imgBack.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        imgBack.setOnClickListener(v -> finish());

        tvCenterTitle.setText(getResources().getString(R.string.address_selected));

    }

    @Override
    protected void onResume() {
        super.onResume();
        getList();
    }

    private void getList() {
        AppDatabase database = AppDatabase.getInMemoryDatabase(this);
        ArrayList<LocationDTO> address = (ArrayList<LocationDTO>) database.addressDao().getAddressList();
        if (address.size() == 0) {
            rootEmptyView.setVisibility(View.VISIBLE);
        } else {
            rootEmptyView.setVisibility(View.GONE);
        }

        setAdapter(address);
    }

    private void setAdapter(ArrayList<LocationDTO> address) {
        adapter = new AddressAdapter(address, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rvAddress.setHasFixedSize(true);
        rvAddress.setLayoutManager(layoutManager);
        rvAddress.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }

    @OnClick({R.id.imgBack, R.id.add_address})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                finish();
                break;
            case R.id.add_address:
                Intent intent = new Intent(this, OfflineActivity.class);
                intent.putExtra("isSelectedMap", true);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onItemAddress(int position, LocationDTO address) {

        Intent intent = new Intent(this, OfflineActivity.class);
        intent.putExtra("lat", address.getLatitude());
        intent.putExtra("long", address.getLongitude());
        intent.putExtra("bodyEditTxt", address.getAddress());
        intent.putExtra("isQuestion", true);
        intent.putExtra("isSelectedMap", true);
        startActivity(intent);
    }

    @Override
    public void onItemDelete(int position, LocationDTO address) {
        AppDatabase database = AppDatabase.getInMemoryDatabase(this);
        database.addressDao().delete(address);
        getList();
    }
}
