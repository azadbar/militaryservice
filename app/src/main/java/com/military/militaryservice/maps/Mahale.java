package com.military.militaryservice.maps;

import com.military.militaryservice.customView.KeyValueObject;

public class Mahale {

    private int Id;
    private String Title;
    private double latitude;
    private double longitude;


    public Mahale(String title, double latitude, double longitude) {
        Title = title;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public KeyValueObject getKeyValue() {
        return new KeyValueObject(this.Id, this.Title);
    }

}
