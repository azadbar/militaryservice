package com.military.militaryservice.maps;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseActivity;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.customView.CustomEditText;
import com.military.militaryservice.customView.KeyValueObject;
import com.military.militaryservice.customView.MaterialSpinnerForKeyValue;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.resultInfo.LocationDTO;
import com.military.militaryservice.utils.Constants;

import org.osmdroid.api.IMapController;
import org.osmdroid.bonuspack.overlays.MapEventsOverlay;
import org.osmdroid.bonuspack.overlays.MapEventsReceiver;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.modules.ArchiveFileFactory;
import org.osmdroid.tileprovider.modules.IArchiveFile;
import org.osmdroid.tileprovider.modules.OfflineTileProvider;
import org.osmdroid.tileprovider.tilesource.FileBasedTileSource;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.tileprovider.util.SimpleRegisterReceiver;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.mylocation.IMyLocationConsumer;
import org.osmdroid.views.overlay.mylocation.IMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class OfflineActivity extends BaseActivity implements IMyLocationProvider {

    @BindView(R.id.map)
    MapView mapView;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvCenterTitle)
    BaseTextView tvCenterTitle;
    @BindView(R.id.btnSave)
    BaseTextView btnSave;
    @BindView(R.id.edtAddress)
    CustomEditText edtAddress;
    @BindView(R.id.spinnerMahale)
    MaterialSpinnerForKeyValue spinnerMahale;
    @BindView(R.id.staticLocation)
    CardView staticLocation;
    @BindView(R.id.spinnerDynamic)
    MaterialSpinnerForKeyValue spinnerDynamic;
    @BindView(R.id.dynamicLocation)
    CardView dynamicLocation;
    private boolean isQuestion;
    private double saveLatitude;
    private double saveLongitude;
    private String bodyEditTxt;
    private double longitude;
    private double latitude;
    private MyLocationNewOverlay mLocationOverlay;
    private MapController mapController;
    private ArrayList<OverlayItem> overlayItemArray;
    private GeoPoint p;
    private Marker tempMarker;
    private ArrayList<KeyValueObject> mahaleObjects = new ArrayList<>();
    private ArrayList<KeyValueObject> dynamicMahaleObjects = new ArrayList<>();
    private static final int STORAGE_PERMISSION_CODE = 101;
    private boolean isSelectedMap;
    private IMapController iMapController;
    private float MAP_ZOOM = 18;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline);
        ButterKnife.bind(this);

        mapView = findViewById(R.id.map);
        checkPermission();

        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
            if (windowManager != null) {
                Point size = Constants.getScreenSize(windowManager);
                if (size.x > getResources().getDimensionPixelSize(R.dimen.marging400dp)) {
                    MAP_ZOOM = (float) 18.3;
                } else {
                    MAP_ZOOM = (float) 17.7;
                }
            }
        }


        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.ic_arrow_forward);
        imgBack.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        tvCenterTitle.setText(getResources().getString(R.string.address_selected));
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isQuestion = bundle.getBoolean("isQuestion");
            saveLatitude = bundle.getDouble("lat");
            saveLongitude = bundle.getDouble("long");
            bodyEditTxt = bundle.getString("bodyEditTxt");
            isSelectedMap = bundle.getBoolean("isSelectedMap");
        }

        if (isSelectedMap) {
            dynamicLocation.setVisibility(View.GONE);
        } else {
            dynamicLocation.setVisibility(View.VISIBLE);
        }
        mapView.setClickable(true);
        mapView.setTag("Mapa");
//        mapView.setTileSource(TileSourceFactory.MAPNIK);
        mapView.setMultiTouchControls(true);
        mapView.setClickable(true);
        mapView.setUseDataConnection(false);
        mapView.setBuiltInZoomControls(false);
        mapController = (MapController) mapView.getController();

//        statusCheck();

        iMapController = mapView.getController();
        iMapController.setZoom(MAP_ZOOM);
        GeoPoint geoPoint = null;
        if (isQuestion) {
            if (saveLatitude == 0) {
                mLocationOverlay = new MyLocationNewOverlay(this, mapView);
                mLocationOverlay.enableMyLocation();
                mLocationOverlay.enableFollowLocation();
                IMyLocationProvider s = mLocationOverlay.getMyLocationProvider();
                mLocationOverlay.getMyLocation();
                mLocationOverlay.runOnFirstFix(new Runnable() {
                    public void run() {
                        Log.d("MyTag", String.format("First location fix: %s", mLocationOverlay.getLastFix()));
                    }
                });
                mapView.getOverlays().add(mLocationOverlay);
                geoPoint = new GeoPoint(35.7115086, 51.4052885);

                iMapController.setCenter(geoPoint);
                iMapController.animateTo(geoPoint);
            } else {
                setMarkerGeoPoint(saveLatitude, saveLongitude);
            }

        } else {
            mLocationOverlay = new MyLocationNewOverlay(this, mapView);
            mLocationOverlay.enableMyLocation();
            mLocationOverlay.enableFollowLocation();
            IMyLocationProvider s = mLocationOverlay.getMyLocationProvider();
            mLocationOverlay.getMyLocation();
            mLocationOverlay.runOnFirstFix(new Runnable() {
                public void run() {
                    Log.d("MyTag", String.format("First location fix: %s", mLocationOverlay.getLastFix()));
                }
            });
            mapView.getOverlays().add(mLocationOverlay);
            geoPoint = new GeoPoint(35.7115086, 51.4052885);

            iMapController.setCenter(geoPoint);
            iMapController.animateTo(geoPoint);
        }


        MapEventsReceiver mReceive = new MapEventsReceiver() {
            @Override
            public boolean singleTapConfirmedHelper(GeoPoint p) {
                return false;
            }

            @Override
            public boolean longPressHelper(GeoPoint p) {
                if (tempMarker != null) {
                    mapView.getOverlayManager().remove(tempMarker);
                    mapView.invalidate();
                }
                OfflineActivity.this.p = p;
                Marker startMarker = new Marker(mapView);
                startMarker.setPosition(p);
                startMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                startMarker.setIcon(getResources().getDrawable(R.drawable.marker));
                startMarker.setTitle(edtAddress.getValueString());
//                startMarker.setSnippet("The White House is the official residence and principal workplace of the President of the United States.");
//                startMarker.setSubDescription("1600 Pennsylvania Ave NW, Washington, DC 20500");
                mapView.getOverlays().add(startMarker);
                tempMarker = startMarker;
                edtAddress.setBody("");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        iMapController.setZoom(MAP_ZOOM);
                        mapView.getController().animateTo(new GeoPoint(OfflineActivity.this.p.getLatitude(), OfflineActivity.this.p.getLongitude()));
                    }
                }, 300);
                return false;
            }
        };


        MapEventsOverlay OverlayEvents = new MapEventsOverlay(getBaseContext(), mReceive);
        mapView.getOverlays().add(OverlayEvents);        //for my location

        ArrayList<Mahale> searchList = Constants.getSearchList();
        if (searchList.size() > 0) {
            mahaleObjects.add(new KeyValueObject(0, "از بین گزینه های پیش فرض انتخاب کنید"));
            for (Mahale s : searchList) {
                mahaleObjects.add(s.getKeyValue());
            }
            spinnerMahale.setAdapter(mahaleObjects);
        }
        spinnerMahale.addOnItemClickListener((parent, view, position, id) -> {
            if (id == 0) {
            } else {
                Mahale mahale1 = searchList.get((int) id - 1);
                if (tempMarker != null) {
                    mapView.getOverlayManager().remove(tempMarker);
                    mapView.invalidate();
                }
                setMarkerGeoPoint(mahale1.getLatitude(), mahale1.getLongitude());
                edtAddress.setBody(mahale1.getTitle());
            }

        });

        AppDatabase database = AppDatabase.getInMemoryDatabase(this);
        ArrayList<LocationDTO> addressList = (ArrayList<LocationDTO>) database.addressDao().getAddressList();
        if (addressList.size() > 0) {
            dynamicMahaleObjects.add(new KeyValueObject(0, "از بین آدرس های منتخب انتخاب کنید"));
            for (LocationDTO l : addressList) {
                dynamicMahaleObjects.add(l.getKeyValue());
            }
            spinnerDynamic.setAdapter(dynamicMahaleObjects);
        }
        spinnerDynamic.addOnItemClickListener((parent, view, position, id) -> {
            if (id == 0) {
            } else {
                LocationDTO mahale1 = addressList.get((int) id - 1);
                if (tempMarker != null) {
                    mapView.getOverlayManager().remove(tempMarker);
                    mapView.invalidate();
                }
                setMarkerGeoPoint(mahale1.getLatitude(), mahale1.getLongitude());
                edtAddress.setBody(mahale1.getAddress());
            }

        });

        Context ctx = getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        if (!new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/osmdroid/", "Mapnik.zip").exists()) {
            copyAssets();
        }

        File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/osmdroid/");
        if (f.exists()) {
            File[] list = f.listFiles();
            if (list != null) {
                for (int i = 0; i < list.length; i++) {
                    if (list[i].isDirectory()) {
                        continue;
                    }
                    String name = list[i].getName().toLowerCase();
                    if (!name.contains(".")) {
                        continue; //skip files without an extension
                    }
                    name = name.substring(name.lastIndexOf(".") + 1);
                    if (name.length() == 0) {
                        continue;
                    }

                    if (ArchiveFileFactory.isFileExtensionRegistered(name)) {
                        try {

                            //ok found a file we support and have a driver for the format, for this demo, we'll just use the first one

                            //create the offline tile provider, it will only do offline file archives
                            //again using the first file
                            OfflineTileProvider tileProvider = new OfflineTileProvider(new SimpleRegisterReceiver(ctx),
                                    new File[]{list[i]});

                            //tell osmdroid to use that provider instead of the default rig which is (asserts, cache, files/archives, online
                            mapView.setTileProvider(tileProvider);


                            //this bit enables us to find out what tiles sources are available. note, that this action may take some time to run
                            //and should be ran asynchronously. we've put it inline for simplicity
                            String source = "";
                            IArchiveFile[] archives = tileProvider.getArchives();
                            if (archives.length > 0) {
                                //cheating a bit here, get the first archive file and ask for the tile sources names it contains
                                Set<String> tileSources = archives[0].getTileSources();
                                //presumably, this would be a great place to tell your users which tiles sources are available
                                if (!tileSources.isEmpty()) {
                                    //ok good, we found at least one tile source, create a basic file based tile source using that name
                                    //and set it. If we don't set it, osmdroid will attempt to use the default source, which is "MAPNIK",
                                    //which probably won't match your offline tile source, unless it's MAPNIK
                                    source = tileSources.iterator().next();
                                    this.mapView.setTileSource(FileBasedTileSource.getSource(source));
                                } else {
                                    this.mapView.setTileSource(TileSourceFactory.DEFAULT_TILE_SOURCE);
                                }

                            } else {
                                this.mapView.setTileSource(TileSourceFactory.DEFAULT_TILE_SOURCE);
                            }
                            this.mapView.invalidate();
                            return;
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            }
            Toast.makeText(ctx, f.getAbsolutePath() + " did not have any files I can open!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(ctx, f.getAbsolutePath() + " dir not found!", Toast.LENGTH_SHORT).show();
        }


//        mapView.setMaxZoomLevel(17);

    }


    public void checkPermission() {
        if (ContextCompat.checkSelfPermission(OfflineActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(OfflineActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
        }
    }


    private void setMarkerGeoPoint(double latitude, double longitude) {
        GeoPoint geoPoint = new GeoPoint(latitude, longitude);
        Marker startMarker = new Marker(mapView);
        startMarker.setPosition(geoPoint);
        startMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        startMarker.setIcon(getResources().getDrawable(R.drawable.marker));
        mapView.getOverlays().add(startMarker);
        edtAddress.setBody(bodyEditTxt);
        tempMarker = startMarker;
        //move camera with animation
        mapController.setCenter(geoPoint);
        iMapController.setZoom(MAP_ZOOM);
        p = geoPoint;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                mapView.getController().animateTo(new GeoPoint(latitude, longitude));

            }
        }, 300);
        mapView.invalidate();
    }

    private void copyAssets() {
        AssetManager assetManager = getAssets();

        InputStream in = null;
        OutputStream out = null;
        try {
            in = assetManager.open("Mapnik.zip");

            File outdir = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "osmdroid");
            if (!outdir.exists()) {
                outdir.mkdirs();
            }
            String outf = Environment.getExternalStorageDirectory().getAbsolutePath() + "/osmdroid/";

            File outFile = new File(outf, "Mapnik.zip");
            if (!outFile.exists()) {
                outFile.createNewFile();
            }

            out = new FileOutputStream(outFile);
            copyFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }


    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (manager != null && !manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("برای ادامه کار باید GPS شما فعال باشد، آیا می خواهید فعال کنید؟")
                .setCancelable(false)
                .setPositiveButton("بله حتما", (dialog, id) -> startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)))
                .setNegativeButton("خیر", (dialog, id) -> dialog.cancel());
        final AlertDialog alert = builder.create();
        alert.show();
    }


    @OnClick({R.id.imgBack, R.id.btnSave})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                finish();
                break;
            case R.id.btnSave:
                if (isValidData()) {
//                    LocationDTO address = new LocationDTO();
//                    address.setLatitude(p.getLatitude());
//                    address.setLongitude(p.getLongitude());
//                    address.setDescription(edtAddress.getValueString());
//
//                    if (isQuestion) {
//                        Intent intent = new Intent(this, AddImageDialog.class);
//                        intent.putExtra("result", address);
//                        setResult(Activity.RESULT_OK, intent);
//                    } else {
//                        Intent intent = new Intent(this, AddImageDialog.class);
//                        intent.putExtra("result", address);
//                        setResult(Activity.RESULT_OK, intent);
//                    }
                    AppDatabase database = AppDatabase.getInMemoryDatabase(this);

                    if (isQuestion) {
                        LocationDTO address = new LocationDTO();
                        address.setLatitude(p.getLatitude());
                        address.setLongitude(p.getLongitude());
                        address.setAddress(edtAddress.getValueString());
                        address.setDate(new Date());
                        address.setUserId(Constants.getUser(this).getId());
                        database.addressDao().insertAddress(address);
                        Intent intent = new Intent();
                        intent.putExtra("result", address);
                        setResult(RESULT_OK, intent);
                        finish();
                    } else {
                        LocationDTO address = new LocationDTO();
                        address.setLatitude(p.getLatitude());
                        address.setLongitude(p.getLongitude());
                        address.setAddress(edtAddress.getValueString());
                        address.setDate(new Date());
                        address.setUserId(Constants.getUser(this).getId());
                        database.addressDao().insertAddress(address);
                        Intent intent = new Intent();
                        intent.putExtra("result", address);
                        setResult(RESULT_OK, intent);
                        finish();
                    }

                }
                break;
        }
    }

    private boolean isValidData() {
        ArrayList<String> errorMsgList = new ArrayList<>();

        if (p != null) {
            if (p.getLatitude() == 0 || p.getLongitude() == 0) {
                String message = getResources().getString(R.string.please_selected_one_address);
                errorMsgList.add(message);
            }
        }

        if (edtAddress.getError() != null) {
            errorMsgList.add(edtAddress.getError());
        }


        if (errorMsgList.size() > 0) {
            showInfoDialog(getString(R.string.fill_following), errorMsgList);
            return false;
        }
        return true;
    }

    @Override
    public boolean startLocationProvider(IMyLocationConsumer myLocationConsumer) {
        return false;
    }

    @Override
    public void stopLocationProvider() {

    }

    @Override
    public Location getLastKnownLocation() {
        return null;
    }

    @Override
    public void destroy() {

    }
}
