package com.military.militaryservice.maps;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.baseView.BaseToolbar;
import com.military.militaryservice.customView.CustomEditText;
import com.military.militaryservice.database.Address;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.dialog.InfoListDialog;
import com.military.militaryservice.recordAudio.AudioRecordDialog;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMapClickListener {

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvCenterTitle)
    BaseTextView tvCenterTitle;
    @BindView(R.id.share)
    BaseImageView share;
    @BindView(R.id.toolbar)
    BaseToolbar toolbar;
    @BindView(R.id.root)
    BaseRelativeLayout root;
    @BindView(R.id.btnSave)
    BaseTextView btnSave;
    @BindView(R.id.edtAddress)
    CustomEditText edtAddress;


    private GoogleMap mMap;
    private double longitude;
    private double latitude;
    private Location myLocation;
    private boolean isQuestion;
    private GoogleApiClient googleApiClient;
    private Marker mSelectedMarker;
    private double saveLongitude;
    private double saveLatitude;
    private String bodyEditTxt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps2);
        ButterKnife.bind(this);
        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.ic_arrow_forward);
        imgBack.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        tvCenterTitle.setText(getResources().getString(R.string.address_selected));
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isQuestion = bundle.getBoolean("isQuestion");
            saveLatitude = bundle.getDouble("lat");
            saveLongitude = bundle.getDouble("long");
            bodyEditTxt = bundle.getString("bodyEditTxt");
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Initializing googleApiClient
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        statusCheck();
    }

    private void setUpMap() {
        mMap.setMapType(GoogleMap.MAP_TYPE_NONE);

        mMap.addTileOverlay(new TileOverlayOptions().tileProvider(new CustomMapTileProvider(getResources().getAssets())));

        CameraUpdate upd = CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 11);
        mMap.moveCamera(upd);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMyLocationButtonClickListener(onMyLocationButtonClickListener);
        mMap.setOnMyLocationClickListener(onMyLocationClickListener);
        enableMyLocationIfPermitted();

        LatLng latLng = new LatLng(35.6967329, 51.2097313);
        float zoom = 11;
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMapLongClickListener(this);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.setMinZoomPreference(4);

//        setUpMap();

        mMap.setOnMapLongClickListener(latLng1 -> {
            MarkerOptions markerOptions = new MarkerOptions();
            latitude = latLng1.latitude;
            longitude = latLng1.longitude;
            markerOptions.position(latLng1);
            markerOptions.title(latLng1.latitude + " : " + latLng1.longitude);

            googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng1));
            googleMap.clear();
            googleMap.addMarker(markerOptions);

        });

    }

    private void enableMyLocationIfPermitted() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_PERMISSION_REQUEST_CODE);
        } else if (mMap != null) {
            mMap.setMyLocationEnabled(true);
            myLocation = mMap.getMyLocation();
        }
    }

    private void showDefaultLocation() {
        Toast.makeText(this, "Location permission not granted, " +
                        "showing default location",
                Toast.LENGTH_SHORT).show();
        LatLng redmond = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLng(redmond));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    enableMyLocationIfPermitted();
                } else {
                    showDefaultLocation();
                }
                return;
            }

        }
    }

    private GoogleMap.OnMyLocationButtonClickListener onMyLocationButtonClickListener =
            new GoogleMap.OnMyLocationButtonClickListener() {
                @Override
                public boolean onMyLocationButtonClick() {
                    mMap.setMinZoomPreference(15);
                    return false;
                }
            };

    private GoogleMap.OnMyLocationClickListener onMyLocationClickListener =
            new GoogleMap.OnMyLocationClickListener() {
                @Override
                public void onMyLocationClick(@NonNull Location location) {

                    mMap.setMinZoomPreference(12);

                    CircleOptions circleOptions = new CircleOptions();
                    circleOptions.center(new LatLng(location.getLatitude(),
                            location.getLongitude()));

                    circleOptions.radius(200);
                    circleOptions.fillColor(Color.RED);
                    circleOptions.strokeWidth(6);

                    mMap.addCircle(circleOptions);
                }
            };

    @OnClick({R.id.imgBack, R.id.btnSave})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                finish();
                break;
            case R.id.btnSave:
                if (isValidData()) {
                    Address address = new Address();
                    address.setLatitude(latitude);
                    address.setLongitude(longitude);
                    address.setAddress(edtAddress.getValueString());
                    address.setDate(new Date());
                    if (isQuestion) {
                        Intent intent = new Intent(this, AudioRecordDialog.class);
                        intent.putExtra("result", address);
                        setResult(Activity.RESULT_OK, intent);
                    } else {
                        AppDatabase database = AppDatabase.getInMemoryDatabase(this);
//                        database.addressDao().insertAddress(address);
                    }
                    finish();
                }
                break;
        }

    }

    private boolean isValidData() {
        ArrayList<String> errorMsgList = new ArrayList<>();

        if (latitude == 0 || longitude == 0) {
            String message = getResources().getString(R.string.please_selected_one_address);
            errorMsgList.add(message);
        }
        if (edtAddress.getError() != null) {
            errorMsgList.add(edtAddress.getError());
        }


        if (errorMsgList.size() > 0) {
            showInfoDialog(getString(R.string.fill_following), errorMsgList);
            return false;
        }
        return true;
    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (manager != null && !manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("برای ادامه کار باید GPS شما فعال باشد، آیا می خواهید فعال کنید؟")
                .setCancelable(false)
                .setPositiveButton("بله حتما", (dialog, id) -> startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS)))
                .setNegativeButton("خیر", (dialog, id) -> dialog.cancel());
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public void showInfoDialog(String title, ArrayList<String> errorMsgList) {
        InfoListDialog infoListDialog = new InfoListDialog(this);
        infoListDialog.errorMsg = errorMsgList;
        infoListDialog.title = title;
        infoListDialog.show();
    }


    private void getCurrentLocation() {
        mMap.clear();
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            if (saveLatitude != 0 && saveLongitude != 0) {
                longitude = saveLongitude;
                latitude = saveLatitude;
                edtAddress.setBody(bodyEditTxt);
                btnSave.setVisibility(View.GONE);
            } else {
                longitude = location.getLongitude();
                latitude = location.getLatitude();
            }

            moveMap();
        }
    }

    private void moveMap() {
        LatLng latLng = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .draggable(true)
                .title("Marker in India"));

        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        mMap.getUiSettings().setZoomControlsEnabled(true);


    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (null != mSelectedMarker) {
            mSelectedMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.logo));
        }
        mSelectedMarker = marker;
        mSelectedMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.shahed));
        return true;
    }

    @Override
    public void onMapClick(LatLng latLng) {
        if (null != mSelectedMarker) {
            mSelectedMarker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.shahed));
        }
        mSelectedMarker = null;

    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        Toast.makeText(MapsActivity.this, "onMarkerDragStart", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onMarkerDrag(Marker marker) {
        Toast.makeText(MapsActivity.this, "onMarkerDrag", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        // getting the Co-ordinates
        latitude = marker.getPosition().latitude;
        longitude = marker.getPosition().longitude;

        //move to current position
        moveMap();
    }

    @Override
    protected void onStart() {
        googleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }


}
