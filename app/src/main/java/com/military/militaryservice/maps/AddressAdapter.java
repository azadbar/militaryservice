package com.military.militaryservice.maps;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.database.User;
import com.military.militaryservice.resultInfo.LocationDTO;
import com.military.militaryservice.utils.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> {


    private ArrayList<LocationDTO> list;
    private final OnItemClickListener listener;
    private AppDatabase database;


    public AddressAdapter(ArrayList<LocationDTO> list, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_address, parent, false);

        return new ViewHolder(itemView);

    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        LocationDTO address = list.get(position);
        holder.tvTitle.setText(address.getAddress());

        SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(address.getDate().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            PersianDate datea = new PersianDate(date);
            PersianDateFormat pd = new PersianDateFormat("j F y");
            holder.tvData.setText(pd.format(datea) + " - ");
        } else {
            holder.tvData.setText(null);
        }

        database = AppDatabase.getInMemoryDatabase(holder.itemView.getContext());
        User user = database.userDao().getUser(address.getUserId());
        if (user != null) {
            holder.tvUser.setText("ثبت کننده : "+user.getUsername());
        }else {
            holder.tvUser.setText(null);
        }
        holder.bind(address, position, listener);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setList(ArrayList<LocationDTO> address) {
        this.list = address;
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.rlItem)
        BaseRelativeLayout rlItem;
        @BindView(R.id.row)
        BaseRelativeLayout row;
        @BindView(R.id.imgDelete)
        BaseImageView imgDelete;
        @BindView(R.id.tvData)
        BaseTextView tvData;
        @BindView(R.id.tvUser)
        BaseTextView tvUser;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }


        public void bind(final LocationDTO address, int position, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemAddress(position, address));
            imgDelete.setOnClickListener(v -> listener.onItemDelete(position, address));
        }

    }


    public interface OnItemClickListener {

        void onItemAddress(int position, LocationDTO address);

        void onItemDelete(int position, LocationDTO address);
    }

}
