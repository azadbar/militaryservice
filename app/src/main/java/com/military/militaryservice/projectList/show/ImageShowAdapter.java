package com.military.militaryservice.projectList.show;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.database.User;
import com.military.militaryservice.enums.UserAccessEnum;
import com.military.militaryservice.resultInfo.FileAttachmentsItem;
import com.military.militaryservice.utils.Constants;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class ImageShowAdapter extends RecyclerView.Adapter<ImageShowAdapter.ViewHolder> {


    private ArrayList<FileAttachmentsItem> list;
    private final OnItemClickListener listener;
    private boolean isShowEdit;
    private AppDatabase database;


    public ImageShowAdapter(ArrayList<FileAttachmentsItem> list, OnItemClickListener listener, boolean isShowEdit) {
        this.list = list;
        this.listener = listener;
        this.isShowEdit = isShowEdit;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_show, parent, false);

        return new ViewHolder(itemView);

    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        FileAttachmentsItem fileAttachmentsItems = list.get(position);
        File file = new File(Environment.getExternalStorageDirectory(), fileAttachmentsItems.getFileAddress());

        Glide.with(holder.itemView.getContext())
                .load(file.getAbsolutePath())
                .apply(new RequestOptions().override(200, 200))
                .into(holder.image);


        if (fileAttachmentsItems.getDescription() != null) {
            holder.tvDescription.setVisibility(View.VISIBLE);
            holder.tvDescription.setText("توضیحات: " + fileAttachmentsItems.getDescription());
        } else {
            holder.tvDescription.setVisibility(View.INVISIBLE);
        }
        SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(fileAttachmentsItems.getDate().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            PersianDate datea = new PersianDate(date);
            PersianDateFormat pd = new PersianDateFormat("j F y");
            holder.tvData.setText(pd.format(datea) + "");
        } else {
            holder.tvData.setText(null);
        }

        if (isShowEdit) {
            holder.cvRegisterWithoutPhotos.setVisibility(View.VISIBLE);
        } else {
            holder.cvRegisterWithoutPhotos.setVisibility(View.GONE);
        }
        database = AppDatabase.getInMemoryDatabase(holder.itemView.getContext());
        User user = database.userDao().getUser(fileAttachmentsItems.getUserId());
        if (user != null) {
            holder.tvUser.setText("ثبت کننده : " + user.getUsername());
        } else {
            holder.tvUser.setText(null);
        }

        if (Constants.getUser(holder.itemView.getContext()).getUserAccess() == UserAccessEnum.READ){
            holder.cvRegisterWithoutPhotos.setVisibility(View.INVISIBLE);
        }else {
            holder.cvRegisterWithoutPhotos.setVisibility(View.VISIBLE);
        }
//        holder.bind(image, position, listener);
        holder.imgDelete.setOnClickListener(v -> listener.onDeleteItem(position, fileAttachmentsItems));
        holder.imgLocation.setOnClickListener(v -> listener.onLocation(position, fileAttachmentsItems));
        holder.edit.setOnClickListener(v -> listener.onEditItem(position, fileAttachmentsItems));
        holder.image.setOnClickListener(v -> listener.onShowImage(position, fileAttachmentsItems));
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setList(ArrayList<FileAttachmentsItem> voices) {
        this.list = voices;
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        BaseImageView image;
        //        @BindView(R.id.tvTitle)
//        BaseTextView tvTitle;
//        @BindView(R.id.tvAddress)
//        BaseTextView tvAddress;
        @BindView(R.id.tvData)
        BaseTextView tvData;
        @BindView(R.id.tvDescription)
        BaseTextView tvDescription;
        @BindView(R.id.imgDelete)
        BaseImageView imgDelete;
        @BindView(R.id.imgLocation)
        BaseImageView imgLocation;
        @BindView(R.id.edit)
        BaseTextView edit;
        @BindView(R.id.cvRegisterWithoutPhotos)
        CardView cvRegisterWithoutPhotos;
        @BindView(R.id.tvUser)
        BaseTextView tvUser;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }


        public void bind(final FileAttachmentsItem image, int position, final OnItemClickListener listener) {
        }

    }


    public interface OnItemClickListener {

        void onDeleteItem(int position, FileAttachmentsItem fileAttachmentsItems);

        void onLocation(int position, FileAttachmentsItem fileAttachmentsItems);

        void onEditItem(int position, FileAttachmentsItem fileAttachmentsItems);

        void onShowImage(int position, FileAttachmentsItem fileAttachmentsItems);
    }

}
