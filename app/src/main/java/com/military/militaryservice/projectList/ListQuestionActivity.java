package com.military.militaryservice.projectList;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseActivity;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.baseView.BaseToolbar;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.database.Description;
import com.military.militaryservice.detail.DetailActivity;
import com.military.militaryservice.dialog.InsertDescriptionDialog;
import com.military.militaryservice.orderInfo.ProjectResponse;
import com.military.militaryservice.orderInfo.QuestionInfosItem;
import com.military.militaryservice.projectList.show.ImageListShowActivity;
import com.military.militaryservice.projectList.show.VideoListShowActivity;
import com.military.militaryservice.projectList.show.VoiceListShowActivity;
import com.military.militaryservice.utils.Constants;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListQuestionActivity extends BaseActivity implements ListQuestionAdapter.OnItemClickListener {


    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvCenterTitle)
    BaseTextView tvCenterTitle;
    @BindView(R.id.share)
    BaseImageView share;
    @BindView(R.id.toolbar)
    BaseToolbar toolbar;
    @BindView(R.id.rvProjectList)
    RecyclerView rvProjectList;
    @BindView(R.id.imgView)
    BaseImageView imgView;
    @BindView(R.id.tvEmptyView)
    BaseTextView tvEmptyView;
    @BindView(R.id.rootEmptyView)
    BaseRelativeLayout rootEmptyView;
    private ListQuestionAdapter adapter;
    private int id;
    private ProjectResponse object;
    private AppDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_list);
        ButterKnife.bind(this);

        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.ic_arrow_forward);
        imgBack.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        imgBack.setOnClickListener(v -> finish());

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id = bundle.getInt("id");
        }
        database = AppDatabase.getInMemoryDatabase(this);


    }

    @Override
    protected void onResume() {
        super.onResume();
        object = database.projectResponseDao().getProject(id);
        SpannableStringBuilder desc_two = new SpannableStringBuilder();
        desc_two.append("محور های پرونده   ");
        int start = desc_two.length();
        desc_two.append(object.getPersonInfo().getFirstName() + " " + object.getPersonInfo().getLastName());
        int end = desc_two.length();
        desc_two.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.white)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        desc_two.setSpan(new RelativeSizeSpan(1.1f), start, end, 0);
        tvCenterTitle.setText(desc_two);
        //for create folder
        for (int i = 0; i < object.getQuestionInfos().size(); i++) {
            File dir = new File(Constants.storage_Dir_file_attachment, i + 1 + "");
            if (!dir.exists()) {
                dir.mkdirs();
            }
        }
        setAdapter();
    }


    private void setAdapter() {
        if (adapter == null) {
            adapter = new ListQuestionAdapter(object.getQuestionInfos(), this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
            rvProjectList.setLayoutManager(layoutManager);
            rvProjectList.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        } else {
            adapter.notifyDataSetChanged();
        }

    }


    @Override
    public void onItemClick(int position, QuestionInfosItem questionInfosItem) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("questionInfosItem", questionInfosItem);
        startActivity(intent);
    }

    @Override
    public void onVoiceClick(QuestionInfosItem object, int position) {
        Intent intent = new Intent(this, VoiceListShowActivity.class);
        intent.putExtra("position", position + 1);
        intent.putExtra("id", object.getQuestionInfosId());
        intent.putExtra("question", object.getQuestionInfo());
        intent.putExtra("firstName", this.object.getPersonInfo().getFirstName());
        intent.putExtra("lastName", this.object.getPersonInfo().getLastName());
        startActivity(intent);
    }

    @Override
    public void onPhotoClick(QuestionInfosItem object, int position) {
        Intent intent = new Intent(this, ImageListShowActivity.class);
        intent.putExtra("position", position + 1);
        intent.putExtra("id", object.getQuestionInfosId());
        intent.putExtra("question", object.getQuestionInfo());
        intent.putExtra("firstName", this.object.getPersonInfo().getFirstName());
        intent.putExtra("lastName", this.object.getPersonInfo().getLastName());
        startActivity(intent);
    }

    @Override
    public void onVideoClick(QuestionInfosItem object, int position) {
        Intent intent = new Intent(this, VideoListShowActivity.class);
        intent.putExtra("position", position + 1);
        intent.putExtra("id", object.getQuestionInfosId());
        intent.putExtra("question", object.getQuestionInfo());
        intent.putExtra("firstName", this.object.getPersonInfo().getFirstName());
        intent.putExtra("lastName", this.object.getPersonInfo().getLastName());
        startActivity(intent);
    }

    @Override
    public void onInsertClick(QuestionInfosItem object, int position) {
        InsertDescriptionDialog dialog = new InsertDescriptionDialog(this);
        dialog.setIcon(R.drawable.ic_description, getResources().getColor(R.color.redColor));
        dialog.setOkListener("ثبت", v -> {
            Description description = new Description();
            description.setDescription(dialog.getEditText());

            if (database.descriptionDao().getDescription(object.getQuestionInfosId()) == null) {
                description.setQuestionIdentifier(object.getQuestionInfosId());
                database.descriptionDao().insert(description);
                Toast.makeText(this, "توضیح جدید اضافه شد", Toast.LENGTH_SHORT).show();
            } else {
                database.descriptionDao().update(dialog.getEditText(), object.getQuestionInfosId());
                Toast.makeText(this, "توضیح بروز شد", Toast.LENGTH_SHORT).show();
            }
            dialog.dismiss();
            adapter.notifyDataSetChanged();
        });
        dialog.setDialogTitle("توضیحات مورد نظر را وارد کنید");
        dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
        dialog.setCancelListener(getResources().getString(R.string.cancel), v -> dialog.dismiss());
        dialog.setEditTextName(database.descriptionDao().getDescription(object.getQuestionInfosId()) == null ? "" : database.descriptionDao().getDescription(object.getQuestionInfosId()).getDescription());
        dialog.show();
    }

    private Spanned createHtmlText(String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(text);
        }
    }
}
