package com.military.militaryservice.projectList.show;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.military.militaryservice.R;
import com.military.militaryservice.audio.AudioRecorderActivity;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.database.User;
import com.military.militaryservice.enums.UserAccessEnum;
import com.military.militaryservice.resultInfo.FileAttachmentsItem;
import com.military.militaryservice.utils.AudioWife;
import com.military.militaryservice.utils.Constants;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class VoiceShowAdapter extends RecyclerView.Adapter<VoiceShowAdapter.ViewHolder> {



    private ArrayList<FileAttachmentsItem> list;
    private final OnItemClickListener listener;
    private boolean isShowEdit;
    private AppDatabase database;


    public VoiceShowAdapter(ArrayList<FileAttachmentsItem> list, OnItemClickListener listener, boolean isShowEdit) {
        this.list = list;
        this.listener = listener;
        this.isShowEdit = isShowEdit;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_voice_show, parent, false);

        return new ViewHolder(itemView);

    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        FileAttachmentsItem voice = list.get(position);
//        holder.tvTitle.setText(image.getNamePerson());
//        holder.tvAddress.setText(image.getAddress());
//
//        Glide.with(holder.imageMag.getContext()).load(new File(image.getPath())).into(holder.imageMag);

        SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(voice.getDate().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            PersianDate datea = new PersianDate(date);
            PersianDateFormat pd = new PersianDateFormat("j F y");
            holder.tvData.setText(pd.format(datea) + "");
        } else {
            holder.tvData.setText(null);
        }

        if (isShowEdit) {
            holder.cvRegisterWithoutPhotos.setVisibility(View.VISIBLE);
        } else {
            holder.cvRegisterWithoutPhotos.setVisibility(View.GONE);
        }

        if (voice.getDescription() != null) {
            holder.tvDescription.setVisibility(View.VISIBLE);
            holder.tvDescription.setText("توضیحات: " + voice.getDescription());
        } else {
            holder.tvDescription.setVisibility(View.INVISIBLE);
        }

        File file = new File(Environment.getExternalStorageDirectory(), voice.getFileAddress());
        holder.audioWife.getInstance()
                .init(holder.itemView.getContext(), file.getAbsolutePath())
                .setPlayView(holder.itemView.findViewById(R.id.pause))
                .setPauseView(holder.itemView.findViewById(R.id.pause))
                .setSeekBar(holder.itemView.findViewById(R.id.media_seekbar))
                .setRuntimeView(holder.itemView.findViewById(R.id.run_time))
                .setTotalTimeView(holder.itemView.findViewById(R.id.total_time));

        holder.audioWife.getInstance().addOnCompletionListener(mp -> {
        });

        holder.audioWife.getInstance().addOnPlayClickListener(v -> {
        });

        holder.audioWife.getInstance().addOnPauseClickListener(v -> {
        });

        database = AppDatabase.getInMemoryDatabase(holder.itemView.getContext());
        User user = database.userDao().getUser(voice.getUserId());
        if (user != null) {
            holder.tvUser.setText("ثبت کننده : " + user.getUsername());
        } else {
            holder.tvUser.setText(null);
        }

        if (Constants.getUser(holder.itemView.getContext()).getUserAccess() == UserAccessEnum.READ){
            holder.cvRegisterWithoutPhotos.setVisibility(View.INVISIBLE);
        }else {
            holder.cvRegisterWithoutPhotos.setVisibility(View.VISIBLE);
        }

        holder.play.setOnClickListener(v -> {
            listener.onItemPlay(position, voice);
        });
        holder.bind(voice, position, listener);
        holder.imgDelete.setOnClickListener(v -> listener.onDeleteItemVoice(position, voice));
        holder.imgLocation.setOnClickListener(v -> listener.onLocationVoice(position, voice));
        holder.edit.setOnClickListener(v -> listener.onEditItemVoice(position, voice));

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setList(ArrayList<FileAttachmentsItem> voices) {
        this.list = voices;
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.play)
        ImageView play;
        @BindView(R.id.pause)
        ImageView pause;
        @BindView(R.id.play_pause_layout)
        FrameLayout playPauseLayout;
        @BindView(R.id.imgDelete)
        BaseImageView imgDelete;
        @BindView(R.id.framelayout)
        FrameLayout framelayout;
        @BindView(R.id.tvData)
        BaseTextView tvData;
        @BindView(R.id.tvDescription)
        BaseTextView tvDescription;
        @BindView(R.id.imgLocation)
        BaseImageView imgLocation;
        @BindView(R.id.edit)
        BaseTextView edit;
        @BindView(R.id.cvRegisterWithoutPhotos)
        CardView cvRegisterWithoutPhotos;
        @BindView(R.id.run_time)
        TextView runTime;
        @BindView(R.id.total_time)
        TextView totalTime;
        @BindView(R.id.llItems)
        BaseRelativeLayout llItems;
        @BindView(R.id.emergancy)
        CardView emergancy;
        @BindView(R.id.card)
        CardView card;

        AudioWife audioWife;
        @BindView(R.id.tvUser)
        BaseTextView tvUser;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            audioWife = new AudioWife();
        }


        public void bind(final FileAttachmentsItem image, int position, final OnItemClickListener listener) {
            playPauseLayout.setOnClickListener(v -> listener.onItemClickVoice(position, image));
        }

    }


    public interface OnItemClickListener {
        void onItemClickVoice(int position, FileAttachmentsItem image);

        void onDeleteItemVoice(int position, FileAttachmentsItem voice);

        void onLocationVoice(int position, FileAttachmentsItem voice);

        void onEditItemVoice(int position, FileAttachmentsItem voice);

        void onItemPlay(int position, FileAttachmentsItem voice);
    }

}
