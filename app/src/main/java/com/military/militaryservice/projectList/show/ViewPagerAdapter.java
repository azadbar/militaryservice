package com.military.militaryservice.projectList.show;

import android.content.Context;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseImageView;

import java.io.File;
import java.util.ArrayList;


public class ViewPagerAdapter extends PagerAdapter {

    private ArrayList<String> images;

    public ViewPagerAdapter(ArrayList<String> images) {
        this.images = images;
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {

        LayoutInflater layoutInflater = (LayoutInflater) container.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.custom_layout, null);
        BaseImageView imageView = view.findViewById(R.id.imageView);
        String imageFileName = images.get(position);
        File file = new File(Environment.getExternalStorageDirectory(), imageFileName);

        Glide.with(imageView.getContext()).load(file).into(imageView);
        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);

    }
}