package com.military.militaryservice.projectList.show;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseActivity;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.enums.AttachmentType;
import com.military.militaryservice.orderInfo.ProjectResponse;
import com.military.militaryservice.resultInfo.FileAttachmentsItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PreviewActivity extends BaseActivity implements PreviewAdapter.OnItemClickListener {

    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    public ArrayList<String> images = new ArrayList<>();
    int currentPosition = 0;

    //    BaseImageView imgLogo;
//    @BindView(R.id.imgBack)
//    BaseImageView imgBack;
//    @BindView(R.id.tvTitle)
//    BaseTextView tvTitle;
//    @BindView(R.id.rlBack)
//    BaseRelativeLayout rlBack;
//    @BindView(R.id.toolbar)
//    BaseToolbar toolbar;
    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.right_nav)
    ImageButton rightNav;
    @BindView(R.id.left_nav)
    ImageButton leftNav;
    @BindView(R.id.backRecycle)
    BaseRelativeLayout backRecycle;
    private FileAttachmentsItem file;
    private static PreviewAdapter adapter;
    private ArrayList<FileAttachmentsItem> imageList = new ArrayList<>();
    private boolean isSelected;
    private boolean isFirst = true;
    private AppDatabase database;
    private ProjectResponse object;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);
        ButterKnife.bind(this);
//        imgBack.setImageResource(R.drawable.ic_arrow_forward);

        database = AppDatabase.getInMemoryDatabase(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            file = (FileAttachmentsItem) bundle.getSerializable("file");
            currentPosition = bundle.getInt("currentPosition");
            id = bundle.getInt("id");
        }

        AppDatabase database = AppDatabase.getInMemoryDatabase(this);

        if (file != null) {
            imageList.add(file);
            recyclerview.setVisibility(View.GONE);
            backRecycle.setVisibility(View.GONE);
        } else {
            imageList = (ArrayList<FileAttachmentsItem>) database.fileAttachmentDao().select(id, AttachmentType.IMAGE.getFormat());
            recyclerview.setVisibility(View.VISIBLE);
            backRecycle.setVisibility(View.VISIBLE);

        }

        ArrayList<FileAttachmentsItem> list = new ArrayList<>();
        for (int i = 0; i < imageList.size(); i++) {
            if (TextUtils.equals(imageList.get(i).getFormat(), AttachmentType.IMAGE.getFormat())) {
                list.add(imageList.get(i));
            }
        }


        for (FileAttachmentsItem im : list) {
            images.add(im.getFileAddress());
        }

//        imageFragmentPagerAdapter = new ImageFragmentPagerAdapter(getSupportFragmentManager(), images);
//        pager.setAdapter(imageFragmentPagerAdapter);
//        File file = new File(Environment.getExternalStorageDirectory(), image);
//        Glide.with(imageView.getContext()).load(file.getAbsolutePath()).into(imageView);

//        imgBack.setOnClickListener(v -> finish());

        if (images != null && images.size() == 1) {
            recyclerview.setVisibility(View.GONE);
        } else {
            recyclerview.setVisibility(View.VISIBLE);
        }
        pager = findViewById(R.id.pager);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(images);

        pager.setAdapter(viewPagerAdapter);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//                if (isSelected) {
//                    isSelected = false;
//                } else {
//                    currentPosition = position;
//                }
//
//                if (isFirst) {
                currentPosition = position;
                if (!isSelected) {
                    adapter.setCurrentPosition(position);
                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            isSelected = false;
                        }
                    }, 200);
                }

//                    isFirst = false;
//                } else {
//                    isFirst = true;
//                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
//        setImageUri();
        setImageAdapter();
        setImageUri(currentPosition);
    }

    public static class ImageFragmentPagerAdapter extends FragmentPagerAdapter {
        private final ArrayList<String> images;

        ImageFragmentPagerAdapter(FragmentManager fm, ArrayList<String> images) {
            super(fm);
            this.images = images;
        }

        @Override
        public int getCount() {
            return images.size();
        }

        @Override
        public Fragment getItem(int position) {
            return SwipeFragment.newInstance(position);
        }
    }

    public static class SwipeFragment extends Fragment {
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View swipeView = inflater.inflate(R.layout.swipe_fragment, container, false);
            DecomTouchImageView imageView = swipeView.findViewById(R.id.imageView);
            Bundle bundle = getArguments();
            if (bundle != null) {
                int position = bundle.getInt("position");
//                String imageFileName = images.get(position);
//                Glide.with(getActivity()).load(imageFileName).into(imageView);
//                adapter.setCurrentPosition(position);
            }

//            int imgResId = getResources().getIdentifier(imageFileName, "drawable", "ir.delta.delta.estateDetail");
//            imageView.setImageResource(imgResId);
            return swipeView;
        }

        static SwipeFragment newInstance(int position) {
            SwipeFragment swipeFragment = new SwipeFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("position", position);
            swipeFragment.setArguments(bundle);
            return swipeFragment;
        }
    }

    private void setImageAdapter() {
        adapter = new PreviewAdapter(images, currentPosition, this);
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerview.setLayoutManager(horizontalLayoutManagaer);
        recyclerview.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(int position) {
        currentPosition = position;
        isSelected = true;
        setImageUri(currentPosition);
        if (adapter != null) {
            adapter.setCurrentPosition(currentPosition);
        }
    }

    public void setImageUri(int position) {
        pager.setCurrentItem(position);
    }

    @OnClick({R.id.left_nav, R.id.right_nav})
    public void onViewClicked(View view) {
        int tab;
        switch (view.getId()) {
            case R.id.left_nav:
                tab = pager.getCurrentItem();
                if (tab > 0) {
                    tab--;
                    pager.setCurrentItem(tab);
                } else if (tab == 0) {
                    pager.setCurrentItem(tab);
                }
                break;
            case R.id.right_nav:
                tab = pager.getCurrentItem();
                tab++;
                pager.setCurrentItem(tab);
                break;

        }
    }


}
