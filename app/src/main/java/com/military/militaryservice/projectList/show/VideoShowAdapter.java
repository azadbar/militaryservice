package com.military.militaryservice.projectList.show;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.database.Image;
import com.military.militaryservice.database.User;
import com.military.militaryservice.database.Video;
import com.military.militaryservice.enums.UserAccessEnum;
import com.military.militaryservice.recordVideo.VideoInsertAdapter;
import com.military.militaryservice.resultInfo.FileAttachmentsItem;
import com.military.militaryservice.utils.Constants;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class VideoShowAdapter extends RecyclerView.Adapter<VideoShowAdapter.ViewHolder> {


    private ArrayList<FileAttachmentsItem> list;
    private final OnItemClickListener listener;
    private boolean isShowEdit;
    private AppDatabase database;


    public VideoShowAdapter(ArrayList<FileAttachmentsItem> list, OnItemClickListener listener, boolean isShowEdit) {
        this.list = list;
        this.listener = listener;
        this.isShowEdit = isShowEdit;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video_show, parent, false);

        return new ViewHolder(itemView);

    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        FileAttachmentsItem fileVideo = list.get(position);
        File file = new File(Environment.getExternalStorageDirectory(), fileVideo.getFileAddress());

        holder.image.setVideoURI(Uri.fromFile(file.getAbsoluteFile()));
        holder.image.setOnPreparedListener(mp -> {
            mp.setVolume(0f, 0f);
            mp.setLooping(false);
        });
        holder.image.start();


        if (fileVideo.getDescription() != null) {
            holder.tvDescription.setVisibility(View.VISIBLE);
            holder.tvDescription.setText("توضیحات: " + fileVideo.getDescription());
        } else {
            holder.tvDescription.setVisibility(View.INVISIBLE);
        }
        SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(fileVideo.getDate().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            PersianDate datea = new PersianDate(date);
            PersianDateFormat pd = new PersianDateFormat("j F y");
            holder.tvData.setText(pd.format(datea) + "");
        } else {
            holder.tvData.setText(null);
        }

        database = AppDatabase.getInMemoryDatabase(holder.itemView.getContext());
        User user = database.userDao().getUser(fileVideo.getUserId());
        if (user != null) {
            holder.tvUser.setText("ثبت کننده : " + user.getUsername());
        } else {
            holder.tvUser.setText(null);
        }

        if (isShowEdit) {
            holder.cvRegisterWithoutPhotos.setVisibility(View.VISIBLE);
        } else {
            holder.cvRegisterWithoutPhotos.setVisibility(View.GONE);
        }

        if (Constants.getUser(holder.itemView.getContext()).getUserAccess() == UserAccessEnum.READ){
            holder.cvRegisterWithoutPhotos.setVisibility(View.INVISIBLE);
        }else {
            holder.cvRegisterWithoutPhotos.setVisibility(View.VISIBLE);
        }

//        holder.bind(image, position, listener);
        holder.imgDelete.setOnClickListener(v -> listener.onDeleteItemVideo(position, fileVideo));
        holder.imgLocation.setOnClickListener(v -> listener.onLocationVideo(position, fileVideo));
        holder.edit.setOnClickListener(v -> listener.onEditItemVideo(position, fileVideo));
        holder.image.setOnClickListener(v -> listener.onShowImageVideo(position, fileVideo));
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setList(ArrayList<FileAttachmentsItem> voices) {
        this.list = voices;
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        VideoView image;
        //        @BindView(R.id.tvTitle)
//        BaseTextView tvTitle;
//        @BindView(R.id.tvAddress)
//        BaseTextView tvAddress;
        @BindView(R.id.tvData)
        BaseTextView tvData;
        @BindView(R.id.tvDescription)
        BaseTextView tvDescription;
        @BindView(R.id.imgDelete)
        BaseImageView imgDelete;
        @BindView(R.id.imgLocation)
        BaseImageView imgLocation;
        @BindView(R.id.edit)
        BaseTextView edit;
        @BindView(R.id.cvRegisterWithoutPhotos)
        CardView cvRegisterWithoutPhotos;
        @BindView(R.id.tvUser)
        BaseTextView tvUser;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }


        public void bind(final Image image, int position, final OnItemClickListener listener) {
        }

    }


    public interface OnItemClickListener {

        void onDeleteItemVideo(int position, FileAttachmentsItem fileVideo);

        void onLocationVideo(int position, FileAttachmentsItem fileVideo);

        void onEditItemVideo(int position, FileAttachmentsItem fileVideo);

        void onShowImageVideo(int position, FileAttachmentsItem fileVideo);
    }

}
