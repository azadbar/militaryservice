package com.military.militaryservice.projectList;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseActivity;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.dialog.CustomDialog;
import com.military.militaryservice.enums.UserAccessEnum;
import com.military.militaryservice.orderInfo.ProjectResponse;
import com.military.militaryservice.utils.Constants;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListProjectActivity extends BaseActivity implements ProjectListAdapter.OnItemClickListener {

    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvCenterTitle)
    BaseTextView tvCenterTitle;
    @BindView(R.id.rvProjectList)
    RecyclerView rvProjectList;
    @BindView(R.id.tvEmptyView)
    BaseTextView tvEmptyView;
    @BindView(R.id.rootEmptyView)
    BaseRelativeLayout rootEmptyView;
    private ArrayList<ProjectResponse> object;
    private ProjectListAdapter adapter;
    private AppDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_list);
        ButterKnife.bind(this);

        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.ic_arrow_forward);
        imgBack.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        imgBack.setOnClickListener(v -> finish());

        tvCenterTitle.setText("پرونده پرونده ها");

        database = AppDatabase.getInMemoryDatabase(this);

        object = (ArrayList<ProjectResponse>) database.projectResponseDao().getPersonList();

//        String mJsonString = PreferencesData.getString(this, "json");
//        JsonParser parser = new JsonParser();
//        JsonElement mJson = parser.parse(mJsonString);
//        Gson gson = new Gson();
//        object = gson.fromJson(mJson, new TypeToken<ArrayList<ProjectResponse>>() {
//        }.getType());

        if (object == null || object.size() == 0) {
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText(getString(R.string.nothing_project_found));
        } else
            setAdapter();

//        //for create folder
//        for (int i = 0; i < object.size(); i++) {
//            File dir = new File(Constants.storage_Dir_file_attachment, i + 1 + "");
//            if (!dir.exists()) {
//                dir.mkdirs();
//            }
//        }
    }

    private void setAdapter() {
        if (adapter == null) {
            adapter = new ProjectListAdapter(object, this);
            LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
            rvProjectList.setHasFixedSize(true);
            rvProjectList.setLayoutManager(layoutManager);
            rvProjectList.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        } else {
            adapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onItemClick(int position, ProjectResponse projectResponse) {
        Intent intent = new Intent(this, ListQuestionActivity.class);
        intent.putExtra("id", projectResponse.getId());
        startActivity(intent);
    }

    @Override
    public void onDeleteItem(int position, ProjectResponse personResponse) {

        if (Constants.getUser(this).getUserAccess() != UserAccessEnum.READ) {
            CustomDialog dialog = new CustomDialog(this);
            dialog.setOkListener(getResources().getString(R.string.ok), view1 -> {
                dialog.dismiss();
                File fdelete = new File(Environment.getExternalStorageDirectory(), "/shahed/Questions");
                if (fdelete.isDirectory()) {
                    String[] children = fdelete.list();
                    for (int i = 0; i < children.length; i++) {
                        new File(fdelete, children[i]).delete();
                    }
                }
                try {
                    FileUtils.deleteDirectory(fdelete);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                database.projectResponseDao().nukeTable();
                database.fileAttachmentDao().nukeTable();
                removeFromList(personResponse.getId());
            });
            dialog.setCancelListener(getResources().getString(R.string.cancel), view12 -> dialog.dismiss());
            dialog.setDialogTitle("آیا برای حذف مطمئن هستید؟");
            dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
            dialog.setIcon(R.drawable.ic_delete, getResources().getColor(R.color.redColor));
            dialog.show();
        }

    }

    private void removeFromList(long id) {
        for (ProjectResponse projectResponse : object) {
            if (projectResponse.getId() == id) {
                object.remove(projectResponse);
                database.projectResponseDao().delete(projectResponse);
                break;
            }
        }
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }

        if (object == null || object.size() == 0) {
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText(getString(R.string.nothing_project_found));
        }
    }

}
