package com.military.militaryservice.projectList.show;

import android.annotation.SuppressLint;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.military.militaryservice.R;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class PreviewAdapter extends RecyclerView.Adapter<PreviewAdapter.ViewHolder> {

    private ArrayList<String> list;
    private final OnItemClickListener listener;
    private int currentPosition = 0;


    public void setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition;
        notifyDataSetChanged();
    }

    PreviewAdapter(ArrayList<String> list, int currentPosition, OnItemClickListener listener) {
        this.list = list;
        this.currentPosition = currentPosition;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_thumb_image, parent, false);
        return new ViewHolder(itemView);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
//        if (position == currentPosition) {
//            holder.card.setBackgroundResource(R.drawable.select_preview_image);
//        } else {
//            holder.card.setBackgroundResource(R.drawable.unselect_preview_image);
//        }
        String s = list.get(position);
        File file = new File(Environment.getExternalStorageDirectory(), s);
        Glide.with(holder.imgCards.getContext()).load(file).into(holder.imgCards);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        if (currentPosition == position) {
            holder.card.setBackgroundColor(holder.itemView.getResources().getColor(R.color.redColor));
            params.setMargins(4, 4, 4, 4);
            holder.nested.setLayoutParams(params);
        } else {
            holder.card.setBackgroundColor(holder.itemView.getResources().getColor(R.color.secondaryBack1));
            params.setMargins(0, 0, 0, 0);
            holder.nested.setLayoutParams(params);
        }
        holder.bind(position, listener);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgDisplay)
        ImageView imgCards;
        @BindView(R.id.card)
        RelativeLayout card;
        @BindView(R.id.nested)
        RelativeLayout nested;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void bind(int position, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(position));
        }
    }


    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}
