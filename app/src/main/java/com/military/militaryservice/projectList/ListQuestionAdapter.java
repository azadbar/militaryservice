package com.military.militaryservice.projectList;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseLinearLayout;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.database.Description;
import com.military.militaryservice.enums.AttachmentType;
import com.military.militaryservice.orderInfo.QuestionInfosItem;
import com.military.militaryservice.resultInfo.FileAttachmentsItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class ListQuestionAdapter extends RecyclerView.Adapter<ListQuestionAdapter.ViewHolder> {


    private List<QuestionInfosItem> list;
    private final OnItemClickListener listener;


    ListQuestionAdapter(List<QuestionInfosItem> list, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_question, parent, false);

        return new ViewHolder(itemView);

    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        QuestionInfosItem object = list.get(position);
        holder.tvCount.setText("محور " + (position + 1));
        holder.tvQuestion.setText(object.getQuestionInfo());
        holder.rlItem.setBackgroundColor(position % 2 == 0 ? res.getColor(R.color.white) : res.getColor(R.color.oddW));

        AppDatabase database = AppDatabase.getInMemoryDatabase(holder.rlItem.getContext());

        List<FileAttachmentsItem> selectVoice = database.fileAttachmentDao().select(object.getQuestionInfosId(), AttachmentType.VOICE.getFormat());
        holder.tvVoice.setText(selectVoice.size() + "");
        if (selectVoice.size() > 0) {
            holder.imgVoice.setImageResource(R.drawable.ic_keyboard_voice);
            holder.imgVoice.setColorFilter(res.getColor(R.color.secondaryBack1));
        } else {
            holder.imgVoice.setImageResource(R.drawable.ic_keyboard_voice);
            holder.imgVoice.setColorFilter(res.getColor(R.color.secondryTextColor));
        }

        List<FileAttachmentsItem> selectImage = database.fileAttachmentDao().select(object.getQuestionInfosId(), AttachmentType.IMAGE.getFormat());
        holder.tvImage.setText(selectImage.size() + "");
        if (selectImage.size() > 0) {
            holder.imgPhoto.setImageResource(R.drawable.ic_add_a_photo);
            holder.imgPhoto.setColorFilter(res.getColor(R.color.secondaryBack1));
        } else {
            holder.imgPhoto.setImageResource(R.drawable.ic_add_a_photo);
            holder.imgPhoto.setColorFilter(res.getColor(R.color.secondryTextColor));
        }

        List<FileAttachmentsItem> selectVideo = database.fileAttachmentDao().select(object.getQuestionInfosId(), AttachmentType.VIDEO.getFormat());
        holder.tvVideo.setText(selectVideo.size() + "");
        if (selectVideo.size() > 0) {
            holder.imgVideo.setImageResource(R.drawable.ic_video);
            holder.imgVideo.setColorFilter(res.getColor(R.color.secondaryBack1));
        } else {
            holder.imgVideo.setImageResource(R.drawable.ic_video);
            holder.imgVideo.setColorFilter(res.getColor(R.color.secondryTextColor));
        }


        holder.btnVoice.setOnClickListener(v -> listener.onVoiceClick(object, position));

        holder.btnPhoto.setOnClickListener(v -> listener.onPhotoClick(object, position));

        holder.btnVideo.setOnClickListener(v -> listener.onVideoClick(object, position));

        Description description = database.descriptionDao().getDescription(object.getQuestionInfosId());
        if (description != null && description.getDescription() != null && !description.getDescription().isEmpty()) {
            holder.insertDescription.setImageResource(R.drawable.ic_textsms);
            holder.insertDescription.setColorFilter(res.getColor(R.color.secondaryBack1));
        } else {
            holder.insertDescription.setImageResource(R.drawable.ic_textsms);
            holder.insertDescription.setColorFilter(res.getColor(R.color.secondryTextColor));
        }

        holder.insertDescription.setOnClickListener(v -> listener.onInsertClick(object, position));

        holder.bind(object, position, listener);
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgDelete)
        BaseImageView imgDelete;
        @BindView(R.id.tvCount)
        BaseTextView tvCount;
        @BindView(R.id.tvQuestion)
        BaseTextView tvQuestion;
        @BindView(R.id.rlItem)
        BaseRelativeLayout rlItem;
        @BindView(R.id.row)
        BaseRelativeLayout row;
        @BindView(R.id.tvImage)
        BaseTextView tvImage;
        @BindView(R.id.tvVideo)
        BaseTextView tvVideo;
        @BindView(R.id.tvVoice)
        BaseTextView tvVoice;
        @BindView(R.id.btnVoice)
        BaseLinearLayout btnVoice;
        @BindView(R.id.btnPhoto)
        BaseLinearLayout btnPhoto;
        @BindView(R.id.btnVideo)
        BaseLinearLayout btnVideo;
        @BindView(R.id.insertDescription)
        BaseImageView insertDescription;
        @BindView(R.id.imgPhoto)
        BaseImageView imgPhoto;
        @BindView(R.id.imgVideo)
        BaseImageView imgVideo;
        @BindView(R.id.imgVoice)
        BaseImageView imgVoice;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }


        public void bind(final QuestionInfosItem questionInfosItem, int position, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(position, questionInfosItem));
        }

    }


    public interface OnItemClickListener {

        void onItemClick(int position, QuestionInfosItem questionInfosItem);

        void onVoiceClick(QuestionInfosItem object, int position);

        void onPhotoClick(QuestionInfosItem object, int position);

        void onVideoClick(QuestionInfosItem object, int position);

        void onInsertClick(QuestionInfosItem object, int position);
    }

}
