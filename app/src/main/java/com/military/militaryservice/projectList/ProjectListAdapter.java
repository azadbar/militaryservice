package com.military.militaryservice.projectList;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.os.Build;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.model.PersonResponse;
import com.military.militaryservice.orderInfo.ProjectResponse;
import com.military.militaryservice.utils.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;


/**
 * Created by m.azadbar on 9/21/2017.
 */

public class ProjectListAdapter extends RecyclerView.Adapter<ProjectListAdapter.ViewHolder> {


    private ArrayList<ProjectResponse> list;
    private final OnItemClickListener listener;


    ProjectListAdapter(ArrayList<ProjectResponse> list, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_project, parent, false);

        return new ViewHolder(itemView);

    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Resources res = holder.itemView.getContext().getResources();
        ProjectResponse object = list.get(position);
        if (object.getPersonInfo() != null) {
            holder.tvTitle.setText(" آقای " + object.getPersonInfo().getFirstName() + " " + object.getPersonInfo().getLastName()
                    + "  فرزند " + (object.getPersonInfo().getFatherName() != null ? object.getPersonInfo().getFatherName() : "نامشخص") + "  کد ملی " + object.getPersonInfo().getNationalCode());
        } else {
            holder.tvTitle.setText("اطلاعات به درستی وارد نشده است");
        }


//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",  Constants.language.getLocale());
//        Date date = null;
//        try {
//            date = format.parse(object.getCreationDateTime());
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        if (date != null) {
//            PersianDate datea = new PersianDate(date);
//            PersianDateFormat pd = new PersianDateFormat("Y/m/d");
//
//        } else {
//            holder.tvDate.setText(null);
//        }

        String[] split = object.getCreationDateTime().split("-");
        String s = null;
        String s1 = null;
        if (split.length > 0) {
            s = split[0];
            s1 = split[1];
        }


        String tvData = "<font color='#FB4129'>" + "تاریخ درخواست :" + " </font> " +
                "<font color='#777777' <strong> " + s1.substring(0, 5) + " مورخ " + s + "</strong></font>";
        holder.tvDate.setText(createHtmlText(tvData));
//
        if (object.getPersonInfo() != null && object.getPersonInfo().getHomeInfo() != null && object.getPersonInfo().getHomeInfo().getAddressInfo() != null) {
            String tvAddressHome = "<font color='#777777' <strong> " + "خیابان " + object.getPersonInfo().getHomeInfo().getAddressInfo().getMainStreet() +
                    " خیابان " + object.getPersonInfo().getHomeInfo().getAddressInfo().getSubStreet() + " کوچه " + object.getPersonInfo().getHomeInfo().getAddressInfo().getAlley() + "</strong></font>";
            holder.tvAddressHome.setText(createHtmlText(tvAddressHome));
        } else {
            holder.tvAddressHome.setText("ندارد");
        }


        if (object.getPersonInfo() != null && object.getPersonInfo().getWorkInfo() != null && object.getPersonInfo().getWorkInfo().getAddressInfo() != null) {
            String tvAddressWork = "<font color='#777777' <strong> " + " خیابان " + object.getPersonInfo().getWorkInfo().getAddressInfo().getMainStreet() +
                    " خیابان " + object.getPersonInfo().getWorkInfo().getAddressInfo().getSubStreet() + " کوچه "
                    + object.getPersonInfo().getWorkInfo().getAddressInfo().getAlley() + "</strong></font>";
            holder.tvAddressWork.setText(createHtmlText(tvAddressWork));
        } else {
            holder.tvAddressWork.setText("ندارد");
        }


        holder.rlItem.setBackgroundColor(position % 2 == 0 ? res.getColor(R.color.white) : res.getColor(R.color.oddW));
        holder.bind(object, position, listener);
        holder.imgDelete.setOnClickListener(v -> listener.onDeleteItem(position, object));
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateReceiptsList(ArrayList<ProjectResponse> newlist) {
        list.clear();
        list.addAll(newlist);
        this.notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgDelete)
        BaseImageView imgDelete;
        @BindView(R.id.tvTitle)
        BaseTextView tvTitle;
        @BindView(R.id.tvDate)
        BaseTextView tvDate;
        @BindView(R.id.tvAddressHome)
        BaseTextView tvAddressHome;
        @BindView(R.id.tvAddressWork)
        BaseTextView tvAddressWork;
        @BindView(R.id.rlItem)
        BaseRelativeLayout rlItem;
        @BindView(R.id.row)
        BaseRelativeLayout row;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }


        public void bind(final ProjectResponse personResponse, int position, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(position, personResponse));
        }

    }

    private Spanned createHtmlText(String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(text);
        }
    }

    public interface OnItemClickListener {

        void onItemClick(int position, ProjectResponse personResponse);

        void onDeleteItem(int position, ProjectResponse personResponse);
    }

}
