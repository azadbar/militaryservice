package com.military.militaryservice.projectList.show;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.widget.MediaController;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseActivity;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.baseView.BaseToolbar;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PreviewVideoActivity extends BaseActivity {


//    BaseImageView imgLogo;
//    @BindView(R.id.imgBack)
//    BaseImageView imgBack;
//    @BindView(R.id.tvTitle)
//    BaseTextView tvTitle;
//    @BindView(R.id.rlBack)
//    BaseRelativeLayout rlBack;
//    @BindView(R.id.toolbar)
//    BaseToolbar toolbar;
    @BindView(R.id.imageView)
    VideoView imageView;
    private String image;
    MediaController mediaC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_video);
        ButterKnife.bind(this);
//        imgBack.setImageResource(R.drawable.ic_arrow_forward);


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            image = bundle.getString("video");
        }
//        imageFragmentPagerAdapter = new ImageFragmentPagerAdapter(getSupportFragmentManager(), images);
//        pager.setAdapter(imageFragmentPagerAdapter);
        mediaC = new MediaController(this);
        File file = new File(Environment.getExternalStorageDirectory(), image);
        imageView.setVideoURI(Uri.fromFile(file.getAbsoluteFile()));
        imageView.setMediaController(mediaC);
        mediaC.setMediaPlayer(imageView);
        imageView.start();

//        imgBack.setOnClickListener(v -> finish());
    }


}
