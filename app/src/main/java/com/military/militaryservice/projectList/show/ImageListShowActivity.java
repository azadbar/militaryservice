package com.military.militaryservice.projectList.show;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseActivity;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.baseView.BaseToolbar;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.dialog.CustomDialog;
import com.military.militaryservice.enums.AttachmentType;
import com.military.militaryservice.enums.UserAccessEnum;
import com.military.militaryservice.image.AddImageDialog;
import com.military.militaryservice.maps.OfflineActivity;
import com.military.militaryservice.model.PersonResponse;
import com.military.militaryservice.recordAudio.AudioRecordDialog;
import com.military.militaryservice.resultInfo.FileAttachmentsItem;
import com.military.militaryservice.utils.Constants;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ImageListShowActivity extends BaseActivity implements ImageShowAdapter.OnItemClickListener, AddImageDialog.updateListenerImage {


    @BindView(R.id.imgHelp)
    BaseImageView imgHelp;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.imgLogo)
    BaseImageView imgLogo;
    @BindView(R.id.tvCenterTitle)
    BaseTextView tvCenterTitle;
    @BindView(R.id.share)
    BaseImageView share;
    @BindView(R.id.toolbar)
    BaseToolbar toolbar;
    @BindView(R.id.rvVoiceList)
    RecyclerView rvVoiceList;
    @BindView(R.id.image)
    AppCompatImageView image;
    @BindView(R.id.imgView)
    BaseImageView imgView;
    @BindView(R.id.tvEmptyView)
    BaseTextView tvEmptyView;
    @BindView(R.id.rootEmptyView)
    BaseRelativeLayout rootEmptyView;
    @BindView(R.id.btnAddImage)
    FloatingActionButton btnAddImage;
    private ImageShowAdapter adapter;
    private int id;
    private PersonResponse object;
    private ArrayList<FileAttachmentsItem> fileAttachmentsItems;
    private AppDatabase database;
    private String firstName;
    private String lastName;

    private static final int REQUEST_IMAGE_PARAM = 120;
    private static final int REQUEST_EDIT_IMAGE = 3;
    private String question;
    private int position;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_show_list);
        ButterKnife.bind(this);

        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.ic_arrow_forward);
        imgBack.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        imgBack.setOnClickListener(v -> finish());
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            position = bundle.getInt("position");
            id = bundle.getInt("id");
            question = bundle.getString("question");
            firstName = bundle.getString("firstName");
            lastName = bundle.getString("lastName");
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        database = AppDatabase.getInMemoryDatabase(this);
        object = database.personResponsDao().getPerson(id);
        tvCenterTitle.setText("عکس های " + question);
        getData();
    }

    private void getData() {
        fileAttachmentsItems = (ArrayList<FileAttachmentsItem>) database.fileAttachmentDao().select(id, AttachmentType.IMAGE.getFormat());
        if (fileAttachmentsItems.size() > 0) {
            rootEmptyView.setVisibility(View.GONE);
            setAdapter();
        } else {
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText("عکسی ثبت نشده است");
        }

    }

    private void setAdapter() {
        adapter = new ImageShowAdapter(fileAttachmentsItems, this, true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rvVoiceList.setLayoutManager(layoutManager);
        rvVoiceList.setVerticalScrollBarEnabled(true);
        rvVoiceList.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        rvVoiceList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0 || dy < 0 && btnAddImage.isShown()) {
                    btnAddImage.hide();
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    btnAddImage.show();
                }
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }


    @Override
    public void onDeleteItem(int position, FileAttachmentsItem fileAttachmentsItem) {
        CustomDialog dialog = new CustomDialog(this);
        dialog.setOkListener(getResources().getString(R.string.ok), view1 -> {
            dialog.dismiss();
            removeFromListAndFile(fileAttachmentsItem.getIdentifier());
        });
        dialog.setCancelListener(getResources().getString(R.string.cancel), view12 -> dialog.dismiss());
        dialog.setDialogTitle("آیا برای حذف مطمئن هستید؟");
        dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
        dialog.setIcon(R.drawable.ic_delete, getResources().getColor(R.color.redColor));
        dialog.show();
    }

    @Override
    public void onLocation(int position, FileAttachmentsItem file) {
        Intent intent = new Intent(this, OfflineActivity.class);
        intent.putExtra("lat", file.getGeoLocationDTO() != null ? file.getGeoLocationDTO().getLatitude() : 0);
        intent.putExtra("long", file.getGeoLocationDTO() != null ? file.getGeoLocationDTO().getLongitude() :0);
        intent.putExtra("bodyEditTxt", file.getGeoLocationDTO() != null ? file.getGeoLocationDTO().getAddress() : 0);
        intent.putExtra("isQuestion", true);
        startActivity(intent);
    }

    @Override
    public void onEditItem(int position, FileAttachmentsItem file) {
        FragmentManager fm = getSupportFragmentManager();
        AddImageDialog addImageDialog = new AddImageDialog();
        Bundle bundle = new Bundle();
        bundle.putInt("id", id);
        bundle.putBoolean("isEdit", true);
        bundle.putSerializable("file", file);
        addImageDialog.setListener(this);
        addImageDialog.setArguments(bundle);
        addImageDialog.show(fm, AudioRecordDialog.class.getName());
    }

    @Override
    public void onShowImage(int position, FileAttachmentsItem file) {
        if (fileAttachmentsItems.size() > 0) {
            Intent intent = new Intent(this, PreviewActivity.class);
            intent.putExtra("currentPosition", position);
            intent.putExtra("id", id);
            startActivity(intent);
        }
    }

    private void removeFromListAndFile(long id) {
        for (FileAttachmentsItem file : fileAttachmentsItems) {
            if (file.getIdentifier() == id) {
                fileAttachmentsItems.remove(file);
                database.fileAttachmentDao().delete(file);
                File fdelete = new File(Environment.getExternalStorageDirectory(), file.getFileFolder());
                if (fdelete.isDirectory()) {
                    String[] children = fdelete.list();
                    for (int i = 0; i < children.length; i++) {
                        new File(fdelete, children[i]).delete();
                    }
                }
                try {
                    FileUtils.deleteDirectory(fdelete);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }

        if (fileAttachmentsItems.size() > 0) {
            setAdapter();
            rootEmptyView.setVisibility(View.GONE);
        } else {
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText("عکسی ثبت نشده است");
        }
    }

    @OnClick(R.id.btnAddImage)
    public void onViewClicked() {
        FragmentManager fm = getSupportFragmentManager();
        AddImageDialog imageDialog = new AddImageDialog();
        Bundle bundle = new Bundle();
        bundle.putInt("id", id);
        bundle.putInt("position",position);
        imageDialog.setArguments(bundle);
        imageDialog.setListener(this);
        imageDialog.show(fm, AddImageDialog.class.getName());
    }

    @Override
    public void onInsetOk() {
        getData();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            List<Fragment> frags = getSupportFragmentManager().getFragments();
            if (frags != null) {
                for (Fragment f :
                        frags) {
                    if (f != null && f.isVisible()) {
                        f.onActivityResult(requestCode, resultCode, data);
                    }

                }
            }
        }
    }
}

