package com.military.militaryservice.projectList.show;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.military.militaryservice.R;
import com.military.militaryservice.audio.AudioRecorderActivity;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.baseView.BaseToolbar;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.dialog.CustomDialog;
import com.military.militaryservice.enums.AttachmentType;
import com.military.militaryservice.maps.OfflineActivity;
import com.military.militaryservice.model.PersonResponse;
import com.military.militaryservice.recordAudio.AudioRecordDialog;
import com.military.militaryservice.resultInfo.FileAttachmentsItem;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VoiceListShowActivity extends AppCompatActivity implements VoiceShowAdapter.OnItemClickListener, AudioRecordDialog.updateListener {


    @BindView(R.id.imgHelp)
    BaseImageView imgHelp;
    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.imgLogo)
    BaseImageView imgLogo;
    @BindView(R.id.tvCenterTitle)
    BaseTextView tvCenterTitle;
    @BindView(R.id.share)
    BaseImageView share;
    @BindView(R.id.toolbar)
    BaseToolbar toolbar;
    @BindView(R.id.rvVoiceList)
    RecyclerView rvVoiceList;
    @BindView(R.id.image)
    AppCompatImageView image;
    @BindView(R.id.imgView)
    BaseImageView imgView;
    @BindView(R.id.tvEmptyView)
    BaseTextView tvEmptyView;
    @BindView(R.id.rootEmptyView)
    BaseRelativeLayout rootEmptyView;
    @BindView(R.id.btnAddVoice)
    FloatingActionButton btnAddVoice;
    private VoiceShowAdapter adapter;
    private ArrayList<FileAttachmentsItem> fileAttachmentsItems;
    private AppDatabase database;
    private int id;
    private String firstName;
    private String lastName;
    private PersonResponse object;
    private String question;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_show_list);
        ButterKnife.bind(this);

        imgBack.setVisibility(View.VISIBLE);
        imgBack.setImageResource(R.drawable.ic_arrow_forward);
        imgBack.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        imgBack.setOnClickListener(v -> finish());
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            position = bundle.getInt("position");
            id = bundle.getInt("id");
            question = bundle.getString("question");
            firstName = bundle.getString("firstName");
            lastName = bundle.getString("lastName");
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        database = AppDatabase.getInMemoryDatabase(this);
        object = database.personResponsDao().getPerson(id);
        tvCenterTitle.setText("صدا های " + question);
        getData();
    }


    private void getData() {
        fileAttachmentsItems = (ArrayList<FileAttachmentsItem>) database.fileAttachmentDao().select(id, AttachmentType.VOICE.getFormat());
        if (fileAttachmentsItems.size() > 0) {
            setAdapter();
            rootEmptyView.setVisibility(View.GONE);
        } else {
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText("صدایی ضبط نشده است");
        }

    }

    private void setAdapter() {
        adapter = new VoiceShowAdapter(fileAttachmentsItems, this, true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        rvVoiceList.setLayoutManager(layoutManager);
        rvVoiceList.setVerticalScrollBarEnabled(true);
        rvVoiceList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        rvVoiceList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0 || dy < 0 && btnAddVoice.isShown()) {
                    btnAddVoice.hide();
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    btnAddVoice.show();
                }

                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }


    @Override
    public void onItemClickVoice(int position, FileAttachmentsItem image) {

    }

    @Override
    public void onDeleteItemVoice(int position, FileAttachmentsItem voice) {
        CustomDialog dialog = new CustomDialog(this);
        dialog.setOkListener(getResources().getString(R.string.ok), view1 -> {
            dialog.dismiss();
            removeFromListAndFile(voice.getIdentifier());
        });
        dialog.setCancelListener(getResources().getString(R.string.cancel), view12 -> dialog.dismiss());
        dialog.setDialogTitle("آیا برای حذف مطمئن هستید؟");
        dialog.setColorTitle(getResources().getColor(R.color.primaryTextColor));
        dialog.setIcon(R.drawable.ic_delete, getResources().getColor(R.color.redColor));
        dialog.show();
    }

    @Override
    public void onLocationVoice(int position, FileAttachmentsItem file) {
        Intent intent = new Intent(this, OfflineActivity.class);
        intent.putExtra("lat", file.getGeoLocationDTO() != null ? file.getGeoLocationDTO().getLatitude() : 0);
        intent.putExtra("long", file.getGeoLocationDTO() != null ? file.getGeoLocationDTO().getLongitude() :0);
        intent.putExtra("bodyEditTxt", file.getGeoLocationDTO() != null ? file.getGeoLocationDTO().getAddress() : 0);
        intent.putExtra("isQuestion", true);
        startActivity(intent);

    }

    @Override
    public void onEditItemVoice(int position, FileAttachmentsItem voice) {

        FragmentManager fm = getSupportFragmentManager();
        AudioRecordDialog audioRecordDialog = new AudioRecordDialog();
        Bundle bundle = new Bundle();
        bundle.putInt("id", id);
        bundle.putBoolean("isEdit", true);
        bundle.putSerializable("file", voice);
        audioRecordDialog.setArguments(bundle);
        audioRecordDialog.setListener(this);
        audioRecordDialog.show(fm, AudioRecordDialog.class.getName());
    }

    @Override
    public void onItemPlay(int position, FileAttachmentsItem voice) {
        Intent intent = new Intent(this, AudioRecorderActivity.class);
        intent.putExtra("isPlay", true);
        File file = new File(Environment.getExternalStorageDirectory(), voice.getFileAddress());
        intent.putExtra("filePath", file.getAbsolutePath());
        startActivity(intent);
    }

    private void removeFromListAndFile(long id) {
        for (FileAttachmentsItem voice : fileAttachmentsItems) {
            if (voice.getIdentifier() == id) {
                fileAttachmentsItems.remove(voice);
                database.fileAttachmentDao().delete(voice);
                File fdelete = new File(Environment.getExternalStorageDirectory(), voice.getFileFolder());
                if (fdelete.isDirectory()) {
                    String[] children = fdelete.list();
                    for (int i = 0; i < children.length; i++) {
                        new File(fdelete, children[i]).delete();
                    }
                }
                try {
                    FileUtils.deleteDirectory(fdelete);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }

        if (fileAttachmentsItems.size() > 0) {
            setAdapter();
            rootEmptyView.setVisibility(View.GONE);
        } else {
            rootEmptyView.setVisibility(View.VISIBLE);
            tvEmptyView.setText("صدایی ضبط نشده است");
        }
    }

    @OnClick(R.id.btnAddVoice)
    public void onViewClicked() {
        FragmentManager fm = getSupportFragmentManager();
        AudioRecordDialog audioRecordDialog = new AudioRecordDialog();
        Bundle bundle = new Bundle();
        bundle.putInt("position",position);
        bundle.putInt("id", id);
        audioRecordDialog.setArguments(bundle);
        audioRecordDialog.setListener(this);
        audioRecordDialog.show(fm, AudioRecordDialog.class.getName());
    }

    @Override
    public void onInsertOk() {
        getData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<Fragment> frags = getSupportFragmentManager().getFragments();
        if (frags != null) {
            for (Fragment f : frags) {
                if (f != null && f.isVisible())
                    f.onActivityResult(requestCode, resultCode, data);
            }
        }
    }
}
