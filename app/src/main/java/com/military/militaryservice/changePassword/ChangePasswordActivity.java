package com.military.militaryservice.changePassword;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.military.militaryservice.R;
import com.military.militaryservice.baseView.BaseActivity;
import com.military.militaryservice.baseView.BaseImageView;
import com.military.militaryservice.baseView.BaseLinearLayout;
import com.military.militaryservice.baseView.BaseRelativeLayout;
import com.military.militaryservice.baseView.BaseTextView;
import com.military.militaryservice.baseView.BaseToolbar;
import com.military.militaryservice.customView.CustomEditText;
import com.military.militaryservice.customView.RoundedLoadingView;
import com.military.militaryservice.database.AppDatabase;
import com.military.militaryservice.database.User;
import com.military.militaryservice.splash.SplashActivity;
import com.military.militaryservice.utils.PreferencesData;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by a.azadbar on 9/27/2017.
 */

public class ChangePasswordActivity extends BaseActivity {


    @BindView(R.id.imgBack)
    BaseImageView imgBack;
    @BindView(R.id.tvCenterTitle)
    BaseTextView tvCenterTitle;
    @BindView(R.id.share)
    BaseImageView share;
    @BindView(R.id.toolbar)
    BaseToolbar toolbar;
    @BindView(R.id.edtNowPassword)
    CustomEditText edtNowPassword;
    @BindView(R.id.edtNewPassword)
    CustomEditText edtNewPassword;
    @BindView(R.id.edtConfirmPassword)
    CustomEditText edtConfirmPassword;
    @BindView(R.id.btnRegisterCode)
    BaseTextView btnRegisterCode;
    @BindView(R.id.cvSend)
    CardView cvSend;
    @BindView(R.id.second)
    BaseLinearLayout second;
    @BindView(R.id.roundedLoadingView)
    RoundedLoadingView roundedLoadingView;
    @BindView(R.id.root)
    BaseRelativeLayout root;
    @BindView(R.id.edtUsername)
    CustomEditText edtUsername;
    @BindView(R.id.repeatEdtUsername)
    CustomEditText repeatEdtUsername;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);


        tvCenterTitle.setText("تغییر رمز عبور");
    }


    @OnClick({R.id.btnRegisterCode, R.id.imgBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnRegisterCode:
                if (validData()) {
                    changePasswordRequest();
                }
                break;
            case R.id.imgBack:
                finish();
                break;
        }

    }

    private void changePasswordRequest() {
        AppDatabase database = AppDatabase.getInMemoryDatabase(this);
        User user = new User();
        user.setUsername(repeatEdtUsername.getValueString());
        user.setPassword(edtNewPassword.getValueString());
        database.userDao().update(user.getPassword(), user.getUsername(), database.userDao().getLogin().getId());
        Toast.makeText(this, "رمز شما به روز شد", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, SplashActivity.class);
        startActivity(intent);
        PreferencesData.isLogin(this, false);
        finish();
    }

    private boolean validData() {
        ArrayList<String> errorMsgList = new ArrayList<>();
        AppDatabase database = AppDatabase.getInMemoryDatabase(this);


        if (!TextUtils.equals(edtUsername.getValueString(), database.userDao().getLogin().getUsername())) {
            String message = "نام کاربری فعلی نادرست است";
            errorMsgList.add(message);
        }
        if (edtUsername.getError() != null) {
            errorMsgList.add(edtUsername.getError());
        }


        if (repeatEdtUsername.getError() != null) {
            errorMsgList.add(repeatEdtUsername.getError());
        }

        if (edtNowPassword.getError() != null) {
            errorMsgList.add(edtNowPassword.getError());
        }

        if (edtNewPassword.getError() != null) {
            errorMsgList.add(edtNewPassword.getError());
        }

        if (edtConfirmPassword.getError() != null) {
            errorMsgList.add(edtConfirmPassword.getError());
        }

        if (!TextUtils.equals(edtNowPassword.getValueString(), database.userDao().getLogin().getPassword())) {
            String message = "رمز فعلی نادرست است";
            errorMsgList.add(message);
        }

        if (!TextUtils.equals(edtNewPassword.getValueString(), edtConfirmPassword.getValueString())) {
            String message = getResources().getString(R.string.no_match_new_assword_repat_password);
            errorMsgList.add(message);
        }

        if (errorMsgList.size() > 0) {
            showInfoDialog(getString(R.string.fill_following), errorMsgList);
            return false;
        }

        return true;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
